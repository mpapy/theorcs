!function($){

	var chatBoxes = new Array();
	var chatboxFocus = new Array();
	
	"use strict"; // jshint ;_;
	$(window).resize(function(){
		resizeChatBox();
		resetSlimScroll();
	});
	
	$(function() {
		resizeChatBox();
		createSlimScroll();
		initChats();
	});
	
	function resizeChatBox() {
		var sideBarSize = $(window).height() - $('#navbar').outerHeight(true) - $('#footer').outerHeight(true) - 20;
		if(sideBarSize > 500) { 
			$('.sidebar').height(sideBarSize);
			var childs = $('.sidebar').children();
			$('.chatbox').height(sideBarSize - $(childs[0]).outerHeight(true));
			var newSize = $('.chatbox').height() - $('.chatbox > h4').outerHeight(true);
			$('.chatbox .scrollable').height($('.chatbox').height() - $('.chatbox > h4').outerHeight(true) - 2);
		}
		
	};
	
	function createSlimScroll() {
		$('.chatbox .scrollable').slimScroll({
	        height: $('.chatbox > .scrollable').height()
	    });
	}
	
	function resetSlimScroll() {
		// We need to get the Scrollable Content first
		var scrollElem = $('.chatbox .scrollable').detach();
		$('div[class*="slimScroll"]').remove();
		scrollElem.appendTo('.chatbox');
		createSlimScroll();
	}
	
	function initChats() {
		// We select all the Users in the list
		$('.chatbox .contact').each(function(index,elem){
			$(this).bind('click', function() {
				 chatWith($(this).children('.name').text(),$(this).children('.name').attr('uid'))
			});
		});
		
	}
	
	function chatWith(chatuser,uid) {
		createChatBox(chatuser,uid);
		$("#chatbox_"+uid+" .chatboxtextarea").focus();
	}

	function createChatBox(chatboxtitle,uid,minimizeChatBox) {
		if ($("#chatbox_"+uid).length > 0) {
			if ($("#chatbox_"+uid).css('display') == 'none') {
				$("#chatbox_"+uid).css('display','block');
				restructureChatBoxes();
			}
			$("#chatbox_"+uid+" .chatboxtextarea").focus();
			return;
		}

		$(" <div />" ).attr("id","chatbox_"+uid)
		.addClass("chatboxwindow")
		.html('<div class="chatboxhead"><div class="chatboxtitle">'+chatboxtitle+'</div><div class="chatboxoptions"><a href="javascript:void(0)" onclick="javascript:toggleChatBoxGrowth(\''+chatboxtitle+'\')">-</a> <a href="javascript:void(0)" onclick="javascript:closeChatBox(\''+chatboxtitle+'\')">X</a></div><br clear="all"/></div><div class="chatboxcontent"></div><div class="chatboxinput"><textarea class="chatboxtextarea" onkeydown="javascript:return checkChatBoxInputKey(event,this,\''+chatboxtitle+'\');"></textarea></div>')
		.appendTo($( "body" ));
				   
		$("#chatbox_"+uid).css('bottom', '0px');
		
		var chatBoxeslength = 0;

		for (x in chatBoxes) {
			if ($("#chatbox_"+chatBoxes[x]).css('display') != 'none') {
				chatBoxeslength++;
			}
		}

		if (chatBoxeslength == 0) {
			$("#chatbox_"+uid).css('right', '20px');
		} else {
			width = (chatBoxeslength)*(225+7)+20;
			$("#chatbox_"+uid).css('right', width+'px');
		}
		
		chatBoxes.push(uid);

		if (minimizeChatBox == 1) {
			minimizedChatBoxes = new Array();

			if ($.cookie('chatbox_minimized')) {
				minimizedChatBoxes = $.cookie('chatbox_minimized').split(/\|/);
			}
			minimize = 0;
			for (j=0;j<minimizedChatBoxes.length;j++) {
				if (minimizedChatBoxes[j] == uid) {
					minimize = 1;
				}
			}

			if (minimize == 1) {
				$('#chatbox_'+uid+' .chatboxcontent').css('display','none');
				$('#chatbox_'+uid+' .chatboxinput').css('display','none');
			}
		}

		chatboxFocus[uid] = false;

		$("#chatbox_"+uid+" .chatboxtextarea").blur(function(){
			chatboxFocus[uid] = false;
			$("#chatbox_"+uid+" .chatboxtextarea").removeClass('chatboxtextareaselected');
		}).focus(function(){
			chatboxFocus[uid] = true;
			newMessages[uid] = false;
			$('#chatbox_'+uid+' .chatboxhead').removeClass('chatboxblink');
			$("#chatbox_"+uid+" .chatboxtextarea").addClass('chatboxtextareaselected');
		});

		$("#chatbox_"+uid).click(function() {
			if ($('#chatbox_'+uid+' .chatboxcontent').css('display') != 'none') {
				$("#chatbox_"+uid+" .chatboxtextarea").focus();
			}
		});

		$("#chatbox_"+uid).show();
	}	

}(window.jQuery);