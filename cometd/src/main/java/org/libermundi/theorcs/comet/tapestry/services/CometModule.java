package org.libermundi.theorcs.comet.tapestry.services;

import org.apache.tapestry5.ioc.Configuration;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.annotations.Contribute;
import org.apache.tapestry5.ioc.annotations.Primary;
import org.apache.tapestry5.services.LibraryMapping;
import org.apache.tapestry5.services.javascript.JavaScriptStack;
import org.apache.tapestry5.services.transform.ComponentClassTransformWorker2;
import org.libermundi.theorcs.comet.CometConstants;


/**
 * This module is automatically included as part of the Tapestry IoC Registry, it's a good place to
 * configure and extend Tapestry, or to place your own service definitions.
 */
public class CometModule {
	public static void contributeComponentClassResolver(final Configuration< LibraryMapping > configuration) {
        configuration.add(new LibraryMapping(CometConstants.TAPESTRY_MAPPING, "org.libermundi.theorcs.comet.tapestry"));
	}
    
	public static void contributeFactoryDefaults(MappedConfiguration<String, String> configuration) {
        configuration.add("comet.assets", "META-INF/assets");

        configuration.add("comet.scripts", "${comet.assets}/js");

    }	

	public static void contributeJavaScriptStackSource(MappedConfiguration<String, JavaScriptStack> configuration) {
		 configuration.addInstance(CometConstants.COMET_JSSTACK, CometJavaScriptStack.class);
	}
	
	@Contribute(ComponentClassTransformWorker2.class)
    @Primary
    public static void addWorker(OrderedConfiguration<ComponentClassTransformWorker2> configuration) {
		configuration.addInstance("ImportCometJSWorker", ImportCometJSWorker.class, "before:Import", "after:RenderPhase");
	}

	public static void contributeIgnoredPathsFilter(Configuration<String> configuration) {
	    configuration.add("/cometd/.*");
	}	
}
