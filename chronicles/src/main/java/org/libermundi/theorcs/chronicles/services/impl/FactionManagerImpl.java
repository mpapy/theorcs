package org.libermundi.theorcs.chronicles.services.impl;

import org.libermundi.theorcs.chronicles.dao.FactionDao;
import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.model.Faction;
import org.libermundi.theorcs.chronicles.services.ChronicleManager;
import org.libermundi.theorcs.chronicles.services.FactionManager;
import org.libermundi.theorcs.core.services.impl.AbstractManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service(FactionManager.SERVICE_ID)
@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
public class FactionManagerImpl extends AbstractManagerImpl<Faction,Long> implements FactionManager {
	private static final Logger logger = LoggerFactory.getLogger(FactionManagerImpl.class);
	
	@Autowired
	private ChronicleManager _chronicleManager;
	
	@Autowired
	public FactionManagerImpl(FactionDao factionDao) {
		super();
		setDao(factionDao);
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.services.impl.AbstractManagerImpl#initialize()
	 */
	@Override
	public void initialize() {
		if(getAllCount() == 0L) {
			logger.debug("Initializing Chronicles");
			Chronicle c = _chronicleManager.loadLast();
			
			//Create 2 Default "Faction"s
			Faction f0 = new Faction("La Camarilla");
			
			Faction f1 = new Faction("Le Sabbat");
			
			f0.setChronicle(c);
			f1.setChronicle(c);
			
			save(f0,f1);
		}

	}

	@Override
	public Faction createNew() {
		return new Faction();
	}
	
	

}
