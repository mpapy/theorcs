package org.libermundi.theorcs.chronicles.tapestry.pages.admin.perso;

import java.util.List;

import org.apache.tapestry5.Asset2;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.alerts.AlertManager;
import org.apache.tapestry5.alerts.Duration;
import org.apache.tapestry5.alerts.Severity;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.libermundi.theorcs.chronicles.dto.SheetEditorSettings;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.sheet.SheetPage;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.chronicles.tapestry.base.ChronicleAdminPage;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;

import com.google.common.collect.Lists;

public class Sheet extends ChronicleAdminPage {
	public static final String PAGE_ID="chronicles.pages.admin.perso.sheet";
	
	@Environmental
	private JavaScriptSupport _javaScriptSupport;
	
	@Inject
	private PersonaManager _personaManager;
	
	@Inject
	private Block _notification;
	
	@Inject
	@Property(write=false)
	private Block _editor;
	
	@Inject
	private Block _empty;
	
	@Inject
	private Messages _messages;
	
	@Inject
	private AppHelper _appHelper;
	
	@Inject
	@Path("${chronicles.scripts}/sheetpage.init.js")
	private Asset2 _sheetInitScript;
	
	@Inject
	private AlertManager _alertManager;
	
	@SessionState
	private SheetEditorSettings _editorSettings;
	
	@Property(write=false)
	private String _notificationType;
	
	@Property(write=false)
	private String _notificationMsg;
	
	private Persona _persoToLoad;
	
	public void onActivate(Persona perso) {
		_persoToLoad = perso;
	}
	
	public List<Object> onPassivate(){
		List<Object> params = Lists.newArrayList();
		if(_editorSettings.getPerso() != null) {
			params.add(_editorSettings.getPerso().getId());
		}
		return params;
	}
	

	@SetupRender
	public void setupRender(){
		if(_persoToLoad != null && !_persoToLoad.equals(_editorSettings.getPerso())) {
			_editorSettings.clear();
			_editorSettings.setPerso(_persoToLoad);
		}
	}
	
	public List<SheetPage> getPageSource(){
		return _editorSettings.getPerso().getSheet().getPages();
	}
	
	public SheetPage getCurrentPage(){
		return _editorSettings.getPage();
	}
	
	@OnEvent("savePage")
	public Block savePage(){
		_editorSettings.getPerso().getSheet().serializePages();
		_personaManager.save(_editorSettings.getPerso());
		
		_notificationMsg=String.format("<strong>%s</strong> %s",
				_messages.get("chronicles.sheeteditor.success"),
				_messages.get("chronicles.sheeteditor.save.success"));
		
		_alertManager.success(_notificationMsg);

		return _notification;		
	}

	@OnEvent("addPage")
	public void addPage(){
		_editorSettings.getPerso().getSheet().addPage();

		_notificationMsg=String.format("<strong>%s</strong> %s",
				_messages.get("chronicles.sheeteditor.success"),
				_messages.get("chronicles.sheeteditor.addpage.success"));
		
		_alertManager.alert(Duration.UNTIL_DISMISSED,Severity.SUCCESS,_notificationMsg,true);
	}
	
	@OnEvent("resetPage")
	public void resetPage(){
		Persona reloaded = _personaManager.load(_editorSettings.getPerso().getId());
		_editorSettings.setPerso(reloaded);
		
		_notificationMsg=String.format("<strong>%s</strong> %s",
				_messages.get("chronicles.sheeteditor.success"),
				_messages.get("chronicles.sheeteditor.reset.success"));
		
		_alertManager.alert(Duration.UNTIL_DISMISSED,Severity.SUCCESS,_notificationMsg,true);
	}
	
	@OnEvent("valueChanged")
	public Block selectPerso(Persona perso){
		// The new Perso is actually loaded in the setSelected method 
		// that is called after this Event
		if(perso == null) {
			return _empty;
		}
		return _editor;
	}
	
	@OnEvent("goToPage")
	public Block gotoPage(int index) {
		_editorSettings.setPageIndex(index);
		return _editor;
	}

	@OnEvent("nextPage")
	public Block nextPage() {
		int currentIndex = _editorSettings.getPageIndex();
		_editorSettings.setPageIndex(++currentIndex);
		return _editor;
	}

	@OnEvent("previousPage")
	public Block previousPage() {
		int currentIndex = _editorSettings.getPageIndex();
		_editorSettings.setPageIndex(--currentIndex);		
		return _editor;
	}

	@OnEvent("loadSheet")
	public Block loadSheet() {
		return _editor;
	}
	
	public Persona getSelectedPerso() {
		return _editorSettings.getPerso();
	}

	public void setSelectedPerso(Persona perso) {
		_editorSettings.setPerso(perso);
	}
	
	public int getCurrentPageIndex() {
		return _editorSettings.getPageIndex();
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.tapestry.base.ChronicleAdminPage#isSideBarHidden()
	 */
	@Override
	public boolean isSideBarHidden() {
		return Boolean.TRUE;
	}
}
