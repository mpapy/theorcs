/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.tapestry.components.persona;

import java.util.List;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.ValueEncoder;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.ValueEncoderFactory;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.core.tapestry.util.GenericSelectModel;

/**
 * @author Martin Papy
 *
 */
public class Selector {
	@Parameter(required=true,defaultPrefix=BindingConstants.PROP)
	private Persona _value;
	
	@Parameter(defaultPrefix=BindingConstants.LITERAL)
	private String _label;
	
	@Parameter(defaultPrefix=BindingConstants.LITERAL)
	private String _zone;
	
	@SessionState
	private ChronicleSettings _chronicleSettings;
	
	@Inject
	private PersonaManager _personaManager;	
	
	@Inject
	private ValueEncoderFactory<Persona> _genericValueEncoderFactory;
	
	public SelectModel getPersoModel() {
		List<Persona> values = _personaManager.getChroniclePersonas(_chronicleSettings.getChronicle());
		return new GenericSelectModel<>(values);
	}
	
	public ValueEncoder<Persona> getPersoEncoder() {
		return _genericValueEncoderFactory.create(Persona.class);
	}
	
	public void setSelectedPerso(Persona perso) {
		_value = perso;
	}
	
	public Persona getSelectedPerso() {
		return _value;
	}
	
	public String getLabel() {
		return _label;
	}

	public String getZone() {
		return _zone;
	}
}
