/**
 * 
 */
package org.libermundi.theorcs.chronicles.tapestry.pages.message;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.alerts.AlertManager;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.internal.BccTarget;
import org.libermundi.theorcs.chronicles.model.message.internal.CcTarget;
import org.libermundi.theorcs.chronicles.model.message.internal.InternalMessage;
import org.libermundi.theorcs.chronicles.model.message.internal.ToTarget;
import org.libermundi.theorcs.chronicles.services.AddressBookManager;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.chronicles.services.message.InternalMessageManager;
import org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage;
import org.libermundi.theorcs.core.tapestry.services.MultipleValueEncoderFactory;
import org.libermundi.theorcs.core.tapestry.util.GenericSelectModel;
import org.libermundi.theorcs.core.tapestry.util.MultipleValueEncoder;

import com.google.common.collect.Maps;

/**
 * Create InternalMessage
 * 
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
@Import(library = "${chronicles.scripts}/message.js")
public class CreateMessage extends ChroniclePage {
	
	/*------------- Services -------------*/	
	@Inject
	private InternalMessageManager _messageManager;

	@Inject
	private PersonaManager _personaManager;
	
	@Inject
	private org.libermundi.theorcs.security.services.SecurityManager _securityManager;
	
	@Inject
	private MultipleValueEncoderFactory<Persona> _genericManagerFactory;

	@Inject
	private AddressBookManager _addressBookManager;
	
	@Environmental
	private JavaScriptSupport _jsSupport;
	
	/*------------- support -------------*/	
	@Persist
	private Map<String, String> _mappedContacts;
	
	@Persist
	private List<Persona> _contacts;
	
	@Inject
	private Messages _messages;
	
	@Inject
	private AlertManager _alertManager;

	@Component
	private Form messageForm;

	/*------------- Fields -------------*/
	@Property
	private List<Persona> _to = new ArrayList<Persona>();
	
	@Property
	private List<Persona> _cc = new ArrayList<Persona>();
	
	@Property
	private List<Persona> _bcc = new ArrayList<Persona>();
		
	@Property
	@Persist
	private boolean _showCcFields;
	
	@Property
	private InternalMessage _internalMessage;
		
	/*------------- Rendering -------------*/

	public MultipleValueEncoder<Persona> getMultipleValueEncoder() {
		return _genericManagerFactory.create(Persona.class);
	}
	
	public SelectModel getGenericModel() {
		return new GenericSelectModel<Persona,Long>(_contacts,Boolean.FALSE);
	}
	
	/**
	 * get the contact of the player's persona 
	 */
	private void initContact() {
		// get all the contacts of the sender
		_contacts = _addressBookManager.loadAddressBook(getCurrentPerso()).getContactsAsList();
		_mappedContacts = Maps.newHashMap();
		for(Persona contact : _contacts) {
			_mappedContacts.put(contact.getId().toString(), contact.getName());
		}
	}
	
	/**
	 * Check if a persona is present into _contacts. 
	 * @param persona Persona to check 
	 */
	private boolean checkPersonaInContacts(Persona persona) {
		//if the persona to write is the writer, no need to check 
		if(_contacts.contains(persona)  && ! getChronicleSettings().getPersona().equals(persona))
			return true;
		else
			return false;
	}
		
	@SetupRender
	public void setupRender(){
		//TODO Hide the cc/bcc fields
		_jsSupport.require("MessageUtils").invoke("toggleCC").with("ccFields");
		if( _contacts == null || _contacts.isEmpty())
			initContact();
	}
	
	// Form bubbles up the PREPARE event during form render and form submission.
	public void onPrepare(){
		if(_internalMessage ==null){
			_internalMessage = new InternalMessage();
			_to.clear();
			_cc.clear();
			_bcc.clear();
		}
	}
	/**
	 * In case of a reply, replyAll or transfer, load the previous message and prepare the new one  
	 */
	public void onActivate(InternalMessage msg, String action){							
		_internalMessage = new InternalMessage();
		if( _contacts == null || _contacts.isEmpty())
			initContact();
		if(msg!=null){ 
			//prepare the body of message
			StringBuffer sb = new StringBuffer();
			sb.append("<br/><p/> --- <p/> ").append(msg. getAuthor().getName()).append(" : <p/> --- <p/> ").append(msg.getContent());
			_internalMessage.setContent(sb.toString());
			
			for (ToTarget tgt: msg.getToTargets()){
				_to.add(tgt.getEntity());
			}
			for (CcTarget tgt: msg.getCcTargets()){
				_cc.add(tgt.getEntity());
			}
			for (BccTarget tgt: msg.getBccTargets()){
				_bcc.add(tgt.getEntity());
			}
			
			if(action != null && action.equals("reply")){
				prepareReply(msg);
			} else if(action != null && action.equals("replyAll")){
				prepareReplyAll(msg);
			}  else if(action != null && action.equals("transfer")){
				prepareTransfer(msg);
			}
		}
	}

	/**
	 * Reply to the message
	 * Add the author into addressBook if it is not already present
	 * @param msg
	 */
	private void prepareReply(InternalMessage msg){
		_to.clear();
		//add the author in contact if not present
		if(!checkPersonaInContacts((msg.getAuthor()))){
			_addressBookManager.addContact(getCurrentPerso(), msg.getAuthor());
			initContact();
		}	
		_to.add(msg.getAuthor());
		_cc = null;
		_bcc = null;
		_internalMessage.setTitle("Re: " + msg.getTitle());
	}

	/**
	 * Transfer the message
	 * @param msg
	 */
	private void prepareTransfer(InternalMessage msg) {
		_to = null;
		_cc = null;
		_bcc = null;
		_internalMessage.setTitle("Tr: " + msg.getTitle());
	}

	/**
	 * Reply all the message
	 * @param msg
	 */
	private void prepareReplyAll(InternalMessage msg) {
		List<Persona> tmp = new ArrayList<Persona>();
		
		//add the author in contact if not present
		_to.clear();
		if(!checkPersonaInContacts((msg.getAuthor()))){
			_addressBookManager.addContact(getCurrentPerso(), msg.getAuthor());
			initContact();
		}
		_to.add(msg.getAuthor());
		
		//add the cc personas if present into the contacts 
		tmp.addAll(_cc);
		_cc.clear();
		for(Persona pr : tmp){		
			if(checkPersonaInContacts(pr))
				_cc.add(pr);
			else
				_alertManager.info(_messages.format("chronicles.message.target.nofound", pr.getName()));
		}
		
		//no bcc personas
		_bcc = null;
		
		_internalMessage.setTitle("Re: " + msg.getTitle());
	}

	/**
	 * Check if there is at least one target
	 */
	@OnEvent(value="validate",component="messageForm")
	void onValidate() {
		if((_to!=null?_to.size():0) + (_cc!=null?_cc.size():0) + (_bcc!=null?_bcc.size():0)==0)
			// record an error, and thereby prevent Tapestry from sending a "success" event
			messageForm.recordError(_messages.format("chronicles.message.write.notarget"));
	}
	
	/**
	 * Send the message
	 * @return the messageIndex page
	 */
	@OnEvent(value="success",component="messageForm")
	public Class<?> success(){		
		if(_to != null)
			for (Persona p : _to) {
				_internalMessage.getToTargets().add(new ToTarget(p));
			}
		
		if(_cc != null) 
		for (Persona p : _cc) {
			_internalMessage.getCcTargets().add(new CcTarget(p));
		}
		
		if(_bcc != null) 
		for (Persona p : _bcc) {
			_internalMessage.getBccTargets().add(new BccTarget(p));
		}
		
		//TODO getCurrentPerso() works but when the persona is evaluated an com.sun.jdi.InvocationException is thrown
		//why?
		//_internalMessage.setAuthor(getCurrentPerso());
		Persona perso = _personaManager.getDefaultPersona(_securityManager.getCurrentUser(), getChronicleSettings().getChronicle());
		_internalMessage.setAuthor(perso);
		_messageManager.sendMessage(_internalMessage);
		return MessageList.class;
	}
}