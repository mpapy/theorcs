package org.libermundi.theorcs.chronicles.model.message;



/**
 * Models the core message with a body and an author.
 * The author can be T as Persona, Player...
 * 
 * @author Stephane Guichard
 * @author Martin Papy
 */
public interface Message<T extends Object> {

	public final static String PROP_CONTENT="content";
	public final static String PROP_AUTHOR="author";
	
	T getAuthor();
	String getContent();
	
	void setContent(String content) ;
	void setAuthor(T author);
}
