/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 package org.libermundi.theorcs.chronicles;

/**
 * Definition of Constants 
 *
 * @author Stephane Guichard
 * @author Martin Papy
 */
public final class ChronicleConstants {
	private ChronicleConstants() {}

	public static final String TAPESTRY_MAPPING="chronicles";
	
	/* -- Names of the tables in DB -- */
	public static final String TBL_GAME="tbl_game";
	public static final String TBL_CHRONICLE="tbl_chronicle";
	public static final String TBL_PERSONA="tbl_persona";
	public static final String TBL_SHEET="tbl_sheet";
	public static final String TBL_SHEETPAGE="tbl_sheetpage";
	public static final String TBL_SHEETTEMPLATE="tbl_sheettemplate";
	public static final String TBL_GAMESYSTEM="tbl_gamesystem";
	public static final String TBL_CONTACT="tbl_contact";
	public static final String TBL_SAVEDINFOS="tbl_savedinfos";
	public static final String TBL_ADDRESSBOOK="tbl_addressbook";
	public static final String TBL_FACTION="tbl_faction";
	public static final String TBL_PERSONA2FACTION="tbl_persona2faction";
	//messages
	public static final String TBL_INTERNAL_MESSAGE="tbl_internalmessage";
	public static final String TBL_INTERNAL_MESSAGE_TARGET="tbl_internalmessagetarget";
	//forum
	public static final String TBL_FORUM_THREAD="tbl_forumthread";
	public static final String TBL_FORUM_MESSAGE="tbl_forummessage";
	public static final String TBL_FORUM_MEMBER="tbl_forummember";
	
	public static final String CHRO_ADMIN_NAV_ID="CHRO_ADMIN_NAV";
	
	public static final String CHRO_ADMIN_NAV_ROOT_LABEL="message:chronicles.pages.index.title";
	
	// List of Menu IDs
	public static final String NAV_FACTIONS_ID="FACTIONS_MENU";
	public static final String NAV_PERSONA_ID="PERSONA_MENU";
	public static final String NAV_MAIL_ID="MAIL_MENU";
	public static final String NAV_SCENE_ID="SCENE_MENU";
	public static final String NAV_BACK_ID="BACKGROUND_MENU";

	public static final String ACL_MANAGE_PERSONA="ACL_MANAGE_PERSONA";
}
