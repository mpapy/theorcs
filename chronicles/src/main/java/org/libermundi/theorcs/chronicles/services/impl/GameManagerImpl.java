package org.libermundi.theorcs.chronicles.services.impl;

import org.libermundi.theorcs.chronicles.dao.GameDao;
import org.libermundi.theorcs.chronicles.model.Game;
import org.libermundi.theorcs.chronicles.services.GameManager;
import org.libermundi.theorcs.core.services.impl.AbstractManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service(GameManager.SERVICE_ID)
@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
public class GameManagerImpl extends AbstractManagerImpl<Game,Long> implements GameManager {
	private static final Logger logger = LoggerFactory.getLogger(GameManagerImpl.class);
	
	@Autowired
	public GameManagerImpl(GameDao gameDao) {
		super();
		setDao(gameDao);
	}

	@Override
	public void initialize() {
		if(getAllCount() == 0L) {
			logger.debug("Initializing Games");
			Game g = new Game();
				g.setName("The Call of Cthulhu");
			save(g);
		}
	}

	@Override
	public Game createNew() {
		return new Game();
	}

}
