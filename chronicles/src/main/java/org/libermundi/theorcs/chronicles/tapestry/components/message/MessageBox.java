/**
 * 
 */
package org.libermundi.theorcs.chronicles.tapestry.components.message;

import java.util.List;

import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.PropertyConduitSource;
import org.apache.tapestry5.services.Request;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.chronicles.model.message.internal.InternalMessage;
import org.libermundi.theorcs.chronicles.services.message.InternalMessageManager;
import org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage;

/**
 * Display a list of messages
 * The list of messages has to provided as parameter
 * 
 * @author Stephane Guichard 
 * @author Martin Papy
 *
 */
public class MessageBox extends ChroniclePage {
	
	/*------------- Services -------------*/
	@SessionState
	private ChronicleSettings _chronicleSettings;
    
	@Inject
	private InternalMessageManager _messageManager;
	
	@Parameter(allowNull=true,required=true)
	@Property
	private List<InternalMessage> _internalMessages;

	@Property
	private InternalMessage _internalMessage;

	@Inject
	private BeanModelSource _beanModelSource;
	
	@Inject
	private Messages _messages;

	@Inject
	private PropertyConduitSource _pcSource;

	@Inject
	private Request _request;
    
	/**
	 * BeanModel of InternalMessage to display the sender.name and targets
	 */
	@Property
	private BeanModel<InternalMessage> _messageBoxModel;
	
	/**
	 * ajax zone to open message 
	 */
	@InjectComponent
	private Zone _messageZone;
	
	/*------------- Rendering Phase -------------*/
	@SetupRender
	public void setupRender(){
		//beanDisplayModel
		// have to add the property sender to display it as it is a complex class
		_messageBoxModel = _beanModelSource.createDisplayModel(
				InternalMessage.class, _messages);
		_messageBoxModel.exclude("id", "content", "modifiedDate", "author");
		_messageBoxModel.add("authorName", _pcSource.create(InternalMessage.class, "author.name"));
		_messageBoxModel.get("authorName").label(_messages.get("author-label"));
		_messageBoxModel.reorder("authorName");	
	}
	
	@OnEvent("openMessage")
	 Object openMessage(InternalMessage msg){
		_internalMessage = msg;
		//the message is seen
		_messageManager.updateMessageOnSeen(msg, getCurrentPerso(), true);
		//the zone is the ajax html item to be updated
		return _messageZone.getBody();
	}

}