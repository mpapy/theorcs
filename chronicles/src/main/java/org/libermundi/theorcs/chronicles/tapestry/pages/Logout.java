package org.libermundi.theorcs.chronicles.tapestry.pages;

import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.chronicles.services.SavedInformationsManager;
import org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage;
import org.libermundi.theorcs.security.services.SecurityManager;

public class Logout extends ChroniclePage {
	@SessionState
	private ChronicleSettings _chronicleSettings;
	
	@Inject
	private SavedInformationsManager _informationsManager;
	
	@Inject
	private SecurityManager _securityManager;
	
	public String onActivate() {
		_chronicleSettings.clear();
		
		_informationsManager.clear(_securityManager.getCurrentUser());
		
		// We return what is supposed to be the Index page
		// Of the Main WebApp
		return "Index";
	}

}
 