/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.model.message;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;

import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Store;
import org.libermundi.theorcs.core.dao.hibernate.listener.TimestampListener;
import org.libermundi.theorcs.core.model.Searchable;
import org.libermundi.theorcs.core.model.base.NumericIdEntity;
import org.libermundi.theorcs.core.model.base.Timestampable;

/**
 *  Abstract Message with persistence method on body and creation date attributes
 *  
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
@MappedSuperclass
@Analyzer(definition = "SuggestionAnalyzer")
@EntityListeners(TimestampListener.class)
public abstract class AbstractMessage extends NumericIdEntity implements Searchable, Timestampable {
	private static final long serialVersionUID = 7269124754089412990L;
	
	private static final String[] SEARCH_FIELDS = new String[]{Message.PROP_CONTENT, Message.PROP_AUTHOR, Timestampable.PROP_CREATED_DATE};
	private static final String[] SUGGESTION_SEARCH_FIELDS = new String[]{Message.PROP_CONTENT};
	
	private String _content;
	private Date _createdDate;
    private Date _modifiedDate;
	
    public AbstractMessage(){
    	setCreatedDate(GregorianCalendar.getInstance().getTime());
    }
    
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.Searchable#loadSearchFields()
	 */
	public String[] loadSearchFields() {
		return SEARCH_FIELDS;
	}
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.Searchable#loadSuggestionFields()
	 */
	public String[] loadSuggestionFields() {
		return SUGGESTION_SEARCH_FIELDS;
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.model.messenging.Message#getBody()
	 */
	@Lob
	@Column(name=Message.PROP_CONTENT,nullable=false)
	@Field(index=Index.YES, store=Store.YES)
	public String getContent() {
		return _content;
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.model.messenging.Message#setBody(java.lang.String)
	 */
	public void setContent(String body) {
		this._content = body;
	}
	
    @Override
    @Column(name = Timestampable.PROP_CREATED_DATE)
	@Field(index=Index.YES, store=Store.YES)
    public Date getCreatedDate() {
        return _createdDate;
    }
    
    /*
     * (non-Javadoc)
     * @see org.libermundi.theorcs.core.model.base.Timestampable#setCreatedDate(java.util.Date)
     */    
    @Override
    public void setCreatedDate(Date createdDate) {
        _createdDate = createdDate;
    }
    
    /*
     * (non-Javadoc)
     * @see org.libermundi.theorcs.core.model.base.Timestampable#getModifiedDate()
     */
    
    @Override
    @Column(name = Timestampable.PROP_MODIFIED_DATE)
    public Date getModifiedDate() {
        return _modifiedDate;
    }
    
    /*
     * (non-Javadoc)
     * @see org.libermundi.theorcs.core.model.base.Timestampable#setModifiedDate(java.util.Date)
     */
    
    @Override
    public void setModifiedDate(Date modifiedDate) {
    	_modifiedDate = modifiedDate;
    }

}
