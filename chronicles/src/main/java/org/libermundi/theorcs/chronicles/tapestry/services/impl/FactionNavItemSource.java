/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.tapestry.services.impl;

import java.util.List;
import java.util.SortedSet;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.ApplicationStateManager;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.chronicles.model.Faction;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.tapestry.pages.Index;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.core.model.NodeFactory;
import org.libermundi.theorcs.core.services.GenericManager;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.model.menu.NavMenuItemType;
import org.libermundi.theorcs.layout.model.menu.StaticNavMenuItem;
import org.libermundi.theorcs.layout.services.NavItemSource;

import com.google.common.collect.Lists;

/**
 * @author Martin Papy
 *
 */
public class FactionNavItemSource implements NavItemSource {
	@Inject
	private ApplicationStateManager _asm;
	
	@Inject
	private GenericManager _gm;
	
	@Inject
	private AppHelper _appHelper;	
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.tapestry.services.NavItemSource#getElements()
	 */
	@Override
	public List<Node<NavMenuItem>> getElements() {
		ChronicleSettings cs = _asm.get(ChronicleSettings.class);
		Persona currentPersona = cs.getPersona();
		Faction currentFaction = cs.getFaction();
		
		List<Node<NavMenuItem>> result = Lists.newArrayList();
		
		SortedSet<Faction> factions = currentPersona.getFactions();
		
		for (Faction faction : factions) {
			if(faction.equals(currentFaction)) {
				continue;
			}
			
			Node<NavMenuItem> node = NodeFactory.getNode();
				node.setData(new StaticNavMenuItem(NavMenuItemType.DEFAULT));
				node.getData().setLabel(faction.getName());
				node.getData().setDescription(faction.getName());
				node.getData().setURI(_appHelper.getEventLink(Index.class, Index.EVENT_SWITCH_FACTION,	faction.getId()).toAbsoluteURI());
			result.add(node);
		}
		
		return result;
	}

}
