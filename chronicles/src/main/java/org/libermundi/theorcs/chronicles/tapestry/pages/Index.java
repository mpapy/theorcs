/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.tapestry.pages;

import java.util.List;

import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.model.Faction;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.SavedInformations;
import org.libermundi.theorcs.chronicles.model.message.internal.InternalMessage;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.chronicles.services.SavedInformationsManager;
import org.libermundi.theorcs.chronicles.services.message.InternalMessageManager;
import org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage;
import org.libermundi.theorcs.layout.tapestry.services.LayoutAppHelper;
import org.libermundi.theorcs.security.SecurityConstants;
import org.libermundi.theorcs.security.model.User;
import org.libermundi.theorcs.security.services.SecurityManager;
import org.libermundi.theorcs.security.util.SecurityUtils;
import org.slf4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.acls.domain.BasePermission;


public class Index extends ChroniclePage {
	public static final String EVENT_SWITCH_FACTION = "switchfaction";
	
	@SessionState
	private ChronicleSettings _chronicleSettings;
	
	@Inject
	private SecurityManager _securityManager;
	
	@Inject
	private SavedInformationsManager _savedInformationsManager;
	
	@Inject
	private PersonaManager _personaManager;
	
	@Inject
	private InternalMessageManager _messageManager;
	
	@Inject
	private LayoutAppHelper _layoutAppHelper;
	
	@Inject
	private Logger _logger;	
	
	@Property
	@Persist(PersistenceConstants.FLASH)
	private List<InternalMessage> _internalMessages;
	
	@Inject
	private Messages _messages;

	public void onActivate(Chronicle chronicle){
		// Let's check the current user as the right to access this
		boolean chronicleAccess = _securityManager.hasPermission(chronicle, BasePermission.READ);
		
		if(!chronicleAccess) {
			if(SecurityUtils.userHasAuthority(SecurityConstants.ROLE_ROOT) || 
				SecurityUtils.userHasAuthority(SecurityConstants.ROLE_ADMIN) ) {
				_securityManager.switchUser(chronicle.getAdmin());
			} else {
				_logger.error("Access Denied !");
				_logger.error("      Access to Chronicle : " + chronicleAccess);
				throw new AccessDeniedException("You do not have access to this Chronicle");
			}
		}
		
		User currentUser = _securityManager.getCurrentUser();

		Persona perso = _personaManager.getDefaultPersona(_securityManager.getCurrentUser(), chronicle);
		
		_logger.info("User {} with Persona {} is granted to access the Chronicle {}.",currentUser.getUsername(), chronicle.getTitle());
		
		if(_chronicleSettings.getChronicle() == null || !_chronicleSettings.getChronicle().equals(chronicle)){
			_chronicleSettings.setChronicle(chronicle);
			_chronicleSettings.setPersona(perso);
		}
		
		//Saving Informations for Later reuse.
		SavedInformations si = _savedInformationsManager.getByUser(_securityManager.getCurrentUser());
		si.addInformation("chronicleId", chronicle.getId().toString());
		si.addInformation("personaId", perso.getId().toString());
		
		_savedInformationsManager.save(si);
		
		// load the unseen messages received by the persona
		_internalMessages = _messageManager.findUnseenMessagesReceivedByPersona(perso);
	}	
	
	public long onPassivate(){
		return _chronicleSettings.getChronicle().getId();
	}
	
	@OnEvent(Index.EVENT_SWITCH_FACTION)
	public void switchFaction(Faction faction) {
		if(_chronicleSettings.getPersona().getFactions().contains(faction)) {
			_chronicleSettings.setFaction(faction);
		}
	}

	public String getMailBoxMessage() {
		return _messages.format("chronicles.mainmenu.mail.new", _chronicleSettings.getPersona().getName());
	}
	
	public String getForumBoxMessage() {
		return _messages.format("chronicles.mainmenu.scene.new", _chronicleSettings.getPersona().getName());
	}
}
