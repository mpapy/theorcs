/**
 * 
 */
package org.libermundi.theorcs.chronicles.tapestry.components.forum;

import java.util.List;

import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.PropertyConduitSource;
import org.apache.tapestry5.services.Request;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMessage;
import org.libermundi.theorcs.chronicles.services.message.ForumThreadManager;
import org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage;

/**
 * Display a list of forumMessages
 * The list of messages has to provided as parameter
 * 
 * @author Stephane Guichard 
 * @author Martin Papy
 *
 */
public class ForumBox extends ChroniclePage {
	
	/*------------- Services -------------*/
	@SessionState
	private ChronicleSettings _chronicleSettings;
    
	@Inject
	private ForumThreadManager _forumThreadManager;
	
	@Property
	@Persist(PersistenceConstants.FLASH)
	private List<ForumMessage> _forumMessages;
	
	@Property
	private ForumMessage _forumMessage;

	@Inject
	private BeanModelSource _beanModelSource;
	
	@Inject
	private Messages _messages;

	@Inject
	private PropertyConduitSource _pcSource;

	@Inject
	private Request _request;
    
	/**
	 * BeanModel of InternalMessage to display the sender.name and targets
	 */
	@Property
	private BeanModel<ForumMessage> _forumBoxModel;
	
	/*------------- Rendering Phase -------------*/
	@SetupRender
	public void setupRender(){
		_forumMessages = _forumThreadManager.findNewMessages(_chronicleSettings.getPersona());
		
		//beanDisplayModel
		// have to add the property sender to display it as it is a complex class
		_forumBoxModel = _beanModelSource.createDisplayModel(
				ForumMessage.class, _messages);
		_forumBoxModel.exclude("id", "content", "modifiedDate", "author");
		_forumBoxModel.add("authorName", _pcSource.create(ForumMessage.class, "author.name"));
		_forumBoxModel.reorder("authorName");	
	}
}