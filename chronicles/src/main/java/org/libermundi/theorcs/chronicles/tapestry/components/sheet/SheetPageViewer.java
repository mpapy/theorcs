/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.tapestry.components.sheet;

import java.util.Iterator;
import java.util.List;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.RequestParameter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONArray;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.libermundi.theorcs.chronicles.model.sheet.Sheet;
import org.libermundi.theorcs.chronicles.model.sheet.SheetContainer;
import org.libermundi.theorcs.chronicles.model.sheet.SheetPage;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;

/**
 * @author Martin Papy
 *
 */
@Import(library={
		"${chronicles.scripts}/sheetpage.settings.js",
		"${chronicles.scripts}/sheetpage.displaymanager.js"
},
	stylesheet="${chronicles.styles}/sheet.css"
)
public class SheetPageViewer {
	@Parameter(required=true)
	private SheetPage _page;
	
	@Environmental
	private JavaScriptSupport _jsSupport;
	
	@Inject
	private Block _containerBlock;
	
	@Inject
	private ComponentResources _resources;
	
	@Inject
	private AppHelper _appHelper;
	
	private SheetContainer _loadedContainer;
	
	@AfterRender
	public void initGrid(){
		_jsSupport.addScript("SheetGridSettings.init('%s',%s,%s,%s,%s,%s);", 
				_appHelper.getBaseUrlEventLink(_resources),
				getEditMode(),
				Sheet.WIDTH,Sheet.HEIGHT,
				SheetContainer.WIDTH,SheetContainer.HEIGHT);
		_jsSupport.addScript("SheetPageDisplayManager.init();");
	}

	@OnEvent("loadContainerUids")
	public JSONArray loadContainerList() {
		List<SheetContainer> containers = _page.getContainers();
		JSONArray result = new JSONArray();
		for (Iterator<SheetContainer> iterator = containers.iterator(); iterator.hasNext();) {
			SheetContainer sheetContainer = iterator.next();
			result.put(sheetContainer.getUid());
		}
		return result;
	}
	
	@OnEvent("loadContainer")
	public Block loadContainer(@RequestParameter("uid") String uid){
		setLoadedContainer(_page.getContainer(uid));
		return getContainerBlock();
	}

	public SheetContainer getLoadedContainer() {
		return _loadedContainer;
	}
	
	public Block getContainerBlock(){
		return _containerBlock;
	}
	
	protected void setLoadedContainer(SheetContainer loadedContainer) {
		_loadedContainer = loadedContainer;
	}
	
	protected SheetPage getPage() {
		return _page;
	}
	
	protected String getEditMode(){
		return "false";
	}
}
