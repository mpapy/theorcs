package org.libermundi.theorcs.chronicles.dao.hibernate;

import org.libermundi.theorcs.chronicles.dao.FactionDao;
import org.libermundi.theorcs.chronicles.model.Faction;
import org.libermundi.theorcs.core.dao.hibernate.HibernateGenericDao;
import org.springframework.stereotype.Repository;

@Repository
public final class FactionDaoImpl extends HibernateGenericDao<Faction, Long> implements FactionDao {
	public FactionDaoImpl(){
		super();
		_mappedClazz = Faction.class;
	}

}
