/**
 * 
 */
package org.libermundi.theorcs.chronicles.tapestry.pages.forum;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMember;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMemberRole;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumThread;
import org.libermundi.theorcs.chronicles.services.AddressBookManager;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.chronicles.services.message.ForumThreadManager;
import org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage;
import org.libermundi.theorcs.core.services.GenericManager;
import org.libermundi.theorcs.core.tapestry.util.GenericMultipleValueEncoder;
import org.libermundi.theorcs.core.tapestry.util.GenericSelectModel;
import org.libermundi.theorcs.core.tapestry.util.MultipleValueEncoder;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 * Manage a thread
 * 
 * @author Stephane Guichard 
 * @author Martin Papy
 *
 */

public class UpdateForum extends ChroniclePage {
	
	/*------------- Services -------------*/    
	@Inject
	private ForumThreadManager _forumThreadManager;

	@Inject
	private AddressBookManager _addressBookManager;
	
	@Inject
	private PersonaManager _personaManager;
	
	@Inject
	private org.libermundi.theorcs.security.services.SecurityManager _securityManager;

	
	@Inject
	private GenericManager _manager;
	
	/*------------- Fields -------------*/	
	@Persist
	private Map<String, String> _mappedContacts;
	
	@Persist
	private List<Persona> _contacts;

	@Inject
	private Messages _messages;
	
	@Property
	private List<Persona> _participants;
	
	@Property
	private List<Persona> _guests;
	
	@Property
	private List<Persona> _moderators;
	
	@Property
	private String _title;

	@Property
	private String _description;

	@Property
	@Persist(PersistenceConstants.FLASH)
	private ForumThread _forumThread;
	
	@Component
	private Form _forumForm;
	
	/*------------- Rendering Phase -------------*/
	@SetupRender	
	public void setupRender(){
		//Load the contacts of the persona
		_contacts = Lists.newArrayList(_addressBookManager.loadAddressBook(getChronicleSettings().getPersona()).getContacts());
		//add the persona of the player to the contacts list as the user is present as moderator
		_contacts.add(getChronicleSettings().getPersona());
	}
		
	public GenericSelectModel<Persona, Long> getPersonaModel() {
        return new GenericSelectModel<Persona, Long>(_contacts);
	}
	
	public MultipleValueEncoder<Persona> getPersonaEncoder() {
	        return new GenericMultipleValueEncoder<Persona>(_manager, Persona.class);
	}
	
	public void onActivate(ForumThread forum){
		 _participants = new ArrayList<Persona>();
		 _guests = new ArrayList<Persona>();
		 _moderators = new ArrayList<Persona>();
		
		if(forum!=null){
			_forumThread = forum;
			ForumMember fmActor = _forumThreadManager.findMemberFromThreadAndPersona(_forumThread, getCurrentPerso());
			
			//only moderator can manage the forum
			if(fmActor!=null&&ForumMemberRole.MODERATOR.equals(fmActor.getRole())){
				_title = _forumThread.getTitle();
				_description = _forumThread.getDescription();
				
				Iterator<ForumMember> ite =  _forumThread.getMembers().values().iterator();
				while(ite.hasNext()){
					ForumMember fm = ite.next();
					if(!fm.isDeleted()){
						if(ForumMemberRole.PARTICIPANT.equals(fm.getRole()))
							_participants.add(fm.getPersona());
						else if (ForumMemberRole.GUEST.equals(fm.getRole()))
								_guests.add(fm.getPersona());
						else if (ForumMemberRole.MODERATOR.equals(fm.getRole()))
								_moderators.add(fm.getPersona());
					}
				}
			}
		} else {
			//as the current perso creates the forum, it will be by default its moderator.
			_moderators.add(getCurrentPerso());
		}

	}
	
	Long onPassivate() {
		if(_forumThread==null)
			return null;
		return _forumThread.getId();
	}

	@OnEvent(value="archive")
	public Object onArchive(){
		_forumThreadManager.setDeleteThread(_forumThread, getChronicleSettings().getPersona());
		return ListForum.class;
	}
	
	/**
	 * Check if there is at least one target
	 */	
	@OnEvent(value="validate",component="forumForm")
	void onValidate() {
		//a minima a title
		if(Strings.isNullOrEmpty(_title))
			_forumForm.recordError(_messages.format("chronicles.scene.error.notitle"));
		//a minima one moderator
		if(_moderators.isEmpty())
			_forumForm.recordError(_messages.format("chronicles.scene.error.nomoderator"));
	}

	public Object onSuccess(){
		if(_forumThread!=null&&_forumThread.getId()!=null){
			//update the forum
			_forumThreadManager.updateThread(getCurrentPerso(), _forumThread, _title, _description, _participants, _moderators, _guests);
		}else{
			//create a forum
			_forumThread = _forumThreadManager.createNew();

			//TODO getCurrentPerso() works but when the persona is evaluated an com.sun.jdi.InvocationException is thrown  
			//why?
			//_moderators.add(getChronicleSettings().getPersona());
			//_forumThread = _forumThreadManager.createThread(getCurrentPerso(), _title, _description, true, _participants, _moderators, _guests);
			Persona perso = _personaManager.getDefaultPersona(_securityManager.getCurrentUser(), getChronicleSettings().getChronicle());

			//the creator is automatically moderator
			_moderators.add(perso);
			_forumThread = _forumThreadManager.createThread(perso, _title, _description, true, _participants, _moderators, _guests);
		}
		return ListForum.class;
	}
}