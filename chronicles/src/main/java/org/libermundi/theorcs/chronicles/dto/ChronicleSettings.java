package org.libermundi.theorcs.chronicles.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.model.Faction;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.core.services.GenericManager;

public final class ChronicleSettings {
	@Inject
	private GenericManager _manager;
	
	private Chronicle _chronicle;
	private Persona _persona;
	private Faction _faction;


	public Chronicle getChronicle() {
		//We need to reflect immediately any change that might have be done
		//By an Administrator in a separate Thread
		return _manager.refresh(_chronicle);
	}

	public void setChronicle(Chronicle chronicle) {
		_chronicle=chronicle;
	}

	public Persona getPersona() {
		//We need to reflect immediately any change that might have be done
		//By an Administrator in a separate Thread
		return _manager.refresh(_persona);
	}

	public void setPersona(Persona perso) {
		_persona=perso;
		_faction = _persona.getDefaultFaction();
	}
	
	public void setFaction(Faction faction) {
		_faction = faction;
	}
	
	public List<Object> getContext() {
		List<Object> context = new ArrayList<>();
			context.add(getChronicle().getId());
			context.add(getPersona().getId());
		return context;
	}
	
	public Faction getFaction() {
		return _manager.refresh(_faction);
	}

	public void clear() {
		_chronicle = null;
		_persona = null;		
	}
}
