package org.libermundi.theorcs.chronicles.dto;

import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.sheet.SheetPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SheetEditorSettings {
	private static final Logger logger = LoggerFactory.getLogger(SheetEditorSettings.class);
	
	private Persona _perso;
	
	private int _pageIndex;
	
	/**
	 * @return the perso
	 */
	public Persona getPerso() {
		return _perso;
	}

	/**
	 * @param perso the perso to set
	 */
	public void setPerso(Persona perso) {
		_perso = perso;
	}	
	
	/**
	 * @return the page currently edited
	 */
	public SheetPage getPage() {
		return _perso.getSheet().getPage(_pageIndex);
	}

	/**
	 * @return the Index of the Page Currently Edited
	 */
	public int getPageIndex() {
		return _pageIndex;
	}

	/**
	 * @param pageIndex : the Index of the Page Currently Edited
	 */
	public void setPageIndex(int pageIndex) {
		_pageIndex = pageIndex;
	}

	public void clear() {
		if(logger.isDebugEnabled()) {
			logger.debug("Clear previous stored settings");
		}
		_perso = null;
		_pageIndex = 0;
	}

	@Override
	public String toString() {
		return "SheetEditorSettings [_perso=" + _perso + ", _pageIndex="
				+ _pageIndex + "]";
	}
}
