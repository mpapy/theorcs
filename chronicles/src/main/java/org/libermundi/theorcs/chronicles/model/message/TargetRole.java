package org.libermundi.theorcs.chronicles.model.message;

/**
 * Enumeration of target roles
 *
 */
public enum TargetRole {TO, CC, BCC}
	