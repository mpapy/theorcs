package org.libermundi.theorcs.chronicles.services.message;

import java.util.List;

import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.internal.InternalMessage;
import org.libermundi.theorcs.core.services.Manager;

/**
 * InternalMessageManager 
 * 
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
public interface InternalMessageManager extends Manager<InternalMessage, Long> {

	public static String SERVICE_ID = "internalMessageManager";

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.services.Manager#initialize()
	 */
	void initialize();

	/**
	 * @param message
	 * @return
	 */
	boolean sendMessage(InternalMessage message);

	/**
	 * Get the messages received (and not deleted) of a persona
	 * @param persona
	 * @return
	 */
	public List<InternalMessage> findMessagesReceivedByPersona(Persona persona);

	/**
	 * Get the unseen messages received of a persona
	 * @param persona
	 * @return
	 */
	List<InternalMessage> findUnseenMessagesReceivedByPersona(Persona persona);
	
	/**
	 * Get the messages sent by a persona
	 * @param persona
	 * @return
	 */
	public List<InternalMessage> findMessagesSentByPersona(Persona persona);
	
	/**
	 * Get the mesages deleted by a persona
	 * @param persona
	 * @return
	 */
	public List<InternalMessage> findMessagesDeletedByPersona(Persona persona);

	/**
	 * Update the message 
	 * @param msg
	 * @param isSeen
	 * @param currentPerso
	 */
	void updateMessageOnSeen(InternalMessage msg, Persona persona, boolean isSeen);

	/**
	 * Set delete on a message for a persona
	 * @param msg
	 * @param persona
	 * @param isDeleted 
	 */
	void updateMessageOnDeleted(InternalMessage msg, Persona persona, boolean isDeleted);

}
