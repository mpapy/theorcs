/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.tapestry.services.impl;

import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.ApplicationStateManager;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.core.services.StringSource;

/**
 * @author Martin Papy
 *
 */
public class FactionMenuStringSource implements StringSource {
	private final static String FACTION_MENU_LABEL="chronicles.mainmenu.factions";
	
	@Inject
	private Messages _messages;
	
	@Inject
	private ApplicationStateManager _asm;
	

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.services.StringSource#renderString()
	 */
	@Override
	public String renderString() {
		ChronicleSettings chronicleSettings = _asm.get(ChronicleSettings.class);
		
		return _messages.format(FACTION_MENU_LABEL, chronicleSettings.getFaction().getName());
	}

}
