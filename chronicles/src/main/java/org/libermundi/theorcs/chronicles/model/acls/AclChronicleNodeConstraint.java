/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.model.acls;

import java.util.List;

import org.libermundi.theorcs.core.model.NodeConstraint;
import org.libermundi.theorcs.core.model.NodeConstraintType;
import org.libermundi.theorcs.core.model.impl.NodeConstraintTypeImpl;
import org.springframework.security.acls.model.Permission;

import com.google.common.collect.Lists;

/**
 * @author Martin Papy
 *
 */
public class AclChronicleNodeConstraint implements NodeConstraint {
	public static final NodeConstraintType TYPE = new NodeConstraintTypeImpl("AclChronicleNodeContraint");
	
	private List<Permission> _permissions;
	
	public AclChronicleNodeConstraint() {
		_permissions = Lists.newArrayList();
	}

	public AclChronicleNodeConstraint(List<Permission> permissions) {
		this();
		_permissions = permissions;
	}

	public AclChronicleNodeConstraint(Permission firstPermission, Permission... otherPermissions) {
		addPermission(firstPermission);
		if(otherPermissions != null){
			for (int i = 0; i < otherPermissions.length; i++) {
				addPermission(otherPermissions[i]);
			}
		}
	}
	
	public void addPermission(Permission acl){
		_permissions.add(acl);
	}
	
	public Permission[] getPermissions(){
		return _permissions.toArray(new Permission[_permissions.size()]);
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.NodeConstrain#getType()
	 */
	@Override
	public NodeConstraintType getType() {
		return TYPE;
	}
	
}
