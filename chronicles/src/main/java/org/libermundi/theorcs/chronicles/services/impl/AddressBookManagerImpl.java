/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.services.impl;

import java.util.List;
import java.util.SortedSet;

import org.libermundi.theorcs.chronicles.dao.AddressBookDao;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.AddressBook;
import org.libermundi.theorcs.chronicles.services.AddressBookManager;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.core.services.impl.AbstractManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;

/**
 * @author Martin Papy
 * @author Stéphane Guichard
 *
 */
@Service(AddressBookManager.SERVICE_ID)
@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
public class AddressBookManagerImpl extends AbstractManagerImpl<AddressBook,Long> implements AddressBookManager {

	@Autowired
	private PersonaManager _personaManager;
	
	@Autowired
	public AddressBookManagerImpl(AddressBookDao addressBookDao) {
		super();
		setDao(addressBookDao);
	}	

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.services.AddressBookManager#loadAddressBook(org.libermundi.theorcs.chronicles.model.Persona)
	 */
	@Override
	public AddressBook loadAddressBook(Persona owner) {
		AddressBook addressBook = getDao().loadAddressBook(owner);
		if(addressBook == null) {
			addressBook = new AddressBook();
			addressBook.setOwner(owner);
			save(addressBook);
		}
		return addressBook;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.services.impl.AbstractManagerImpl#getDao()
	 */
	@Override
	public AddressBookDao getDao() {
		return (AddressBookDao)super.getDao();
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.services.impl.AbstractManagerImpl#initialize()
	 */
	@Override
	public void initialize() {
		if(getAllCount() == 0L) {
			//Add the Address Book for the admin only
			List<Persona> persos = _personaManager.getAll();
			Persona owner = persos.get(0);
			
			SortedSet<Persona> ctc = Sets.newTreeSet();
			for (int i = 1; i < persos.size(); i++) {
				ctc.add(persos.get(i));			
			}
			
			AddressBook ab = loadAddressBook(owner);
			ab.setContacts(ctc);
			//Save Address Book
			save(ab);
		}
	}

	@Override
	public AddressBook createNew() {
		return new AddressBook();
	}

	public void addContact(Persona owner, Persona newContact){
		AddressBook ab = loadAddressBook(owner);
		SortedSet<Persona> ctc = ab.getContacts();
		ctc.add(newContact);
		ab.setContacts(ctc);
		save(ab);
	}
	
}
