/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.libermundi.theorcs.chronicles.dao.PersonaDao;
import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.core.dao.exception.OrcsDataAccessException;
import org.libermundi.theorcs.core.dao.hibernate.HibernateGenericDao;
import org.libermundi.theorcs.security.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public final class PersonaDaoImpl extends HibernateGenericDao<Persona, Long> implements PersonaDao {
	private static final Logger logger = LoggerFactory.getLogger(PersonaDaoImpl.class);
	
	public PersonaDaoImpl(){
		super();
		_mappedClazz = Persona.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Persona> getChroniclePersonas(Chronicle chronicle) {
		List<Persona> result;
		try{
			result = createSafeCriteria()
						.add(Restrictions.eq(Persona.PROP_CHRONICLE, chronicle))
						.list();
		} catch (HibernateException e) {
			logger.error("Exception in getChroniclePersonas(" + chronicle +")", e);
			throw new OrcsDataAccessException("Exception in getChroniclePersonas : " + chronicle, e);
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Persona> getAllPersona(User user, Chronicle chronicle) {
		List<Persona> result = null;
		try{
			result = createQuery("Select perso " +
					"from Persona as perso inner join perso.player as player " +
						"inner join perso.chronicle as chronique " +
					"where perso.player = :user " +
						"and perso.chronicle = :chronicle " +
						"and perso.deleted = :deleted " +
						"and perso.active = :active " +
					"order by perso.id")
					.setParameter("user", user)
					.setParameter("chronicle", chronicle)
					.setBoolean("deleted", Boolean.FALSE)
					.setBoolean("active", Boolean.TRUE)
					.list();
		} catch (HibernateException e) {
			logger.error("Exception in getAllPersona(" + user + ", "+ chronicle +")", e);
			throw new OrcsDataAccessException("Exception in getAllPersona : " + user + ", "+ chronicle, e);
		}
		return result;
	}
	
}
