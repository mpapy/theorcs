/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.tapestry.pages.admin.perso;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.tapestry.base.ChronicleAdminPage;

/**
 * @author Martin Papy
 *
 */
public class Contacts extends ChronicleAdminPage {
	@Property
	private Persona _selectedPerso;
	
	@Inject
	@Property(write=false)
	private Block _editor;
	
	@Inject
	@Property(write=false)
	private Block _empty;	
	
	@OnEvent("valueChanged")
	public Block selectPerso(Persona perso){
		// The new Perso is actually loaded in the setSelected method 
		// that is called after this Event
		if(perso == null) {
			return _empty;
		}
		return _editor;
	}

}
