/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.model.menu;

import java.util.List;

import org.apache.tapestry5.services.ApplicationStateManager;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.layout.model.menu.AbstractNavMenuItem;
import org.libermundi.theorcs.layout.model.menu.NavMenuItemType;
import org.libermundi.theorcs.security.services.SecurityManager;


/**
 * @author Martin Papy
 *
 */
public class PersonaSwitchNavMenuItem extends AbstractNavMenuItem {

	private static final long serialVersionUID = -1572939587041334677L;
	
	private PersonaManager _personaManager;
	
	private SecurityManager _securityManager;
	
	private ApplicationStateManager _asm;

	public PersonaSwitchNavMenuItem(NavMenuItemType type, PersonaManager personaManager, SecurityManager securityManager, ApplicationStateManager asm){
		super(type);
		_personaManager = personaManager;
		_securityManager = securityManager;
		_asm = asm;		
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.NodeData#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		ChronicleSettings cs = _asm.get(ChronicleSettings.class);
		List<Persona> persos = _personaManager.getAllPersona(_securityManager.getCurrentUser(), cs.getChronicle());
			persos.remove(cs.getPersona()); // Cannot select the current Persona
		return persos.isEmpty();
	}
}
