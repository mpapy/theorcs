/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.services.impl;

import java.util.List;

import org.libermundi.theorcs.chronicles.dao.ChronicleDao;
import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.model.ChronicleGenre;
import org.libermundi.theorcs.chronicles.model.ChroniclePace;
import org.libermundi.theorcs.chronicles.model.ChronicleStyle;
import org.libermundi.theorcs.chronicles.model.ChronicleType;
import org.libermundi.theorcs.chronicles.model.Game;
import org.libermundi.theorcs.chronicles.model.acls.ChroniclePermission;
import org.libermundi.theorcs.chronicles.services.ChronicleManager;
import org.libermundi.theorcs.chronicles.services.FactionManager;
import org.libermundi.theorcs.chronicles.services.GameManager;
import org.libermundi.theorcs.core.services.impl.AbstractManagerImpl;
import org.libermundi.theorcs.security.acls.ExtendedPermissionFactory;
import org.libermundi.theorcs.security.model.User;
import org.libermundi.theorcs.security.services.SecurityManager;
import org.libermundi.theorcs.security.services.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service(ChronicleManager.SERVICE_ID)
@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
public class ChronicleManagerImpl extends AbstractManagerImpl<Chronicle,Long> implements ChronicleManager {
	private static final Logger logger = LoggerFactory.getLogger(ChronicleManagerImpl.class);
	
	@Autowired
	private GameManager _gameManager;
	
	@Autowired
	private UserManager _userManager;
	
	@Autowired
	private SecurityManager _securityManager;
	
	@Autowired
	private FactionManager _factionManager;
	
	@Autowired
	private ExtendedPermissionFactory _permissionFactory;
	
	@Autowired
	public ChronicleManagerImpl(ChronicleDao chronicleDao) {
		super();
		setDao(chronicleDao);
	}

	@Override
	public void initialize() {
		
		//Register new Specific Permissions
		_permissionFactory.registerPermission(ChroniclePermission.class);
		
		// Initializing new set of Data if necessary
		if(getAllCount() == 0L) {
			logger.debug("Initializing Chronicles");
			Game defaultGame = _gameManager.loadLast();
			User admin = _userManager.getUserByUsername("admin");
			
			Chronicle c = new Chronicle();
				c.setTitle("Demo Chronicle");
				c.setSubTitle("A small description for this chronicle");
				c.setGame(defaultGame);
				c.setAdmin(admin);
				c.setPassword("demo1234");
				c.setType(ChronicleType.STANDARD);
				c.setGenre(ChronicleGenre.OTHER);
				c.setStyle(ChronicleStyle.BALANCED);
				//pace cannot be null
				c.setPace(ChroniclePace.AVERAGE);
			
			saveAndGrantAdminRights(c,admin);

		}

	}
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.services.ChronicleManager#saveAndGrantAdminRights(org.libermundi.theorcs.chronicles.model.Chronicle, org.libermundi.theorcs.security.model.User)
	 */
	@Override
	public void saveAndGrantAdminRights(Chronicle chronicle, User admin) {
		save(chronicle);
		_securityManager.grantAdminAcl(admin, chronicle);
		_securityManager.setAcl(admin, chronicle, ChroniclePermission.MJ, Boolean.TRUE);
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.services.ChronicleManager#getChroniclesByUser(org.libermundi.theorcs.security.model.User)
	 */
	@Override
	public List<Chronicle> getChroniclesByUser(User user) {
		return getDao().getChroniclesByUser(user);
	}
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.services.ChronicleManager#getUsersByChronicle(org.libermundi.theorcs.chronicles.model.Chronicle)
	 */
	@Override
	public List<User> getUsersByChronicle(Chronicle chronicle) {
		return getDao().getUsersByChronicle(chronicle);
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.services.impl.AbstractManagerImpl#getDao()
	 */
	@Override
	public ChronicleDao getDao() {
		return (ChronicleDao)super.getDao();
	}

	@Override
	public Chronicle createNew() {
		Chronicle chronicle = new Chronicle();
			chronicle.setActive(Boolean.FALSE);
			chronicle.setDeleted(Boolean.FALSE);
		return chronicle;
	}	

}
