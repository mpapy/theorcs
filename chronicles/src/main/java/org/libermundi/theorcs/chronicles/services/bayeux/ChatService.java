/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.services.bayeux;

import java.util.HashMap;
import java.util.Map;

import org.cometd.bayeux.Message;
import org.cometd.bayeux.server.BayeuxServer;
import org.cometd.bayeux.server.ServerSession;
import org.cometd.server.AbstractService;
import org.libermundi.theorcs.core.util.AppContext;
import org.libermundi.theorcs.security.services.UserManager;

public class ChatService extends AbstractService {
	@SuppressWarnings("unused")
	private UserManager _userManager; 
	
    public ChatService(BayeuxServer bayeux) {
        super(bayeux, "chroniclechat");
        addService("/service/chronicle/listusers", "processListUsers");
        
        // Add Spring Service calls
        _userManager = AppContext.getBean("userManager");
        
    }

    public void processListUsers(ServerSession remote, Message message) {
        Map<String, Object> input = message.getDataAsMap();
        String name = (String)input.get("name");

        Map<String, Object> output = new HashMap<>();
        output.put("greeting", "Hello, " + name);
        remote.deliver(getServerSession(), "/hello", output, null);
    }
}
