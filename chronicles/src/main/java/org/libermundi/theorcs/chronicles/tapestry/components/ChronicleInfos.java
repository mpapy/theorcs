package org.libermundi.theorcs.chronicles.tapestry.components;

import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.libermundi.theorcs.chronicles.model.Chronicle;

public class ChronicleInfos {
	@Parameter(allowNull=false,required=true)
	@Property(write=false)
	private Chronicle _chronicle;
	
	@Parameter(value="true")
	@Property(write=false)
	private boolean _displayTitle;
	

}
