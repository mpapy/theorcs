package org.libermundi.theorcs.chronicles.services.message.impl;

import java.util.ArrayList;
import java.util.List;

import org.libermundi.theorcs.chronicles.dao.InternalMessageDao;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.internal.BccTarget;
import org.libermundi.theorcs.chronicles.model.message.internal.CcTarget;
import org.libermundi.theorcs.chronicles.model.message.internal.InternalMessage;
import org.libermundi.theorcs.chronicles.model.message.internal.ToTarget;
import org.libermundi.theorcs.chronicles.services.ChronicleManager;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.chronicles.services.message.InternalMessageManager;
import org.libermundi.theorcs.core.services.impl.AbstractManagerImpl;
import org.libermundi.theorcs.security.model.User;
import org.libermundi.theorcs.security.services.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service for internal email service (notifications of new posts, new messages from other personas...)
 * 
 * @author Stephane Guichard
 * @author Martin Papy
 */
@Service(InternalMessageManager.SERVICE_ID)
@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
public class InternalMessageManagerImpl extends AbstractManagerImpl<InternalMessage,Long> implements InternalMessageManager {
	
	private static final Logger logger = LoggerFactory.getLogger(InternalMessageManagerImpl.class);

	@Autowired
	private UserManager _userManager;

	@Autowired
	private ChronicleManager _chronicleManager;
	
	@Autowired
	private PersonaManager _personaManager;
	
	@Autowired
	public InternalMessageManagerImpl(InternalMessageDao messageDao) {
		super();
		setDao(messageDao);
	}
	
	@Override
	public InternalMessageDao getDao() {
		return (InternalMessageDao)super.getDao();
	}
	

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.services.impl.AbstractManagerImpl#initialize()
	 */
	@Override
	public void initialize() {
		if(getAllCount() == 0L) {
			logger.debug("Initializing messaging");
			User player = _userManager.getUserByUsername("user2");
			Persona perso1 = _personaManager.getDefaultPersona(player, _chronicleManager.loadLast());
			User player1 = _userManager.getUserByUsername("admin");	
			Persona persoAdmin = _personaManager.getDefaultPersona(player1, _chronicleManager.loadLast());
			User player2 = _userManager.getUserByUsername("user1");	
			Persona perso2 = _personaManager.getDefaultPersona(player2, _chronicleManager.loadLast());
			
			List<ToTarget> to1 = new ArrayList<ToTarget>();
			to1.add(new ToTarget(perso1));			
			List<CcTarget> cc1 = new ArrayList<CcTarget>();
			cc1.add(new CcTarget(perso2));
	
			InternalMessage m1 = new InternalMessage(perso1,"title of an internal Email1", "body of an internal Email1", to1, cc1, null);
			sendMessage(m1);
		
			List<ToTarget> to2 = new ArrayList<ToTarget>();
			to2.add(new ToTarget(perso1));	
			List<BccTarget> bcc2 = new ArrayList<BccTarget>();
			bcc2.add(new BccTarget(perso2));
			BccTarget t22 = new BccTarget(persoAdmin);
			bcc2.add(t22);
			InternalMessage m2 = new InternalMessage(persoAdmin,"title of an internal Email2", "body of an internal Email2", to2, null, bcc2);
			sendMessage(m2);
			
			List<ToTarget> to3 = new ArrayList<ToTarget>();
			to3.add(new ToTarget(perso1));
			List<BccTarget> bcc3 = new ArrayList<BccTarget>();
			BccTarget t32 = new BccTarget(persoAdmin);
			t32.setDeleted(true);
			t32.setSeen(true);
			bcc3.add(t32);
			InternalMessage m3 = new InternalMessage(perso2,"title of an internal Email3", "body of an internal Email3", to3, null, bcc3);
			sendMessage(m3);
			
			List<ToTarget> to4 = new ArrayList<ToTarget>();
			to4.add(new ToTarget(persoAdmin));
			List<CcTarget> cc4 = new ArrayList<CcTarget>();
			CcTarget c4 = new CcTarget(perso2);
			cc4.add(c4);
			List<BccTarget> bcc4 = new ArrayList<BccTarget>();
			BccTarget bc4 = new BccTarget(perso1);
			bcc4.add(bc4);
			InternalMessage m4 = new InternalMessage(perso2,"title of an internal Email4", "body of an internal Email4", to4, cc4, bcc4);
			sendMessage(m4);
	
			List<ToTarget> to5 = new ArrayList<ToTarget>();
			to5.add(new ToTarget(persoAdmin));
			List<CcTarget> cc5 = new ArrayList<CcTarget>();
			CcTarget c55 = new CcTarget(perso2);
			cc5.add(c55);
			List<BccTarget> bcc5 = new ArrayList<BccTarget>();
			BccTarget bc55 = new BccTarget(perso1);
			bcc5.add(bc55);
			InternalMessage m5 = new InternalMessage(perso2,"title of an internal Email5", "body of an internal Email5", to5, cc5, bcc5);
			sendMessage(m5);
			
			logger.debug("end of initiliazing InternalMessage");
		}
			
	}

	@Override
	public InternalMessage createNew() {
		return new InternalMessage();
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.services.message.InternalMessageManager#sendMessage(org.libermundi.theorcs.chronicles.model.message.internal.InternalMessage)
	 */
	@Override
	public boolean sendMessage(InternalMessage message) {
		try{
			save(message);
			return true;
		}catch(Exception e){
			logger.error("cannot process message sent by " + message.getAuthor(), e.getMessage());
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.services.message.InternalMessageManager#findMessagesSentByPersona(org.libermundi.theorcs.chronicles.model.Persona)
	 */
	public List<InternalMessage> findMessagesSentByPersona(Persona persona) {
		return ((InternalMessageDao)getDao()).findSentMessagesByPersona(persona);
	}
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.services.message.InternalMessageManager#findMessagesDeletedByPersona(org.libermundi.theorcs.chronicles.model.Persona)
	 */
	public List<InternalMessage> findMessagesDeletedByPersona(Persona persona) {
		return ((InternalMessageDao)getDao()).findDeletedMessagesByPersona(persona);
	}
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.services.message.InternalMessageManager#getMessagesReceivedByPersona(org.libermundi.theorcs.chronicles.model.Persona)
	 */
	@Override
	public List<InternalMessage> findMessagesReceivedByPersona(Persona persona) {
		return ((InternalMessageDao)getDao()).findMessagesReceivedByPersona(persona);
	}
	
	@Override
	public List<InternalMessage> findUnseenMessagesReceivedByPersona(Persona persona) {
		return ((InternalMessageDao)getDao()).findUnseenMessagesReceivedByPersona(persona);	
	}

	@Override
	public void updateMessageOnSeen(InternalMessage msg, Persona persona, boolean isSeen) {
		((InternalMessageDao)getDao()).updateMessageOnSeen(msg, persona, isSeen);	
	}

	@Override
	public void updateMessageOnDeleted(InternalMessage msg, Persona persona, boolean isDeleted) {
		((InternalMessageDao)getDao()).updateMessageOnDelete(msg, persona, isDeleted);		
	}

}
