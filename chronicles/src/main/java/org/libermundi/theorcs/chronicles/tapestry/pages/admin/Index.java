package org.libermundi.theorcs.chronicles.tapestry.pages.admin;

import java.util.List;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.chronicles.ChronicleConstants;
import org.libermundi.theorcs.chronicles.tapestry.base.ChronicleAdminPage;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.tapestry.services.MenuService;
import org.libermundi.theorcs.security.services.SecurityManager;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UserDetails;

@Secured("ROLE_USER")
public class Index extends ChronicleAdminPage {
	@Inject
	private MenuService _menuService;
	
	@Inject
	private SecurityManager _securityManager;
	
	public List<Node<NavMenuItem>> getAdminMenuItems(){
		Node<NavMenuItem> adminMenu = _menuService.getNodeById(ChronicleConstants.CHRO_ADMIN_NAV_ID);
		
		return adminMenu.getChildren();
		
	}
	
	public UserDetails getUserDetails() {
		return _securityManager.getCurrentUserDetails();
	}

}
