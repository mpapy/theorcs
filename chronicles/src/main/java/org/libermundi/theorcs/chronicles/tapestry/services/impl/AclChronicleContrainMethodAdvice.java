/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.tapestry.services.impl;

import java.util.List;

import org.apache.tapestry5.services.ApplicationStateManager;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.chronicles.model.acls.AclChronicleNodeConstraint;
import org.libermundi.theorcs.core.model.NodeConstraint;
import org.libermundi.theorcs.core.model.NodeConstrainVote;
import org.libermundi.theorcs.core.model.impl.AbstractNodeConstraintMethodAdvice;
import org.libermundi.theorcs.security.model.impl.AclNodeConstraint;
import org.libermundi.theorcs.security.services.SecurityManager;

/**
 * @author Martin Papy
 *
 */
public final class AclChronicleContrainMethodAdvice extends AbstractNodeConstraintMethodAdvice {
	private final SecurityManager _manager;
	
	private final ApplicationStateManager _applicationStateManager;

	public AclChronicleContrainMethodAdvice(SecurityManager manager, ApplicationStateManager applicationStateManager) {
		_manager = manager;
		_applicationStateManager = applicationStateManager;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.impl.AbstractNodeConstrainMethodAdvice#vote(java.util.List, org.libermundi.theorcs.core.model.NodeConstrain)
	 */
	@Override
	protected void vote(List<NodeConstrainVote> votes, NodeConstraint constrain) {
		ChronicleSettings cs = _applicationStateManager.getIfExists(ChronicleSettings.class);
		if(constrain.getType() == AclChronicleNodeConstraint.TYPE) {
			if(cs == null || cs.getChronicle() == null) {
				votes.add(NodeConstrainVote.NEGATIVE);
			}
			if(_manager.hasPermission(cs.getChronicle(), ((AclNodeConstraint)constrain).getPermissions())) {
				votes.add(NodeConstrainVote.AFFIMATIVE);
			} else {
				votes.add(NodeConstrainVote.NEGATIVE);
			}
		} else {
			votes.add(NodeConstrainVote.NEUTRAL);
		}
	}

}
