package org.libermundi.theorcs.chronicles.dao.hibernate;

import org.libermundi.theorcs.chronicles.dao.GameDao;
import org.libermundi.theorcs.chronicles.model.Game;
import org.libermundi.theorcs.core.dao.hibernate.HibernateGenericDao;
import org.springframework.stereotype.Repository;

@Repository
public final class GameDaoImpl extends HibernateGenericDao<Game, Long> implements GameDao {
	public GameDaoImpl(){
		super();
		_mappedClazz = Game.class;
	}

}
