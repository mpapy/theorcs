/**
 * 
 */
package org.libermundi.theorcs.chronicles.tapestry.pages.forum;

import java.util.Collection;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.PropertyConduitSource;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMember;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMemberRole;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMessage;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumThread;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.chronicles.services.message.ForumThreadManager;
import org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage;
import org.libermundi.theorcs.core.tapestry.services.assets.AssetServices;

/**
 * Displays the details of a thread
 * s
 * @author Stephane Guichard 
 * @author Martin Papy
 *
 */
public class ReadForum extends ChroniclePage {
	@Persist
	private Long _forumId;
	
	@Property
	@Persist(PersistenceConstants.FLASH)
	private ForumThread _forumThread;
	
	@Property
	private ForumMessage _message;
	
	@Property
	private Boolean _moderator;
	
	@Property
	private Boolean _guest;
	
	@Inject
	private JavaScriptSupport _jsSupport;
	
	/**
	 * ajax zone to open message 
	 */
	@InjectComponent
	private Zone _addPostZone;
	
	@Inject
	@Path("${layout.images}/avatar/avatar250.png")
	private Asset _defaultAvatar;
	
	/*------------- Services -------------*/
	@SessionState
	private ChronicleSettings _chronicleSettings;
    
	@Inject
	private ForumThreadManager _forumThreadManager;
	
	@Property
	private Collection<ForumMember> members;
	
	@Property
	private ForumMember member;
	
	@Inject
	private PersonaManager _personaManager;
	
	@Inject
	private AssetServices _assetServices;
	
	/**
	 * BeanModel of ForumThread to display 
	 */
	@Property
	private BeanModel<ForumMessage> _forumMessageModel;
	
	@Inject
	private BeanModelSource _beanModelSource;
	
	@Inject
	private Messages _messages;

	@Inject
	private PropertyConduitSource _pcSource;

	@Inject
	private Request _request;
		
	
	/*------------- Rendering Phase -------------*/
	public void setupRender(){
		_forumMessageModel = _beanModelSource.createDisplayModel(ForumMessage.class, _messages);
		_forumMessageModel.exclude("id", "createdDate", "modifiedDate");
		_forumMessageModel.add("author", _pcSource.create(ForumMessage.class, "author.name"));
		_forumMessageModel.get("author").sortable(false);
		_forumMessageModel.get("content").sortable(false);
		_forumMessageModel.reorder("author","content");

		ForumMember fmActor= _forumThreadManager.findMemberFromThreadAndPersona(_forumThread, _chronicleSettings.getPersona());
		//only a moderator can manage the thread
		_moderator = ForumMemberRole.MODERATOR.equals(fmActor.getRole());
		//a guest cannot post
		_guest = ForumMemberRole.GUEST.equals(fmActor.getRole());
	}
	
	public void onActivate(Long forumId){
		if(forumId!=null){
			Persona currentPerso = _chronicleSettings.getPersona();
			currentPerso = _personaManager.load(currentPerso.getId());
			_forumId = forumId;
			_forumThread = _forumThreadManager.findActiveThreadFromPersona(_forumId.longValue(), currentPerso);
			//the persona has read the last message
			_forumThreadManager.flagThreadAsReadenByPersona(_forumThread, currentPerso);
			members = _forumThread.getMembers().values();
		}
	}
	
	Long onPassivate() {
		return this._forumId;
	}
	
	@OnEvent("renderAddPostJs")
    public void renderLoginJs(){
		_jsSupport.require("zone.modal").invoke("init").with("AddPost");
    }
	
	public Asset getPersonaPicture(){
		String pict = _message.getAuthor().getPicture();
		
		return _assetServices.getAsset(pict, _defaultAvatar);
	}
	
	 @OnEvent("addPost")
	 public void addPost(){
		 //do nothing
	 }
}