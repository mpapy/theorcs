package org.libermundi.theorcs.chronicles.model;

public enum ChronicleStyle {
	AMBIANCE,
	ACTION,
	BALANCED
}
