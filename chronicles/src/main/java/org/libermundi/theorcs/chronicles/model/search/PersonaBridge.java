package org.libermundi.theorcs.chronicles.model.search;

import org.hibernate.search.bridge.StringBridge;
import org.libermundi.theorcs.chronicles.model.Persona;

public class PersonaBridge implements StringBridge {

	/* (non-Javadoc)
	 * @see org.hibernate.search.bridge.StringBridge#objectToString(java.lang.Object)
	 */
	@Override
	public String objectToString(Object object) {
		if(object != null)
			return ((Persona)object).getName();
		else
			return null;
	}

}
