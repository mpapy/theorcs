/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.model.sheet.old;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class SkillBlock implements Serializable {
	private static final long serialVersionUID = -1128691644082171005L;
	
	private String _uid;
	
	private String _name="";
	
	private List<Skill> _skills = new ArrayList<>(0);
	
	public SkillBlock() {
		setUid(UUID.randomUUID().toString());
	}
	
	public SkillBlock(String name) {
		this();
		_name=name;
	}

	public String getUid() {
		return _uid;
	}

	public void setUid(String uid) {
		_uid=uid;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name=name;
	}	
	
	public List<Skill> getSkills() {
		return _skills;
	}

	public void setSkills(List<Skill> skills) {
		_skills = skills;
	}

	public Skill addSkill() {
		return addSkill("");
	}

	public Skill addSkill(String name) {
		return addSkill(name, Boolean.FALSE);
	}

	public Skill addSkill(String name, boolean trunk) {
		return addSkillAt(_skills.size(), name, trunk);
	}

	public Skill addSkillAt(int index) {
		return addSkillAt(index, "");
	}

	public Skill addSkillAt(int index, String name) {
		return addSkillAt(_skills.size(), name, Boolean.FALSE);
	}
	
	public Skill addSkillAt(int index, String name, boolean trunk) {
		Skill sk = new Skill(name,trunk);
		if(index >= _skills.size()) {
			_skills.add(sk);
		} else {
			_skills.add(index, sk);
		}
		return sk;
	}

}
