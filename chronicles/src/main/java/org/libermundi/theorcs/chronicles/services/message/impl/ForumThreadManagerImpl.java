package org.libermundi.theorcs.chronicles.services.message.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.libermundi.theorcs.chronicles.dao.ForumThreadDao;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMember;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMemberRole;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMessage;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumThread;
import org.libermundi.theorcs.chronicles.services.ChronicleManager;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.chronicles.services.message.ForumThreadManager;
import org.libermundi.theorcs.core.services.impl.AbstractManagerImpl;
import org.libermundi.theorcs.security.model.User;
import org.libermundi.theorcs.security.services.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
@Service(ForumThreadManager.SERVICE_ID)
@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
public class ForumThreadManagerImpl extends AbstractManagerImpl<ForumThread, Long> implements ForumThreadManager {

	private static final Logger logger = LoggerFactory.getLogger(ForumThreadManagerImpl.class);

	@Autowired
	private UserManager _userManager;

	@Autowired
	private ChronicleManager _chronicleManager;

	@Autowired
	private PersonaManager _personaManager;

	//add the entityManager to avoid to create a ForumMemberManager class only to add or remove it from the Map
	@PersistenceContext
    protected EntityManager _entityManager;

	@Autowired
	public ForumThreadManagerImpl(ForumThreadDao forumThreadDao) {
		super();
		setDao(forumThreadDao);
	}

	@Override
	public void initialize() {
		if (getAllCount() == 0L) {
			logger.debug("Initializing ForumThread");
			User user2 = _userManager.getUserByUsername("user2");
			Persona perso2 = _personaManager.getDefaultPersona(user2,
					_chronicleManager.loadLast());
			User admin = _userManager.getUserByUsername("admin");
			Persona persoAdmin = _personaManager.getDefaultPersona(admin,
					_chronicleManager.loadLast());
			User player1 = _userManager.getUserByUsername("user1");
			Persona perso1 = _personaManager.getDefaultPersona(player1,
					_chronicleManager.loadLast());
	
			ForumThread f0 = createThread(perso1, "Premier forum",
					"un forum qu'il est bien parce que vide", true, null, null,
					null);
			
			addMember(f0, persoAdmin, ForumMemberRole.PARTICIPANT);
			
			ForumThread f1 = createThread(persoAdmin, "Forum de perso 1",
					"Le forum où l'admin est aussi moderator", true, null, null,
					null);
			
			addMember(f1, persoAdmin, ForumMemberRole.MODERATOR);
			
			ForumThread f2 = createThread(persoAdmin, "Forum de perso 2",
					"Le forum où l'admin est aussi moderator et perso1 invisible dans un forum fermé", false, null, null,
					null);
			
			addMember(f2, persoAdmin, ForumMemberRole.MODERATOR);
			addMember(f2, perso1, ForumMemberRole.PARTICIPANT);
			addMember(f2, perso2, ForumMemberRole.GUEST);
			
			addPostToThread(f2, perso1, "Ping from Perso1!");
			addPostToThread(f2, persoAdmin, "Pong from Admin!");
		}
	
	}

	@Override
	public List<ForumThread> findPersonaThreads(Persona persona) {
		return getDao().findPersonaThreads(persona);
	}

	@Override
	public ForumThread createThread(Persona persona, String title, String description,
			Boolean open, List<Persona> basicParticipants, List<Persona> moderators, List<Persona> guests) {
	
		ForumThread th = new ForumThread();
		th.setActive(true);
		th.setChronicle(persona.getChronicle());
		th.setTitle(title);
		th.setDescription(description);
//		th.setParent(null);
		save(th);
		
		if (basicParticipants != null)
			for (Persona p : basicParticipants)
				addMember(th, p, ForumMemberRole.PARTICIPANT);
		
		if (moderators != null)
			for (Persona p : moderators)
				addMember(th, p, ForumMemberRole.MODERATOR);
		
		if (guests != null)
			for (Persona p : guests) 
				addMember(th, p, ForumMemberRole.GUEST);
		
		//by default, the creator is moderator
		if(!(moderators!=null&&moderators.contains(persona)))
			addMember(th, persona, ForumMemberRole.MODERATOR);

		flush();
		return th;
	}

	@Override
	public void setDeleteThread(ForumThread thread, Persona persona) {
		// check if the persona is an Admin of this thread!
		ForumMember member = findMemberFromThreadAndPersona(thread, persona);
		
		if(member!=null && ForumMemberRole.MODERATOR.equals(member.getRole())){
			thread.setActive(!thread.isActive());
			save(thread);
		}
	}

	@Override
	public void addMember(ForumThread thread, Persona persona, ForumMemberRole role) {
		ForumMember newFm = null;
		if(!thread.getMembers().containsKey(persona)){
			//new persona
			newFm = new ForumMember(persona, role, thread, null);
			//_entityManager is only used for this case 
			_entityManager.persist(newFm);
			thread.getMembers().put(persona, newFm);
		}else{
			newFm = thread.getMembers().get(persona);
			if(newFm.isDeleted()){
				newFm.setDeleted(false);
			}
			newFm.setRole(role);
		}
		save(thread);
	}

	@Override
	public ForumMember findMemberFromThreadAndPersona(ForumThread thread, Persona persona) {
		return getDao().findMemberFromForumThreadAndPersona(thread, persona);
	}

	@Override
	public Boolean removeMember(ForumThread thread, ForumMember memberToRemove, ForumMember memberRemoving) {
		//only a moderator can remove participant
		if(!ForumMemberRole.MODERATOR.equals(memberRemoving.getRole())&&memberRemoving.getForumThread()==thread)
			return false;
		memberToRemove.setDeleted(true);
		save(thread);
		flush();
		return true;
	}

	@Override
	public void addPostToThread(ForumThread thread, Persona author, String post) {
		if(thread!=null){
			List<ForumMessage> messages = thread.getMessages();
			if(messages==null)
				messages = new ArrayList<ForumMessage>();
			ForumMessage newMessage = new ForumMessage(author, post);
			messages.add(newMessage);
			thread.setMessages(messages);
			save(thread);
			
			//flag for a new message on this thread!
			flagNewMessage(thread, newMessage);
		}
		
	}

	@Override
	public void flagNewMessage(ForumThread thread, ForumMessage newMessage) {
		getDao().flagNewMessage(thread, newMessage);
	}

	@Override
	public List<ForumMessage> findNewMessages(Persona persona) {
		return getDao().findNewMessages(persona);
	}

	@Override
	public void removePostFromThread(ForumThread thread, String post, ForumMember actor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePostIntoThread(ForumThread thread, String post, ForumMember actor) {
		// TODO Auto-generated method stub
		
	}
	
	public ForumThreadDao getDao() {
		return (ForumThreadDao)super.getDao();
	}

	@Override
	public void updateThread(Persona actor, ForumThread thread, String title,
			String description, List<Persona> participants,
			List<Persona> moderators, List<Persona> guests) {
		
		//only a moderator can remove participant
		ForumMember fmActor= findMemberFromThreadAndPersona(thread, actor);
		if(ForumMemberRole.MODERATOR.equals(fmActor.getRole())){
			thread.setTitle(title);
			thread.setDescription(description);
			save(thread);			
			//remove the old personaMembers
			List<ForumMember> toRemoveMembers = new ArrayList<ForumMember>();
			
			for (ForumMember oldMember : thread.getMembers().values()){
				if(!oldMember.isDeleted()){
					Persona oldPersona = oldMember.getPersona();
						if(!participants.contains(oldPersona) && !guests.contains(oldPersona) && !moderators.contains(oldPersona))
								toRemoveMembers.add(oldMember);
				}
			}
			for (ForumMember oldMember : toRemoveMembers)
				removeMember(thread, oldMember, fmActor);
				
			//add new personaMembers
			for(Persona participant : participants){
				addMember(thread, participant, ForumMemberRole.PARTICIPANT);
			}
			
			for(Persona guest : guests){ 
				addMember(thread, guest, ForumMemberRole.GUEST);
			}
			
			for(Persona moderator : moderators){ 
				addMember(thread, moderator, ForumMemberRole.MODERATOR);
			}
		}		
	}

	@Override
	public ForumThread findActiveThreadFromPersona(long forumId, Persona persona) {
		return getDao().findActiveThread(forumId, persona);
	}

	@Override
	public List<ForumMessage> findReadableMessagesForThreadAndPersona(
			ForumThread thread, Persona persona) {
		return getDao().findReadableMessagesForThreadAndPersona(thread, persona);
	}

	@Override
	public ForumThread createNew() {
		return new ForumThread();
	}

	@Override
	public void flagThreadAsReadenByPersona(ForumThread forumThread, Persona persona) {
		getDao().flagThreadAsReadenByPersona(forumThread, persona);
	}
}
