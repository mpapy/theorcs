package org.libermundi.theorcs.chronicles.model;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.libermundi.theorcs.chronicles.ChronicleConstants;
import org.libermundi.theorcs.core.model.base.BasicEntity;
import org.libermundi.theorcs.core.model.base.Labelable;

@Entity(name="Game")
@Table(name=ChronicleConstants.TBL_GAME)
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public final class Game extends BasicEntity implements Labelable {
	public final static String PROP_NAME="name";
	
	private static final long serialVersionUID = -1960152640824769027L;

	private String _name;
	
	private GameSystem _gameSystem;

	@Basic
	@Column(name = Game.PROP_NAME, length=50, nullable=false)
	public String getName() {
		return _name;
	}
	
	@ManyToOne(cascade={CascadeType.PERSIST,CascadeType.REFRESH}, fetch=FetchType.LAZY)
	@JoinColumn(name="systemId")
	public GameSystem getGameSystem() {
		return _gameSystem;
	}

	public void setName(String name) {
		_name=name;
	}
	
	public void setGameSystem(GameSystem system) {
		_gameSystem=system;
	}

	@Override
	public String toString() {
		return "Game [Id=" + getId() + ", Name=" + _name + ", Game System=" + _gameSystem + "]";
	}

	@Override
	public String printLabel() {
		return getName();
	}	
}
