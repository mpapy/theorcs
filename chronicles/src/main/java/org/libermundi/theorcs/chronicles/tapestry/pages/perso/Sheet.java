/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.libermundi.theorcs.chronicles.tapestry.pages.perso;

import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.libermundi.theorcs.chronicles.model.sheet.SheetPage;
import org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage;

public class Sheet extends ChroniclePage {
	@Persist
	private int _currentPageIndex;

	@SetupRender
	public void setupRender() {
		if(_currentPageIndex==-1){ //Only Init when we load the page the first time
			_currentPageIndex=0;
		}
	}
	
	public SheetPage getCurrentPage(){
		// We call the persona here because the Sheet are Lazily loaded
		return getCurrentPerso().getSheet().getPage(_currentPageIndex);
	}	
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage#isSideBarHidden()
	 */
	@Override
	public boolean isSideBarHidden() {
		return Boolean.TRUE;
	}
	

}
