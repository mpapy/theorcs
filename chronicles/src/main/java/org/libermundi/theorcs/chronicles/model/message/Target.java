package org.libermundi.theorcs.chronicles.model.message;

import org.libermundi.theorcs.core.model.base.NumericIdEntity;


/**
 * Interface modeling the different targets of a message
 *
 */
public interface Target<T extends NumericIdEntity> {
	
	public T getEntity();
	public void setEntity(T entity);
}
