/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.services.impl;

import java.util.Random;

import org.libermundi.theorcs.chronicles.model.Dice;
import org.libermundi.theorcs.chronicles.services.DiceService;
import org.springframework.stereotype.Service;

/**
 *
 *
 */
@Service("DiceRoller")
public class DiceServiceImpl implements DiceService {

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.services.DiceRoller#roll(org.libermundi.theorcs.chronicles.model.game.Dice)
	 */
	@Override
	public void roll(Dice dice) {
		// Random integers that range from from 1 to n
		int n = dice.getDiceType().intValue();
		Random rand = new Random();
		//rand.nextInt(n) has a range from 0 to n, so -1/+1
		dice.setValue(rand.nextInt(n-1)+1);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.services.DiceService#throwDice(org.libermundi.theorcs.chronicles.model.Dice, java.lang.Integer, boolean)
	 */
	@Override
	public boolean throwDice(Dice dice, Integer difficulty, boolean strictlyMore) {
		roll(dice);
		int result = dice.getValue().compareTo(difficulty);
		if ((result==0 && strictlyMore) || result==-1) {
			return false;
		}
		return true;
	}
}
