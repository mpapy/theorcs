/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.model.sheet;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author Martin Papy
 *
 */
public class Skill implements Serializable {
	private static final long serialVersionUID = 4377692351248855724L;
	
	private String _uid;

	private String _name="";	
	
	private String _value="";
	
	public Skill() {
		setUid(UUID.randomUUID().toString());
	}
	
	public Skill(String name) {
		this();
		setName(name);
	}
	
	public Skill(String name, String value) {
		this(name);
		setValue(value);
	}
	
	public String getUid() {
		return _uid;
	}

	public void setUid(String uid) {
		_uid=uid;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return _name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		_name = name;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return _value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		_value = value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_uid == null) ? 0 : _uid.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Skill other = (Skill) obj;
		if (_uid == null) {
			if (other._uid != null)
				return false;
		} else if (!_uid.equals(other._uid))
			return false;
		return true;
	}

	
}
