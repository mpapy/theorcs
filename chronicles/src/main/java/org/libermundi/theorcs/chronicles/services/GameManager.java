package org.libermundi.theorcs.chronicles.services;

import org.libermundi.theorcs.chronicles.model.Game;
import org.libermundi.theorcs.core.services.Manager;

public interface GameManager extends Manager<Game, Long> {
	public static final String SERVICE_ID = "gameManager";
}
