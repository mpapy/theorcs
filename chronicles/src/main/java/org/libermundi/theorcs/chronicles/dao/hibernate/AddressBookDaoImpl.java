package org.libermundi.theorcs.chronicles.dao.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.libermundi.theorcs.chronicles.dao.AddressBookDao;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.AddressBook;
import org.libermundi.theorcs.core.dao.exception.OrcsDataAccessException;
import org.libermundi.theorcs.core.dao.hibernate.HibernateGenericDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public final class AddressBookDaoImpl extends HibernateGenericDao<AddressBook, Long> implements AddressBookDao {
	private static final Logger logger = LoggerFactory.getLogger(AddressBookDaoImpl.class);

	public AddressBookDaoImpl(){
		super();
		_mappedClazz = AddressBook.class;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.dao.AddressBookDao#loadAddressBook(org.libermundi.theorcs.chronicles.model.Persona)
	 */
	@Override
	public AddressBook loadAddressBook(Persona owner) {
		AddressBook result;
		try{
			result = (AddressBook) createSimpleCriteria()
						.add(Restrictions.eq(AddressBook.PROP_OWNER, owner))
						.uniqueResult();
		} catch (HibernateException e) {
			logger.error("Exception in loadAddressBook(" + owner +")", e);
			throw new OrcsDataAccessException("Exception in loadAddressBook : " + owner, e);
		}
		if(logger.isDebugEnabled()) {
			logger.debug("Result found : " + result);
		}
		return result;
	}

}
