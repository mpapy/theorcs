package org.libermundi.theorcs.chronicles.model;

public enum ChronicleType {
	STANDARD,
	PRIVATE
}
