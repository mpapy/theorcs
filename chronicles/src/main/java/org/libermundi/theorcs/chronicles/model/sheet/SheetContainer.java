package org.libermundi.theorcs.chronicles.model.sheet;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import com.google.common.collect.Lists;

public final class SheetContainer implements Serializable {
	
	public static enum ContainerType { SKILLS, IMAGE, TEXT, PERSOIMG }

	public static final int WIDTH=450;
	public static final int HEIGHT=300;
	
	private static final long serialVersionUID = -3879669940501856611L;
	
	private List<Skill> _skills = Lists.newArrayList();
	private String _uid;
	private String _name="";
	private int _x;
	private int _y;
	private int _z;
	private int _height;
	private int _width;
	private String _imagePath;
	private String _text;
	private ContainerType _type;

	public SheetContainer(int x, int y) {
		setUid(UUID.randomUUID().toString());
		_x = x;
		_y = y;
		_height = WIDTH;
		_width = HEIGHT;
		setType(ContainerType.SKILLS);
	}
	
	public String getUid() {
		return _uid;
	}

	public void setUid(String uid) {
		_uid=uid;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name=name;
	}	

	public void resetUid() {
		setUid(UUID.randomUUID().toString());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_uid == null) ? 0 : _uid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SheetContainer other = (SheetContainer) obj;
		if (_uid == null) {
			if (other._uid != null) {
				return false;
			}
		} else if (!_uid.equals(other._uid)) {
			return false;
		}
		return true;
	}

	// ~ --------------- Getters & Setters ------------------------

	public int getX() {
		return _x;
	}

	public void setX(int x) {
		_x = x;
	}

	public int getY() {
		return _y;
	}

	public void setY(int y) {
		_y = y;
	}

	public int getZ() {
		return _z;
	}

	public void setZ(int z) {
		_z = z;
	}

	public int getHeight() {
		return _height;
	}

	public void setHeight(int height) {
		_height = height;
	}

	public int getWidth() {
		return _width;
	}

	public void setWidth(int width) {
		_width = width;
	}

	/**
	 * @return the type
	 */
	public ContainerType getType() {
		return _type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(ContainerType type) {
		_type = type;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return _imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		_imagePath = imagePath;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return _text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		_text = text;
	}

	/**
	 * @return the skills
	 */
	public List<Skill> getSkills() {
		return _skills;
	}

	/**
	 * @param skills the skills to set
	 */
	public void setSkills(List<Skill> skills) {
		_skills = skills;
	}
	
	public Skill addSkill(){
		Skill s = new Skill();
		return addSkill(s);
	}

	public Skill addSkill(Skill skill){
		_skills.add(skill);
		return skill;
	}

	public void removeSkill(Skill skill){
		_skills.remove(skill);
	}
}
