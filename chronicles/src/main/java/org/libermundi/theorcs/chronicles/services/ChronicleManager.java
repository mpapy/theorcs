/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.services;

import java.util.List;

import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.core.services.Manager;
import org.libermundi.theorcs.security.model.User;

public interface ChronicleManager extends Manager<Chronicle, Long> {
	public static final String SERVICE_ID = "chronicleManager";
	
	/**
	 * Returns the list of the {@link Chronicle}s the user is registered to
	 * @param user the {@link User} we look for
	 * @return the {@link List} of corresponding {@link Chronicle}s
	 */
	List<Chronicle> getChroniclesByUser(User user);

	/**
	 * Returns the list of the users currently having a {@link Persona} in this {@link Chronicle}
	 * @param chronicle the {@link Chronicle} to check upon
	 * @return the {@link List} of {@link User}s registered in this {@link Chronicle}
	 */
	List<User> getUsersByChronicle(Chronicle chronicle);
	
	/**
	 * Saves/Update the Chronicle and Grant Admin right to the user.
	 * @param chronicle the {@link Chronicle} to update
	 * @param admin the new Administrator
	 */
	void saveAndGrantAdminRights(Chronicle chronicle, User admin); 
	
}
