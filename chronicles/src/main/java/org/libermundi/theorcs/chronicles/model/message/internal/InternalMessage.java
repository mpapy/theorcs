package org.libermundi.theorcs.chronicles.model.message.internal;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.libermundi.theorcs.chronicles.ChronicleConstants;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.AbstractMessage;
import org.libermundi.theorcs.chronicles.model.message.Message;
import org.libermundi.theorcs.chronicles.model.search.PersonaBridge;

/**
 * InternalMessage is a message between Persona
 * 
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
@Entity(name="InternalMessage")
@Table(name=ChronicleConstants.TBL_INTERNAL_MESSAGE)
@Indexed(index="InternalMessage")
public class InternalMessage extends AbstractMessage implements Message<Persona>{
	private static final long serialVersionUID = -2735061412672167618L;
	
	public final static String PROP_TITLE="title";

	/* -- Specific fields -- */
	private String _title;
	private Persona _author;
	private List<ToTarget> _tos;
	private List<CcTarget> _ccs;
	private List<BccTarget> _bccs;
	
	public InternalMessage() {
		super();
		_tos = new ArrayList<ToTarget>();
		_ccs = new ArrayList<CcTarget>();
		_bccs = new ArrayList<BccTarget>();
	}

	public InternalMessage(Persona author, String subject, String body, List<ToTarget> tos, List<CcTarget> ccs, List<BccTarget> bccs) {
		super();
		setAuthor(author);
		setTitle(subject);
		setContent(body);
		setToTargets(tos);
		setCcTargets(ccs);
		setBccTargets(bccs);
	}
	
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(nullable=false, name="personaId")
	@Field(store=Store.YES,index=Index.YES)
	@FieldBridge(impl=PersonaBridge.class)
	public Persona getAuthor() {
		return _author;
	}

	public void setAuthor(Persona author) {
		_author = author;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.model.messenging.Message#getTitle()
	 */
	@Basic
	@Column(name = PROP_TITLE, length=255, nullable=false)
	@Field(index=Index.YES, store=Store.YES)
	public String getTitle() {
		return _title;
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.model.messenging.Message#setTitle(java.lang.String)
	 */
	public void setTitle(String title) {
		this._title = title;
	}
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="internalMsgId")
	@Where(clause="role='TO'") //have to add the where clause to get the right class target 
	public List<ToTarget> getToTargets() {
		return _tos;
	}
	
	public void setToTargets(List<ToTarget> recipient) {
		this._tos = recipient;
	}
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="internalMsgId")
	@Where(clause="role='CC'")//have to add the where clause to get the right class target
	public List<CcTarget> getCcTargets() {
		return _ccs;
	}
	
	public void setCcTargets(List<CcTarget> recipient) {
		this._ccs = recipient;
	}
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="internalMsgId")
	@Where(clause="role='BCC'")//have to add the where clause to get the right class target
	public List<BccTarget> getBccTargets() {
		return _bccs;
	}
	
	public void setBccTargets(List<BccTarget> recipient) {
		this._bccs = recipient;
	}
}
