package org.libermundi.theorcs.chronicles.model;

public enum ChronicleGenre {
	HISTORICAL,
	MED_FAN,
	CONTEMPORARY,
	CYBERPUNK,
	POST_APO,
	SPACE_OP,
	OTHER
}