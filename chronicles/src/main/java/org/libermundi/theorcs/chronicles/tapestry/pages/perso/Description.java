package org.libermundi.theorcs.chronicles.tapestry.pages.perso;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage;

public class Description extends ChroniclePage {
	@SessionState
	private ChronicleSettings _chronicleSettings;
	
	@Inject
	private Block _descriptionForm;
	
	@Inject
	private Request _request;
	
	public Persona getPersona(){
		return _chronicleSettings.getPersona();
	}
	
	@OnEvent(value="switchMode")
	public Block switchMode(){
		if(_request.isXHR()) {
			return _descriptionForm;
		}
		return null;
	}
		
}
