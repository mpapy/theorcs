package org.libermundi.theorcs.chronicles.model.message.forum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SortNatural;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.libermundi.theorcs.chronicles.ChronicleConstants;
import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.core.model.Searchable;
import org.libermundi.theorcs.core.model.base.StatefulEntity;


/**
 * Models the thread of a forum.
 * A thread can be made of a chain of threads: a chain is the same forumthread, 
 * but whenever a member come in or out, a new thread is created.
 *
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
@Entity(name="ForumThread")
@Table(name=ChronicleConstants.TBL_FORUM_THREAD)
@Indexed(index="ForumThread")
public final class ForumThread extends StatefulEntity implements Searchable{

	private static final long serialVersionUID = 3821078009734794582L;
	
	public final static String PROP_CHRONICLE="chronicle";
	public final static String PROP_TITLE="title";
	public final static String PROP_DESCRIPTION="description";
	public final static String PROP_MEMBERS="members";
	public final static String PROP_MESSAGES="messages";
	
	private static final String[] SEARCHABLE_FIELDS = new String[]{PROP_CHRONICLE, PROP_TITLE, PROP_DESCRIPTION, StatefulEntity.PROP_CREATED_DATE, PROP_MEMBERS};
	private static final String[] SUGGESTION_SEARCHABLE_FIELDS = new String[]{PROP_TITLE, PROP_DESCRIPTION};
	
	/**
	 * A forum belongs to a chronicle
	 */
	private Chronicle _chronicle;
		
	private String _title;
	
	private String _description;
	
	private Map<Persona, ForumMember> _members;
	
	private List<ForumMessage> _messages;
			
	public ForumThread() {
		_messages = new ArrayList<ForumMessage>(0);
		_members = new HashMap<Persona, ForumMember>(0);
	}

	@ManyToOne(cascade = CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="chronicleId")
	public Chronicle getChronicle() {
		return _chronicle;
	}

	public void setChronicle(Chronicle _chronicle) {
		this._chronicle = _chronicle;
	}

	@Basic
	@Column(name = ForumThread.PROP_TITLE, length=50, nullable=false)
	@Field(store=Store.YES,index=Index.YES)
	public String getTitle() {
		return _title;
	}

	public void setTitle(String _title) {
		this._title = _title;
	}

	@Lob
	@Column(name = ForumThread.PROP_DESCRIPTION, nullable=true)
	@Field(store=Store.YES,index=Index.YES)
	public String getDescription() {
		return _description;
	}

	public void setDescription(String _description) {
		this._description = _description;
	}
	
	@OneToMany(mappedBy="forumThread", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@MapKeyJoinColumn(name="personaId")
	public Map<Persona, ForumMember> getMembers() {
		return _members;
	}

	public void setMembers(Map<Persona, ForumMember> members) {
		this._members = members;
	}
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinColumn(name="forumThreadId")
	@SortNatural
	public List<ForumMessage> getMessages() {
		return _messages;
	}

	public void setMessages(List<ForumMessage> _messages) {
		this._messages = _messages;
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.Searchable#loadSearchFields()
	 */
	@Override
	public String[] loadSearchFields() {
		return SEARCHABLE_FIELDS; 
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.Searchable#loadSuggestionFields()
	 */
	@Override
	public String[] loadSuggestionFields() {
		return SUGGESTION_SEARCHABLE_FIELDS;
	}
}
