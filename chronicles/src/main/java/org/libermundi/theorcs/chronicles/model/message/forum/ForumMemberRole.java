package org.libermundi.theorcs.chronicles.model.message.forum;

/**
 * Enumeration of target roles
 *
 */
public enum ForumMemberRole {PARTICIPANT, GUEST, MODERATOR}
	