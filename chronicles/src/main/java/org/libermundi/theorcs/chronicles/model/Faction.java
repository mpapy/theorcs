/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.model;

import java.util.SortedSet;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.SortNatural;
import org.libermundi.theorcs.chronicles.ChronicleConstants;
import org.libermundi.theorcs.core.model.base.BasicEntity;
import org.libermundi.theorcs.core.model.base.Labelable;

import com.google.common.collect.Sets;

/**
 * @author Martin Papy
 *
 */

@Entity(name="Faction")
@Table(name=ChronicleConstants.TBL_FACTION)
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public final class Faction extends BasicEntity implements Labelable, Comparable<Faction> {
	private static final long serialVersionUID = -4695664093143633172L;

	public final static String PROP_NAME="name";
	
	public final static String PROP_CHRONICLE="chronicle";
	
	private String _name;
	
	private Chronicle _chronicle;
	
	private SortedSet<Persona> _members = Sets.newTreeSet();

	/**
	 * Default Constructor
	 */
	public Faction() {
		// Default Constructor
	}
	
	/**
	 * @param name the name of the {@link Faction}
	 */
	public Faction(String name) {
		_name = name;
	}

	@Basic
	@Column(name = Faction.PROP_NAME, length=50, nullable=false)
	public String getName() {
		return _name;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="chronicleId",nullable=false,updatable=false)
	public Chronicle getChronicle() {
		return _chronicle;
	}

	@ManyToMany(mappedBy=Persona.PROP_FACTIONS,fetch=FetchType.LAZY)
	@SortNatural
	public SortedSet<Persona> getMembers(){
		return _members;
	}
	
	public void setName(String name) {
		_name=name;
	}
	
	public void setChronicle(Chronicle chronicle) {
		_chronicle=chronicle;
	}
	
	public void setMembers(SortedSet<Persona> members) {
		_members=members;
	}
	
	@Override
	public String toString() {
		return "Faction [Id=" + getId() + ", Name=" + _name + ", Chronique=" + _chronicle + ", Nbre of Members=" + _members.size() + "]";
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.base.Labelable#printLabel()
	 */
	@Override
	public String printLabel() {
		return getName();
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Faction otherFaction) {
		if(otherFaction == null || otherFaction.getName() == null){
			return -1;
		}
		
		if(getName() == null) {
			return 1;
		}
		return getName().compareTo(otherFaction.getName());
	}
}
