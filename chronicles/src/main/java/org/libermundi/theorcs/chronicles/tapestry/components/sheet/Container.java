/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.tapestry.components.sheet;

import java.util.List;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.annotations.SupportsInformalParameters;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.chronicles.dto.SheetEditorSettings;
import org.libermundi.theorcs.chronicles.model.sheet.SheetContainer;
import org.libermundi.theorcs.chronicles.model.sheet.SheetContainer.ContainerType;
import org.libermundi.theorcs.chronicles.model.sheet.Skill;
import org.libermundi.theorcs.core.tapestry.services.assets.AssetServices;

/**
 * @author Martin Papy
 *
 */
@SupportsInformalParameters
public class Container {
	@SessionState
	private SheetEditorSettings _editorSettings;
	
	@Parameter(required=true,defaultPrefix=BindingConstants.PROP,allowNull=false)
	@Property(write=false)
	private SheetContainer _container;
		
	@Parameter(required=false,defaultPrefix=BindingConstants.LITERAL,allowNull=false,value="false")
	@Property(write=false)
	private boolean _edit;
	
	@Inject
	private Block _skills;
	
	@Inject
	private Block _image;
	
	@Inject
	private Block _text;
	
	@Inject
	private Block _persoImage;
	
	@Inject
	@Path("${layout.images}/avatar/avatar250.png")
	private Asset _defaultAvatar;
	
    @Inject
    private ComponentResources _resources;
    
	@Inject
	private AssetServices _assetServices;
	
	@Property
	private Skill _skill;
	
	@BeginRender
	public void beginRender(MarkupWriter writer) {
		writer.element("div","id",_container.getUid(),
				"class",getCss(),
				"style",getPosition());
		_resources.renderInformalParameters(writer);
	}
	
	@AfterRender
	public void afterRender(MarkupWriter writer){
		writer.end(); //div
	}
	
	public Block getContainerRenderer(){
		switch (_container.getType()) {
			case SKILLS:
				return _skills;
			case IMAGE:
				return _image;
			case TEXT:
				return _text;
			case PERSOIMG:
				return _persoImage;
			default:
				throw new UnsupportedOperationException();
		}
	}
	
	public Asset getContainerImage(){
		return _assetServices.getAsset(_container.getImagePath(),null);
	}

	public String getContainerText(){
		return _container.getText();
	}
	
	public Asset getPersonaPicture(){
		String pict = _editorSettings.getPerso().getPicture();
		
		return _assetServices.getAsset(pict, _defaultAvatar);
	}
	
	public String getPersonaName(){
		return _editorSettings.getPerso().getName();
	}
	
	public List<Skill> getSkillList(){
		return _container.getSkills();
	}
	
	public String getCss() {
		String css="sheetcontainer";
		
		css+= " sheetcontainer-" + _container.getType().toString().toLowerCase();
		
		if(_edit){
			css += " sheetcontainer-edit";
		}
		
		return css;
	}
	
	public String getPosition(){
		int cTop, cLeft, cWidth, cHeight,zIndex;
		
		cTop = _container.getY();
		cLeft = _container.getX();
		cWidth = _container.getWidth();
		cHeight = _container.getHeight();
		zIndex = _container.getZ();
		
		if(!_edit && _container.getType() == ContainerType.IMAGE) {
			cWidth = cWidth+2;
			cHeight = cHeight+1;
		}
		
		return "position:absolute;"
			+"top:"+cTop+"px;"
			+"left:"+cLeft+"px;"
			+"width:"+cWidth+"px;"
			+"height:"+cHeight+"px;"
			+"z-index:"+zIndex;
	}

}
