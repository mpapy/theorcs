package org.libermundi.theorcs.chronicles.tapestry.pages.forum;

import java.util.List;

import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.PropertyConduitSource;
import org.apache.tapestry5.services.Request;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumThread;
import org.libermundi.theorcs.chronicles.services.message.ForumThreadManager;
import org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage;

/**
 *
 * @author Stephane Guichard 
 * @author Martin Papy
 */
public class ListForum extends ChroniclePage {

	@Inject
	private ForumThreadManager _forumManager;

	@Property
	private List<ForumThread> _forums;

	@Property
	private ForumThread _forum;

	@Property
	@Persist
	private BeanModel<ForumThread> _forumThreadModel;
	
	@Inject
	private BeanModelSource _beanModelSource;
	
	@Inject
	private Messages _messages;

	@Inject
	private PropertyConduitSource _pcSource;

	@Inject
	private Request _request;
		
	
	/*------------- Rendering Phase -------------*/
	/**
	 * Load the contacts of the persona
	 */
	@SetupRender
	public void setupRender(){
		//beanDisplayModel
		_forumThreadModel = _beanModelSource.createDisplayModel(ForumThread.class, _messages);
		_forumThreadModel.exclude("id", "deleted", "active", "createdDate", "modifiedDate", "chronicle", "messages");
		_forumThreadModel.reorder("title", "description");	
	}
	
	/**
	 * when the page is loaded the first time, 
	 * it loads the scenes of the persona 
	 */
	public void onActivate(){
		// load the messages of the persona
		if(_forums==null) {
			_forums = _forumManager.findPersonaThreads(getCurrentPerso());
		}
	}
}
