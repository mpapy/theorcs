/**
 * 
 */
package org.libermundi.theorcs.chronicles.tapestry.components.message;

import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.PropertyConduitSource;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.chronicles.model.message.internal.InternalMessage;
import org.libermundi.theorcs.chronicles.model.message.internal.InternalMessageTarget;
import org.libermundi.theorcs.chronicles.services.AddressBookManager;
import org.libermundi.theorcs.chronicles.services.message.InternalMessageManager;

/**
 * Displays the details of a message
 * @author Stephane Guichard 
 * @author Martin Papy
 *
 */
@Import(library = "${chronicles.scripts}/message.js")
public class ReadMessage{
	
	/*------------- Services -------------*/
	@SessionState
	private ChronicleSettings _chronicleSettings;
    
	@Inject
	private InternalMessageManager _messageManager;

	@Inject
	private AddressBookManager _addressBookManager;
	
	@Environmental
	private JavaScriptSupport _jsSupport;
	
	@Inject
	private BeanModelSource _beanModelSource;
	
	@Inject
	private Messages _messages;

	@Inject
	private PropertyConduitSource _pcSource;

	@Inject
	private Request _request;
	
    /*------------- Fields -------------*/
	
	@Parameter(allowNull=true,required=true)
	@Property
	private InternalMessage _internalMessage;

	/**
	 * BeanModel of InternalMessage to display the sender.name and targets
	 */
	@Property
	private BeanModel<InternalMessage> _internalEmailModel;
	
	@Property
	private String _tos;
	
	@Property
	private String _ccs;
	
	@Property
	private String _bccs;
		
	@Property
	@Persist
	private boolean _showCcFields;
	
	/*------------- Rendering Phase -------------*/
	/**
	 * Load the contacts of the persona
	 */
	@SetupRender
	public void setupRender(){
		
		//Hide the cc/bcc fields
		_jsSupport.require("MessageUtils").invoke("toggleCC").with("ccFields");
		
		if(_internalMessage!=null){
			//load the different targets
			if(_internalMessage.getToTargets()!=null) {
				StringBuffer toSb = new StringBuffer();
				for(InternalMessageTarget tgt : _internalMessage.getToTargets()) {
					toSb.append(tgt.getEntity().getName());
					toSb.append(";");
				}
				_tos = toSb.toString();
			}else {
				_tos = "";
			}

			if(_internalMessage.getCcTargets()!=null) {
				StringBuffer toSb = new StringBuffer();
				for(InternalMessageTarget tgt : _internalMessage.getCcTargets()) {
					toSb.append(tgt.getEntity().getName());
					toSb.append(";");
				}
				_ccs = toSb.toString();
			}else {
				_ccs = "";
			}			

			//beanDisplayModel
			// have to add the property sender to display it as it is a complex class
			_internalEmailModel = _beanModelSource.createDisplayModel(
					InternalMessage.class, _messages);
			_internalEmailModel.exclude("id", "content", "modifiedDate");
			_internalEmailModel.add("author", _pcSource.create(InternalMessage.class, "author.name"));
			_internalEmailModel.add("to", _pcSource.create(InternalMessage.class, "'"+_tos+"'"));
			_internalEmailModel.add("cc", _pcSource.create(InternalMessage.class, "'"+_ccs+"'"));
			_internalEmailModel.reorder("author","createddate", "to", "cc");
			
			//hide the bcc if the current persona is not the author
			if(_internalMessage.getAuthor().getId() == _chronicleSettings.getPersona().getId()){
				if(_internalMessage.getBccTargets()!=null) {
					StringBuffer toSb = new StringBuffer();
					for(InternalMessageTarget tgt : _internalMessage.getBccTargets()) {
						toSb.append(tgt.getEntity().getName());
						toSb.append(";");
					}
					_bccs = toSb.toString();
				}else {
					_bccs = "";
				}
				_internalEmailModel.add("bcc", _pcSource.create(InternalMessage.class, "'"+_bccs+"'"));
				_internalEmailModel.reorder("author","createddate", "to", "cc", "bcc");
			}else{
				_internalEmailModel.reorder("author","createddate", "to", "cc");
			}
		}
	}
	
    @OnEvent("renderMessageDisplayJs")
    public void renderLoginJs(){
		_jsSupport.require("zone.modal").invoke("init").with("MessageDisplay");
    }

    @OnEvent("unseenMessage")
    public void unseenMessage(InternalMessage msg){
    	_messageManager.updateMessageOnSeen(msg, _chronicleSettings.getPersona(), false);
    }

    @OnEvent("deleteMessage")
    public void deleteMessage(InternalMessage msg){
    	_messageManager.updateMessageOnDeleted(msg, _chronicleSettings.getPersona(), true);
    }
}