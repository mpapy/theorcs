package org.libermundi.theorcs.chronicles.json;

import org.libermundi.theorcs.chronicles.json.annotation.NoJson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class SheetExlusionStrategy implements ExclusionStrategy {

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		return f.getAnnotation(NoJson.class) != null;
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}

}
