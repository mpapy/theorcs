package org.libermundi.theorcs.chronicles.model.sheet;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.libermundi.theorcs.chronicles.ChronicleConstants;
import org.libermundi.theorcs.chronicles.model.GameSystem;
import org.libermundi.theorcs.security.model.UserStatefulEntity;

@Entity(name="SheetTemplate")
@Table(name=ChronicleConstants.TBL_SHEETTEMPLATE)
public class SheetTemplate extends UserStatefulEntity {
	public final static String PROP_NAME="name";

	private static final long serialVersionUID = -2878189503452000408L;
	
	private String _name;
	
	private GameSystem _gameSystem;

	@Basic
	@Column(name = SheetTemplate.PROP_NAME, length=50, nullable=false)
	public String getName() {
		return _name;
	}

	@ManyToOne(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, targetEntity=GameSystem.class)
	@JoinColumn(name="systemId")
	public GameSystem getGameSystem() {
		return _gameSystem;
	}

	public void setName(String name) {
		_name=name;
	}

	public void setGameSystem(GameSystem system) {
		_gameSystem=system;
	}

}
