package org.libermundi.theorcs.chronicles.model.message.forum;

import java.util.GregorianCalendar;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.search.annotations.Indexed;
import org.libermundi.theorcs.chronicles.ChronicleConstants;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.AbstractMessage;
import org.libermundi.theorcs.chronicles.model.message.Message;

/**
 * A ForumMessage is posted by a Persona
 * 
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
@Entity(name="ForumMessage")
@Table(name=ChronicleConstants.TBL_FORUM_MESSAGE)
@Indexed(index="ForumMessage")
public class ForumMessage extends AbstractMessage implements Message<Persona>{
	private static final long serialVersionUID = -2735061412672167618L;

	private Persona _author ;
	private ForumThread _thread;
	
	public ForumMessage() {
		super();
	}
	
	public ForumMessage(Persona author, String body) {
		super();
		this._author = author;
		setContent(body);
		setCreatedDate(GregorianCalendar.getInstance().getTime());
	}

	@ManyToOne(cascade = CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="personaId")
	@Override
	public Persona getAuthor() {
		return _author ;
	}

	@Override
	public void setAuthor(Persona author) {
		_author = author;	
	}
	
	@ManyToOne(cascade = CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="forumThreadId")
	public ForumThread getForumThread() {
		return _thread;
	}

	public void setForumThread(ForumThread _thread) {
		this._thread = _thread;
	}
}
