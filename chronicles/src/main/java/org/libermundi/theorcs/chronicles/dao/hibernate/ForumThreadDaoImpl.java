/**
 * 
 */
package org.libermundi.theorcs.chronicles.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.libermundi.theorcs.chronicles.dao.ForumThreadDao;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMember;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMessage;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumThread;
import org.libermundi.theorcs.core.dao.exception.OrcsDataAccessException;
import org.libermundi.theorcs.core.dao.hibernate.HibernateGenericDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
@Repository
public class ForumThreadDaoImpl extends HibernateGenericDao<ForumThread, Long> implements ForumThreadDao {

	private static final Logger logger = LoggerFactory.getLogger(ForumThreadDaoImpl.class); 

	public ForumThreadDaoImpl(){
		super();
		_mappedClazz = ForumThread.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ForumThread> findPersonaThreads(Persona persona) {
		List<ForumThread> result;
		try{
			result = createQuery(
					" SELECT thread " +
					" FROM ForumThread as thread " +
					" JOIN thread.members as members " +
					" WHERE members.persona = :perso " + 
					" AND members.deleted = false ")
					.setParameter("perso", persona)
					.list();
			
		} catch (HibernateException e) {
			logger.error("Exception in findForumThreadByPersona(" + persona +")", e);
			throw new OrcsDataAccessException("Exception in findForumThreadByPersona : " + persona, e);
		}
		return result;
	}

	@Override
	public ForumMember findMemberFromForumThreadAndPersona(ForumThread fmThread,
			Persona persona) {
		ForumMember result;
		try{ 
			result = (ForumMember) createQuery("SELECT members" +
					" FROM ForumThread as thread " +
					" JOIN thread.members as members " +
					" WHERE members.persona = :perso " +
					" and thread = :fmThread" +
					" and members.deleted = false")
					.setParameter("perso", persona)
					.setParameter("fmThread", fmThread)
					.uniqueResult();
		} catch (HibernateException e) {
			logger.error("Exception in findForumMemberFromForumThreadAndPersona(" + fmThread + ";" + persona +")", e);
			throw new OrcsDataAccessException("Exception in findForumMemberFromForumThreadAndPersona(" + fmThread + ";" + persona +")", e);
		}
		return result;
	}

	@Override
	public ForumThread findActiveThread(long forumId, Persona persona) {
		ForumThread result;
		try{
			result = (ForumThread) createQuery(
					" SELECT thread " +
					" FROM ForumThread as thread " +
					" JOIN thread.members as members " +
					" WHERE members.persona = :perso " + 
					" AND members.deleted = false " +
					" AND thread.id = :forumId ")
					.setParameter("perso", persona)
					.setParameter("forumId", forumId)
					.uniqueResult();
			
		} catch (HibernateException e) {
			logger.error("Exception in findForumThreadByPersona(" + persona +")", e);
			throw new OrcsDataAccessException("Exception in findForumThreadByPersona : " + persona, e);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ForumMessage> findReadableMessagesForThreadAndPersona(
			ForumThread thread, Persona persona) {
		List<ForumMessage> result;
		try{
			result = createQuery(
					" SELECT message " +
					" FROM ForumMessage as message " +
					" JOIN message.forumThread as thread " +
					" JOIN thread.members as members " +
					" JOIN members.persona as perso " +
					" WHERE perso = :persona " + 
					" AND members.deleted = false " +
					" AND thread = :fThread" 
					).setParameter("persona", persona)
					.setParameter("fThread", thread)
					.list();
			
		} catch (HibernateException e) {
			logger.error("Exception in findForumThreadByPersona(" + persona +")", e);
			throw new OrcsDataAccessException("Exception in findForumThreadByPersona : " + persona, e);
		}
		return result;
	}

	@Override
	public int flagThreadAsReadenByPersona(ForumThread forumThread,
			Persona persona) {
		int result = 0;
		try{
			result = createQuery("UPDATE ForumMember " +
					" SET firstUnseenMessage = :idMsg " +					
					" WHERE persona = :perso " +
					" and forumThread = :forumThread ")
					.setParameter("perso", persona)
					.setParameter("forumThread", forumThread)
					.setParameter("idMsg", null)
					.executeUpdate();
			flush();
		} catch (HibernateException e) {
			logger.error("Exception in setLastMessageHasBeenReadByPersona(" + forumThread+ "," + persona +")", e);
			throw new OrcsDataAccessException("Exception in setLastMessageHasBeenReadByPersona(" + forumThread+ "," + persona +")", e);
		}
		return result;
	}

	@Override
	public int flagNewMessage(ForumThread thread, ForumMessage newMessage) {
		int result = 0;
		try{
			result = createQuery("UPDATE ForumMember " +
					" SET firstUnseenMessage = :newMessage " +					
					" WHERE firstUnseenMessage = null " +
					" and forumThread = :forumThread ")
					.setParameter("newMessage", newMessage)
					.setParameter("forumThread", thread)
					.executeUpdate();
			flush();
		} catch (HibernateException e) {
			logger.error("Exception in flagNewMessage(" + thread+ "," + newMessage +")", e);
			throw new OrcsDataAccessException("Exception in flagNewMessage(" + thread+ "," + newMessage +")", e);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ForumMessage> findNewMessages(Persona persona) {
		List<ForumMessage> result;
		try{
			result = createQuery(
					" FROM ForumMessage as message " +
					" WHERE message in (" + 
						" SELECT firstUnseenMessage " +
						" FROM ForumMember " +
						" WHERE persona = :persona " + 
						" AND deleted = false " +
						" AND firstUnseenMessage is not null "
						+ ")"
					).setParameter("persona", persona)
					.list();
			
		} catch (HibernateException e) {
			logger.error("Exception in findNewMessages(" + persona +")", e);
			throw new OrcsDataAccessException("Exception in findNewMessages : " + persona, e);
		}
		return result;
	}
}
