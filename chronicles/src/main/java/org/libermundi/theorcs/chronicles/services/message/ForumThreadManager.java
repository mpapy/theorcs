package org.libermundi.theorcs.chronicles.services.message;

import java.util.List;

import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMember;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMemberRole;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMessage;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumThread;
import org.libermundi.theorcs.core.services.Manager;

/**
 * Manager for ForumThread features
 * 
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
public interface ForumThreadManager extends Manager<ForumThread, Long> {
	
	public static String SERVICE_ID = "ForumThreadManager";
	
	/**
	 * Get all ForumThread of a persona
	 * 
	 * @param persona
	 * @return List<ForumThread>
	 */
	public List<ForumThread> findPersonaThreads(Persona persona);
	
	/**
	 * Create a forum
	 * 
	 * @param persona
	 * @param name
	 * @param description
	 * @param open
	 * @param basicParticipants
	 * @param moderators
	 * @param invisibles
	 * @return 
	 */
	public ForumThread createThread(Persona persona, String name, String description, Boolean open, List<Persona> basicParticipants, List<Persona> moderators, List<Persona> invisibles);
	
	/**
	 * The admin closes the thread.
	 * No deleting in database.
	 * 
	 * @param thread to close 
	 * @param persona
	 */
	public void setDeleteThread(ForumThread thread, Persona persona);

	/**
	 * Add a member to a thread
	 * 
	 * @param thread
	 * @param persona
	 * @param role role of the member
	 */
	public void addMember(ForumThread thread, Persona persona, ForumMemberRole role);
	
	/**
	 * A persona unregisters from a thread  
	 * @param thread
	 * @param memberToRemove
	 * @param actor which updates 
	 * @return
	 */
	public Boolean removeMember(ForumThread thread, ForumMember memberToRemove, ForumMember actor);
	
	/**
	 * Get the ForumMember profile from a Persona
	 *    
	 * @param thread
	 * @param persona
	 * @return
	 */
	public ForumMember findMemberFromThreadAndPersona(ForumThread thread, Persona persona);
	
	/**
	 * A member adds a post to a thread
	 * @param thId
	 * @param author which updates 
	 * @param post
	 */
	public void addPostToThread(ForumThread thread, Persona author, String post);
	
	/**
	 * A member removes its post from a thread, or the moderator can remove any post
	 * @param thread
	 * @param post
	 * @param actor
	 */
	public void removePostFromThread(ForumThread thread, String post, ForumMember actor);
	
	/**
	 * A member updates its post from a thread, or the moderator can update any post
	 * @param thread
	 * @param post
	 * @param actor 
	 */
	public void updatePostIntoThread(ForumThread thread, String post, ForumMember actor);

	/**
	 * Update a thread
	 * @param actor which updates 
	 * @param _forumId 
	 * @param title
	 * @param description
	 * @param participants
	 * @param moderators
	 * @param guests
	 */
	public void updateThread(Persona actor, ForumThread forum, String title,
			String description, List<Persona> participants,
			List<Persona> moderators, List<Persona> guests);

	/**
	 * find a forum active for a persona from its id
	 * @param forumId
	 * @param persona
	 * @return
	 */
	public ForumThread findActiveThreadFromPersona(long forumId, Persona persona);

	/**
	 * Find all messages for a persona of a forumThread in function of the first and last id
	 * @param thread
	 * @param persona
	 * @return
	 */
	public List<ForumMessage> findReadableMessagesForThreadAndPersona(
			ForumThread thread, Persona persona);

	/**
	 * set Last Message Has Been Read by persona in this thread
	 * @param forumThreads
	 * @param persona
	 */
	public void flagThreadAsReadenByPersona(ForumThread forumThread,
			Persona persona);

	public void flagNewMessage(ForumThread thread, ForumMessage newMessage);

	List<ForumMessage> findNewMessages(Persona author);
}
