package org.libermundi.theorcs.chronicles.tapestry.components.chat;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

@Import(library={
		"${chronicles.scripts}/jquery.slimscroll.min.js",
		"${chronicles.scripts}/chat.js",
}, stylesheet={
		"${chronicles.styles}/chat.css"
}, module = {
		"tjq/vendor/ui/jquery.ui.mouse",
		"tjq/vendor/ui/jquery.ui.draggable"
})
public class ChatBox {
	@Inject
	@Path("${layout.images}/avatar/avatar27.png")
	@Property(write=false)
	private Asset _defaultAvatar;
}
