package org.libermundi.theorcs.chronicles.model;

public enum ChroniclePace {
	SLOW,
	AVERAGE,
	FAST
}
