package org.libermundi.theorcs.chronicles.services;

import org.libermundi.theorcs.chronicles.model.Faction;
import org.libermundi.theorcs.core.services.Manager;

public interface FactionManager extends Manager<Faction, Long> {

	String SERVICE_ID = "factionManager";

}
