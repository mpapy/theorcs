package org.libermundi.theorcs.chronicles.dao;

import org.libermundi.theorcs.chronicles.model.SavedInformations;
import org.libermundi.theorcs.core.dao.GenericDao;
import org.libermundi.theorcs.security.model.User;

public interface SavedInformationsDao extends GenericDao<SavedInformations, Long> {

	public SavedInformations getByUser(User user);

	public void clear(User user);

}
