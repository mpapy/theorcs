/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.dao;

import java.util.List;

import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.internal.InternalMessage;
import org.libermundi.theorcs.core.dao.GenericDao;

/**
 * Dao for InternalMessage
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
public interface InternalMessageDao extends GenericDao<InternalMessage, Long> {
	
	/**
	 * get all messages sent to the persona
	 * @param persona who have received the messages
	 * @return all internalMessage received by the persona
	 */
	public List<InternalMessage> findMessagesReceivedByPersona(Persona persona);

	/**
	 * get all unseen messages sent to the persona
	 * @param persona who have received the messages
	 * @return all internalMessage received by the persona
	 */
	public List<InternalMessage> findUnseenMessagesReceivedByPersona(Persona persona);
	
	/**
	 * get all messages sent by the persona
	 * @param persona who have sent the messages
	 * @return all internalMessage sent by the persona
	 */
	List<InternalMessage> findSentMessagesByPersona(Persona persona);

	/**
	 * get all messages deleted by the persona
	 * @param persona who have deleted its messages
	 * @return all internalMessage deleted by the persona
	 */
	List<InternalMessage> findDeletedMessagesByPersona(Persona persona);
	
	public int updateMessageOnSeen(InternalMessage msg, Persona persona, boolean b);

	public int updateMessageOnDelete(InternalMessage msg, Persona persona, boolean isDeleted);

}
