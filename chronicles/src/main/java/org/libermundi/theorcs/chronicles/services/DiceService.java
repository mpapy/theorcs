/**
 * 
 */
package org.libermundi.theorcs.chronicles.services;

import org.libermundi.theorcs.chronicles.model.Dice;

/**
 * Service to roll the dice
 *
 */
public interface DiceService {

	/**
	 * Roll and set the value of the Dice
	 * @param dice
	 */
	public void roll(Dice dice);
	
	/**
	 * Throw a dice against a difficulty
	 * @param dice 
	 * @param difficulty
	 * @param strictMore true if the dice has to be strictly more than the difficulty
	 * @return true if success
	 */
	public boolean throwDice(Dice dice, Integer difficulty, boolean strictMore);
	
}
