/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.model.acls;

import org.springframework.security.acls.domain.AbstractPermission;
import org.springframework.security.acls.model.Permission;

/**
 * @author Martin Papy
 *
 */
public class ChroniclePermission extends AbstractPermission {
	private static final long serialVersionUID = 5217166774711319845L;
	
	public static final Permission MJ = new ChroniclePermission(1 << 5, 'M'); // 32;
	public static final Permission PERSONA = new ChroniclePermission(1 << 6, 'P'); // 64;
	public static final Permission FACTION = new ChroniclePermission(1 << 7, 'F'); // 128;

	protected ChroniclePermission(int mask, char code) {
		super(mask, code);
	}

	protected ChroniclePermission(int mask) {
		super(mask);
	}
}
