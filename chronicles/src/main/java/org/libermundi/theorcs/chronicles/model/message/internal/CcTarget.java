/**
 * 
 */
package org.libermundi.theorcs.chronicles.model.message.internal;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.libermundi.theorcs.chronicles.model.Persona;

/**
 * 
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
@Entity
@DiscriminatorValue("CC")
public class CcTarget extends InternalMessageTarget {

	private static final long serialVersionUID = 8721488972243128258L;

	public CcTarget() {
		super();
	}
	
	public CcTarget(Persona entity) {
		setEntity(entity);
	}

}
