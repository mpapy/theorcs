/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.model.sheet.old;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public final class Skill implements Serializable {
	private static final long serialVersionUID = 3547760883651804295L;
	
	private String _uid;
	
	private String _name="";	
	
	private String _value="";
	
	private SkillType _skillType;
	
	private boolean _trunk;
	
	private List<Skill> _childSkills=new ArrayList<>(0);

	// ~ ------------ Public Methods -----------------------
	
	/**
	 * Creates a new Skills based on the value off the parent
	 * The new Skill cannot be trunk. 
	 */
	public Skill addChildSkill(String name) {
		if(!isTrunk()) {
			throw new RuntimeException("Cannot add a child skill to a Skill with trunk propertie set to false");
		}
		Skill cs = new Skill(name,Boolean.FALSE);
		cs.setValue(this.getValue());
		_childSkills.add(cs);
		return cs;
	}
	
	// ~ ------------ GETTERS & SETTERS --------------------
	public Skill(String name, boolean trunk) {
		_trunk = trunk;
		_name=name;
		setUid(UUID.randomUUID().toString());
	}

	public String getUid() {
		return _uid;
	}

	public void setUid(String uid) {
		_uid=uid;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name=name;
	}

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		_value = value;
	}

	public SkillType getSkillType() {
		return _skillType;
	}

	public void setSkillType(SkillType skillType) {
		_skillType = skillType;
	}

	public boolean isTrunk() {
		return _trunk;
	}	
	
}
