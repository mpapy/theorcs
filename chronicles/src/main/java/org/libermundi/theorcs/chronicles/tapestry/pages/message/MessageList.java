package org.libermundi.theorcs.chronicles.tapestry.pages.message;

import java.util.List;

import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.PropertyConduitSource;
import org.apache.tapestry5.services.Request;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.chronicles.model.message.internal.InternalMessage;
import org.libermundi.theorcs.chronicles.services.message.InternalMessageManager;
import org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage;

/**
 * Main page for the message management. 
 * Displays the list of messages and a message when selected by the user
 * 
 * @author Stephane Guichard 
 * @author Martin Papy
 * 
 */
public class MessageList extends ChroniclePage {

	@Property
	@Persist
	private BeanModel<InternalMessage> _messageBoxModel;

	@Inject
	private BeanModelSource _beanModelSource;

	@Inject
	private Messages _messages;

	@Inject
	private PropertyConduitSource _pcSource;

	@Inject
	private Request _request;
	
	/**
	 * get the settings of the chronicle loaded for the session by the user
	 */
	@SessionState
	private ChronicleSettings _chronicleSettings;

	@Inject
	private InternalMessageManager _messageManager;

	@Property
	@Persist(PersistenceConstants.FLASH)
	private List<InternalMessage> _internalMessages;

	@Property
	private InternalMessage _internalMessage;

	/**
	 * ajax zone to list messages 
	 */
	@InjectComponent
	private Zone _messageBoxZone;

	@SetupRender
	public void setupRender(){
		// have to add the property sender to display it as it is a complex class
		_messageBoxModel = _beanModelSource.createDisplayModel(InternalMessage.class, _messages);
		_messageBoxModel.exclude("id", "content", "modifiedDate");
		_messageBoxModel.add("author", _pcSource.create(InternalMessage.class, "author.name")).sortable(true);
		//add the delete colomn with no value
		_messageBoxModel.addEmpty("delete");
		_messageBoxModel.reorder("author","title","createddate");
	}

	/**
	 * when the page is loaded the first time, it loads the message in the received box 
	 */
	public void onActivate(){
		// load the messages received by the persona
		_internalMessages = _messageManager.findMessagesReceivedByPersona(_chronicleSettings.getPersona());
	}
	
	@OnEvent("receivedMessages")
	 Object receivedMessages(){
		_internalMessages = _messageManager.findMessagesReceivedByPersona(_chronicleSettings.getPersona());
		//the zone is the ajax html item to be updated
		return _messageBoxZone.getBody();
	}
	
	@OnEvent("deletedMessages")
	 Object deletedMessages(){
		_internalMessages = _messageManager.findMessagesDeletedByPersona(_chronicleSettings.getPersona());
		//the zone is the ajax html item to be updated
		return _messageBoxZone.getBody();
	}
		
	@OnEvent("sentMessages")
	 Object sentMessages(){
		_internalMessages = _messageManager.findMessagesSentByPersona(_chronicleSettings.getPersona());
		//the zone is the ajax html item to be updated
		return _messageBoxZone.getBody();
	}
}