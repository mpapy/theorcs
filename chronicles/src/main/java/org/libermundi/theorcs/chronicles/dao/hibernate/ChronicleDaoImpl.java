/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.libermundi.theorcs.chronicles.dao.ChronicleDao;
import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.core.dao.exception.OrcsDataAccessException;
import org.libermundi.theorcs.core.dao.hibernate.HibernateGenericDao;
import org.libermundi.theorcs.security.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public final class ChronicleDaoImpl extends HibernateGenericDao<Chronicle, Long> implements ChronicleDao {
	
	private static final Logger logger = LoggerFactory.getLogger(ChronicleDaoImpl.class);
	
	public ChronicleDaoImpl(){
		super();
		_mappedClazz = Chronicle.class;
	}
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.dao.ChronicleDao#getUsersByChronicle(org.libermundi.theorcs.chronicles.model.Chronicle)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUsersByChronicle(Chronicle chronicle) {
		List<User> result = null;
		try{
			result = createQuery("Select distinct perso.player " +
					"from Persona as perso inner join perso.player as player " +
					"inner join perso.chronicle as chronicle " +
				"where perso.chronicle = :chronicle " +
					"and perso.player.deleted = :deleted " +
					"and perso.player.active = :active " +
					"and perso.deleted = :persodeleted " +
					"order by perso.player asc")
				.setBoolean("deleted", Boolean.FALSE)
				.setBoolean("active", Boolean.TRUE)
				.setBoolean("persodeleted", Boolean.FALSE)
				.setParameter("chronicle", chronicle)
				.list();
		} catch (HibernateException e) {
			logger.error("Exception in getUsersByChronicle(" + chronicle +")", e);
			throw new OrcsDataAccessException("Exception in getUsersByChronicle : " + chronicle, e);
		}
		return result;
	}


	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.dao.ChronicleDao#getChroniclesByUser(org.libermundi.theorcs.security.model.User)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Chronicle> getChroniclesByUser(User user) {
		List<Chronicle> result = null;
		try	{
			result = createQuery("Select distinct chronicle " +
					"from Persona as perso inner join perso.player as player " +
						"inner join perso.chronicle as chronicle " +
					"where perso.player = :user " +
						"and chronicle.deleted = :deleted " +
						"and chronicle.active = :active " +
						"group by chronicle, perso")
					.setBoolean("deleted", Boolean.FALSE)
					.setBoolean("active", Boolean.TRUE)
					.setParameter("user", user)
					.list();
			if(logger.isDebugEnabled()) {
				logger.debug("getChroniclesByUser(" + user +") : " + result);
			}
		} catch (HibernateException e) {
			logger.error("Exception in getChroniclesByUser(" + user +")", e);
			throw new OrcsDataAccessException("Exception in getChroniclesByUser : " + user, e);
		}
		return result;
	}

}
