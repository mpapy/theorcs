package org.libermundi.theorcs.chronicles.tapestry.pages.perso;

import org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage;

import com.google.common.base.Strings;

public class Background extends ChroniclePage {
	public String getPersoBackground(){
		return notEmptyString(getChronicleSettings().getPersona().getBackground());
	}

	private String notEmptyString(String input){
		if(Strings.isNullOrEmpty(input)){
			return "<p><em>" + getMessages().get("chronicles.undefined") + "</em></p>";
		}
		return input;
	}
}
