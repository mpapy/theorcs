package org.libermundi.theorcs.chronicles.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.libermundi.theorcs.chronicles.ChronicleConstants;
import org.libermundi.theorcs.core.model.base.BasicEntity;

@Entity(name="GameSystem")
@Table(name=ChronicleConstants.TBL_GAMESYSTEM)
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public final class GameSystem extends BasicEntity {
	public final static String PROP_NAME="name";
	
	private static final long serialVersionUID = 7637662776253993576L;

	private String _name;
	
	@Basic
	@Column(name = GameSystem.PROP_NAME, length=50, nullable=false)
	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name=name;
	}

	@Override
	public String toString() {
		return "GameSystem [_id=" + getId() + ", _name=" + _name + "]";
	}
}
