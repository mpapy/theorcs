/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.tapestry.components.sheet;

import java.util.List;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.RequestParameter;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONArray;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.libermundi.theorcs.chronicles.model.sheet.SheetContainer;
import org.libermundi.theorcs.core.tapestry.model.ContextMenuElement;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;
import org.libermundi.theorcs.core.util.ObjectUtils;
import org.slf4j.Logger;

import com.google.common.collect.Lists;

@Import(library={
	"${chronicles.scripts}/sheetpage.editorhandler.js"
}, module={
		"bootstrap/modal",
		"bootstrap/transition",
		"tjq/vendor/ui/jquery.ui.mouse",
		"tjq/vendor/ui/jquery.ui.draggable",
		"tjq/vendor/ui/jquery.ui.resizable",
		"tjq/vendor/ui/jquery.ui.dialog"
})
public class SheetPageEditor extends SheetPageViewer {
	@Inject
	private Messages _messages;
	
	@Environmental
	private JavaScriptSupport _jsSupport;

	@Inject 
	private ComponentResources _resources;
	
	@Inject
	private Block _containerBlock;
	
	@Inject
	private AppHelper _appHelper;
	
	@Inject
	private Logger logger;
	
	@Persist
	private SheetContainer _savedContainer;
	
	@Persist
	@Property(write=false)
	private List<ContextMenuElement> _menuElements;
	
	@SetupRender
	public void createContextualMenuElements() {
		_menuElements= Lists.newArrayList();
		
		_menuElements.add(new ContextMenuElement("add",  _messages.get("chronicles.sheeteditor.contextmenu.add"), Boolean.TRUE));
		_menuElements.add(new ContextMenuElement("cut",  _messages.get("chronicles.sheeteditor.contextmenu.cut"), Boolean.FALSE));
		_menuElements.add(new ContextMenuElement("copy",  _messages.get("chronicles.sheeteditor.contextmenu.copy"), Boolean.FALSE));
		_menuElements.add(new ContextMenuElement("paste",  _messages.get("chronicles.sheeteditor.contextmenu.paste"), Boolean.FALSE));
		_menuElements.add(new ContextMenuElement("delete",  _messages.get("chronicles.sheeteditor.contextmenu.delete"), Boolean.TRUE));
		_menuElements.add(new ContextMenuElement("edit", _messages.get("chronicles.sheeteditor.contextmenu.edit"), Boolean.FALSE));
		_menuElements.add(new ContextMenuElement("settings",  _messages.get("chronicles.sheeteditor.contextmenu.settings"), Boolean.TRUE));
		_menuElements.add(new ContextMenuElement("quit", _messages.get("chronicles.sheeteditor.contextmenu.quit"), Boolean.FALSE));
		
	}
	
	@AfterRender
	public void initDeleteZoneDialog(){
		_jsSupport.addScript("$(SheetGridSettings.deleteZoneModal).appendTo('body');");
		_jsSupport.addScript("$(SheetGridSettings.deleteZoneModal).modal({show:false,keyboard:true});");
	}
	
	@OnEvent("addcontainer")
	public Block addContainer(
			@RequestParameter("x") int x,
			@RequestParameter("y") int y
			) {
		setLoadedContainer(getPage().addContainer(x, y));
		return getContainerBlock();
	}
	

	@OnEvent("cutContainer")
	public void cutContainer(@RequestParameter("uid") String uid){
		_savedContainer = getPage().getContainer(uid);
		getPage().getContainers().remove(_savedContainer);
	}
	
	@OnEvent("deleteContainer")
	public void deleteContainer(@RequestParameter("uid") String uid){
		getPage().getContainers().remove(getPage().getContainer(uid));
	}
	
	@OnEvent("copyContainer")
	public void copyContainer(@RequestParameter("uid") String uid){
		SheetContainer orig = getPage().getContainer(uid);
		SheetContainer copy = ObjectUtils.safeDeepCopy(orig);
		copy.resetUid();
		_savedContainer = copy;
	}
	
	@OnEvent("pasteContainer")
	public JSONArray pasteContainer(){
		if(_savedContainer != null) {
			getPage().getContainers().add(_savedContainer);
			JSONArray result = new JSONArray((Object)_savedContainer.getUid()); 
			_savedContainer = null;
			return result;
		}	
		throw new RuntimeException("No Container previously copied or cut");
	}
	
	@OnEvent("saveContainerLayout")
	public void saveContainerLayout(
				@RequestParameter("uid") String uid,
				@RequestParameter("x") int x,
				@RequestParameter("y") int y,
				@RequestParameter("width") int width,
				@RequestParameter("height") int height				
			){
		if(logger.isDebugEnabled()) {
			logger.debug("Saving Container Layout");
			logger.debug("       Container UID : " + uid);
			logger.debug("       Container x : " + x);
			logger.debug("       Container y : " + y);
			logger.debug("       Container width : " + width);
			logger.debug("       Container height : " + height);
		}
		SheetContainer updt = getPage().getContainer(uid);
		updt.setX(x);
		updt.setY(y);
		updt.setWidth(width);
		updt.setHeight(height);
		
	}
	
	@Override
	public Block getContainerBlock(){
		return _containerBlock;
	}
	
	@Override
	protected String getEditMode(){
		return "true";
	}	
}
