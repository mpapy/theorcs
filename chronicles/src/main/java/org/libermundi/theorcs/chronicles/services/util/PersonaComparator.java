/**
 * 
 */
package org.libermundi.theorcs.chronicles.services.util;

import java.util.Comparator;

import org.libermundi.theorcs.chronicles.model.Persona;

/**
 * Persona Comparator by Id
 * @author Stephane Guichard
 *
 */
public class PersonaComparator implements Comparator<Persona> {
	
    @Override
	public int compare(Persona c1, Persona c2) {
    	if(c1.getId()==c2.getId())
    		return 0;
    	if(c1.getId()<c2.getId())
        	return -1;
        else
        	return 1;
    }
}
