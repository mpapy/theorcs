/**
 * 
 */
package org.libermundi.theorcs.chronicles.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.libermundi.theorcs.chronicles.dao.InternalMessageDao;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.internal.InternalMessage;
import org.libermundi.theorcs.core.dao.exception.OrcsDataAccessException;
import org.libermundi.theorcs.core.dao.hibernate.HibernateGenericDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Hibernate implementation of InternalMessageDao
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
@Repository
public class InternalMessageDaoImpl extends HibernateGenericDao<InternalMessage, Long> implements InternalMessageDao {

	private static final Logger logger = LoggerFactory.getLogger(InternalMessageDaoImpl.class); 

	public InternalMessageDaoImpl(){
		super();
		_mappedClazz = InternalMessage.class;
	}
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.dao.MessageDao#getMessagesNotDeletedByPersona(org.libermundi.theorcs.chronicles.model.Persona)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<InternalMessage> findMessagesReceivedByPersona(Persona persona) {
		List<InternalMessage> result;
		try{			
			result = createQuery("SELECT message " +
				"FROM InternalMessageTarget as target INNER JOIN target.internalMessage as message INNER JOIN target.entity as perso" +
				" WHERE target.entity = :perso " +
				" and message.author <> :perso" +
				" and target.deleted = :deleted " +
				" order by message.id")
				.setParameter("perso", persona)
				.setParameter("deleted", false)
				.list();
		} catch (HibernateException e) {
			logger.error("Exception in getMessagesReceivedByPersona(" + persona +")", e);
			throw new OrcsDataAccessException("Exception in getMessagesReceivedByPersona : " + persona, e);
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<InternalMessage> findUnseenMessagesReceivedByPersona(Persona persona) {
		List<InternalMessage> result;
		try{			
			result = createQuery("SELECT message " +
				"FROM InternalMessageTarget as target INNER JOIN target.internalMessage as message INNER JOIN target.entity as perso" +
				" WHERE target.entity = :perso " +
				" and message.author <> :perso" +
				" and target.deleted = :deleted " +
				" and target.seen = :seen " +
				" order by message.id")
				.setParameter("perso", persona)
				.setParameter("deleted", false)
				.setParameter("seen", false)
				.list();
		} catch (HibernateException e) {
			logger.error("Exception in getMessagesReceivedByPersona(" + persona +")", e);
			throw new OrcsDataAccessException("Exception in getMessagesReceivedByPersona : " + persona, e);
		}
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.dao.InternalMessageDao#getPersonaSentMessages(org.libermundi.theorcs.chronicles.model.Persona)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<InternalMessage> findSentMessagesByPersona(Persona persona) {
		List<InternalMessage> result;
		try{
			result = createQuery(" FROM InternalMessage as message " +
					" where author = :perso " + 
					" order by message.id")
					.setParameter("perso", persona)
					.list();
		} catch (HibernateException e) {
			logger.error("Exception in findSentMessagesByPersona(" + persona +")", e);
			throw new OrcsDataAccessException("Exception in findSentMessagesByPersona : " + persona, e);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.chronicles.dao.InternalMessageDao#findDeletedMessagesByPersona(org.libermundi.theorcs.chronicles.model.Persona)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<InternalMessage> findDeletedMessagesByPersona(Persona persona) {
		List<InternalMessage> result;
		try{
			result = createQuery("SELECT message " +
					"FROM InternalMessageTarget as target INNER JOIN target.internalMessage as message INNER JOIN target.entity as perso" +					
					" WHERE target.entity = :perso " +
					" and target.deleted = :deleted " +
					" order by message.id")
					.setParameter("perso", persona)
					.setParameter("deleted", true)
					.list();
		} catch (HibernateException e) {
			logger.error("Exception in findDeletedMessagesByPersona(" + persona +")", e);
			throw new OrcsDataAccessException("Exception in findDeletedMessagesByPersona : " + persona, e);
		}
		return result;
	}

	@Override
	public int updateMessageOnSeen(InternalMessage message, Persona persona, boolean isSeen) {
		int result = 0;
		try{
			result = createQuery("UPDATE InternalMessageTarget as target " +
					" SET target.seen = :seen " +					
					" WHERE target.entity = :perso " +
					" and target.internalMessage = :message ")
					.setParameter("perso", persona)
					.setParameter("seen", isSeen)
					.setParameter("message", message)
					.executeUpdate();
			flush();
		} catch (HibernateException e) {
			logger.error("Exception in updateMessageOnDeleted(" + persona +")", e);
			throw new OrcsDataAccessException("Exception in updateMessageOnDeleted : " + persona, e);
		}
		return result;
	}

	@Override
	public int updateMessageOnDelete(InternalMessage message, Persona persona, boolean isDeleted) {
		int result = 0;
		try{
			result = createQuery("UPDATE InternalMessageTarget as target " +
					" SET target.deleted = :delete " +					
					" WHERE target.entity = :perso " +
					" and target.internalMessage = :message ")
					.setParameter("perso", persona)
					.setParameter("delete", isDeleted)
					.setParameter("message", message)
					.executeUpdate();
			flush();
		} catch (HibernateException e) {
			logger.error("Exception in updateMessageOnDeleted(" + persona +")", e);
			throw new OrcsDataAccessException("Exception in updateMessageOnDeleted : " + persona, e);
		}
		return result;
	}
}

