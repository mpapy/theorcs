/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.tapestry.base;

import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.chronicles.dto.ChronicleSettings;
import org.libermundi.theorcs.security.services.SecurityManager;
import org.slf4j.Logger;
import org.springframework.security.acls.AclPermissionEvaluator;
import org.springframework.security.acls.domain.BasePermission;

public abstract class ChronicleAdminPage extends ChroniclePage {
	@Inject
	private SecurityManager _securityManager;
	
	@Inject
	private AclPermissionEvaluator _aclPermissionEvaluator;
	
	@SessionState
	private ChronicleSettings _chronicleSettings;
	
	@Inject
	private Logger _logger;
	
	@SetupRender
	public void checkAdminSecurity() {
		// Let's check the current user as the right to access this
		if(!_aclPermissionEvaluator.hasPermission(_securityManager.getCurrentAuthentication(), _chronicleSettings.getChronicle(), BasePermission.ADMINISTRATION)) {
			sendRedirect();
			return;
		}
		if(_logger.isDebugEnabled()){
			_logger.debug("Access has been granted to administer Chronicle : '{}'",_chronicleSettings.getChronicle().getTitle());
		}
	}
	
	/**
	 * Controls whether or not we display the SideBar on the Page. True by default.
	 */
	public boolean isSideBarHidden(){
		return Boolean.FALSE;
	}
}
