/**
 * 
 */
package org.libermundi.theorcs.chronicles.dao;

import java.util.List;

import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMember;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMessage;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumThread;
import org.libermundi.theorcs.core.dao.GenericDao;

/**
 * DAO for ForumThread
 * 
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
public interface ForumThreadDao extends GenericDao<ForumThread, Long> {

	/**
	 * Get all threads which the persona is member of
	 * @param persona
	 * @return
	 */
	List<ForumThread> findPersonaThreads(Persona persona);

	/**
	 * Find the ForumMember from a ForumThread and a Persona
	 * Return null if the persona is not a ForumMember of this ForumThread
	 *  
	 * @param thread
	 * @param persona
	 * 
	 * @return
	 */
	ForumMember findMemberFromForumThreadAndPersona(ForumThread thread,
			Persona persona);

	/**
	 * Find the forum from its id if the persona can access to it 
	 * (i.e. the persona is not a ForumMember deleted)
	 * @param forumId
	 * @param persona
	 * @return
	 */
	ForumThread findActiveThread(long forumId, Persona persona);

	/**
	 * Get all messages of a forumThread for a persona 
	 * @param thread
	 * @param persona
	 * @return
	 */
	List<ForumMessage> findReadableMessagesForThreadAndPersona(
			ForumThread thread, Persona persona);

	/**
	 * @param forumThread
	 * @param persona
	 * @return
	 */
	int flagThreadAsReadenByPersona(ForumThread forumThread,
			Persona persona);

	/**
	 * @param thread
	 * @param newMessage
	 */
	int flagNewMessage(ForumThread thread, ForumMessage newMessage);

	List<ForumMessage> findNewMessages(Persona persona);
}
