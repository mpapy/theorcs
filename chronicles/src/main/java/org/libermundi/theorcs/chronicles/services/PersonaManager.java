/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.services;

import java.util.List;

import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.sheet.Sheet;
import org.libermundi.theorcs.core.services.Manager;
import org.libermundi.theorcs.security.model.User;

public interface PersonaManager extends Manager<Persona, Long> {
	public static final String SERVICE_ID = "personaManager";
	
	Sheet buildEmptySheet();

	List<Persona> getChroniclePersonas(Chronicle chronicle);
	
	List<Persona> getAllPersona(User user, Chronicle chronicle);
	
	Persona getDefaultPersona(User user, Chronicle chronicle);
	
	Persona createBasicPersona(User user, Chronicle chronicle, String personaName);
}
