package org.libermundi.theorcs.chronicles.dao;

import org.libermundi.theorcs.chronicles.model.Game;
import org.libermundi.theorcs.core.dao.GenericDao;

public interface GameDao extends GenericDao<Game, Long> {

}
