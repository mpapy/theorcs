package org.libermundi.theorcs.chronicles.tapestry.components;

import java.util.List;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.ValueEncoder;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.ValueEncoderFactory;
import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.model.ChronicleGenre;
import org.libermundi.theorcs.chronicles.model.ChroniclePace;
import org.libermundi.theorcs.chronicles.model.ChronicleStyle;
import org.libermundi.theorcs.chronicles.model.ChronicleType;
import org.libermundi.theorcs.chronicles.model.Game;
import org.libermundi.theorcs.chronicles.services.GameManager;
import org.libermundi.theorcs.core.tapestry.util.GenericSelectModel;

public class ChronicleForm {
	
	@Inject
	private GameManager _gameManager;
	
	@SuppressWarnings("rawtypes")
	@Inject
	private ValueEncoderFactory _valueEncoderFactory;
	
	@Property(write=false)
	private ChronicleType _standardType=ChronicleType.STANDARD;
	
	@Property(write=false)
	private ChronicleType _privateType=ChronicleType.PRIVATE;

	@Property(write=false)
	private ChronicleStyle _ambianceStyle=ChronicleStyle.AMBIANCE;
	
	@Property(write=false)
	private ChronicleStyle _actionStyle=ChronicleStyle.ACTION;
	
	@Property(write=false)
	private ChronicleStyle _balancedStyle=ChronicleStyle.BALANCED;	

	@Property(write=false)
	private ChroniclePace _slowPace=ChroniclePace.SLOW;
	
	@Property(write=false)
	private ChroniclePace _averagePace=ChroniclePace.AVERAGE;
	
	@Property(write=false)
	private ChroniclePace _fastPace=ChroniclePace.FAST;	
	
	@Property(write=false)
	private ChronicleGenre _historicalGenre=ChronicleGenre.HISTORICAL;

	@Property(write=false)
	private ChronicleGenre _contemporaryGenre=ChronicleGenre.CONTEMPORARY;

	@Property(write=false)
	private ChronicleGenre _cyberpunkGenre=ChronicleGenre.CYBERPUNK;

	@Property(write=false)
	private ChronicleGenre _medfanGenre=ChronicleGenre.MED_FAN;

	@Property(write=false)
	private ChronicleGenre _otherGenre=ChronicleGenre.OTHER;

	@Property(write=false)
	private ChronicleGenre _postapoGenre=ChronicleGenre.POST_APO;

	@Property(write=false)
	private ChronicleGenre _spaceopGenre=ChronicleGenre.SPACE_OP;
	
	@Parameter(required=true,allowNull=false)
	@Property
	private Chronicle _chronicle;
	
	public SelectModel getGameModel() {
		List<Game> games = _gameManager.getAll();
		return new GenericSelectModel<Game,Long>(games);
	}
	
	@SuppressWarnings("unchecked")
	public ValueEncoder<Game> getGameEncoder(){
		return _valueEncoderFactory.create(Game.class);
	}
	
	public boolean isEditing() {
		return _chronicle.getId() != null;
	}
}
