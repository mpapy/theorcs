/**
 * 
 */
package org.libermundi.theorcs.chronicles.tapestry.encoders;

import org.apache.tapestry5.ValueEncoder;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.ValueEncoderFactory;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.services.PersonaManager;

/**
 * Encoder to construct the Select menu and to interpret the encoded value that
 * is submitted back to the server.
 * 
 */
public class PersonaEncoder implements ValueEncoder<Persona>, ValueEncoderFactory<Persona> {

	@Inject
	private PersonaManager personaService;

	@Override
	public String toClient(Persona value) {
		// return the given object's ID
		return String.valueOf(value.getId());
	}

	@Override
	public Persona toValue(String id) {
		// find the Persona object of the given ID in the database 	
		Persona p = personaService.load(Long.parseLong(id));
		return p;
	}

	// let this ValueEncoder also serve as a ValueEncoderFactory
	@Override
	public ValueEncoder<Persona> create(Class<Persona> type) {
		return this;
	}

}
