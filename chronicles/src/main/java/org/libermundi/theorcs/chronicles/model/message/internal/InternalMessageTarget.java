package org.libermundi.theorcs.chronicles.model.message.internal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.libermundi.theorcs.chronicles.ChronicleConstants;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.Target;
import org.libermundi.theorcs.core.model.base.BasicEntity;

/**
 * The InternalMessage specify the role of the persona 
 * and specify if its InternalMessage has been read and or deleted
 *
 * @author Stephane Guichard
 * @author Martin Papy
 */
@Entity(name="InternalMessageTarget")
@Table(name=ChronicleConstants.TBL_INTERNAL_MESSAGE_TARGET)
@DiscriminatorColumn(name="role", discriminatorType=DiscriminatorType.STRING)

public abstract class InternalMessageTarget extends BasicEntity implements Target<Persona>{

	private static final long serialVersionUID = 1461128885981925776L;
	public static final String PROP_SEEN = "seen";
	//bidirectional mapping association
	private InternalMessage _internalMessage;
	/**
	 * The message has been seen or not
	 */
	private boolean _seen;
	private Persona _persona;
	
	public InternalMessageTarget() {
		super();
	}
	
	@Basic
	@Column(name=PROP_SEEN)
	public boolean isSeen() {
		return _seen;
	}

	public void setSeen(boolean seen) {
		this._seen = seen;
	}

	@Override
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="personaId")
	public Persona getEntity() {
		return _persona;
	}

	@Override
	public void setEntity(Persona entity) {
		_persona = entity;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="internalMsgId")
	public InternalMessage getInternalMessage() {
		return _internalMessage;
	}

	public void setInternalMessage (InternalMessage internalMsg) {
		_internalMessage = internalMsg;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "InternalMessageTarget [perso.id="+_persona.getId()+", perso.name="+_persona.getName()+"; seen:"+_seen+"]";
	}
}
