/**
 * 
 */
package org.libermundi.theorcs.chronicles.tapestry.components.forum;

import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMessage;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumThread;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.chronicles.services.message.ForumThreadManager;
import org.libermundi.theorcs.chronicles.tapestry.base.ChroniclePage;
import org.libermundi.theorcs.chronicles.tapestry.pages.forum.ReadForum;

/**
 * Add a new post to a threadForum
 * s
 * @author Stephane Guichard 
 * @author Martin Papy
 *
 */
public class AddPost extends ChroniclePage {
	
	@Property
	private ForumMessage message;
	
	/*------------- Services -------------*/    
	@Inject
	private ForumThreadManager _forumThreadManager;
	
	@Inject
	private PersonaManager _personaManager;
	
	@Inject
	private org.libermundi.theorcs.security.services.SecurityManager _securityManager;

	
	/*------------- Fields -------------*/

	@Parameter(allowNull=true,required=true)
	@Property
	ForumThread _forumThread;
	
	@Property 
	private String _content;	
	
	/*------------- Rendering Phase -------------*/
	/**
	 * Create a new post for the forum
	 * @return the page
	 */
	@OnEvent(value="success",component="forumForm")
	public Object success(){
		//TODO getCurrentPerso() works but when the persona is evaluated an com.sun.jdi.InvocationException is thrown
		//why?
		//_internalMessage.setAuthor(getCurrentPerso());
		Persona perso = _personaManager.getDefaultPersona(_securityManager.getCurrentUser(), getChronicleSettings().getChronicle());

		_forumThreadManager.addPostToThread(_forumThread, perso, _content);
		return ReadForum.class;
	}
}