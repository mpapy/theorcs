package org.libermundi.theorcs.chronicles.services;

import org.libermundi.theorcs.chronicles.model.SavedInformations;
import org.libermundi.theorcs.core.services.Manager;
import org.libermundi.theorcs.security.model.User;

public interface SavedInformationsManager extends Manager<SavedInformations, Long> {
	
	SavedInformations getByUser(User user);

	void clear(User user);

}
