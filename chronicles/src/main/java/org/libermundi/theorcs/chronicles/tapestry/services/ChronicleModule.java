/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.tapestry.services;

import org.apache.tapestry5.ioc.Configuration;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.Resource;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.ioc.annotations.ImportModule;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.ioc.services.Coercion;
import org.apache.tapestry5.ioc.services.CoercionTuple;
import org.apache.tapestry5.services.ApplicationInitializerFilter;
import org.apache.tapestry5.services.LibraryMapping;
import org.apache.tapestry5.services.ValueEncoderFactory;
import org.libermundi.theorcs.chronicles.ChronicleConstants;
import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.model.Faction;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumThread;
import org.libermundi.theorcs.chronicles.model.message.internal.InternalMessage;
import org.libermundi.theorcs.chronicles.services.ChronicleManager;
import org.libermundi.theorcs.chronicles.services.FactionManager;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.chronicles.services.message.ForumThreadManager;
import org.libermundi.theorcs.chronicles.services.message.InternalMessageManager;
import org.libermundi.theorcs.chronicles.tapestry.encoders.PersonaEncoder;
import org.libermundi.theorcs.chronicles.tapestry.services.impl.ChronicleNavItemSource;
import org.libermundi.theorcs.chronicles.tapestry.services.impl.FactionMenuStringSource;
import org.libermundi.theorcs.chronicles.tapestry.services.impl.FactionNavItemSource;
import org.libermundi.theorcs.core.services.StringSource;
import org.libermundi.theorcs.core.util.OrcsHome;
import org.libermundi.theorcs.layout.services.NavItemSource;


/**
 * 
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
@ImportModule(ChronicleMenuModule.class)
public class ChronicleModule {
	public static void contributeComponentClassResolver(final Configuration< LibraryMapping > configuration) {
        configuration.add(new LibraryMapping(ChronicleConstants.TAPESTRY_MAPPING, "org.libermundi.theorcs.chronicles.tapestry"));
	}
	
	public static void bind(ServiceBinder binder) {        
        binder.bind(NavItemSource.class, ChronicleNavItemSource.class).withId("ChronicleNavItemSource");
        binder.bind(NavItemSource.class, FactionNavItemSource.class).withId("FactionNavItemSource");
        binder.bind(StringSource.class, FactionMenuStringSource.class).withId("FactionMenuStringSource");
    }
	
	public static void contributeFactoryDefaults(MappedConfiguration<String, String> configuration) {
        configuration.add("chronicles.assets", "META-INF/assets");

        configuration.add("chronicles.scripts", "classpath:${chronicles.assets}/js");
        configuration.add("chronicles.styles", "classpath:${chronicles.assets}/css");
        configuration.add("chronicles.images", "classpath:${chronicles.assets}/images");
    }	
	
	public static void contributeComponentMessagesSource(
			@Value("META-INF/lang/chronicles.properties")
			Resource chroniclesCatalogResource,
			OrderedConfiguration<Resource> configuration) {
			configuration.add("ChroniclesCatalog", chroniclesCatalogResource, "before:AppCatalog");
	}
	
	public static void contributeApplicationConfiguration(
			OrderedConfiguration<String> configuration,
			OrcsHome theOrcsHome) {
		configuration.add("chronicles-default", "classpath:META-INF/conf/chronicles-default.properties","after:core-default");
		configuration.add("chronicles-local", theOrcsHome.getPath() + "/conf/chronicles.properties","after:chronicles-default");
	}
	
	public void contributeApplicationInitializer(OrderedConfiguration<ApplicationInitializerFilter> configuration) {
        configuration.add("chroniclesModuleInitializer", new ChroniclesModuleInitializer(), "after:securityModuleInitializer");
    }
	
	@SuppressWarnings("rawtypes")
	public static void contributeTypeCoercer(Configuration<CoercionTuple> configuration,
			@InjectService(ChronicleManager.SERVICE_ID) final ChronicleManager chronicleManager,
			@InjectService(PersonaManager.SERVICE_ID) final PersonaManager persoManager,
			@InjectService(FactionManager.SERVICE_ID) final FactionManager factionManager,
			@InjectService(ForumThreadManager.SERVICE_ID) final ForumThreadManager forumThreadManager,
			@InjectService(InternalMessageManager.SERVICE_ID) final InternalMessageManager internalMessageManager) {
		
		Coercion<Long, Chronicle> c1 = new Coercion<Long, Chronicle>() {
			@Override
			public Chronicle coerce(Long input) {
				return chronicleManager.load(input);
			}
		};
		
		Coercion<Long, Persona> c2 = new Coercion<Long, Persona>() {
			@Override
			public Persona coerce(Long input) {
				return persoManager.load(input);
			}
		};
		
		Coercion<Long, Faction> c3 = new Coercion<Long, Faction>() {
			@Override
			public Faction coerce(Long input) {
				return factionManager.load(input);
			}
		};
		
		Coercion<Long, ForumThread> c4 = new Coercion<Long, ForumThread>() {
			@Override
			public ForumThread coerce(Long input) {
				return forumThreadManager.load(input);
			}
		};

		Coercion<Long, InternalMessage> c5 = new Coercion<Long, InternalMessage>() {
			@Override
			public InternalMessage coerce(Long input) {
				return internalMessageManager.load(input);
			}
		};
		configuration.add(new CoercionTuple<>(Long.class, Chronicle.class, c1));
		configuration.add(new CoercionTuple<>(Long.class, Persona.class, c2));
		configuration.add(new CoercionTuple<>(Long.class, Faction.class, c3));
		configuration.add(new CoercionTuple<>(Long.class, ForumThread.class, c4));
		configuration.add(new CoercionTuple<>(Long.class, InternalMessage.class, c5));

	}
	
	/**
	 * Contribute for the Encoder 
	 * @param configuration
	 */
	@SuppressWarnings("rawtypes")
	public static void contributeValueEncoderSource(MappedConfiguration<Class,ValueEncoderFactory> configuration) { 
		 configuration.addInstance(Persona.class, PersonaEncoder.class);
	 }

}
