package org.libermundi.theorcs.chronicles.model;

import java.util.SortedSet;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.SortNatural;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.libermundi.theorcs.chronicles.ChronicleConstants;
import org.libermundi.theorcs.core.model.Searchable;
import org.libermundi.theorcs.security.model.User;
import org.libermundi.theorcs.security.model.UserStatefulEntity;

import com.google.common.collect.Sets;

@Entity(name="Chronicle")
@Table(name=ChronicleConstants.TBL_CHRONICLE)
@Indexed(index="Chronicles")
@Analyzer(definition = "SuggestionAnalyzer")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public final class Chronicle extends UserStatefulEntity implements Searchable{
	public final static String PROP_TITLE="title";
	public final static String PROP_SUBTITLE="subTitle";
	public final static String PROP_DESCRIPTION="description";
	public final static String PROP_FACTIONS="factions";
	public final static String PROP_TYPE="type";
	public final static String PROP_GENRE="genre";
	public final static String PROP_PASSWORD="password";
	public final static String PROP_STYLE="style";
	public final static String PROP_DATE="date";
	public final static String PROP_BACKGROUND="background";
	public final static String PROP_PACE="pace";
	public final static String PROP_OPENFORINSCRIPTION="openForInscription";
	
	private static final String[] SEARCHABLE_FIELDS = new String[]{PROP_TITLE, PROP_SUBTITLE, PROP_DESCRIPTION, PROP_BACKGROUND};
	private static final String[] SUGGESTION_SEARCHABLE_FIELDS = new String[]{PROP_TITLE, PROP_SUBTITLE, PROP_DESCRIPTION, PROP_BACKGROUND};
	
	private static final long serialVersionUID = -623462418684825618L;
	
	private String _title;
	private String _subTitle;
	private String _description;
	private ChronicleType _type;
	private ChronicleGenre _genre;
	private ChronicleStyle _style;
	private ChroniclePace _pace;
	private String _password;
	private String _date;
	private String _background;
	private Game _game;
	private User _admin;
	private boolean _openForInscription;
	private SortedSet<Faction> _factions = Sets.newTreeSet();

	@Basic
	@Column(name=Chronicle.PROP_TITLE, length=50)
	@Field(store=Store.YES,index=Index.YES)
	public String getTitle() {
		return _title;
	}

	@Basic
	@Column(name=Chronicle.PROP_SUBTITLE, length=125)	
	@Field(store=Store.YES,index=Index.YES)
	public String getSubTitle() {
		return _subTitle;
	}
	
	@Lob
	@Column(name=Chronicle.PROP_DESCRIPTION,nullable=true)
	@Field(store=Store.YES,index=Index.YES)
	public String getDescription() {
		return _description;
	}
	
	@Lob
	@Column(name=Chronicle.PROP_BACKGROUND,nullable=true)
	@Field(store=Store.YES,index=Index.YES)
	public String getBackground() {
		return _background;
	}

	@Enumerated(EnumType.STRING)
	@Column(name=Chronicle.PROP_TYPE,length=8,nullable=false)
	public ChronicleType getType() {
		return _type;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name=Chronicle.PROP_GENRE,length=12,nullable=false)
	public ChronicleGenre getGenre() {
		return _genre;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name=Chronicle.PROP_STYLE,length=12,nullable=false)
	public ChronicleStyle getStyle() {
		return _style;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name=Chronicle.PROP_PACE,length=7,nullable=false)
	public ChroniclePace getPace() {
		return _pace;
	}
	
	@Basic
	@Column(name=Chronicle.PROP_PASSWORD, length=15)	
	public String getPassword() {
		return _password;
	}

	@Basic
	@Column(name=Chronicle.PROP_DATE, length=50)	
	public String getDate() {
		return _date;
	}

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="gameId")
	public Game getGame() {
		return _game;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="adminId")
	public User getAdmin() {
		return _admin;
	}
	
    @Column(name = Chronicle.PROP_OPENFORINSCRIPTION)
    public boolean isOpenForInscription() {
        return _openForInscription;
    }
    
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,orphanRemoval=true,mappedBy=Faction.PROP_CHRONICLE)
	@SortNatural
	public SortedSet<Faction> getFactions() {
		return _factions;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public void setSubTitle(String subTitle) {
		_subTitle = subTitle;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public void setType(ChronicleType type) {
		_type = type;
	}

	public void setGenre(ChronicleGenre genre) {
		_genre = genre;
	}

	public void setStyle(ChronicleStyle style) {
		_style = style;
	}

	public void setPace(ChroniclePace pace) {
		_pace = pace;
	}

	public void setPassword(String password) {
		_password = password;
	}

	public void setGame(Game game) {
		_game = game;
	}

	public void setAdmin(User admin) {
		_admin = admin;
	}
	
	public void setFactions(SortedSet<Faction> factions){
		_factions = factions;
	}

	public void setDate(String date) {
		_date = date;
	}

	public void setBackground(String background) {
		_background = background;
	}
	
	public void setOpenForInscription(boolean openForInscription) {
		_openForInscription = openForInscription;
	}

	@Override
	public String toString() {
		return "Chronicle [Title=" + _title + ", Game=" + _game + ", Admin=" + _admin + "]";
	}
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.Searchable#loadSearchfields()
	 */
	@Override
	public String[] loadSearchFields() {
		return SEARCHABLE_FIELDS;
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.Searchable#loadSuggestionFields()
	 */
	@Override
	public String[] loadSuggestionFields() {
		return SUGGESTION_SEARCHABLE_FIELDS;
	}
}
