package org.libermundi.theorcs.chronicles.services.impl;

import org.libermundi.theorcs.chronicles.dao.SavedInformationsDao;
import org.libermundi.theorcs.chronicles.model.SavedInformations;
import org.libermundi.theorcs.chronicles.services.SavedInformationsManager;
import org.libermundi.theorcs.core.services.impl.AbstractManagerImpl;
import org.libermundi.theorcs.security.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("savedInformationsManager")
@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
public class SavedInformationsManagerImpl extends AbstractManagerImpl<SavedInformations,Long> implements SavedInformationsManager  {
	@Autowired
	public SavedInformationsManagerImpl(SavedInformationsDao infosDao) {
		super();
		setDao(infosDao);
	}
	
	@Override
	public SavedInformations getByUser(User user) {
		SavedInformations result = getDao().getByUser(user);
		if(result == null){
			result = new SavedInformations();
			result.setUser(user);
		}
		return result;
		
	}

	@Override
	public void clear(User user) {
		getDao().clear(user);
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.services.impl.AbstractManagerImpl#getDao()
	 */
	@Override
	public SavedInformationsDao getDao() {
		return (SavedInformationsDao)super.getDao();
	}

	@Override
	public SavedInformations createNew() {
		return new SavedInformations();
	}
}
