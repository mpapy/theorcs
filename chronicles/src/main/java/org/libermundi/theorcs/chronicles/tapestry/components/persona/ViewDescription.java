/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.tapestry.components.persona;

import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.chronicles.model.Persona;

import com.google.common.base.Strings;

/**
 * @author Martin Papy
 *
 */
public class ViewDescription {
	@Inject
	private Messages _messages;
	
	@Parameter(required=true)
	@Property(write=false)
	private Persona _persona;
	
	
	public String getPersonaDescription(){
		return notEmptyString(_persona.getDescription());
	}
	
	private String notEmptyString(String input){
		if(Strings.isNullOrEmpty(input)){
			return "<em>" + _messages.get("chronicles.undefined") + "</em>";
		}
		return input;
	}
}
