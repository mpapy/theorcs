package org.libermundi.theorcs.chronicles.model.message.forum;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Indexed;
import org.libermundi.theorcs.chronicles.ChronicleConstants;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.search.PersonaBridge;
import org.libermundi.theorcs.core.model.Searchable;
import org.libermundi.theorcs.core.model.base.BasicEntity;

/**
 * A ForumMember is a persona who can read its thread's messages between the first and the last message specified 
 * 
 * @author Stephane Guichard
 * @author Martin Papy
 *
 */
@Entity(name="ForumMember")
@Table(name=ChronicleConstants.TBL_FORUM_MEMBER)
@Indexed(index="ForumMember")
public class ForumMember extends BasicEntity implements Searchable {
	
	public final static String PROP_PERSONA="persona";
	public final static String PROP_THREAD="forumThread";
	public final static String PROP_ROLE="role";
	public final static String PROP_FIRST_UNSEEN_MESSAGE="firstUnseenMessage";
	
	private static final String[] SEARCHABLE_FIELDS = new String[]{PROP_PERSONA, PROP_ROLE};

	private static final String[] SUGGESTION_SEARCHABLE_FIELDS = new String[]{};
	
	private static final long serialVersionUID = -8949790093545330724L;
	
	private Persona _persona;
	private ForumThread _thread;
	private ForumMemberRole _role;
	private ForumMessage _firstUnseenMessage;

	public ForumMember() {
		// Auto-generated constructor stub
	}
	
	public ForumMember(Persona persona, ForumMemberRole role, 
			ForumThread thread, ForumMessage firstUnseenMessage) {
		super();
		this._persona = persona;
		this._role = role;
		this._thread = thread;
		this._firstUnseenMessage = firstUnseenMessage;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="personaId")
	@FieldBridge(impl=PersonaBridge.class)
	public Persona getPersona() {
		return _persona;
	}

	public void setPersona(Persona _persona) {
		this._persona = _persona;
	}

	@ManyToOne
	@JoinColumn(name="forumThreadId")
	public ForumThread getForumThread() {
		return _thread;
	}

	public void setForumThread(ForumThread _thread) {
		this._thread = _thread;
	}
			
	/**
	 * @return the _role
	 */
	@Enumerated(EnumType.STRING)
	public ForumMemberRole getRole() {
		return _role;
	}

	/**
	 * @param role the _role to set
	 */
	public void setRole(ForumMemberRole role) {
		this._role = role;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="firstUnseenMessageId", referencedColumnName="id", nullable=true)
	public ForumMessage getFirstUnseenMessage() {
		return _firstUnseenMessage;
	}

	public void setFirstUnseenMessage(ForumMessage firstUnseenMessage) {
		_firstUnseenMessage = firstUnseenMessage;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.Searchable#loadSearchfields()
	 */
	@Override
	public String[] loadSearchFields() {
		return SEARCHABLE_FIELDS;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.Searchable#loadSuggestionFields()
	 */
	@Override
	public String[] loadSuggestionFields() {
		return SUGGESTION_SEARCHABLE_FIELDS;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(ForumMember.class.equals(obj.getClass()))
			return getId().equals(((ForumMember)obj).getId());
		return false;
	}
	
	@Override
	public String toString() {
		return "[ForumMember [id="+getId() + ", name="+getPersona().getName()+", persoId="+getPersona().getId()+", role="+getRole().toString()+"]]";
	}

}
