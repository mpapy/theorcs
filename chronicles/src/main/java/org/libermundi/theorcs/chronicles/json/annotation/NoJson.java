/**
 * 
 */
package org.libermundi.theorcs.chronicles.json.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used to prevent Serialization in JSON of marked fileds
 * 
 * @author Martin Papy
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface NoJson {
	
}
