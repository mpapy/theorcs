/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.chronicles.tapestry.services.impl;

import java.util.List;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.services.ChronicleManager;
import org.libermundi.theorcs.chronicles.tapestry.pages.Index;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.core.model.NodeFactory;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.model.menu.NavMenuItemType;
import org.libermundi.theorcs.layout.model.menu.StaticNavMenuItem;
import org.libermundi.theorcs.layout.services.NavItemSource;
import org.libermundi.theorcs.security.services.SecurityManager;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 * @author Martin Papy
 *
 */
public class ChronicleNavItemSource implements NavItemSource {

	@Inject
	private ChronicleManager _chronicleManager;
	
	@Inject
	private SecurityManager _securityManager;
	
	@Inject
	private AppHelper _appHelper;
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.tapestry.services.NavItemSource#getElements()
	 */
	@Override
	public List<Node<NavMenuItem>> getElements() {
		List<Chronicle> list = _chronicleManager.getChroniclesByUser(_securityManager.getCurrentUser());
		List<Node<NavMenuItem>> result = Lists.newArrayList();
		
		for (Chronicle chronicle : list) {
			Node<NavMenuItem> node = NodeFactory.getNode();
			node.setData(new StaticNavMenuItem(NavMenuItemType.DEFAULT));
			node.getData().setLabel(chronicle.getTitle());
			String extraDesc = Strings.isNullOrEmpty(chronicle.getSubTitle()) ? " - " + chronicle.getSubTitle() : "";
			node.getData().setDescription(chronicle.getTitle() + extraDesc);
			node.getData().setURI(_appHelper.getPageURI(Index.class, chronicle.getId()));
			result.add(node);
		}
		
		return result;
	}

}
