package org.libermundi.theorcs.config.spring;

import java.util.List;

import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.model.Faction;
import org.libermundi.theorcs.chronicles.model.Game;
import org.libermundi.theorcs.chronicles.model.GameSystem;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.model.SavedInformations;
import org.libermundi.theorcs.chronicles.model.message.AddressBook;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMember;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumMessage;
import org.libermundi.theorcs.chronicles.model.message.forum.ForumThread;
import org.libermundi.theorcs.chronicles.model.message.internal.BccTarget;
import org.libermundi.theorcs.chronicles.model.message.internal.CcTarget;
import org.libermundi.theorcs.chronicles.model.message.internal.InternalMessage;
import org.libermundi.theorcs.chronicles.model.message.internal.InternalMessageTarget;
import org.libermundi.theorcs.chronicles.model.message.internal.ToTarget;
import org.libermundi.theorcs.chronicles.model.sheet.Sheet;
import org.libermundi.theorcs.chronicles.model.sheet.SheetPage;
import org.libermundi.theorcs.chronicles.model.sheet.SheetTemplate;
import org.libermundi.theorcs.core.config.SearchServicePostProcessor;
import org.libermundi.theorcs.core.dao.hibernate.EntityFactoryPostProcessor;
import org.libermundi.theorcs.core.model.Searchable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import com.google.common.collect.Lists;

@Configuration
@ImportResource({
		"classpath:META-INF/spring/chronicles-context.xml",
		"classpath:META-INF/spring/chronicles-acl.xml"
})
public class ChronicleConfiguration {
	
	@Bean
	public static EntityFactoryPostProcessor chronicleEntityFactoryPostProcessor() {
		EntityFactoryPostProcessor efpp = new EntityFactoryPostProcessor();
		List<Class<?>> managedClasses =Lists.newArrayList();

			managedClasses.add(Game.class);
			managedClasses.add(Chronicle.class);
			managedClasses.add(Persona.class);
			managedClasses.add(Faction.class);
			managedClasses.add(AddressBook.class);
			managedClasses.add(Sheet.class);
			managedClasses.add(SheetPage.class);
			managedClasses.add(GameSystem.class);
			managedClasses.add(SheetTemplate.class);
			managedClasses.add(SavedInformations.class);
			managedClasses.add(InternalMessage.class);
			managedClasses.add(InternalMessageTarget.class);
			managedClasses.add(ToTarget.class);
			managedClasses.add(CcTarget.class);
			managedClasses.add(BccTarget.class);
			managedClasses.add(ForumThread.class);
			managedClasses.add(ForumMessage.class);
			managedClasses.add(ForumMember.class);
			
		efpp.setManagedClasses(managedClasses);
		
		return efpp;
	}
	
	@Bean
	public static SearchServicePostProcessor chronicleSearchServicePostProcessor() {
		SearchServicePostProcessor ssfpp = new SearchServicePostProcessor();
		List<Class<? extends Searchable>> searchableEntities = Lists.newArrayList();
			searchableEntities.add(Persona.class);
			searchableEntities.add(InternalMessage.class);
			searchableEntities.add(Chronicle.class);
			
		ssfpp.setSearchableClasses(searchableEntities);
		
		return ssfpp;
	}

}
