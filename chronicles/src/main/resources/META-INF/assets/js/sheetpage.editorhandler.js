var SheetEditorHandler = {
	handleAction: function(action,el,event) {
		switch(action){
			case 'add' :
				SheetEditorHandler.addSheetContainer(el,event.pageX,event.pageY);
				break;
			case 'cut' :
				SheetEditorHandler.cutSheetContainer(el);
				break;
			case 'copy' :
				SheetEditorHandler.copySheetContainer(el);
				break;
			case 'paste' :
				SheetEditorHandler.pasteSheetContainer(el,event.pageX,event.pageY);
				break;
			case 'delete' :
				SheetEditorHandler.deleteSheetContainer(el);
				break;
			case 'edit':
				ContainerEditorHandler.editContainer(el);
				break;	
			case 'settings':
				ContainerEditorHandler.editSettings(el);
				break;	
			case 'quit' :
				break;
			default:
				alert("Unsupported Operation : " + action);
				break;			
		};
	},
	
	addSheetContainer: function(el,posX,posY) {
		// Requete Ajax pour ajouter le nouveau Container dans la Page
		var settings = SheetGridSettings;
		var offset = el.offset();
		var cX = parseInt(posX-offset.left);
		var cY = parseInt(posY-offset.top);
		$.ajax({
			url:settings.baseurl.replace(/replace_event/i,"addcontainer"),
			type:"POST",
			data:{
				x:cX,
				y:cY
			},
			success:function(response) {
				SheetPageDisplayManager.displayNewContainer(response.content,cX,cY);
				$.tapestry.utils.loadScriptsInReply(response);
			},
			error:function(response){
				console.error('Ajax request failed');
			}
		});
	},
	
	pasteSheetContainer: function(el,posX,posY) {
		// Requete Ajax pour ajouter le nouveau Container dans la Page
		var settings = SheetGridSettings;
		$.ajax({
			url:settings.baseurl.replace(/replace_event/i,"pastecontainer"),
			type:"POST",
			success:function(response) {
				SheetPageDisplayManager.pasteContainer(response[0],el,posX,posY);
				$('.contextual').each(function(index,element){
					var old = $(element).attr('cm-enabled');
					$(element).attr('cm-enabled',old.replace(",paste",""));
				});
			},
			error:function(response){
				console.error('Ajax request failed');
			}
		});
	},
	
	deleteSheetContainer: function(el) {
		// Requete Ajax pour ajouter le nouveau Container dans la Page
		var settings = SheetGridSettings;
		var processDelete = function(){
			$(settings.deleteZoneModal).modal('hide');
			$.ajax({
				url:settings.baseurl.replace(/replace_event/i,"deletecontainer"),
				type:"POST",
				data:{
					uid:$(el).attr('id')
				},
				success:function(response) {
					SheetPageDisplayManager.removeContainer($(el).attr('id'));
				},
				error:function(response){
					console.error('Ajax request failed');
				}
			});
		};
		
		$(settings.deleteZoneModal).find(".btn-primary").click(function(){
			processDelete.call();
		})
		
		$(settings.deleteZoneModal).modal('show');
	},	
	
	cutSheetContainer: function(el) {
		// Requete Ajax pour ajouter le nouveau Container dans la Page
		var settings = SheetGridSettings;
		$.ajax({
			url:settings.baseurl.replace(/replace_event/i,"cutcontainer"),
			type:"POST",
			data:{
				uid:$(el).attr('id')
			},
			success:function(response) {
				SheetPageDisplayManager.removeContainer($(el).attr('id'));
				$('.contextual').each(function(index,element){
					var old = $(element).attr('cm-enabled');
					$(element).attr('cm-enabled',old+",paste");
				});
			},
			error:function(response){
				console.error('Ajax request failed');
			}
		});
	},	
	
	copySheetContainer: function(el) {
		// Requete Ajax pour ajouter le nouveau Container dans la Page
		var settings = SheetGridSettings;
		$.ajax({
			url:settings.baseurl.replace(/replace_event/i,"copycontainer"),
			type:"POST",
			data:{
				uid:$(el).attr('id')
			},
			success:function(response) {
				$('.contextual').each(function(index,element){
					var old = $(element).attr('cm-enabled');
					$(element).attr('cm-enabled',old+",#paste");
				});
			},
			error:function(response){
				console.error('Ajax request failed');
			}
		});
	},	

	saveContainerLayout:function(container) {
		var settings = SheetGridSettings;
		$.ajax({
			url:settings.baseurl.replace(/replace_event/i,"saveContainerLayout"),
			type:"POST",
			data:{
				uid:container.attr('id'),
				x:parseInt(container.position().left),
				y:parseInt(container.position().top),
				width:parseInt(container.width()),
				height:parseInt(container.height())
			},
			error:function(response){
				console.error('Ajax request failed');
			}
		});
	}
};