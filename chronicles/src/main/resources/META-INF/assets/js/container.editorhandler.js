var ContainerEditorHandler = {
	init: function() {
		$('#containerManagementZone').appendTo('body');
		$('#containerManagementZone').modal({show:false,keyboard:true});
	},
	
	editContainer:function(el,pos) {
		// Load Edit Container Form
		var settings = ContainerEditorSettings;
		var obj = this;
		$.ajax({
			url:settings.baseurl.replace(/replace_event/i,"editcontainer"),
			type:"POST",
			data:{
				uid:$(el).attr('id')
			},
			success:function(response) {
				obj.processResponse(response);
			},
			error:function(response){
				console.error('Ajax request failed');
			}
		});		
	},
	
	editSettings:function(el,pos) {
		// Load Edit Settings Form
		var settings = ContainerEditorSettings;
		var obj = this;
		$.ajax({
			url:settings.baseurl.replace(/replace_event/i,"editsettings"),
			type:"POST",
			data:{
				uid:$(el).attr('id')
			},
			success:function(response) {
				obj.processResponse(response);
			},
			error:function(response){
				console.error('Ajax request failed');
			}
		});
	},
	
	processResponse:function(response){
		var settings = ContainerEditorSettings;
		var obj = this;
		$(settings.mngtZoneSelector).html(response.content);
		$(settings.mngtZoneSelector).modal('show');
		$.tapestry.utils.loadScriptsInReply(response);
	},
	
	reloadContainer:function(uid){
		var settings = ContainerEditorSettings;
		var obj = this;
		$(settings.mngtZoneSelector).modal('hide');
		SheetPageDisplayManager.refreshContainer(uid);
	}
};