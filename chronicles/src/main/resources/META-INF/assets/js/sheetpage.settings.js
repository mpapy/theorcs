var SheetGridSettings = new function() {
	this.gridSelector="#sheetGrid";
	this.gridDivSelector="#sheetGridDiv";
	this.cmZoneSelector="#masterZone";
	this.deleteZoneModal="#confirmDelete";
	this.sheetContainerSelector=".sheetcontainer";
	this.baseurl=null;
	this.editmode=false;
	this.gridWidth=960;
	this.gridHeight=1358;
	this.containerWidth=450;
	this.containerHeight=300;
	
	this.init = function(baseurl,editmode,width,height,cWidth,cHeight){
		this.baseurl=baseurl;
		this.editmode=editmode;
		this.gridWidth=parseInt(width);
		this.gridHeight=parseInt(height);
		this.containerWidth=parseInt(cWidth);
		this.containerHeight=parseInt(cHeight);
	}
};