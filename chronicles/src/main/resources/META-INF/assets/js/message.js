// Copyright (c) 2014 the original author or authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
var MessageUtils = new function() {
    this.showOrHide = false;
    
    this.toggleCC = function(elementId) {
        var toggleBlock = $('#' + elementId);
        toggleBlock.toggle(this.showOrHide);
        if(this.showOrHide) {
            //Select all chosen containers
            var styleValue = null;
            $('.chosen-container').each(function(){
                if(styleValue == undefined) {
                    styleValue = $(this).attr("style");
                } else {
                    $(this).attr("style",styleValue);
                }
            });
            styleValue = null;
            $('input.default').each(function(){
                if(styleValue == undefined) {
                    styleValue = $(this).attr("style");
                } else {
                    $(this).attr("style",styleValue);
                }
            });
        }
        this.showOrHide = ! this.showOrHide;
    }
    
    //function disable 
    this.readOnly = function(ro) {
    	if(ro) {
	        //disable the inputs
	        $('#sender').attr('disabled', 'disabled');
	        $("#to").prop('disabled',true).trigger("liszt:updated");
	       // $("#to").attr('disabled', 'disabled');
	        $("#bcc").attr('disabled', 'disabled');
	        $("#cc").attr('disabled', 'disabled');   
	        $('#subject').attr('disabled', 'disabled');
	      
    	} else {
    		$('#sender').removeAttr('disabled');
    		//$("#to").prop('disabled',false).trigger("liszt:updated");
    		$("#to").removeAttr('disabled').trigger("liszt:updated");
    		$("#bcc").removeAttr('disabled');
    		$("#cc").removeAttr('disabled');   
    		$('#subject').removeAttr('disabled');
    		//CKEDITOR.instances.setReadOnly(true);
    	} 
    }
    
    //function reply
    this.reply = function() {
    	this.readOnly(false);
    	$("#to").val($('#sender').val());
    	$('#sender').val('');
    	$("#senderField").hide();
    	$('#subject').val('RE : ' + $('#subject'));
    }
};