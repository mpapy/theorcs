package org.libermundi.theorcs.chronicles.services.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.libermundi.theorcs.chronicles.model.Dice;

public class DiceRollerImplTest {
	
	private DiceServiceImpl srv = null;
	
	@Before public void setUp() { 
		srv = new DiceServiceImpl();
	}
	
	@Test
	public void testRoll() {
		Dice d = new Dice(new Integer(20), null);
		srv.roll(d);
		assertNotNull( "The dice was not rolled",d.getValue());
		System.out.println(d.getValue());
	}
	
	@Test
	public void testRollWithoutInit() {
		Dice d = new Dice(null, null);
		srv.roll(d);
		assertNotNull( "The dice was not rolled",d.getValue());
		System.out.println(d.getValue());
	}

	
	@Test
	public void testThrowDice(){
		Dice dice = new Dice(new Integer(20), null);
		boolean result = srv.throwDice(dice, 16, true);
		assertNotNull( "The dice was not rolled",dice.getValue());
		System.out.println(dice.getValue());
		if(dice.getValue()<=16){
			assertFalse(result);
			System.out.println("Fail");
		}else{
			assertTrue(result);
			System.out.println("Sucess");
		}
	}
}
