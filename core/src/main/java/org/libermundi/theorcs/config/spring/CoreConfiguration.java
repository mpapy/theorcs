/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.config.spring;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;

import org.libermundi.theorcs.core.dao.hibernate.GenericHibernateDaoFactory;
import org.libermundi.theorcs.core.dao.hibernate.HibernateGenericDaoFactoryImpl;
import org.libermundi.theorcs.core.util.OrcsHome;
import org.libermundi.theorcs.core.util.StringListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.ResourceUtils;

@Configuration
@ImportResource("classpath:META-INF/spring/core-context.xml")
public class CoreConfiguration {
	private static final Logger logger = LoggerFactory.getLogger(CoreConfiguration.class);
	
	private OrcsHome _path2Home;
	
	@Bean(name="ehCacheConfigLocation")
	public Resource ehCacheConfigLocation() throws FileNotFoundException {
		String confPath= getResolvedPath2Home() + "/conf/theorcs-ehcache.xml";
		try {
			URL url = ResourceUtils.getURL(confPath);
			File conf = new File(url.getFile());
			if(conf.exists() && conf.isFile()){
				logger.info("Creating CacheManager using configuration location : " + confPath);		
				return new UrlResource(url);
			}
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage(),e);
		}
		logger.warn("Creating CacheManager using default configuration location.");		
		return new UrlResource(ResourceUtils.getURL("classpath:META-INF/conf/theorcs-ehcache.xml"));
	}
	
	@Bean
	public GenericHibernateDaoFactory genericDaoFactory() {
		return new HibernateGenericDaoFactoryImpl();
	}
	
	@Bean(name="persistenceXmlLocation")
	public String persistenceXmlLocation() {
		String confPath = getResolvedPath2Home() + "/conf/persistence.xml";
		logger.info("Configuration Path for persistence.xml is : " + confPath);
		if(StringListUtils.listFirst(confPath, ":").equals("classpath")) {
			try {
				ResourceUtils.getURL(confPath); // Allow to test existence of the file
				return confPath;
			} catch (FileNotFoundException e) {
				logger.error(e.getMessage(),e);
			}
		} else if(StringListUtils.listFirst(confPath, ":").equals("file")){
			File conf = new File(StringListUtils.listLast(confPath, ":"));
			if(conf.exists() && conf.isFile()) {
				return confPath;
			}
		}
		logger.warn("No persistence.xml file has been found. Using Default Configuration Path.");
		return "classpath:META-INF/conf/persistence.xml";
	}
	
	public String getResolvedPath2Home(){
		if(_path2Home == null) {
			_path2Home = new OrcsHome();
		}
		return _path2Home.getPath();
	}	

}
