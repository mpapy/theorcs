/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.services;

import java.io.Serializable;
import java.util.Map;

import org.apache.tapestry5.Asset;
import org.libermundi.theorcs.core.CoreConstants;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;

/**
 * Utility class use to manage various aspect of the Page Rendering
 * 
 * @author Martin Papy
 *
 */
public class PageRenderSupport implements Serializable {
	private static final long serialVersionUID = -3992648252600204643L;

	private String _keywords;
	private String _pageTitle;
	private String _description;
	private Asset _favicon;
	
	private Map<String, String> _properties = Maps.newHashMap();

	/**
	 * 
	 * @return the page Title
	 */
	public String getPageTitle() {
		if(Strings.isNullOrEmpty(_pageTitle)) {
			return CoreConstants.PRODUCT_NAME;
		}
		return _pageTitle;
	}
	
	public void setPageTitle(String pageTitle) {
		_pageTitle = pageTitle;
	}
	
	/**
	 * 
	 * @return the page keywords
	 */
	public String getPageKeywords() {
		return _keywords;
	}
	public void setPageKeywords(String keywords) {
		_keywords = keywords;
	}
	
	/**
	 * @return the page description
	 */
	public String getPageDescription() {
		return _description;
	}
	/**
	 * @param description the description to set
	 */
	public void setPageDescription(String description) {
		_description = description;
	}

	/**
	 * @return the Favicon
	 */
	public Asset getFavicon() {
		return _favicon;
	}
	
	public void setFavicon(Asset favicon){
		_favicon = favicon;
	}
	
	public void setProperty(String key, String value) {
		_properties.put(key, value);
	}

	public String getProperty(String key) {
		if(_properties.containsKey(key)){
			return _properties.get(key);
		}
		return "";
	}
	
}
