/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.services.assets;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.upload.services.UploadedFile;

/**
 * @author Martin Papy
 *
 */
public interface FileManager {
	public Asset save(UploadedFile uploadedFile);
	
	public Asset save(UploadedFile uploadedFile, boolean makeUnique);
	
	public Asset save(UploadedFile uploadedFile, FileType fileType);
	
	public Asset save(UploadedFile uploadedFile, boolean makeUnique, FileType fileType);
	/**
	 * Delete the specified file and also the associated cache folder
	 * @param path
	 */
	public void delete(String relativePath);
	
	/**
	 * Delete the specified file and also the associated cache folder
	 * @param path
	 */
	public void delete(File file);

	/**
	 * Check the existence of the File base on his relativePath (relative to the base path
	 * of the FileManager )
	 * 
	 * @param relativePath
	 * @return true if the file exists
	 */
	public boolean exists(String relativePath);
	
	/**
	 * Creates a new Folder
	 * @param newDir
	 */
	public void createFolder(File newDir);
	
	/**
	 * @param file
	 * @return
	 */
	public long getFileSize(File path);
	
	/**
	 * @param dir
	 * @return
	 */
	long getDirSize(File dir);
	/**
	 * @param file
	 * @return
	 */
	public boolean isSpecialDir(File file);
	
	/**
	 * @return
	 */
	public Asset getAsset(String relativePath);
	
	
	/**
	 * @param file
	 * @return
	 */
	public Asset getAsset(File file);
	
	
	public String getBasePath();
	
	/**
	 * @param targetFile
	 * @param futureFile
	 * @throws IOException 
	 */
	public void rename(File targetFile, File futureFile) throws IOException;
	
	/**
	 * @param fileTarget
	 * @param futureFile
	 * @throws IOException 
	 */
	public void copy(File fileTarget, File futureFile) throws IOException;
	
	/**
	 * @param newFile
	 * @param object
	 * @throws IOException 
	 */
	public void createFile(File newFile, ByteArrayOutputStream object) throws IOException;

	/**
	 * @param value
	 * @param dirCurrent
	 * @param any
	 */
	public Asset save(UploadedFile uploadedFile, File targetDir, FileType fileType);
}
