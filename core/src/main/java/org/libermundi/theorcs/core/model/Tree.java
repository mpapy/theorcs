package org.libermundi.theorcs.core.model;

import java.util.List;

import org.libermundi.theorcs.core.exceptions.NodeNotFoundException;

/**
 * Represents a Tree of Objects of generic type T. The Tree is represented as
 * a single rootElement which points to a List<Node<T>> of children. There is
 * no restriction on the number of children that a particular node may have.
 * This Tree provides a method to serialize the Tree into a List by doing a
 * pre-order traversal. It has several methods to allow easy updation of Nodes
 * in the Tree.
 */
public interface Tree<T extends NodeData> {
    /**
     * Return the root Node of the tree.
     * @return the root element.
     */
    public Node<T> getRootElement();
 
    /**
     * Set the root Element for the tree.
     * @param rootElement the root element to set.
     */
    public void setRootElement(Node<T> rootElement);
     
    /**
     * Returns the Tree<T> as a List of Node<T> objects. The elements of the
     * List are generated from a pre-order traversal of the tree.
     * @return a List<Node<T>>.
     */
    public List<Node<T>> toList();
    
    /**
     * Returns a particular Node in the Tree, based on the Id of the Node
     * @return a Node
     * @throws NodeNotFoundException
     */
    public Node<T> getElement(String id) throws NodeNotFoundException;

}