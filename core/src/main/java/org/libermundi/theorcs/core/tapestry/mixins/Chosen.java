/*
 * Copyright (c) 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.mixins;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.Field;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.InjectContainer;
import org.apache.tapestry5.annotations.MixinAfter;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;

import com.google.common.base.Strings;

/**
 * @author Martin Papy
 *
 */
@Import(
		stylesheet="${core.styles}/jquery.chosen.css"
	)
@MixinAfter
public class Chosen {
	@InjectContainer
	private Field _field;
	
	@Parameter(defaultPrefix=BindingConstants.MESSAGE,allowNull=true)
	private String _placeHolder;
	
	@Parameter(defaultPrefix=BindingConstants.MESSAGE,allowNull=true,value="core.chosen.noresult")
	private String _noResultMessage;	
	
	@Environmental
	private JavaScriptSupport _jsSupport;
	
	@BeginRender
	public void beginRender(MarkupWriter writer) {
		if(!Strings.isNullOrEmpty(_placeHolder)) {
			writer.attributes("data-placeholder",_placeHolder);
		}
	}
	
	@AfterRender
	public void afterRender() {
		_jsSupport.require("chosen").with(_field.getClientId(),_noResultMessage);
	}
}
