/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core;


/**
 * Definition of Constants 
 * @author Martin Papy
 *
 */
public final class CoreConstants {
	private CoreConstants() {}
	public static final String TAPESTRY_MAPPING="orcscore";
	public static final String CHARACTER_ENCODING="UTF-8";
	public static final String EMAIL_DEFAULT_FROM = "core.email.default.from";
	public static final String ORGANIZATION_NAME="core.organisation.name";
	public static final String SITE_URL="core.site.url";
	public static final String CKEDITOR_CUSTOM_CONFIG="core.ckeditor.custom_config";
	
	
	public static final String MIMETYPE_JSON="application/json";
	public static final String PRODUCT_NAME = "TheORCS - The Online RPG Chronicle System";
	
	public static final String UPLOAD_MAX_SIZE="core.upload.file-maxsize";
	public static final String FILEVALIDATOR_ALLOWED_MISC_EXT="core.filevalidator.allowed-misc-ext";
	public static final String FILEVALIDATOR_ALLOWED_IMG_EXT="core.filevalidator.allowed-img-ext";
	public static final String FILEVALIDATOR_ALLOWED_DOC_EXT="core.filevalidator.allowed-doc-ext";
	public static final String FILEVALIDATOR_ALLOWED_VID_EXT="core.filevalidator.allowed-vid-ext";
	public static final String FILEVALIDATOR_ALLOWED_SND_EXT="core.filevalidator.allowed-snd-ext";
	public static final String HOME_PAGE = "core.pages.home";
	public static final String PRODUCTION_MODE = "core.production_mode";
}

