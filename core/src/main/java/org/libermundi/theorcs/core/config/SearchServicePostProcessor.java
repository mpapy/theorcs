/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.config;

import java.util.List;

import org.libermundi.theorcs.core.model.Searchable;
import org.libermundi.theorcs.core.services.SearchServices;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * 
 * Hibernate Search entity contribution class. For each module, declare this class to
 * contribute more managed entities.
 * 
 */
public class SearchServicePostProcessor implements BeanPostProcessor {
    private List<Class<? extends Searchable>> _searchableClasses;

    public List<Class<? extends Searchable>> getManagedClasses() {
        return _searchableClasses;
    }

    @Required
    public void setSearchableClasses(List<Class<? extends Searchable>> searchableClasses) {
        _searchableClasses = searchableClasses;
    }

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
	 */
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
	 */
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if( bean instanceof SearchServices) {
			SearchServices searchServices = (SearchServices)bean;
			searchServices.addSearchableEntities(_searchableClasses);
		}
		return bean;
	}

}
