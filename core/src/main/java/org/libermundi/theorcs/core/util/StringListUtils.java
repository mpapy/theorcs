/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * String List utilities class
 *
 */
public final class StringListUtils {
	private StringListUtils() {}
    
    public static final String DEFAULT_DELIMITER = ",";
    public static String listLast(String list) {
        return listLast(list, DEFAULT_DELIMITER);
    }
    
    public static String listLast(String list, String delimiter) {
        if(list.isEmpty()) {
            return null;
        }

        List<String> vList = stringToList(list, delimiter);
        return vList.get(vList.size()-1);
    }
    
    public static String listFirst(String list) {
        return listFirst(list, DEFAULT_DELIMITER);
    }
    
    public static String listFirst(String list, String delimiter) {
        if(list==null || list.isEmpty()) {
            return "";
        }
        List<String> vList = stringToList(list, delimiter);
        return vList.get(0);
    }
    
    public static boolean listFind(String list, String item) {
        return listFind(list, item, DEFAULT_DELIMITER);
    }
    
    public static boolean listFind(String list, String item, String delimiter) {
        if(list.isEmpty()) {
            return false;
        }
        
        List<String> vList = stringToList(list, delimiter);
        for(int i=0; i<vList.size(); i++) {
            if(vList.get(i).equals(item)) {
                return true;
            }
        }
            
        return false;
    }
    
    public static String listAppend(String list, String element) {
        return listAppend(list,element,DEFAULT_DELIMITER);
    }
    
    public static String listAppend(String list, String element, String delimiter) {
    	List<String> v = stringToList(list, delimiter);
        v.add(element);
        StringBuffer newList= new StringBuffer(); 
        for (int i = 0; i < v.size(); i++) {
            newList.append(v.get(i));
            if (i != v.size()-1) {
            	newList.append(delimiter);
            }
        }
        return newList.toString();
    }
    
    public static List<String> stringToList(String aString) {
        return stringToList(aString, DEFAULT_DELIMITER);
    }

	public static List<String> stringToList(String aString, String delimiter) {
    	List<String> v = new ArrayList<>();
        if (aString != null) {
            String[] arrString=aString.split(Pattern.quote(delimiter));
            for(int i=0; i < arrString.length; i++){
                if(arrString[i].length()>0) {
                    v.add(arrString[i]);
                }
            }
        }
        return v;
    }
    
    public static List<String> aggregate(List<String> list1, List<String> list2) {
        List<String> list = new ArrayList<>();
        if (list1 != null) {
            list.addAll(list1);
        } 
        if (list2 != null){
            list.addAll(list2);
        }
        return list;
    }
    
    public static String listDeleteAt(String list, int pos) {
    	return listDeleteAt(list, pos, DEFAULT_DELIMITER);
    }

    public static String listDeleteAt(String list, int pos, String delimiter) {
    	List<String> v = stringToList(list, delimiter);
    	v.remove(pos);
    	return StringUtils.fromListToStr(v, delimiter);
    }
    
    public static int listLenght(String list) {
    	return listLenght(list, DEFAULT_DELIMITER);
    }

	public static int listLenght(String list, String delimiter) {
		List<String> v = stringToList(list, delimiter);
		return v.size();
	}
}

