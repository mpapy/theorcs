package org.libermundi.theorcs.core.tapestry.services;

import java.util.Locale;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.services.PersistentLocale;
import org.libermundi.theorcs.core.tapestry.BasePage;

public interface AppHelper {
    /**
     * Gets the event link.
     *
     * @param pageName the page name
     * @param eventName the event name
     * @param context the context
     * @return the event link
     */
	Link getEventLink(String pageName, String eventName, Object... context);
	<T extends BasePage> Link getEventLink(Class<T> pageName, String eventName, Object... context);

    /**
     * Gets the event link.
     *
     * @param pageName the page name
     * @param eventName the event name
     * @return the event link
     */
    Link getEventLink(String pageName, String eventName);
	<T extends BasePage> Link getEventLink(Class<T> pageName, String eventName);
    
    /**
     * Gets the Locale either from the Request Header or from the {@link PersistentLocale} service
     * @return the {@link Locale} currently used
     */
    Locale getCurrentLocale();
    
    Link getPageLink(String pageName);
    
    <T extends BasePage> Link getPageLink(Class<T> pageClass);
    
    <T extends BasePage> String getPageURI(Class<T> pageClass);

    <T extends BasePage> String getPageURI(Class<T> pageClass, Object... context);

    Link getPageLink(String pageName, Object... context);

    <T extends BasePage> Link getPageLink(Class<T> pageClass, Object... context);
    <T extends BasePage> String getPageNameFromClass(Class<T> pageClass);
    
    String getCurrentPageUrl(ComponentResources ressources);

	String getBaseUrlEventLink(ComponentResources resources);

	String getRootUrl();
	
	<T extends BasePage> String getPageId(BasePage page);
	
	<T extends BasePage> String getPageId(Class<T> page);
	
}
