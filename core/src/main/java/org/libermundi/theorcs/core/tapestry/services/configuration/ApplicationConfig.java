package org.libermundi.theorcs.core.tapestry.services.configuration;

public interface ApplicationConfig {
	
	Object getObject(String key);

	Object getObject(String key, Object defaultValue);
	
	String getString(String key);
	
	String getString(String key, String defaultValue);

	boolean getBoolean(String key);
	
	boolean getBoolean(String key, boolean defaultValue);
	
	int getInteger(String key);

	int getInteger(String key, int defaultValue);
}
