// [LICENCE-HEADER]
//

package org.libermundi.theorcs.core.model.base;

import java.io.Serializable;

import org.libermundi.theorcs.core.dao.GenericDao;

/**
 * Interface marks class which have default flag. DAO will check if there is only
 * one default object using <code>getExample()</code> method.
 *
 */

public interface Defaultable<I extends Serializable> extends Identifiable<Serializable> {
    /**
     * Property which represents default flag.
     */
    static final String PROP_DEFAULT = "default";
        
    /**
     * Check if object is default one.
     *
     * @return true when object is default one
     */
    boolean isDefault();

    /**
     * Set object as default one.
     *
     * @param default value of default flag
     * @see #getExample()
     */
    void setDefault(boolean defaulz);

    /**
     * Get examplary object using in <code>GenericDao.setAsDefault()</code> method.
     *
     * Examplary object should have setted only this properties which are commons in group
     * with one default object, for example parent or category.
     *
     * If method returns null none of entities will be changed into not default.
     *
     * @see GenericDao#setAsDefault(Defaultable)
     * @return examplary object
     */
    Identifiable<I> getExample();
}
