package org.libermundi.theorcs.core.tapestry.services.assets;
import java.io.InputStream;

public class GIFInline extends InlineStreamResponse {
    public GIFInline(InputStream is, String... args) {
            super(is, args);
            this.contentType = "image/png";
            this.extension = "png";
    }
}