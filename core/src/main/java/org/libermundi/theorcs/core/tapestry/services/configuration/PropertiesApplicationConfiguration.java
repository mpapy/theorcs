package org.libermundi.theorcs.core.tapestry.services.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.libermundi.theorcs.core.exceptions.ApplicationConfigurationException;
import org.libermundi.theorcs.core.util.IOHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertiesApplicationConfiguration extends AbstractApplicationConfiguration {
	private static final Logger logger = LoggerFactory.getLogger(PropertiesApplicationConfiguration.class);
	
	private final Properties _appConfig = new Properties();
	
	public PropertiesApplicationConfiguration(List<String> propertiesPaths) {
		if(logger.isDebugEnabled()){
			logger.debug("Building propertie based Application Configuration");
		}
		fillProperties(_appConfig, buildProperties(propertiesPaths));
	}

	@Override
	public Object getObject(String key, Object defaultValue) {
		Object result;
		if(_appConfig.containsKey(key)) {
			result = _appConfig.get(key);
		} else {
			result = defaultValue;
		}
		if(result!=null){
			return result;
		}
		throw new ApplicationConfigurationException("The Key '" + key + "' is not defined in the Application Configuration, nor has a default value");
	}
	
	/*
	 * Private Methods
	 */
	
	private Properties buildProperties(List<String> propertiesPaths) {
		Properties result = new Properties();
		for (Iterator<String> iterator = propertiesPaths.iterator(); iterator.hasNext();) {
				InputStream propFIS = null;
				try {
					propFIS = IOHelper.getInputStream(iterator.next());
				} catch (IOException ex) {
					logger.error(ex.getMessage());
				}
				
				if(propFIS != null) {
					try {
						Properties p = new Properties();
						p.load(propFIS);
						Enumeration<Object> e = p.keys();
						while(e.hasMoreElements()){
							String key = (String)e.nextElement();
							String value = p.getProperty(key);
							if(logger.isDebugEnabled()){
								if(result.containsKey(key)) {
									logger.debug("Configuration Key '{}' is overrided with value : '{}'",key,value);
								} else {
									logger.debug("Configuration Key '{}' is defined with value : '{}'",key,value);
								}
							}
							result.setProperty(key, value);
						}
						propFIS.close();
					} catch (IOException exception) {
						logger.error(exception.getMessage());
					} finally {
						try {
							propFIS.close();
						} catch (IOException e1) {
							logger.error(e1.getMessage());
						}
					}
				}


		}
		return result;
	}
	
	private static void fillProperties(Properties base, Properties in) {
		Enumeration<Object> e = in.keys();
		while(e.hasMoreElements()){
			String key = (String)e.nextElement();
			base.setProperty(key, in.getProperty(key));
		}
	}
	
}
