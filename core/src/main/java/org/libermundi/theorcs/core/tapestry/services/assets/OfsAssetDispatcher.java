/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.services.assets;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.apache.tapestry5.services.Dispatcher;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.Response;
import org.apache.tapestry5.services.assets.AssetRequestHandler;

import com.google.common.base.Strings;

public class OfsAssetDispatcher implements Dispatcher {

	private final AssetRequestHandler _ofsHandler;
	
	private final String _contextPath;

    public OfsAssetDispatcher(
    		@InjectService("OfsAssetRequestHandler")
    		AssetRequestHandler assetRequestHandler,
    		
    		@Inject @Symbol(SymbolConstants.CONTEXT_PATH)
    		String contextPath) {
        this._ofsHandler = assetRequestHandler;
        this._contextPath = contextPath;
    }

    /*
     * (non-Javadoc)
     * @see org.apache.tapestry5.services.Dispatcher#dispatch(org.apache.tapestry5.services.Request, org.apache.tapestry5.services.Response)
     */
    @Override
	public boolean dispatch(Request request, Response response) throws IOException {
        String path = request.getPath();
        
        String virtualPath = OfsFilesUtils.getPrefixedPathFromUrl(_contextPath, path);
        
        if(Strings.isNullOrEmpty(virtualPath)){
        	return false;
        }

        boolean handled = _ofsHandler.handleAssetRequest(request, response, virtualPath);
    	
        if (!handled) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, path);
        }
       
        return true;
    }

}
