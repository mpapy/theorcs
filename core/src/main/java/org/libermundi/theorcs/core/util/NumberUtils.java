/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.google.common.base.Strings;

/**
 * <Briefly describing the purpose of the class/interface...>
 *
 */
public final class NumberUtils extends org.apache.commons.lang.math.NumberUtils{
    
    private NumberUtils(){}
    
    public static <T extends Number> List<T> fromStrToList(String source, String seperator, Class<T> target){
        List<T> list = new ArrayList<>();
        if (!Strings.isNullOrEmpty(source)) {
            StringTokenizer tokenizer = new StringTokenizer(source, seperator);
            while (tokenizer.hasMoreTokens()) {
                String tmp = tokenizer.nextToken();
                if (isNumber(tmp)){
                    try {
                        Constructor<T> constructor = target.getConstructor(String.class);
                        list.add(constructor.newInstance(tmp));
                    } catch (SecurityException e) {
                        return list;
                    } catch (NoSuchMethodException e) {
                        return list;
                    } catch (IllegalArgumentException e) {
                        return list;
                    } catch (InstantiationException e) {
                        return list;
                    } catch (IllegalAccessException e) {
                        return list;
                    } catch (InvocationTargetException e) {
                        return list;
                    }
                }
            }
        }
        return list;
    }
    
    public static <T extends Number> String fromListToStr(List<T> source, String seperator) {
        StringBuffer result = new StringBuffer();
        if (source != null && source.size() > 0) {
            for (T aSource : source) {
                if (result.length() > 0) {
                    result.append(seperator);
                }
                result.append(aSource);
            }
        }
        return result.toString();
    }
    
    public static String numberFormat(long number, String suffix) {
    	DecimalFormat myFormatter = new DecimalFormat("###,###");
        String output = myFormatter.format(number);
        if(suffix !=null && !suffix.trim().equals("")) {
        	output+= " "+suffix;
        }
        return output;
    }
}
