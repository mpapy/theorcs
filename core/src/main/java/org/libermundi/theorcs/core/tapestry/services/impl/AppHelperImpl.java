/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.services.impl;

import java.util.Locale;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.internal.EmptyEventContext;
import org.apache.tapestry5.internal.services.ArrayEventContext;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.apache.tapestry5.ioc.services.TypeCoercer;
import org.apache.tapestry5.services.ComponentClassResolver;
import org.apache.tapestry5.services.ComponentEventLinkEncoder;
import org.apache.tapestry5.services.ComponentEventRequestParameters;
import org.apache.tapestry5.services.PageRenderRequestParameters;
import org.apache.tapestry5.services.PersistentLocale;
import org.apache.tapestry5.services.Request;
import org.libermundi.theorcs.core.tapestry.BasePage;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppHelperImpl implements AppHelper {
	private static final Logger logger = LoggerFactory.getLogger(AppHelperImpl.class);
	
	private static final String PACKAGE_BASE="org.libermundi.theorcs.";
	
	@Inject
	private ComponentEventLinkEncoder _componentEventLinkEncoder;
	
	@Inject 
	private ComponentClassResolver _componentClassResolver;
	
    @Inject
    private TypeCoercer _typeCoercer;
    
	@Inject
	private PersistentLocale _locale;
	
	@Inject
	private Request _request;
	
	@Inject
	@Symbol(SymbolConstants.CONTEXT_PATH)
	private String _contextPath;
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getEventLink(java.lang.String, java.lang.String, java.lang.Object[])
	 */
    @Override
	public Link getEventLink(String pageName, String eventName, Object... context) {
        ComponentEventRequestParameters parameters =
        	new ComponentEventRequestParameters(pageName, pageName, "", eventName, new EmptyEventContext(), new ArrayEventContext(_typeCoercer, context));
                
	    Link eventLink = _componentEventLinkEncoder.createComponentEventLink(parameters, false);
	    return eventLink;
    }
    
    /*
     * (non-Javadoc)
     * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getEventLink(java.lang.Class, java.lang.String, java.lang.Object[])
     */
	@Override
	public <T extends BasePage> Link getEventLink(Class<T> pageName, String eventName, Object... context){
		return getEventLink(getPageNameFromClass(pageName), eventName, context);
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getEventLink(java.lang.String, java.lang.String)
	 */
	@Override
    public Link getEventLink(String pageName, String eventName) {
        ComponentEventRequestParameters parameters = 
        	
        	new ComponentEventRequestParameters(pageName, pageName, "", eventName, new EmptyEventContext(), new EmptyEventContext());
                
	    Link eventLink = _componentEventLinkEncoder.createComponentEventLink(parameters, false);
	    return eventLink;
    }
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getEventLink(java.lang.Class, java.lang.String)
	 */
	@Override
    public <T extends BasePage> Link getEventLink(Class<T> pageName, String eventName){
		return getEventLink(getPageNameFromClass(pageName), eventName);
	}

    /*
     * (non-Javadoc)
     * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getCurrentLocale()
     */
	@Override
    public Locale getCurrentLocale() {
    	if(_locale.isSet()) {
    		return _locale.get();
    	}
    	return _request.getLocale();
    }

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getPageLink(java.lang.String)
	 */
	@Override
	public Link getPageLink(String pageName) {
		PageRenderRequestParameters parameters = new PageRenderRequestParameters(pageName, new EmptyEventContext(), false);
		return _componentEventLinkEncoder.createPageRenderLink(parameters);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getPageLink(java.lang.String, java.lang.Object[])
	 */
	@Override
	public Link getPageLink(String pageName, Object... context) {
		PageRenderRequestParameters parameters;
		if(context.length == 1 && context[0] == null)  {
			parameters = new PageRenderRequestParameters(pageName, new EmptyEventContext(), false);
		} else {
			parameters = new PageRenderRequestParameters(pageName, new ArrayEventContext(_typeCoercer, context), false);
		}
		return _componentEventLinkEncoder.createPageRenderLink(parameters);
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getPageLink(java.lang.Class)
	 */
	@Override
	public <T extends BasePage> Link getPageLink(Class<T> pageClass) {
		return getPageLink(getPageNameFromClass(pageClass));
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getPageLink(java.lang.Class, java.lang.Object[])
	 */
	@Override
	public <T extends BasePage> Link getPageLink(Class<T> pageClass,Object... context) {
		return getPageLink(getPageNameFromClass(pageClass), context);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getPageNameFromClass(java.lang.Class)
	 */
	@Override
	public <T extends BasePage> String getPageNameFromClass(Class<T> pageClass) {
		return _componentClassResolver.resolvePageClassNameToPageName(pageClass.getName());
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getPageURI(java.lang.Class)
	 */
	@Override
	public <T extends BasePage> String getPageURI(Class<T> pageClass) {
		String uri = getPageLink(pageClass).toURI();
		if(logger.isDebugEnabled()) {
			logger.debug("Processed URI : " + uri);
		}
		return uri;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getPageURI(java.lang.Class, java.lang.Object[])
	 */
	@Override
	public <T extends BasePage> String getPageURI(Class<T> pageClass, Object... context) {
		String uri = getPageLink(pageClass, context).toURI();
		if(logger.isDebugEnabled()) {
			logger.debug("Processed URI : " + uri);
		}
		return uri;
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getCurrentPageUrl(org.apache.tapestry5.ComponentResources)
	 */
	@Override
	public String getCurrentPageUrl(ComponentResources ressources) {
		return getPageLink(ressources.getPageName()).toURI();
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getBaseUrlEventLink(org.apache.tapestry5.ComponentResources)
	 */
	@Override
	public String getBaseUrlEventLink(ComponentResources resources) {
		return resources.createEventLink("REPLACE_EVENT").toURI();
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getRootUrl()
	 */
	@Override
	public String getRootUrl() {
		return _contextPath + "/";
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getPageId(org.libermundi.theorcs.core.tapestry.BasePage)
	 */
	public <T extends BasePage> String getPageId(BasePage page) {
		return getPageId(page.getClass());
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.AppHelper#getPageId(java.lang.Class)
	 */
	public <T extends BasePage> String getPageId(Class<T> page) {
		String pageName = page.getName();
		if(pageName.startsWith(PACKAGE_BASE)) {
			return pageName.substring(PACKAGE_BASE.length()).toLowerCase();
		}
		return pageName.toLowerCase();
	}
}
