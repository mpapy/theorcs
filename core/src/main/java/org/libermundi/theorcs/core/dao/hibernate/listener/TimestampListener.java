package org.libermundi.theorcs.core.dao.hibernate.listener;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.libermundi.theorcs.core.model.base.Timestampable;

public class TimestampListener {
	
	@PreUpdate
	public void setModifiedDate(Timestampable t) {
		Date modifiedDate = new Date();
		t.setModifiedDate(modifiedDate);		
	}
	
	@PrePersist
	public void setCreatedDate(Timestampable t) {
		Date createdDate = new Date();
		t.setCreatedDate(createdDate);
		t.setModifiedDate(createdDate);		
	}
}
