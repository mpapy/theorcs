/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.services.assets;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.tapestry5.ioc.Resource;
import org.apache.tapestry5.services.AssetSource;
import org.apache.tapestry5.services.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.cache.TemplateLoader;

public class OfsTemplateLoader implements TemplateLoader {
	private static final Logger log = LoggerFactory.getLogger(OfsTemplateLoader.class);
	
	private final AssetSource _assetSource;
	
	private final Request _request;
	
	public OfsTemplateLoader(Request request, AssetSource assetSource) {
		_assetSource = assetSource;
		_request = request;
	}

	/*
	 * (non-Javadoc)
	 * @see freemarker.cache.TemplateLoader#findTemplateSource(java.lang.String)
	 */
	@Override
	public Object findTemplateSource(String name) throws IOException {
		
		Resource r = _assetSource.resourceForPath(name);
		if(r.exists()) {
			//We then need to check for Localized version of the Template
			Resource rl = r.forLocale(_request.getLocale());
			if(rl != null && rl.exists()) {
				return rl;
			}
			return r;
		}
		throw new IOException("Template not found : " + name);
	}

	/*
	 * (non-Javadoc)
	 * @see freemarker.cache.TemplateLoader#getLastModified(java.lang.Object)
	 */
	@Override
	public long getLastModified(Object templateSource) {
		Resource r = (Resource) templateSource;
    	URL url = r.toURL();
    	if(url.getProtocol().equals("jar")){
    		return -1; // The File cannot be "Modified" as it is located in a Jar
    	}

    	try {
    		File f = new File(r.toURL().toURI());
    		return f.lastModified();
    	} catch (URISyntaxException e) {
    		log.error("URISyntaxException for : " + templateSource, e);
    		return -1;
    	}
	}
	
	/*
	 * (non-Javadoc)
	 * @see freemarker.cache.TemplateLoader#getReader(java.lang.Object, java.lang.String)
	 */
	@Override
	public Reader getReader(Object templateSource, String encoding) throws IOException {
		Resource r = (Resource) templateSource;
		return new InputStreamReader(r.openStream(), encoding);
	}

	/*
	 * (non-Javadoc)
	 * @see freemarker.cache.TemplateLoader#closeTemplateSource(java.lang.Object)
	 */
	@Override
	public void closeTemplateSource(Object templateSource) throws IOException {
		// Nothing to Do
	}

}
