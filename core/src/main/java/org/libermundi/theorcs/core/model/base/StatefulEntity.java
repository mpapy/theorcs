// [LICENCE-HEADER]
//
package org.libermundi.theorcs.core.model.base;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.libermundi.theorcs.core.dao.hibernate.listener.TimestampListener;


/**
 * Entity with full status: Identifiable, Activable, Undeletable and Timestampable
 *
 */
@MappedSuperclass
@EntityListeners(TimestampListener.class)
public class StatefulEntity extends BasicEntity implements Activable, Timestampable {
    private static final long serialVersionUID = 1L;
    
    private boolean _active = Boolean.TRUE;
    private Date _createdDate;
    private Date _modifiedDate;

    /*
     * (non-Javadoc)
     * @see org.libermundi.theorcs.core.model.base.Activable#isActive()
     */
    
    @Override
    @Column(name = Activable.PROP_ACTIVE)
    public boolean isActive() {
        return _active;
    }
    
    /*
     * (non-Javadoc)
     * @see org.libermundi.theorcs.core.model.base.Activable#setActive(boolean)
     */
    
    @Override
    public void setActive(boolean active) {
        _active = active;
    }
    
    /*
     * (non-Javadoc)
     * @see org.libermundi.theorcs.core.model.base.Timestampable#getCreatedDate()
     */
    
    @Override
    @Column(name = Timestampable.PROP_CREATED_DATE)
    public Date getCreatedDate() {
        return _createdDate;
    }
    
    /*
     * (non-Javadoc)
     * @see org.libermundi.theorcs.core.model.base.Timestampable#setCreatedDate(java.util.Date)
     */
    
    @Override
    public void setCreatedDate(Date createdDate) {
    	if(createdDate != null)
    		_createdDate = new Date(createdDate.getTime());
    }
    
    /*
     * (non-Javadoc)
     * @see org.libermundi.theorcs.core.model.base.Timestampable#getModifiedDate()
     */
    
    @Override
    @Column(name = Timestampable.PROP_MODIFIED_DATE)
    public Date getModifiedDate() {
        return _modifiedDate;
    }
    
    /*
     * (non-Javadoc)
     * @see org.libermundi.theorcs.core.model.base.Timestampable#setModifiedDate(java.util.Date)
     */
    
    @Override
    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = new Date(modifiedDate.getTime());
    }
    
    /*
     * (non-Javadoc)
     * @see org.libermundi.theorcs.core.model.BasicEntity#toString()
     */
    
    @Override
    public String toString() {
        String className = getClass().getSimpleName();
        return String.format("%s{id: %s; active: %s; createdDate: %s; updatedDate: %s; hidden: %s}", 
                        className, getId(), isActive(), getCreatedDate(), getModifiedDate(), isDeleted());
    }
}
