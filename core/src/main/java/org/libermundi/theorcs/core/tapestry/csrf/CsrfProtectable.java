// Copyright 2011 The Apache Software Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package org.libermundi.theorcs.core.tapestry.csrf;

import org.apache.tapestry5.MarkupWriter;

/**
 * This interface acts as a marker interface for protectable components. The {@link org.apache.tapestry5.csrfprotection.mixins.CsrfProtected} mixin
 * uses this interface to insert a anti cross-site request forgery token into the component.
 *
 */
public interface CsrfProtectable {
	public void insertCSRFToken(MarkupWriter writer, String token);
}
