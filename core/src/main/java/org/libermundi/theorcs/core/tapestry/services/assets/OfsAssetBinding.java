/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.services.assets;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.Asset2;
import org.apache.tapestry5.internal.TapestryInternalUtils;
import org.apache.tapestry5.internal.bindings.AbstractBinding;
import org.apache.tapestry5.ioc.Location;

/**
 * <Briefly describing the purpose of the class/interface...>
 *
 */
public class OfsAssetBinding extends AbstractBinding {
    private final String _description;
    private final Asset2 _asset;

    public OfsAssetBinding(Location location, String description, Asset asset) {
        super(location);
        _description = description;
        _asset = TapestryInternalUtils.toAsset2(asset);
    }

    /*
     * (non-Javadoc)
     * @see org.apache.tapestry5.internal.bindings.AbstractBinding#getBindingType()
     */
    @Override
    public Class<?> getBindingType() {
        return Asset2.class;
    }

    /*
     * (non-Javadoc)
     * @see org.apache.tapestry5.Binding#get()
     */
	@Override
    public Object get() {
        return _asset;
    }

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return String.format("OrcsAssetBinding[%s: %s]", _description, _asset);
    }
}