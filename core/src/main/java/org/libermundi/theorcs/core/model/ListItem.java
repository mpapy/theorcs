package org.libermundi.theorcs.core.model;


public interface ListItem extends NodeData {
	/**
	 * Return the Label of the Item
	 * @return {@link String}
	 */
	String getLabel();
	void setLabel(String label);

	/**
	 * Return a Description of the Item
	 * @return {@link String}
	 */
	String getDescription();
	void setDescription(String description);
}
