/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.util;

import org.springframework.util.SystemPropertyUtils;

import com.google.common.base.Strings;

/**
 * @author Martin Papy
 *
 */
public class OrcsHome {
	public static final String ORCS_HOME_PROP = "theorcs.home";

	private static String ORCS_HOME_DEFAULT = "classpath:META-INF";

	private String _systemValue;
	
	public OrcsHome(){
		_systemValue = "${" + ORCS_HOME_PROP + "}";
	}

	public String getSystemValue(){
		return _systemValue;
	}
	
	public String getPath(){
		String result = SystemPropertyUtils.resolvePlaceholders(getSystemValue(),true);
		
		if(Strings.isNullOrEmpty(result) || result.equals(getSystemValue())){
			return ORCS_HOME_DEFAULT;
		}
		
		return result;
	}
}
