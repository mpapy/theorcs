package org.libermundi.theorcs.core.tapestry.base;

import org.apache.tapestry5.services.ApplicationInitializerFilter;
import org.libermundi.theorcs.core.util.AppContext;

public abstract class AbstractModuleInitializer implements ApplicationInitializerFilter {
	private AppContext _applicationContext;
	
	public AbstractModuleInitializer(){
		
	}
	
	public AbstractModuleInitializer(AppContext applicationContext) {
		this._applicationContext=applicationContext;
	}

	protected AppContext getApplicationContext() {
		return _applicationContext;
	}
}
