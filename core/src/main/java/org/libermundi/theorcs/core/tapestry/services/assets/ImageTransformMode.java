package org.libermundi.theorcs.core.tapestry.services.assets;
public enum ImageTransformMode {
	PROPORTIONAL, CROP, FORCE, FIT, THUMBNAIL
}
