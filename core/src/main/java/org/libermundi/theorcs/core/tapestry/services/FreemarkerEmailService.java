/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.services;

import java.util.List;
import java.util.Map;

import org.libermundi.theorcs.core.exceptions.EmailException;
import org.libermundi.theorcs.core.model.Account;

public interface FreemarkerEmailService {

	public void sendEmail(String emailId, Map<String, Object> parameters, Account to) throws EmailException;
	public void sendEmail(String emailId, Map<String, Object> parameters, List<Account> to) throws EmailException;
	public void sendEmail(String emailId, Map<String, Object> parameters, List<Account> to, List<Account> cc) throws EmailException;
	public void sendEmail(String emailId, Map<String, Object> parameters, List<Account> to, List<Account> cc, List<Account> bcc) throws EmailException;
	
}
