/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.services.assets;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.internal.services.AbstractAsset;
import org.apache.tapestry5.ioc.Resource;
import org.apache.tapestry5.services.AssetFactory;
import org.apache.tapestry5.services.Context;
import org.apache.tapestry5.services.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OfsAssetFactory implements AssetFactory {
	private static final Logger logger = LoggerFactory.getLogger(OfsAssetFactory.class);
	
	private static final String FILE_SEPARATOR = System.getProperty("file.separator");
	
	public static final String TEMP_FOLDER = "file:///" + System.getProperty("java.io.tmpdir") + FILE_SEPARATOR + "TheORCS"  + FILE_SEPARATOR;
	
	public static final String OFS_RESOURCE_FOLDER="resources" + FILE_SEPARATOR;
	
	public static final String DEFAULT_CLASSPATH_LOCATION="org" + FILE_SEPARATOR + "libermundi" + FILE_SEPARATOR + "theorcs"  + FILE_SEPARATOR;
    
	public static final String DEFAULT_CONTEXT_LOCATION="WEB-INF" + FILE_SEPARATOR + "resources" + FILE_SEPARATOR;
    
	public static final String OFS_VIRTUAL_FOLDER = "ofs";
	
	private final String _ofsAssetPrefix;

	private final Context _context;
    
    private final OfsResource _rootResource;
    private final boolean _invariant;
    private final String _localInstallRootDir;
    private final String _contextPath;
    
    public OfsAssetFactory(Request request, Context context, String localInstallRootDir, String contextPath) {
        _context = context;
        _invariant = false;
        _localInstallRootDir=sanitiseDirPath(localInstallRootDir);
        _rootResource=new OfsResource(_context, _localInstallRootDir, "");
        _contextPath = contextPath;
        _ofsAssetPrefix = "/" + OFS_VIRTUAL_FOLDER + "/";
    }
    
    public String getLocalInstallRootDir(){
    	return _localInstallRootDir;
    }
    
    /* (non-Javadoc)
     * @see org.apache.tapestry5.services.AssetFactory#createAsset(org.apache.tapestry5.ioc.Resource)
     */
    @Override
    public Asset createAsset(final Resource resource) {
    	return new AbstractAsset(_invariant) {
    	    @Override
            public Resource getResource() {
                return resource;
            }

            @Override
            public String toClientURL() {
            	return _contextPath  + _ofsAssetPrefix + convertPath(resource.getPath());
            }
        };
    }
    
    /* (non-Javadoc)
     * @see org.apache.tapestry5.services.AssetFactory#getRootResource()
     */
    @Override
    public Resource getRootResource() {
        return _rootResource;
    }
    
	private static String sanitiseDirPath(String dirPath) {
		String path = dirPath.trim();
		
		if(path.startsWith("classpath")){
			path=TEMP_FOLDER;
		}

		try {
			File dir = new File(new URI(path));
			if (!dir.exists()) {
				dir.mkdirs();
			}
			
			//Make sure the path contains a final separator
			//With Windows, the adding of FILE_SEPARATOR invalids the URI.
			//So we need to do this after using the URI object
			if (!path.endsWith(FILE_SEPARATOR)) {
				path += FILE_SEPARATOR;
			}

			return path;
		} catch (URISyntaxException e) {
			logger.error(e.getMessage(),e);
			throw new RuntimeException(e);
		}

	}
	
	private static String convertPath(String path){
		return path.replaceAll("\\\\", "/") ;
	}

}
