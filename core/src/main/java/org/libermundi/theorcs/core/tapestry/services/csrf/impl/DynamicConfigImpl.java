// Copyright 2011 The Apache Software Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package org.libermundi.theorcs.core.tapestry.services.csrf.impl;

import org.apache.tapestry5.ioc.annotations.Value;
import org.libermundi.theorcs.core.tapestry.services.csrf.CsrfProtectionModule;
import org.libermundi.theorcs.core.tapestry.services.csrf.DynamicConfig;

/**
 * Implements the {@link DynamicConfig} service. See the interface description for details.
 * @see DynamicConfig
 */
public class DynamicConfigImpl implements DynamicConfig {
	private final String antiCsrfmode;
	private final String antiCsrfTokenType;
	private final String antiCsrfTokenPersistence;
	
	public DynamicConfigImpl(@Value("${" + CsrfProtectionModule.ANTI_CSRF_MODE + "}") String antiCsrfMode,
			@Value("${" + CsrfProtectionModule.ANTI_CSRF_TOKENTYPE + "}") String antiCsrfTokenType,
			@Value("${" + CsrfProtectionModule.ANTI_CSRF_TOKENPERSISTENCE + "}") String antiCsrfTokenPersistence){
		this.antiCsrfmode = antiCsrfMode;
		this.antiCsrfTokenPersistence = antiCsrfTokenPersistence;
		this.antiCsrfTokenType = antiCsrfTokenType;
	}

	@Override
	public String getAntiCsrfmode() {
		return antiCsrfmode;
	}

	@Override
	public String getAntiCsrfTokenType() {
		return antiCsrfTokenType;
	}

	@Override
	public String getAntiCsrfTokenPersistence() {
		return antiCsrfTokenPersistence;
	}
	
	
}
