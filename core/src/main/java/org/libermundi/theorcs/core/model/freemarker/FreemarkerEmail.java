/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.model.freemarker;

import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.libermundi.theorcs.core.exceptions.EmailException;
import org.libermundi.theorcs.core.model.Email;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;


public interface FreemarkerEmail extends Email {
	/**
	 * Convenient method to get a {@link SimpleMailMessage} initialized with the parameters
	 * @param mimeMsg 
	 * @param parameters Parameters to be passed to the FreeMarker Engine
	 * @return {@link SimpleMailMessage} with default settings
	 * @throws EmailException
	 */
	MimeMessageHelper getMailMessage(MimeMessage mimeMessage, Map<String, Object> parameters) throws EmailException;	

}
