// [LICENCE-HEADER]
//

package org.libermundi.theorcs.core.tapestry.services;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import org.apache.tapestry5.ioc.Resource;

/**
 * The Interface FreeMarkerService. This interface provides template engine services.
 */
public interface FreeMarkerService extends Serializable {
    
    /** The CONFIG_RESOURCE_KEY. */
    static final String CONFIG_RESOURCE_KEY = "freemarker.configuration";
    

    /**
     * Merge data with resource. merge data from parameter map with template resource into given output stream.
     *
     * @param template the template
     * @param outputStream the output stream
     * @param rootMap the root map
     */
    void mergeDataWithResource(Resource template, OutputStream outputStream, Object rootMap);

    /**
     * Merge data with stream.
     *
     * @param templateStream the template stream
     * @param outputStream the output stream
     * @param rootMap the root map
     */
    void mergeDataWithStream(InputStream templateStream, OutputStream outputStream, Object rootMap);
}
