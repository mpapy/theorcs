/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.dao.hibernate;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.libermundi.theorcs.core.model.base.Identifiable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HibernateGenericDaoFactoryImpl implements GenericHibernateDaoFactory {
	private static final Logger logger = LoggerFactory.getLogger(HibernateGenericDaoFactoryImpl.class);
    protected EntityManager _entityManager;
    
    public HibernateGenericDaoFactoryImpl() {
		super();
	}

    /**
     * Set entity manager.
     *
     * @param entityManager entity manager
     */
    @PersistenceContext
    public final void setEntityManager(EntityManager entityManager) {
        _entityManager = entityManager;
    }


	@Override
	public <T extends Identifiable<I>, I extends Serializable> HibernateGenericDao<T, I> create(Class<T> type) {
    	if(logger.isDebugEnabled()) {
    		logger.debug("Building an HibernateGenericDao based on Type : " + type.getName());
    	}
		return new HibernateGenericDao<>(type,_entityManager); 
	}


}
