/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.services.assets;

import java.io.File;

import org.apache.tapestry5.Asset;

/**
 * @author Martin Papy
 *
 */
public interface ImageCache {
	
	public boolean isImage(String filename);
	
	public boolean isImage(File file);
	
	public boolean isImage(Asset asset);
	
//	public String getImageUrlInCache(String originalImageName,int width, int height);
//
//	public String getImageUrlInCache(Asset originalImage,int width, int height);
	
//	public void saveImageInCache(BufferedImage image, String relativePath);
	
//	public String saveImageInCache(File image, int width, int height);
	/**
	 * @param fileToDelete
	 */
	public void removeFromCache(File fileToDelete);
	
	/**
	 * @return
	 */
	String getBasePath();

	/**
	 * @param image
	 * @param width
	 * @param height
	 * @param transform 
	 * @return
	 */
	public Asset getImageAssetFromCache(Asset image, int width, int height, ImageTransformMode transform);

	/**
	 * @param path
	 * @param width
	 * @param height
	 * @param proportional
	 * @return
	 */
	public Asset getImageAssetFromCache(File path, int width, int height, ImageTransformMode transform);
}
