package org.libermundi.theorcs.core.model;

import org.apache.tapestry5.ComponentResources;

public interface LinkItem extends ListItem {
	/**
	 * Returns the URL associated with this Item
	 * @return
	 */
	String getURL();
	void setUrl(String url);
	
	/**
	 * Access to the Tapestry 5 PageName ( as defined by {@link ComponentResources} ) if any
	 * Return null is no T5 Pages is associated.
	 * @return pagename
	 */
	String getPageName();
	void setPageName(String pageName);
	

}
