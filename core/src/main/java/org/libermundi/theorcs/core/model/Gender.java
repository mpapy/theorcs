package org.libermundi.theorcs.core.model;

public enum Gender {DONTSAY, MALE, FEMALE, BOTH, OTHER}
