/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.services.impl;

import org.apache.tapestry5.ValueEncoder;
import org.apache.tapestry5.services.ValueEncoderFactory;
import org.libermundi.theorcs.core.model.base.Identifiable;
import org.libermundi.theorcs.core.services.GenericManager;
import org.libermundi.theorcs.core.tapestry.util.GenericValueEncoder;

public class GenericValueEncoderFactory<V extends Identifiable<Long>> implements ValueEncoderFactory<V> {
	private final GenericManager _manager;
	
	public GenericValueEncoderFactory(GenericManager manager) {
		_manager = manager;
	}

	@Override
	public ValueEncoder<V> create(Class<V> type) {
		return new GenericValueEncoder<>(_manager,type);
	}

}
