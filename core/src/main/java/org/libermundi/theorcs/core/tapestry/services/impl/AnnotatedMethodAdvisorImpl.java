/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.services.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.apache.tapestry5.ioc.MethodAdviceReceiver;
import org.apache.tapestry5.plastic.MethodAdvice;
import org.libermundi.theorcs.core.tapestry.annotation.AdviseMethod;
import org.libermundi.theorcs.core.tapestry.services.AnnotatedMethodAdvisor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Martin Papy
 *
 */
public class AnnotatedMethodAdvisorImpl implements AnnotatedMethodAdvisor {
	private final static Logger logger = LoggerFactory.getLogger(AnnotatedMethodAdvisorImpl.class);
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.tapestry.services.MenuServiceAdvisor#addAdvice(org.apache.tapestry5.plastic.MethodAdvice, org.apache.tapestry5.ioc.MethodAdviceReceiver)
	 */
	@Override
	public void addAdvice(MethodAdvice advice, MethodAdviceReceiver receiver) {
		logger.debug("Adding Advice [{}] to Receiver [{}]",advice,receiver);
		for (Method m : receiver.getInterface().getMethods()){
			for(Annotation a : m.getAnnotations()) {
				if(a instanceof AdviseMethod) {
					receiver.adviseMethod(m, advice);
				}
			}
		}

	}

}
