package org.libermundi.theorcs.core.tapestry.model;

public class ContextMenuElement {
	private String _action;
	
	private boolean _separator;
	
	private String _label;

	public ContextMenuElement(String action, String label, boolean separator) {
		_action = action;
		_separator = separator;
		_label = label;
	}

	public String getAction() {
		return _action;
	}

	/**
	 * Indicate whether this element should be followed by a Separator
	 * @return
	 */
	public boolean hasSeparator() {
		return _separator;
	}

	public String getLabel() {
		return _label;
	}
	
}
