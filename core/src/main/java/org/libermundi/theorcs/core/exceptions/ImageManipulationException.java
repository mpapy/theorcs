package org.libermundi.theorcs.core.exceptions;

public class ImageManipulationException extends Exception {
	private static final long serialVersionUID = 4770709785227565086L;

	public ImageManipulationException(String message) {
		super(message);
	}

	public ImageManipulationException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public ImageManipulationException(Throwable cause) {
		super(cause);
	}
}
