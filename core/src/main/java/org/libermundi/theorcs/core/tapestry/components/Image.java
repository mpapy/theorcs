/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.components;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.annotations.SupportsInformalParameters;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.AssetFactory;
import org.libermundi.theorcs.core.tapestry.services.assets.ImageCache;
import org.libermundi.theorcs.core.tapestry.services.assets.ImageTransformMode;
import org.libermundi.theorcs.core.tapestry.services.assets.OrcsFileSystem;


/**
 * @author Martin Papy
 *
 */
@SupportsInformalParameters
public class Image {
	@Parameter(required=true,name="src", defaultPrefix=BindingConstants.PROP)
	private Asset _image;
	
	@Parameter(required=false)
	private int _width;
	
	@Parameter(required=false)
	private int _height;
	
	@Parameter(required=false)
	private ImageTransformMode _transform;
	
	@Inject
	private ImageCache _imageCache;
	
    @Inject
    private ComponentResources _resources;
	
	@Inject
	@OrcsFileSystem
	private AssetFactory _assetFactory;	
	
	private Asset _cachedImage; 
	
	@SetupRender
	public void setupRender(){
		if(_transform == null){
			_transform = ImageTransformMode.PROPORTIONAL;
		}
		
		if(_width > 0 && _height > 0){
			_transform = ImageTransformMode.FORCE;
		}
		
		if(_width == 0 && _height == 0){
			throw new IllegalArgumentException("Both width and height cannot be 0");
		}

		if(_width == 0 || _height == 0){
			_transform = ImageTransformMode.PROPORTIONAL;
		}
		
		_cachedImage = _imageCache.getImageAssetFromCache(_image, _width, _height,_transform);
		
	}
	
	@BeginRender
	public boolean renderImageTag (MarkupWriter writer) {
		if(_cachedImage == null ) {
			writer.element ( "img", "src", _image.toClientURL() );
		} else {
			writer.element ( "img", "src", _cachedImage.toClientURL() );
		}
		_resources.renderInformalParameters(writer);
		
		writer.end(); //img
		return Boolean.FALSE; // No Body to render.
	}
}
