package org.libermundi.theorcs.core.exceptions;

public class ApplicationConfigurationException extends RuntimeException {
	private static final long serialVersionUID = 4221301478540476903L;
	
	public ApplicationConfigurationException(String message) {
		super(message);
	}
}
