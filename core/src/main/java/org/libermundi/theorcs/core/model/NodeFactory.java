/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.model;

import org.libermundi.theorcs.core.model.impl.NodeImpl;

/**
 * @author Martin Papy
 *
 */
public class NodeFactory {
	private NodeFactory() {}
	
	public static <T extends NodeData> Node<T> getNode() {
		return new NodeImpl<>();
	}
	
	public static <T extends NodeData> Node<T> getNode(T data){
		return new NodeImpl<>(data);
	}

	public static <T extends NodeData> Node<T> getNode(String id) {
		return new NodeImpl<>(id);
	}

	public static <T extends NodeData> Node<T> getNode(String id, T data) {
		return new NodeImpl<>(id,data);
	}
	
}
