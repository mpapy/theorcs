package org.libermundi.theorcs.core.dao.hibernate;

import org.hibernate.dialect.HSQLDialect;
import org.hsqldb.types.Types;

public class HSQLSafeDialect extends HSQLDialect {
	 
	    public HSQLSafeDialect() {
	        super();
	 
	        registerColumnType(Types.BLOB, "longvarbinary");
	        registerColumnType(Types.CLOB, "longvarchar");
	    }
}