package org.libermundi.theorcs.core.dao.exception;

import org.hibernate.HibernateException;
import org.springframework.dao.DataAccessException;

/**
 * Exception for Orcs Application about the Data Access. It extends the <code>DataAccessException</code> from Spring framework
 *
 */
public class OrcsDataAccessException extends DataAccessException {
	private static final long	serialVersionUID	= -4336363201263807426L;

	/**
	 * Constructor with a HibernateException and wrap it into a OrcsDataAccessException
	 * @param errorMsg
	 * @param exception
	 */
	public OrcsDataAccessException(String errorMsg, HibernateException exception) {
		super(errorMsg, exception);
	}

	/**
	 * @param errorMsg
	 */
	public OrcsDataAccessException(String errorMsg) {
		super(errorMsg);
	}
}
