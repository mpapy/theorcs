// [LICENCE-HEADER]
//
package org.libermundi.theorcs.core.util;

import org.hibernate.proxy.HibernateProxy;

/**
 * <Briefly describing the purpose of the class/interface...>
 * 
 */
public final class PersistenceHelper {
	private PersistenceHelper() {}

	public static <T> T deproxy(Object entity, Class<T> baseClass) throws ClassCastException {
        if (entity instanceof HibernateProxy) {
            return baseClass.cast(((HibernateProxy) entity).getHibernateLazyInitializer().getImplementation());
        }
        return baseClass.cast(entity);    
    }
    
    public static boolean isProxyed(Object entity) {
    	return entity instanceof HibernateProxy;
    }
    
}
