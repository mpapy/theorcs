/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.services.assets;

import java.util.List;

import javax.inject.Inject;

import org.apache.commons.io.FilenameUtils;
import org.libermundi.theorcs.core.CoreConstants;
import org.libermundi.theorcs.core.tapestry.services.configuration.ApplicationConfig;
import org.libermundi.theorcs.core.util.StringUtils;

import com.google.common.base.Strings;

/**
 * @author Martin Papy
 *
 */
public class OfsFileValidator implements FileValidator {
	@Inject
	private ApplicationConfig _appConfig; 

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.assets.FileValidator#isAllowed(java.lang.String)
	 */
	@Override
	public boolean isAllowed(String filename) {
		return isAllowed(filename, FileType.ANY);
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.assets.FileValidator#isAllowed(java.lang.String, org.libermundi.theorcs.core.tapestry.services.assets.FileType)
	 */
	@Override
	public boolean isAllowed(String filename, FileType type) {
		String fileExt = FilenameUtils.getExtension(filename);
		if(Strings.isNullOrEmpty(fileExt)) {
			return Boolean.FALSE;
		}

		String[] allowedExt = getExtensions(type);
		for (int i = 0; i < allowedExt.length; i++) {
			if(allowedExt[i].equalsIgnoreCase(fileExt)){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.assets.FileValidator#isValidFilename(java.lang.String)
	 */
	@Override
	public boolean isValidFilename(String filename) {
		if (Strings.isNullOrEmpty(filename)) {
			return Boolean.FALSE;
		}

		return filename.matches("|^[^\\\\/\\<\\>:]+$|");
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.assets.FileValidator#getExtensions(org.libermundi.theorcs.core.tapestry.services.assets.FileType)
	 */
	@Override
	public String[] getExtensions(FileType type) {
		StringBuilder allowedExt = new StringBuilder();

		switch (type) {
			case ANY:
				allowedExt.append(_appConfig.getString(CoreConstants.FILEVALIDATOR_ALLOWED_MISC_EXT))
					.append(",")
					.append(_appConfig.getString(CoreConstants.FILEVALIDATOR_ALLOWED_IMG_EXT))
					.append(",")
					.append(_appConfig.getString(CoreConstants.FILEVALIDATOR_ALLOWED_DOC_EXT))
					.append(",")
					.append(_appConfig.getString(CoreConstants.FILEVALIDATOR_ALLOWED_SND_EXT))
					.append(",")
					.append(_appConfig.getString(CoreConstants.FILEVALIDATOR_ALLOWED_VID_EXT));
				break;
			case IMAGE:
				allowedExt.append(_appConfig.getString(CoreConstants.FILEVALIDATOR_ALLOWED_IMG_EXT));
				break;
			case DOCUMENT:
				allowedExt.append(_appConfig.getString(CoreConstants.FILEVALIDATOR_ALLOWED_DOC_EXT));
				break;
			case SOUND:
				allowedExt.append(_appConfig.getString(CoreConstants.FILEVALIDATOR_ALLOWED_SND_EXT));
				break;
			case VIDEO:
				allowedExt.append(_appConfig.getString(CoreConstants.FILEVALIDATOR_ALLOWED_VID_EXT));
				break;
			default:
				break;
		}
		List<String> lAllowedExt = StringUtils.fromStrToList(allowedExt.toString(),",");
		return lAllowedExt.toArray(new String[lAllowedExt.size()]);
	}
	
	

}
