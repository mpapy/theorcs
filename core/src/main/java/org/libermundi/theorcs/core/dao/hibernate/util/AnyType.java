// [LICENCE-HEADER]
//
package org.libermundi.theorcs.core.dao.hibernate.util;

import org.hibernate.type.StandardBasicTypes;

/**
 * <Briefly describing the purpose of the class/interface...>
 *
 */
public class AnyType extends org.hibernate.type.AnyType {
    private static final long serialVersionUID = 1L;

    public AnyType() {
        super(StandardBasicTypes.STRING, StandardBasicTypes.LONG);
    }
}
