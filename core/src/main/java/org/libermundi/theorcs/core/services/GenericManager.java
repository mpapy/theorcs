package org.libermundi.theorcs.core.services;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.libermundi.theorcs.core.model.base.Identifiable;


public interface GenericManager {
	/**
	 * Used to re-attach the Object to the Session (Hibernate for example).
	 * Merge the state of the given entity into the current persistence context.
	 * @see EntityManager#merge(Object)
	 * @param obj Object to re-attach
	 */
	<T extends Identifiable<I>, I extends Serializable> T refresh(final T obj);
	<T extends Identifiable<I>, I extends Serializable> T find(Class<T> clazz, I primaryKey);
	<T extends Identifiable<I>, I extends Serializable> T getLast(Class<T> clazz);
}
