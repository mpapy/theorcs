package org.libermundi.theorcs.core.tapestry.services.assets;
import java.io.InputStream;

public class PNGInline extends InlineStreamResponse {
    public PNGInline(InputStream is, String... args) {
            super(is, args);
            this.contentType = "image/png";
            this.extension = "png";
    }
}