/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.model.impl;

import org.libermundi.theorcs.core.model.NodeConstraintType;

/**
 * @author Martin Papy
 *
 */
public class NodeConstraintTypeImpl implements NodeConstraintType {
	private final String _typeName;
	
	public NodeConstraintTypeImpl(String typeName){
		_typeName = typeName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((_typeName == null) ? 0 : _typeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		NodeConstraintTypeImpl other = (NodeConstraintTypeImpl) obj;
		if (_typeName == null) {
			if (other._typeName != null){
				return false;
			}
		} else if (!_typeName.equals(other._typeName)) {
			return false;
		}
		return true;
	}


	@Override
	public String toString() {
		return "NodeConstraintType [" + _typeName + "]";
	}
	
}
