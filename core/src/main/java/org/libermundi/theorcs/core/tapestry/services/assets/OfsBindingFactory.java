// [LICENCE-HEADER]
//
package org.libermundi.theorcs.core.tapestry.services.assets;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.Binding;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.ioc.Location;
import org.apache.tapestry5.services.AssetFactory;
import org.apache.tapestry5.services.AssetSource;
import org.apache.tapestry5.services.BindingFactory;

/**
 * <Briefly describing the purpose of the class/interface...>
 *
 */
public class OfsBindingFactory implements BindingFactory {
    public static final String OFS_BINDING_PREFIX = "ofs";
	private final AssetFactory _ofsAssetFactory;
    private final AssetSource _assetSource;
    
    public OfsBindingFactory(@OrcsFileSystem AssetFactory ofsAssetFactory, AssetSource assetSource) {
        _ofsAssetFactory = ofsAssetFactory;
        _assetSource = assetSource;
    }

    /*
     * @see org.apache.tapestry5.services.BindingFactory#newBinding(java.lang.String, org.apache.tapestry5.ComponentResources, org.apache.tapestry5.ComponentResources, java.lang.String, org.apache.tapestry5.ioc.Location)
     */
    @Override
    public Binding newBinding(String description, ComponentResources container, ComponentResources component,
                                    String expression, Location location) {
    	Asset asset = _assetSource.getAsset(_ofsAssetFactory.getRootResource(), expression, container.getLocale());
        return new OfsAssetBinding(location, description, asset);
    }

}
