// Copyright 2011 The Apache Software Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package org.libermundi.theorcs.core.tapestry.services.csrf;

/**
 * This service holds all pages that are unprotected.
 *
 */
public interface CsrfUnProtectedPages {
	/**
	 * Adds a unprotected page. The identification is done by the page name.
	 * This is done in the auto mode.  
	 * @param pageName
	 */
	public void addUnprotectedPageName(String pageName);
	
	/**
	 * 
	 * @param pageName
	 * @return
	 */
	public boolean isUnprotected(String pageName);
}
