/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.libermundi.theorcs.core.tapestry.util;

import java.io.Serializable;

import org.apache.tapestry5.AbstractOptionModel;
import org.libermundi.theorcs.core.model.base.Identifiable;

public class GenericOptionModel<T extends Identifiable<I>, I extends Serializable> extends AbstractOptionModel {
    private final String _label;

    private final T _value;
    
    public GenericOptionModel(String label, T value) {
        _label = label;
        _value = value;
    }
    
	@Override
	public String getLabel() {
		return _label;
	}

	@Override
	public T getValue() {
		return _value;
	}

    @Override
    public String toString(){
        return String.format("GenericOptionModel[%s %s]", _label, _value);
    }
}
