// Copyright 2011 The Apache Software Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package org.libermundi.theorcs.core.tapestry.services.csrf;

/**
 * This service is used to late evaluate the contributed application defaults for the cross-site request
 * forgery protection. The values are initialized after the application defaults of the client application
 * have been contributed. The service is used to retrieve these application defaults at runtime within 
 * the CsrfProtection module. Without this service only the factory defaults would be available.
 *
 */
public interface DynamicConfig {
	/**
	 * Provides the anti CSRF mode configuration at runtime.
	 * @return
	 */
	public String getAntiCsrfmode();

	/**
	 * Provides the token type configuration at runtime.
	 * @return
	 */
	public String getAntiCsrfTokenType();

	/**
	 * Provides the token persistence configuration at runtime.
	 * @return
	 */
	public String getAntiCsrfTokenPersistence();
}
