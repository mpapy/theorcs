package org.libermundi.theorcs.core.tapestry.services.configuration;


public abstract class AbstractApplicationConfiguration implements
		ApplicationConfig {

	@Override
	public Object getObject(String key) {
		return getObject(key, null);
	}

	@Override
	public String getString(String key) {
		return (String)getObject(key, null);
	}

	@Override
	public String getString(String key, String defaultValue) {
		return (String)getObject(key, defaultValue);
	}

	@Override
	public boolean getBoolean(String key) {
		return getBoolean(key, Boolean.FALSE);
	}

	@Override
	public boolean getBoolean(String key, boolean defaultValue) {
		return Boolean.parseBoolean(getString(key, Boolean.toString(defaultValue)));
	}

	@Override
	public int getInteger(String key) {
		return getInteger(key, 0);
	}

	@Override
	public int getInteger(String key, int defaultValue) {
		return Integer.parseInt(getString(key, Integer.toString(defaultValue)));
	}

	
}
