/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.libermundi.theorcs.core.tapestry.util;
import java.util.List;
import java.util.Map;

import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.SelectModelVisitor;
import org.apache.tapestry5.ioc.internal.util.TapestryException;

@SuppressWarnings("rawtypes")
public class SelectMultipleModelRenderer implements SelectModelVisitor {
	
    private final MarkupWriter _writer;

	private final MultipleValueEncoder _encoder;

    public SelectMultipleModelRenderer(final MarkupWriter writer, MultipleValueEncoder encoder) {
        _writer = writer;
        _encoder = encoder;
    }

    public void beginOptionGroup(OptionGroupModel groupModel) {
        _writer.element("optgroup", "label", groupModel.getLabel());

        writeDisabled(groupModel.isDisabled());
        writeAttributes(groupModel.getAttributes());
    }

    public void endOptionGroup(OptionGroupModel groupModel) {
        _writer.end(); // select
    }

    @SuppressWarnings("unchecked")
	public void option(OptionModel optionModel) {
        Object optionValue = optionModel.getValue();
        
        if (_encoder == null) {
        	throw new TapestryException("value encoder cannot be null", this, null);
        }
        
        List<String> clientValues = _encoder.toClient(optionValue);
        for (String clientValue : clientValues) {
	        _writer.element("option", "value", clientValue);
	
	        if (isOptionSelected(optionModel)){
	        	_writer.attributes("selected", "selected");
	        }
	
	        writeDisabled(optionModel.isDisabled());
	        writeAttributes(optionModel.getAttributes());
	
	        _writer.write(optionModel.getLabel());
        }
        _writer.end();
    }

    private void writeDisabled(boolean disabled) {
        if (disabled){
        	_writer.attributes("disabled", "disabled");
        }
    }

    private void writeAttributes(Map<String, String> attributes) {
        if (attributes == null){
        	return;
        }

        for (Map.Entry<String, String> e : attributes.entrySet()) {
        	_writer.attributes(e.getKey(), e.getValue());
        }
    }

    protected boolean isOptionSelected(OptionModel optionModel, String clientValue) {
        return false;
    }

    protected boolean isOptionSelected(OptionModel optionModel) {
        return false;
    }

}