/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.core.tapestry.services.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.libermundi.theorcs.core.exceptions.EmailException;
import org.libermundi.theorcs.core.model.Account;
import org.libermundi.theorcs.core.model.freemarker.FreemarkerEmail;
import org.libermundi.theorcs.core.tapestry.services.FreemarkerEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.google.common.collect.Lists;

public class FreemarkerEmailServiceImpl implements FreemarkerEmailService {
	private final Logger log = LoggerFactory.getLogger(FreemarkerEmailServiceImpl.class);
	
	private final JavaMailSender _mailSender;
	
	private final Map<String, FreemarkerEmail> _emails;
	
	private final ThreadPoolTaskExecutor _executor;
	
	public FreemarkerEmailServiceImpl(JavaMailSender mailSender, Map<String, FreemarkerEmail> emails, ThreadPoolTaskExecutor executor) {
		_mailSender = mailSender;
		_emails = emails;
		_executor = executor;
	}
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.EmailService#sendEmail(java.lang.String, java.util.Map, org.libermundi.theorcs.core.model.Account)
	 */
	@Override
	public void sendEmail(String emailId, Map<String, Object> parameters,
			Account to) throws EmailException {
		Account[] tos = { to };
		sendEmail(emailId, parameters, Arrays.asList(tos), null, null);
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.EmailService#sendEmail(java.lang.String, java.util.Map, java.util.List)
	 */
	@Override
	public void sendEmail(String emailId, Map<String, Object> parameters,
			List<Account> to) throws EmailException {
		sendEmail(emailId, parameters, to, null, null);
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.EmailService#sendEmail(java.lang.String, java.util.Map, java.util.List, java.util.List)
	 */
	@Override
	public void sendEmail(String emailId, Map<String, Object> parameters,
			List<Account> to, List<Account> cc) throws EmailException {
		sendEmail(emailId, parameters, to, cc, null);
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.tapestry.services.EmailService#sendEmail(java.lang.String, java.util.Map, java.util.List, java.util.List, java.util.List)
	 */
	@Override
	public void sendEmail(final String emailId, final Map<String, Object> parameters, List<Account> to, List<Account> cc, List<Account> bcc)
			throws EmailException {
		String uuid = UUID.randomUUID().toString();
		FreemarkerEmail email = _emails.get(emailId);
		MimeMessage mimeMessage = _mailSender.createMimeMessage();
		MimeMessageHelper message = email.getMailMessage(mimeMessage, parameters);
		
		List<InternetAddress> toEmails = Lists.newArrayList();
		List<InternetAddress> ccEmails = Lists.newArrayList();
		List<InternetAddress> bccEmails = Lists.newArrayList();
		
		String subject;
		try {
			subject = message.getMimeMessage().getSubject();
			if(log.isDebugEnabled()){
				log.debug("Email UID["+uuid+"] | {Preparing email} | Subject [" + subject + "]");
			}

			for (Account account : to) {
				if(log.isDebugEnabled()){
					log.debug("Email UID["+uuid+"] | {Adding Recipient} | To [" + account.getEmail() + "]");
				}
				toEmails.add(new InternetAddress(account.getEmail()));
			}
			message.setTo(transformToArray(toEmails));
			
			if (cc != null && !cc.isEmpty()) {
				for (Account account : cc) {
					if(log.isDebugEnabled()){
						log.debug("Email UID["+uuid+"] | {Adding Recipient} | Cc [" + account.getEmail() + "]");
					}
					ccEmails.add(new InternetAddress(account.getEmail()));
				}
				message.setCc(transformToArray(ccEmails));
			}
			
			if (bcc != null && !bcc.isEmpty()) {
				for (Account account : bcc) {
					if(log.isDebugEnabled()){
						log.debug("Email UID["+uuid+"] | {Adding Recipient} | Bcc [" + account.getEmail() + "]");
					}
					bccEmails.add(new InternetAddress(account.getEmail()));
				}
				message.setBcc(transformToArray(bccEmails));
			}				
		} catch(MessagingException e) {
			log.error(e.getMessage());
			throw new EmailException(e.getMessage());
		}
		if(log.isDebugEnabled()){
    		log.debug("Email UID["+uuid+"] | {Queuing email}");
    	}
		
		_executor.submit(new SendMail(message,uuid));
		
	}
	
	private static InternetAddress[] transformToArray(List<InternetAddress> input){
		return input.toArray(new InternetAddress[input.size()]);
	}
	
	private class SendMail extends Thread {
		private MimeMessageHelper _messageHelper;
		private String _uuid;
		
	    SendMail(MimeMessageHelper messageHelper, String uuid) {
	        this._messageHelper = messageHelper;
	        this._uuid=uuid;
	    }

	    @Override
	    public void run() {
	    	if(log.isDebugEnabled()){
    	   		log.debug("Email UID["+_uuid+"] | {Sending email}");
	    	}
	    	try{
	    		_mailSender.send(_messageHelper.getMimeMessage());
	    	} catch (MailException e) {
	    		log.error("Email UID["+_uuid+"]");
	    		log.error(e.getMessage());
	    	}
	    }

	}

}
