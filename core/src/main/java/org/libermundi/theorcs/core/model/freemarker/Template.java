package org.libermundi.theorcs.core.model.freemarker;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import org.libermundi.theorcs.core.tapestry.services.assets.AssetModule;
import org.libermundi.theorcs.core.tapestry.services.assets.OfsResource;


public interface Template extends Serializable {
	/**
	 * Method to get a raw Template path.
	 * The file can be located in the Classpath (as part of the deployment)
	 * or it could be in the installation directory
	 * @return relative path within the Orcs Framework
	 * @see {@link AssetModule} {@link OfsResource}
	 */
	String getTemplatePath();
	
	/**
	 * @return the {@link Collection} of mandatory parameters that have to be programmatically defined ( list of names )
	 */
	Collection<String> getMandatoryParams();

	/**
	 * @return the {@link Map} of optional parameters that can be programmatically defined ( list of names / default values)
	 */
	Map<String, Object> getOptionnalParams();
	
	/**
	 * Used to check if the passed parameters matches at least the minimum of Mandatory parameter needed by the Template
	 * @param parameters to be check against the configuration of the Template
	 * @throws IllegalArgumentException
	 */
	void checkConsistency(Map<String, Object> parameters) throws IllegalArgumentException;
}
