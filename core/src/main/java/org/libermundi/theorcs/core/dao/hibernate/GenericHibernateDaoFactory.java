package org.libermundi.theorcs.core.dao.hibernate;

import java.io.Serializable;

import org.libermundi.theorcs.core.model.base.Identifiable;

public interface GenericHibernateDaoFactory {
	<T extends Identifiable<I>, I extends Serializable> HibernateDao<T, I> create(Class<T> type);
}
