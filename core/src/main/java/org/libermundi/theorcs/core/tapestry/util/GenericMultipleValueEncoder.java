/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.libermundi.theorcs.core.tapestry.util;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import org.libermundi.theorcs.core.model.base.Identifiable;
import org.libermundi.theorcs.core.services.GenericManager;
import org.apache.commons.beanutils.BeanUtils;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

public class GenericMultipleValueEncoder<T extends Identifiable<Long>> implements MultipleValueEncoder<T> {

	private final GenericManager _manager;
	
	private final Class<T> _type;
	
	public GenericMultipleValueEncoder(GenericManager manager, Class<T> type) {
		_manager = manager;
		_type = type;
	}

    public List<String> toClient(T value){
    	try {
			return Arrays.asList(BeanUtils.getArrayProperty(value, "id"));
		} catch (IllegalAccessException | InvocationTargetException
				| NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }

    public List<T> toValue(String[] clientValues) {
    	List<T> result = Lists.newArrayList();
    	for (String clientValue : clientValues) {
    		if(!Strings.isNullOrEmpty(clientValue)){
    			result.add(_manager.find(_type,Long.valueOf(clientValue)));
    		}
		}
    	return result;
    }
}