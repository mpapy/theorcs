// [LICENCE-HEADER]
//
package org.libermundi.theorcs.core.model.base;

/**
 * <Briefly describing the purpose of the class/interface...>
 *
 */
public interface SortableItem {
    String getId();    
    
    String[] getOrderConstrains();
}
