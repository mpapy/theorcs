Tapestry.Initializer.initContextualMenu = function(params) {
	var selector = params.selector;
	var handler = params.handler;
	var items = params.items;
	$.contextMenu({
			selector: selector,
			build: function($trigger, e){
				var arrEnabledOptions = $trigger.attr("cm-enabled").split(",");
				var finalItems = new Object();
				$.each(items, function(action,options){
					if(typeof options == "string"){
						finalItems[action]=options; 
					} else {
						finalItems[action]=$.extend(true,{},options); // We need to do a safe deep copy to keep original values
					}
					if($.inArray(action,arrEnabledOptions) > -1){
						finalItems[action].disabled=false;
					}
				});
				return {
					callback: function(action,option){
						var handlerObj = eval(handler);
						handlerObj['handleAction'].call(this,action,option.$trigger, e);
					},
					items: finalItems
				}
			}
		}
	);
};