var realTimeValidationCallBack = function(message) {
	var field = this.element;

	var wrapper = field.closest('.form-group');
	
	wrapper.removeClass("has-warning has-error has-success");
	
	if(message.success == "SUCCESS") {
		wrapper.addClass("has-success");
	} else if (message.success == "WARNING") {
		wrapper.addClass("has-warning");
	} else {
		wrapper.addClass("has-error");
	}

	var errors = wrapper.find(".rtv-error-hint");
	if(errors.size()!=0)
		$(errors[0]).empty();
	
	if(message.message.length > 0) {
		field.after("<span class='help-block rtv-error-hint'>" + message.message + "</span>");
	}
	
};