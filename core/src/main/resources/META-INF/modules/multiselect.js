requirejs.config({
	shim : {
		"./jquery.fcbkcomplete": ["jquery"]
	}
});
define(["jquery","./jquery.fcbkcomplete"], function($) {
	var init;
	
	init = function(fieldId) {
		$('#' + fieldId).multiSelect();
	};
	
	return init;
});