requirejs.config({
	shim : {
		"./jquery.chosen": ["jquery"]
	}
});
define(["jquery","./jquery.chosen"], function($) {
	var init;
	
	init = function(fieldId,noResultMessage) {
		$('#'+fieldId).chosen({no_results_text:noResultMessage});
	};
	
	return init;
});