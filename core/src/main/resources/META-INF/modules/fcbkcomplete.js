requirejs.config({
	shim : {
		"./jquery.fcbkcomplete": ["jquery"]
	}
});
define(["jquery","./jquery.fcbkcomplete"], function($) {
	var init;
	
	init = function(params) {
		$(params.selector).fcbkcomplete({
			json_url: params.eventUrl,
	        addontab: params.addontab,                   
	        maxitems: params.maxitems,
	        input_min_size: params.input_min_size,
	        height: params.height,
	        cache: params.cache,
	        newel: params.newel,
	        width: (params.width != null)?params.width:"", // Allow to remove the width style and let the global CSS take care
	        dropdownWidth: params.dropdownWidth,
	        oncreate:params.oncreate,
	        filter_selected:params.filter_selected,
	        complete_text:params.complete_text,
	        input_placeholder:params.input_placeholder,
	        select_all_text:params.select_all_text,
	        bricket:false // Must be false to be compatible with Tapestry
		});
	
	return init;
});