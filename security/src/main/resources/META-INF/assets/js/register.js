var registerValidateCallBack = function(message) {
	var field = this.element;

	var wrapper = field.closest('.control-group');
	
	wrapper.removeClass("warning error success");
	
	if(message.success) {
		wrapper.addClass("success");
	} else {
		wrapper.addClass("error");
	}

	var errors = wrapper.find(".help-inline");
	if(message.message.length > 0) {
		//Check if already have a <span class="help-inline">
		if(errors.size()==0) //if the span isn't on the page yet, we create it
			field.after("<span class='help-inline'>" + message.message + "</span>");
		else
			$(errors[0]).html(message.message);
	} else {
		if(errors.size()!=0)
			$(errors[0]).empty();
	}
	
};