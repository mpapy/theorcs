define(["jquery"], function($) {
	var showLoginUI, exports, init;
	
	showLoginUI = function(params) {
		gigya.socialize.showLoginUI(params);
	};
	
	init = function(conf) {
		var gigyaLoginCallback = new Object();

		gigyaLoginCallback.conf = conf;
		gigyaLoginCallback.onLogin = function(eventObj) {
			$.ajax({
			  url: gigyaLoginCallback.conf.url,
			  type : "POST",
			  data: {
			  	UID:eventObj.UID,
			  	signatureTimestamp:eventObj.signatureTimestamp,
			  	UIDSignature:eventObj.UIDSignature,
			  	rememberMe:$(gigyaLoginCallback.conf.snsRemembeMeId).is(':checked'),
				jsonUser:$.toJSON(eventObj.user)
			  },
			  success: function(data, textStatus, jqXHR){
				if( data._tapestry != undefined && data._tapestry.redirectURL != undefined ) {
					// In case we received a Redirect Instruction
					window.location = data._tapestry.redirectURL;
				}
				if( data.result != undefined ) {
			  		if (data.result == 'success') {
			  			//Refresh the Page
			  			window.location.reload();
			  		} else {
			  			$(gigyaLoginCallback.conf.gigyaErrorId).html('<span>'+data.errormsg+'</span>');
			  			$(gigyaLoginCallback.conf.gigyaErrorId).removeClass('invisible');
			  		}
			  	}
			  }
			});	
		};
		
		gigya.socialize.addEventHandlers({   
		    onLogin:gigyaLoginCallback.onLogin  
		});
	};
	
	return exports = {
		showLoginUI : showLoginUI,
		init : init
	};
});