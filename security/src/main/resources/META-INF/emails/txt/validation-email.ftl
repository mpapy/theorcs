<#include "header.ftl" >
Bienvenu dans le Système de Jeux de Rôle en Ligne de ${organization}!
		
Pour rappel, voici ton Identifiant : ${login}

Pour terminer ton inscription, merci de valider ton adresse email en 
cliquant sur ce lien d'activation : 

${activationUrl}		
		
Voilà...Bon Jeu et à bientôt !

L'équipe de ${organization}

PS : Attention ! Ton inscription sera conservée durant les 7 prochains jours 
	 seulement ! Delais pendant lequel tu dois achever la création de ton 
	 profil.
<#include "footer.ftl" >