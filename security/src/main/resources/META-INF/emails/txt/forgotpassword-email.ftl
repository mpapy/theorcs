<#include "header.ftl" >
Quelqu'un ( probablement vous ? ) a demandé la réinitialisation du mot de passe
associé au compte suivant.
		
Compte / Identifiant : ${login}

Pour réinitialiser le mot de passe, veuillez cliquer sur le lien suivant :

${resetPasswordUrl}		
		
Si vous n'êtes pas à l'origine de cette demande, ignorez simplement ce mail.
Votre compte est en sécurité :)
		
Voilà...Bon Jeu et à bientôt !

L'équipe de ${organization}

<#include "footer.ftl" >