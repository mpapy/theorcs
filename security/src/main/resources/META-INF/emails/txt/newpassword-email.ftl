<#include "header.ftl" >
Suite à votre demande, nous vous envoyons un nouveau mot de passe pour vous connecter au site.
		
Pour rappel, voici ton identifiant : ${login}

Nouveau mot de passe : ${newpassword}	
		
Voilà...Bon Jeu et à bientôt !
L'équipe de ${organization}

<#include "footer.ftl" >