<#include "header.ftl" >
		<p>Quelqu'un ( probablement vous ? ) a demandé la réinitialisation du mot de passe associé au compte suivant. </p>
		
		<p>Identifiant : ${login}</p>

		<p>Pour réinitialiser le mot de passe, veuillez cliquer ici : <a href="${resetPasswordUrl}">Générer un nouveau mot de passe</a></p>		
		
		<p>Si vous n'êtes pas à l'origine de cette demande, ignorez simplement ce mail. Votre compte est en sécurité :)</p>
		
		<p>
			Voilà...Bon Jeu et à bientôt !<br />
			<em><strong>L'équipe de ${organization}</strong></em><br/>
		</p>
<#include "footer.ftl" >