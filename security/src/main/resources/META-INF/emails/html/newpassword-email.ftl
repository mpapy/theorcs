<#include "header.ftl" >
		<p>Suite à votre demande, nous vous envoyons un nouveau mot de passe pour vous connecter au site.</p>
		
		<p>Pour rappel, voici ton identifiant : ${login}</p>

		<p>Nouveau mot de passe : ${newpassword}</p>		
		
		<p>
			Voilà...Bon Jeu et à bientôt !<br />
			<em><strong>L'équipe de ${organization}</strong></em><br/>
		</p>
<#include "footer.ftl" >