<#include "header.ftl" >
		<p>Bienvenu dans le Système de Jeux de Rôle en Ligne de ${organization}!</p>
		
		<p>Pour rappel, voici ton Identifiant : ${login}</p>

		<p>Pour terminer ton inscription, merci de valider ton adresse email en cliquant ici : <a href="${activationUrl}">Lien d'activation</a></p>		
		
		<p>
			Voilà...Bon Jeu et à bientôt !<br />
			<em><strong>L'équipe de ${organization}</strong></em><br/>
		</p>
		
		<br />
		<br />
		PS : Attention ! Ton inscription sera conservée durant les 7 prochains jours seulement ! Delais pendant lequel tu dois achever la création de ton profil.<br />
<#include "footer.ftl" >