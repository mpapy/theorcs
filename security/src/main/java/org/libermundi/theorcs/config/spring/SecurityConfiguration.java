package org.libermundi.theorcs.config.spring;

import java.util.ArrayList;
import java.util.List;

import org.libermundi.theorcs.core.config.SearchServicePostProcessor;
import org.libermundi.theorcs.core.dao.hibernate.EntityFactoryPostProcessor;
import org.libermundi.theorcs.core.model.Searchable;
import org.libermundi.theorcs.security.model.Authority;
import org.libermundi.theorcs.security.model.RememberMeToken;
import org.libermundi.theorcs.security.model.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import com.google.common.collect.Lists;

@Configuration
@ImportResource({
	"classpath:META-INF/spring/security-context.xml",
	"classpath:META-INF/spring/acl-context.xml"
})
public class SecurityConfiguration {
	@Bean
	public static EntityFactoryPostProcessor securityEntityFactoryPostProcessor() {
		EntityFactoryPostProcessor efpp = new EntityFactoryPostProcessor();
		ArrayList<Class<?>> managedClasses = new ArrayList<>();
		
			managedClasses.add(User.class);
			managedClasses.add(Authority.class);
			managedClasses.add(RememberMeToken.class);
		
		efpp.setManagedClasses(managedClasses);
		
		return efpp;
	}
	
	@Bean
	public static SearchServicePostProcessor securitySearchServiceFactoryPostProcessor() {
		SearchServicePostProcessor ssfpp = new SearchServicePostProcessor();
		List<Class<? extends Searchable>> searchableEntities = Lists.newArrayList();
			
			searchableEntities.add(User.class);
			
		ssfpp.setSearchableClasses(searchableEntities);
		
		return ssfpp;
	}

}