/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.security.tapestry.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.libermundi.theorcs.core.exceptions.EmailException;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;
import org.libermundi.theorcs.core.tapestry.services.FreemarkerEmailService;
import org.libermundi.theorcs.security.SecurityConstants;
import org.libermundi.theorcs.security.model.User;
import org.libermundi.theorcs.security.services.UserManager;
import org.libermundi.theorcs.security.tapestry.pages.Activation;
import org.libermundi.theorcs.security.tapestry.pages.ResetPassword;
import org.libermundi.theorcs.security.tapestry.services.UserServices;
import org.libermundi.theorcs.security.util.SecurityUtils;

/**
 * @author Martin Papy
 *
 */
public class UserServicesImpl implements UserServices {
	private final UserManager _userManager;
	
	private final AppHelper _appHelper;
	
	private final FreemarkerEmailService _emailService;
	
	public UserServicesImpl(UserManager userManager, AppHelper appHelper, FreemarkerEmailService emailService){
		this._userManager = userManager;
		this._appHelper = appHelper;
		this._emailService = emailService;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.security.tapestry.services.UserServices#sendActivationEmail(org.libermundi.theorcs.security.model.User)
	 */
	@Override
	public void sendActivationEmail(User user) throws EmailException {
		user.resetActivationKey();
		
		_userManager.save(user);
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		parameters.put("login", user.getUsername());
		
		parameters.put("activationUrl", _appHelper.getEventLink(Activation.class, "activate",
					user.getUid(), user.getActivationKey()).toAbsoluteURI(Boolean.TRUE));
		
		_emailService.sendEmail(SecurityConstants.EMAIL_VALIDATION_ID, parameters, user);
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.security.tapestry.services.UserServices#activateUser(java.lang.String)
	 */
	@Override
	public boolean unlockUserAccount(String userUid, String activationKey) {
		User userToActivate = _userManager.getUserByUID(userUid,true);
		if(userToActivate == null){
			return Boolean.FALSE;
		}
		
		if(userToActivate.isDeleted() || !userToActivate.isEnabled()) {
			return Boolean.FALSE;
		}
		
		if(userToActivate.getActivationKey().equals(activationKey)){
			userToActivate.setAccountNonLocked(Boolean.TRUE);
			userToActivate.resetActivationKey();
			_userManager.save(userToActivate);
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.security.tapestry.services.UserServices#sendForgotPasswordEmail(org.libermundi.theorcs.security.model.User)
	 */
	@Override
	public void sendForgotPasswordEmail(User user) throws EmailException {
		user.resetActivationKey();
		_userManager.save(user);
		
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("login", user.getUsername());
		
		parameters.put("resetPasswordUrl", _appHelper.getEventLink(ResetPassword.class, "resetpassword",
					user.getUid(), user.getActivationKey()).toAbsoluteURI(Boolean.TRUE));
		
		_emailService.sendEmail(SecurityConstants.EMAIL_FORGOTPASSWORD_ID, parameters, user);
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.security.tapestry.services.UserServices#validateResetPasswordRequest(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean resetPassword(String userUid,	String activationKey) throws EmailException {
		User userToReset = _userManager.getUserByUID(userUid); // We can Reset a Password only on an Active user.
		
		if(userToReset == null){
			return Boolean.FALSE;
		}
		
		if(userToReset.getActivationKey().equals(activationKey)){
			String newPassword = SecurityUtils.generatePassword(10);
			userToReset.resetActivationKey(); //disable the Link
			userToReset.setPassword(newPassword);
			_userManager.save(userToReset);
			
			sendNewPassword(userToReset,newPassword);
			
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}
	
	private void sendNewPassword(User user, String newPassword) throws EmailException {
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("login", user.getUsername());
		
		parameters.put("newpassword", newPassword);
		
		_emailService.sendEmail(SecurityConstants.EMAIL_NEWPASSWORD_ID, parameters, user);
		
	}

}
