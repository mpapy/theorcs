package org.libermundi.theorcs.security.dao;

import org.libermundi.theorcs.core.dao.GenericDao;
import org.libermundi.theorcs.security.model.RememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

public interface RememberMeTokenDao extends GenericDao<RememberMeToken, Long>, PersistentTokenRepository {
	public RememberMeToken getTokenbySeries(String seriesId);

}
