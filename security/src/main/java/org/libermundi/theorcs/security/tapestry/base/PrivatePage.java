package org.libermundi.theorcs.security.tapestry.base;

import org.libermundi.theorcs.security.SecurityConstants;
import org.springframework.security.access.annotation.Secured;

@Secured(SecurityConstants.ROLE_USER)
public abstract class PrivatePage extends PublicPage {

}
