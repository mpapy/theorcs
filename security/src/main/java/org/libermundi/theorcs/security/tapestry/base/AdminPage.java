package org.libermundi.theorcs.security.tapestry.base;

import org.libermundi.theorcs.security.SecurityConstants;
import org.springframework.security.access.annotation.Secured;

@Secured({SecurityConstants.ROLE_ROOT,SecurityConstants.ROLE_ADMIN})
public abstract class AdminPage extends PrivatePage {

}
