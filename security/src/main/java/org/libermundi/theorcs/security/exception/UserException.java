package org.libermundi.theorcs.security.exception;

/**
 * Exception about the User Model Object
 *
 */
public class UserException extends Exception {
	private static final long serialVersionUID = 6064274800725308981L;

	/**
	 * Constructor
	 */
	public UserException() {
		super();
	}
	
	/**
	 * Constructor
	 * @param msg
	 */
	public UserException(String msg) {
		super(msg);
	}
}
