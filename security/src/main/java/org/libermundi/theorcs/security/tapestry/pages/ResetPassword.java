/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.security.tapestry.pages;

import org.apache.tapestry5.alerts.AlertManager;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.core.exceptions.EmailException;
import org.libermundi.theorcs.core.tapestry.base.BasePageImpl;
import org.libermundi.theorcs.core.tapestry.services.configuration.ApplicationConfig;
import org.libermundi.theorcs.security.tapestry.services.UserServices;
import org.slf4j.Logger;

public class ResetPassword extends BasePageImpl {
	@Inject
	private Logger _logger;
	
	@Inject
	private UserServices _userServices;
	
	@Inject
	private AlertManager _alertManager;
	
	@Inject
	private ApplicationConfig _appConfig;
	
	@Inject
	private Messages _messages;
	
	@OnEvent("resetpassword")
	public String resetPassword(String userUid, String activationKey) {
		boolean success = Boolean.FALSE;
		try {
			success = _userServices.resetPassword(userUid, activationKey);
		} catch (EmailException e) {
			_logger.error("Could not send New Password by Email to User UID : " + userUid, e);
			_alertManager.error(_messages.get("security.forgotpassword.email.sendfailure"));
		}
		
		if(success) {
			_alertManager.success(_messages.get("security.forgotpassword.success"));
		} else {
			_alertManager.error(_messages.get("security.forgotpassword.failure"));
		}
		
		return "Index";
		
	}

}
