/**
 * 
 */
package org.libermundi.theorcs.security.tapestry.components;

import java.io.IOException;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Response;
import org.libermundi.theorcs.security.tapestry.services.SecurityHelper;


/**
 * @author papy
 *
 */
public class LogoutLink {
	@Inject
	private Response _response;
	
	@Inject
	private SecurityHelper _securityHelper;
	/**
	 * On Action logout
	 * @throws IOException 
	 */
	public void onActionFromLogout() throws IOException {
		_response.sendRedirect(_securityHelper.getSpringSecurityLogoutUrl()); 
	}	
}
