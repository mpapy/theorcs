package org.libermundi.theorcs.security.tapestry.model.emails;

import org.libermundi.theorcs.core.CoreConstants;
import org.libermundi.theorcs.core.model.freemarker.EmailMimeTemplate;
import org.libermundi.theorcs.core.tapestry.model.AbstractFreemarkerTemplate;
import org.libermundi.theorcs.core.tapestry.services.configuration.ApplicationConfig;


public class ValidationEmailTemplate extends AbstractFreemarkerTemplate implements EmailMimeTemplate {
	private static final long serialVersionUID = 1951498202784298632L;
	
	private static final String TEMPLATE_NAME = "validation-email.ftl";

	public ValidationEmailTemplate(ApplicationConfig appConfig) {
		addParam("login");
		addParam("organization",appConfig.getString(CoreConstants.ORGANIZATION_NAME));
		addParam("activationUrl");
		addParam("siteUrl",appConfig.getString(CoreConstants.SITE_URL));
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.freemarker.EmailMimeTemplate#getHtmlTemplatePath()
	 */
	@Override
	public String getHtmlTemplatePath() {
		return EmailMimeTemplate.EMAIL_HTML_TEMPLATE_PATH + TEMPLATE_NAME;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.freemarker.Template#getTemplatePath()
	 */
	@Override
	public String getTemplatePath() {
		return EmailMimeTemplate.EMAIL_TXT_TEMPLATE_PATH + TEMPLATE_NAME;
	}

}
