/*
 * Copyright (c) 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.libermundi.theorcs.security.tapestry.services;

import org.libermundi.theorcs.security.exception.UserNotFoundException;
import org.libermundi.theorcs.security.model.User;

public interface GigyaServices {
	public static final String GIGYA_GET_USERINFO = "socialize.getUserInfo";
	public static final String GIGYA_NOTIFY_REGISTRATION = "socialize.notifyRegistration";
	public static final String GIGYA_NOTIFY_LOGIN = "socialize.notifyLogin";
	public static final String GIGYA_SETUID = "socialize.setUID";
	public static final String GIGYA_DELETE_ACCOUNT = "socialize.deleteAccount";
	
	boolean isSignatureValid(String gigyaUid, String timestamp, String signature);

	void authenticate(String gigyaUid, boolean rememberMe) throws UserNotFoundException;

	boolean notifyRegistration(String uid, User user, String contextId);
	
	public boolean setUid(String uid, User user);

	public boolean deleteAccount(String uid);
}
