/**
 * 
 */
package org.libermundi.theorcs.security.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.libermundi.theorcs.core.dao.hibernate.HibernateGenericDao;
import org.libermundi.theorcs.security.dao.AuthorityDao;
import org.libermundi.theorcs.security.model.Authority;
import org.libermundi.theorcs.security.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;


/**
 * @author Martin Papy
 *
 */

@Repository
public class AuthorityDaoImpl extends HibernateGenericDao<Authority,Long> implements AuthorityDao {
	private static final Logger logger = LoggerFactory.getLogger(AuthorityDaoImpl.class);

	public AuthorityDaoImpl(){
		super();
		_mappedClazz = Authority.class;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.security.dao.AuthorityDao#getAuthorities(org.libermundi.core.model.User)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Authority> getAuthorities(User user) {
		logger.debug("Loading Authorities for the User : "+ user.getUsername());
		Criteria getAuthorities = createSimpleCriteria()
								.add(Restrictions.eq("user",user));
		
		return getAuthorities.list();
	}

	@Override
	public Authority getAuthority(String role) {
		logger.debug("Loading Authority "+ role);
		Criteria result = createSimpleCriteria()
			.add(Restrictions.eq(Authority.PROP_AUTHORITY, role));
			
		return (Authority)result.uniqueResult();
	}
	
	
}
