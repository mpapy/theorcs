/**
 * 
 */
package org.libermundi.theorcs.security.dao;

import java.util.List;

import org.libermundi.theorcs.core.dao.GenericDao;
import org.libermundi.theorcs.security.model.Authority;
import org.libermundi.theorcs.security.model.User;

/**
 * @author Martin Papy
 *
 */
public interface AuthorityDao extends GenericDao<Authority,Long> {
	List<Authority> getAuthorities(User user);

	Authority getAuthority(String role);
}
