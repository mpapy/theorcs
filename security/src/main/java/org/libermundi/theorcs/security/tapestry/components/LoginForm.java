/*
 * Copyright (c) 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.libermundi.theorcs.security.tapestry.components;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.core.CoreConstants;
import org.libermundi.theorcs.core.tapestry.services.configuration.ApplicationConfig;
import org.libermundi.theorcs.security.SecurityConstants;
import org.libermundi.theorcs.security.tapestry.services.SecurityHelper;


/**
 * LoginForm
 *
 */
public class LoginForm {
	@Parameter(required=false,defaultPrefix=BindingConstants.PROP)
	private String _redirectTo;
	
	@Persist
    private String _userName;
	
	@Persist
	private boolean _rememberMe;
	
	@Persist
	@Property(write=false)
	private String _redirectUrl;

	private String _password;

	@Component(id = "loginForm")
	private Form _loginForm;

	@Property(write=false)
	private boolean _gigyaEnabled;
	
	@Property(write=false)
	private String _forgotPage;
	
	@Property(write=false)
	private String _registerPage;
	
	@Inject
	private Messages _messages;

	@Inject
	private SecurityHelper _securityHelper;
	
	@Inject
	private Block _springSecurityForm;
	
	@Inject
	private ApplicationConfig _appConfig;
	
	/**
	 * SetupRender to clean the errors on the form at each load
	 * @param writer
	 */
	@SetupRender
	public void setupRender() {
		_loginForm.clearErrors();
		_gigyaEnabled =_appConfig.getBoolean(SecurityConstants.GIGYA_ENABLED);
		_redirectUrl=_redirectTo;
		_forgotPage =_appConfig.getString(SecurityConstants.PAGE_FORGOTPASSWORD);
		_registerPage = _appConfig.getString(SecurityConstants.PAGE_REGISTRATION);
	}
	
	/**
	 * On Success event on the LoginForm
	 * Return a Hidden form to post back on 
	 * Spring Security URL
	 * @return springSecurityForm
	 *  
	 */
	@OnEvent(value="success", component="loginForm")
	public Block onSuccessFromLogin() {
		return _springSecurityForm;
	}
	

	/*------- Getters & Setters -------------*/
	
	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		this._userName = userName;
	}

	public String getPassword() {
		return _password;
	}

	public void setPassword(String password) {
		this._password = password;
	}

	public boolean isRememberMe() {
		return _rememberMe;
	}

	public void setRememberMe(boolean rememberMe) {
		this._rememberMe = rememberMe;
	}

	/*------- Some Message Related Methods -------------*/
	
	public String getLegendMessage(){
		return _messages.format("security.loginform.orcslegend", _appConfig.getString(CoreConstants.ORGANIZATION_NAME));
	}
	
	/*------- Some Utility Methods -------------*/
	public String getSpringSecurityUrl(){
		return _securityHelper.getSpringSecurityLoginUrl(_redirectUrl);
	}
	
}
