package org.libermundi.theorcs.security.dao.hibernate.listener;

import java.util.UUID;

import javax.persistence.PrePersist;

import org.libermundi.theorcs.core.model.Uid;

public class UidListener {
	
	@PrePersist
	public void setUid(Uid uid) {
		if(uid.getUid() == null || uid.getUid().isEmpty()){
			uid.setUid(UUID.randomUUID().toString());
		}
	}
}
