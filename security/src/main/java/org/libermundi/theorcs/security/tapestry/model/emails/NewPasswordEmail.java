/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.security.tapestry.model.emails;

import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.services.AssetFactory;
import org.libermundi.theorcs.core.CoreConstants;
import org.libermundi.theorcs.core.tapestry.model.AbstractFreemarkerEmail;
import org.libermundi.theorcs.core.tapestry.services.FreeMarkerService;
import org.libermundi.theorcs.core.tapestry.services.configuration.ApplicationConfig;

public class NewPasswordEmail extends AbstractFreemarkerEmail {
	private static final long serialVersionUID = 853747803629339368L;

	public NewPasswordEmail(ApplicationConfig appConfig,
			FreeMarkerService freeMarkerService,
			AssetFactory assetFactory,
			Messages messages) {
		super(appConfig,freeMarkerService,assetFactory,messages);
		setTemplate(new NewPasswordEmailTemplate(appConfig));
	}

	@Override
	protected String getTitle() {
		return getMessages().format(("security.newpassword.email.title"),getAppConfig().getString(CoreConstants.ORGANIZATION_NAME));
	}
}
