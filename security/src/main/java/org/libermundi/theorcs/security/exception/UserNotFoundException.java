package org.libermundi.theorcs.security.exception;

import org.springframework.security.core.AuthenticationException;

public class UserNotFoundException extends AuthenticationException {
	private static final long serialVersionUID = -2268610633221516918L;

	public UserNotFoundException(String msg) {
		super(msg);
	}
}
