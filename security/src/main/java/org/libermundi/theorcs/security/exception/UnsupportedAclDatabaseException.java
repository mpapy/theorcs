package org.libermundi.theorcs.security.exception;

public class UnsupportedAclDatabaseException extends Exception {
	private static final long serialVersionUID = 6537072012238385267L;
	public UnsupportedAclDatabaseException(String message) {
		super(message);
	}
}
