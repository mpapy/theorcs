package org.libermundi.theorcs.security.dao;

import org.libermundi.theorcs.core.dao.GenericDao;
import org.libermundi.theorcs.core.model.base.Activable;
import org.libermundi.theorcs.core.model.base.Undeletable;
import org.libermundi.theorcs.security.model.User;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Interface of UserDAO
 *
 */
public interface UserDao extends GenericDao<User,Long> {
	
	/**
	 *  Get the {@link User} from its login. 
	 *
	 * @param username
	 * @param force : allow to ignore all restrictions related to :
	 * - {@link Undeletable} => Deleted
	 * - {@link Activable} => Activated
	 * - {@link UserDetails} => Enabled
	 * @return
	 */
	User getUserByUsername(String username, boolean force);
	
	/**
	 * Get the {@link User} from its UID.
	 * 
	 * @param uid
	 * @param force : allow to ignore all restrictions related to :
	 * - {@link Undeletable} => Deleted
	 * - {@link Activable} => Activated
	 * - {@link UserDetails} => Enabled
	 * @return
	 */
	User getUserByUID(String uid, boolean force);

	/**
	 * Get the {@link User} from its nickname. 
	 * 
	 * @param nickname
	 * @param  force : allow to ignore all restrictions related to :
	 * - {@link Undeletable} => Deleted
	 * - {@link Activable} => Activated
	 * - {@link UserDetails} => Enabled
	 * @return
	 */
	User getUserByNickname(String nickname, boolean force);

	/**
	 * Get the {@link User} from its email address. 
	 * @param email
	 * @param force
	 * @return
	 */
	User getUserByEmail(String email, boolean force);
}
