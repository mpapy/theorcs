package org.libermundi.theorcs.security.exception;

import org.springframework.security.core.AuthenticationException;

public class InvalidGigyaSignatureException extends AuthenticationException {
	private static final long serialVersionUID = -2268610633221516918L;

	public InvalidGigyaSignatureException(String msg) {
		super(msg);
	}
}
