package org.libermundi.theorcs.security.dao.hibernate.listener;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.libermundi.theorcs.security.model.User;
import org.libermundi.theorcs.security.util.SecurityUtils;

public class PasswordListener {
	@PrePersist
	@PreUpdate
	public void setPassword(User user) {
		String password = user.getPassword();
		if(!SecurityUtils.isValid(password)) {
			user.setPassword(SecurityUtils.encodePassword(password));
		}
	}
}
