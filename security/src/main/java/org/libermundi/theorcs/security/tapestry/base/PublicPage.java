package org.libermundi.theorcs.security.tapestry.base;

import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.core.tapestry.base.BasePageImpl;
import org.libermundi.theorcs.security.model.User;
import org.libermundi.theorcs.security.services.SecurityManager;

public abstract class PublicPage extends BasePageImpl {
	@SessionState
	private User _user;
	
	@Inject
	private SecurityManager _securityManager;	

	/**
	 * Override the SetupRender Tapestry
	 */
	@SetupRender
	public void initPageRenderSupport() {
		if(_securityManager.isLoggedIn()) {
			_user=_securityManager.getCurrentUser();
		}
	}

}
