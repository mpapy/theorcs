/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 package org.libermundi.theorcs.security;

/**
 * Definition of Constants 
 * @author Martin Papy
 *
 */
public final class SecurityConstants {
	private SecurityConstants() {}

	
	public static final String TAPESTRY_MAPPING="security";
	
	public static final String TBL_USER="tbl_user";
	public static final String TBL_AUTHORITY="tbl_authority";
	public static final String TBL_TOKEN="tbl_token";
	public static final String TBL_USER2AUTHORITIES="tbl_user2authorities";
	
	public static final String ROLE_SYSTEM="ROLE_SYSTEM";
	public static final String ROLE_ROOT="ROLE_ROOT";
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_USER = "ROLE_USER";
	public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
	
	public static final String SESSION_SECURITY_ROOT_NAME="org.libermundi.theorcs.security.";
	public static final String SESSION_REGISTERING_USER=SESSION_SECURITY_ROOT_NAME + "registeringUser";
	public static final String SESSION_GIGYA_JSON=SESSION_SECURITY_ROOT_NAME + "gigyaJson";
	
	public static final String SECURITY_EMAIL_VALIDATION_ID= "security.validation-email";
	public static final String SECURITY_EMAIL_RESETPASSWORD_ID= "security.forgotpassword-email";
	public static final String SECURITY_EMAIL_NEWPASSWORD_ID= "security.newpassword-email";
	
	public static final String GIGYA_ENABLED = "security.gigya.enabled";
	public static final String GIGYA_API_KEY = "security.gigya.api-key";
	public static final String GIGYA_API_SECRET= "security.gigya.api-secret";
	public static final String GIGYA_BASE_URL="security.gigya.base-url";
	
	public static final String SSL_ENABLED = "security.ssl.enabled";
	public static final String SSL_PORT = "security.ssl.port";

	public static final String HMAC_PASSPHRASE = "security.hmac-passphrase";
	
	public static final String USERNAME_ANONYMOUS="ANONYMOUS";
	public static final String USERNAME_SYSTEM="SYSTEM";
	
	public static final String PAGE_REGISTRATION="security.page.register";
	public static final String PAGE_FORGOTPASSWORD="security.page.forgotpassword";
	
	public static final String EMAIL_VALIDATION_ID="security.email.validation";
	public static final String EMAIL_FORGOTPASSWORD_ID="security.email.forgotpassword";
	public static final String EMAIL_NEWPASSWORD_ID="security.email.newpassword";
	
	public static final String SPRING_SECURITY_CHECK="/j_spring_security_check";
	public static final String SPRING_SECURITY_REDIRECT="spring-security-redirect";	
	public static final String SPRING_SECURITY_LOGOUT="/j_spring_security_logout";
	
}
