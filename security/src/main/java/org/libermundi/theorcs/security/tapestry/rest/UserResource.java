package org.libermundi.theorcs.security.tapestry.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.libermundi.theorcs.core.CoreConstants;
import org.libermundi.theorcs.security.model.User;
import org.libermundi.theorcs.security.services.UserManager;
import org.libermundi.theorcs.security.tapestry.rest.UserResource;

@Path("/user")
public class UserResource {
	
	private UserManager _userManager;
	
	public UserResource(UserManager userManager) {
		_userManager = userManager;
	}
	
	@GET
	@Path("{id}")
	@Produces(CoreConstants.MIMETYPE_JSON)
	public User getUser(@PathParam("id") Long id) {
		User u = _userManager.load(id);
		if(u == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		return u;
	}
	
}
