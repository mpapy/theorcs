/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.security.tapestry.services;

import org.libermundi.theorcs.core.exceptions.EmailException;
import org.libermundi.theorcs.security.model.User;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author Martin Papy
 *
 */
public interface UserServices {
	/**
	 * Used to send an "Activation Email" to the user to check his registred email adress
	 * The implementation should reset the Activation Key everytime it is invoked 
	 * @param user
	 */
	void sendActivationEmail(User user) throws EmailException;
	
	/**
	 * Used to unlock a User based on activationKey (received by email)
	 * Bottom line the property {@link UserDetails#isAccountNonLocked()} will be set to true if the key is valid
	 * @param userUid
	 * @param activationKey
	 * @return true is activation was successful. False otherwise.
	 */
	boolean unlockUserAccount(String userUid, String activationKey);
	
	/**
	 * Send an email to the User with a Link to Reset its Password
	 * @param user
	 */
	void sendForgotPasswordEmail(User user) throws EmailException;
	
	/**
	 * Checks if the Parameters used in the Link are correct or not. If they are,
	 * generates a new password and send it to the user.
	 * 
	 * @param userUid
	 * @param activationKey
	 * @return true if the Link is valid
	 * @throws EmailException 
	 */
	boolean resetPassword(String userUid, String activationKey) throws EmailException;
}
