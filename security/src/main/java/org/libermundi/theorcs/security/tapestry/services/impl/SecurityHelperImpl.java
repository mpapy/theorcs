/*
 * Copyright (c) 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.libermundi.theorcs.security.tapestry.services.impl;

import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.libermundi.theorcs.security.SecurityConstants;
import org.libermundi.theorcs.security.tapestry.services.SecurityHelper;

import com.google.common.base.Strings;

public class SecurityHelperImpl implements SecurityHelper {
	
	@Inject
	@Symbol(SymbolConstants.CONTEXT_PATH)
	private String _context;
	
	@Override
	public String getSpringSecurityLoginUrl() {
		return getSpringSecurityLoginUrl(null);
	}

	@Override
	public String getSpringSecurityLoginUrl(String redirectTo){
		StringBuffer springUrl = new StringBuffer(_context + SecurityConstants.SPRING_SECURITY_CHECK); 
		if(!Strings.isNullOrEmpty(redirectTo)){
			springUrl.append("?")
			.append(SecurityConstants.SPRING_SECURITY_REDIRECT)
			.append("=")
			.append(redirectTo);
		}
		return springUrl.toString();
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.security.tapestry.services.SecurityHelper#getSpringSecurityLogoutUrl()
	 */
	@Override
	public String getSpringSecurityLogoutUrl() {
		StringBuffer springSecurityLogoutURL = new StringBuffer(_context)
        						.append(SecurityConstants.SPRING_SECURITY_LOGOUT);
		return springSecurityLogoutURL.toString();
	}

	
}
