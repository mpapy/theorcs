/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.security.tapestry.services.impl;

import java.io.File;

import org.apache.tapestry5.Asset;
import org.libermundi.tapestry.elfinder.exception.VolumeIOException;
import org.libermundi.tapestry.elfinder.services.VolumeOperation;
import org.libermundi.tapestry.elfinder.services.impl.AbstractVolume;
import org.libermundi.theorcs.core.tapestry.services.assets.ImageCache;
import org.libermundi.theorcs.core.tapestry.services.assets.ImageTransformMode;
import org.libermundi.theorcs.core.tapestry.services.assets.OfsFilesUtils;
import org.libermundi.theorcs.security.services.SecurityManager;

/**
 * @author Martin Papy
 *
 */
public class OrcsVolume extends AbstractVolume {
	private SecurityManager _securityManager;
	
	private ImageCache _imageCache;

	/**
	 * @param volumeId
	 * @param basePath
	 * @param baseUrl
	 * @param volumeAlias
	 * @param securityManager 
	 * @param maxFileSize 
	 */
	public OrcsVolume(String volumeId, File basePath, String baseUrl, SecurityManager securityManager, ImageCache imageCache, long maxFileSize) {
		super(volumeId, basePath, baseUrl, maxFileSize);
		_securityManager = securityManager;
		_imageCache = imageCache;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.services.Volume#isOperationAllowed(java.io.File, org.libermundi.tapestry.elfinder.services.VolumeOperation)
	 */
	@Override
	public boolean isOperationAllowed(File file, VolumeOperation operation) {
		if(_securityManager.isLoggedIn()){
			try {
				checkIfFileBelongsToVolume(file);
			} catch (VolumeIOException e) {
				return false;
			}
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.services.Volume#getBasePath()
	 */
	@Override
	public File getBasePath() {
		File newBase = new File(super.getBasePath(),OfsFilesUtils.computePartialStoreFilePath(_securityManager.getCurrentUsername()));
		return newBase ;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.services.Volume#getBaseUrl()
	 */
	@Override
	public String getBaseUrl() {
		String relativeUrl = OfsFilesUtils.computePartialStoreFilePath(_securityManager.getCurrentUsername()).replaceAll("\\\\", "/");
		return super.getBaseUrl() + relativeUrl + "/"; // It is a directory, so we add the trailing slash
	}

	/* (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.services.impl.AbstractVolume#getAlias()
	 */
	@Override
	public String getAlias() {
		return _securityManager.getCurrentUsername();
	}

	/* (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.services.impl.AbstractVolume#getThumbnailUrl(java.io.File)
	 */
	@Override
	public String getThumbnailUrl(File file) throws VolumeIOException {
		Asset thumbnail = _imageCache.getImageAssetFromCache(file, 48, 48, ImageTransformMode.THUMBNAIL);
		return thumbnail.toClientURL();
	}
	
	/* (non-Javadoc)
	 * @see org.libermundi.tapestry.elfinder.services.Volume#delete(java.io.File)
	 */
	@Override
	public void delete(File path) throws VolumeIOException {
		super.delete(path);
		_imageCache.removeFromCache(path);
	}	
}
