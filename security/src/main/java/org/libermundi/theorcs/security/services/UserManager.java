package org.libermundi.theorcs.security.services;

import org.libermundi.theorcs.core.model.base.Activable;
import org.libermundi.theorcs.core.model.base.Undeletable;
import org.libermundi.theorcs.core.services.Manager;
import org.libermundi.theorcs.security.model.User;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Interface of the User Manager Service
 *
 */
public interface UserManager extends Manager<User,Long> {
	public static final String SERVICE_ID = "userManager";
	
	/**
	 * Getter of User
	 * @return User
	 */
	User getUser();
	
	/**
	 * Getter of User by his Username
	 * @param login
	 * @return User
	 */
	User getUserByUsername(String username);
	
	/**
	 * Same as above, but ignore all restrictions related to : 
	 * - {@link Undeletable} => Deleted
	 * - {@link Activable} => Activated
	 * - {@link UserDetails} => Enabled
	 * 
	 * @param username
	 * @param force
	 * @return
	 */
	User getUserByUsername(String username,boolean force);
	
	/**
	 * Getter of User by his Nickname
	 * @param login
	 * @return User
	 */
	User getUserByNickname(String nickname);
	
	/**
	 * Same as above, but ignore all restrictions related to : 
	 * - {@link Undeletable} => Deleted
	 * - {@link Activable} => Activated
	 * - {@link UserDetails} => Enabled
	 * 
	 * @param nickname
	 * @param force
	 * @return
	 */
	User getUserByNickname(String nickname,boolean force);	
	
	/**
	 * Get a user by his UID
	 * @param uid
	 * @return
	 */
	User getUserByUID(String uid);
	
	/**
	 * Same as above, but ignore all restriction related to : 
	 * - {@link Undeletable} => Deleted
	 * - {@link Activable} => Activated
	 * - {@link UserDetails} => Enabled
	 * 	
	 * @param uid
	 * @param force
	 * @return
	 */
	User getUserByUID(String uid,boolean force);
	
	boolean isUsernameAvailable(String username);

	boolean isNickNameAvailable(String nickname);
	
	boolean isEmailAvailable(String email);
	
	String getUniqueNickName(String nickname);

	/**
	 * Get a user by his Email adress
	 * @param email
	 * @return User
	 */
	User getUserByEmail(String email);
	
	/**
	 * Same as above, but ignore all restriction related to : 
	 * - {@link Undeletable} => Deleted
	 * - {@link Activable} => Activated
	 * - {@link UserDetails} => Enabled
	 * 	
	 * @param email
	 * @param force
	 * @return User
	 */
	User getUserByEmail(String email,boolean force);
}
