/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.security.tapestry.services;

import org.apache.tapestry5.services.ApplicationInitializer;
import org.apache.tapestry5.services.Context;
import org.libermundi.theorcs.core.tapestry.base.AbstractModuleInitializer;
import org.libermundi.theorcs.core.util.AppContext;
import org.libermundi.theorcs.security.services.SecurityManager;
import org.libermundi.theorcs.security.services.UserManager;

public class SecurityModuleInitializer extends AbstractModuleInitializer {
	
	@Override
	public void initializeApplication(Context context, ApplicationInitializer initializer) {
		SecurityManager sm = AppContext.getBean("securityManager");
		UserManager um = AppContext.getBean("userManager");
		
		sm.switchUser(sm.getSystemUserDetails());
		
		um.initialize();
		sm.initialize();
		
		initializer.initializeApplication(context);
		
		sm.restoreUser();
	}
}
