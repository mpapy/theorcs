/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.security.model.impl;

import org.libermundi.theorcs.core.model.NodeConstraint;
import org.libermundi.theorcs.core.model.NodeConstraintType;
import org.libermundi.theorcs.core.model.impl.NodeConstraintTypeImpl;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * @author Martin Papy
 *
 */
public class RoleNodeConstraint implements NodeConstraint {
	public static final NodeConstraintType TYPE = new NodeConstraintTypeImpl("RoleNodeContraint");
	
	private GrantedAuthority _grantedAuthority;

	public RoleNodeConstraint(String role){
		_grantedAuthority = new SimpleGrantedAuthority(role);
	}
	
	/**
	 * 
	 * @return
	 */
	public GrantedAuthority getAuthority() {
		return _grantedAuthority;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.NodeConstrain#getType()
	 */
	@Override
	public NodeConstraintType getType() {
		return TYPE;
	}
	
}
