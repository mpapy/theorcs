/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.security.tapestry.components;

import org.apache.tapestry5.alerts.AlertManager;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.core.CoreConstants;
import org.libermundi.theorcs.core.exceptions.EmailException;
import org.libermundi.theorcs.core.tapestry.services.configuration.ApplicationConfig;
import org.libermundi.theorcs.security.model.User;
import org.libermundi.theorcs.security.services.UserManager;
import org.libermundi.theorcs.security.tapestry.services.UserServices;
import org.slf4j.Logger;

/**
 * @author Martin Papy
 *
 */
public class ForgotPasswordForm {
	@Inject
	private Logger _logger;

	@Inject
	private AlertManager _alertManager;

	@Inject
	private ApplicationConfig _appConfig;
	
	@Inject
	private Messages _messages;
	
	@Inject
	private UserManager _userManager;
	
	@Inject
	private UserServices _userServices;
	
	@Property
	private String _hint;
	
	@OnEvent(value="success",component="forgotPasswordForm")
	public String sendForgotPasswordEmail() {
		User forgot = _userManager.getUserByNickname(_hint);
		if(forgot == null){
			_userManager.getUserByEmail(_hint);
		}
		
		if(forgot != null) {
			try {
				_userServices.sendForgotPasswordEmail(forgot);
				_alertManager.success(_messages.get("security.forgotpassword.confirmation"));
			} catch (EmailException e) {
				_logger.error("Could not send Reset Password Email to " + forgot, e);
				_alertManager.success(_messages.get("security.forgotpassword.confirmation"));
			}
		}
		
		return _appConfig.getString(CoreConstants.HOME_PAGE);
	}

}
