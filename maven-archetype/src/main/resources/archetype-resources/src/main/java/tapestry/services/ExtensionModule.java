package ${package}.tapestry.services;

import org.apache.tapestry5.ioc.Configuration;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.Resource;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.services.ApplicationInitializerFilter;
import org.apache.tapestry5.services.LibraryMapping;
import org.springframework.web.context.WebApplicationContext;

import ${package}.ExtensionConstants;

#set( $D = '$' )

/**
 * This module is automatically included as part of the Tapestry IoC Registry, it's a good place to
 * configure and extend Tapestry, or to place your own service definitions.
 */
public class ExtensionModule {
	public static void contributeComponentClassResolver(final Configuration< LibraryMapping > configuration) {
        configuration.add(new LibraryMapping(ExtensionConstants.TAPESTRY_MAPPING, "${package}.tapestry"));
	}
    
	public static void contributeFactoryDefaults(MappedConfiguration<String, String> configuration) {
        configuration.add("${artifactId}.assets", "META-INF/assets");

        configuration.add("${artifactId}.scripts", "classpath:${D}{${artifactId}.assets}/js");
        configuration.add("${artifactId}.styles", "classpath:${D}{${artifactId}.assets}/css");
        configuration.add("${artifactId}.images", "classpath:${D}{${artifactId}.assets}/images");
    }	
	
	public static void contributeApplicationConfiguration(
			OrderedConfiguration<String> configuration,
			OrcsHome theOrcsHome) {
		configuration.add("${artifactId}-default", "classpath:META-INF/${artifactId}-default.properties","after:core-default");
		configuration.add("${artifactId}-local", theOrcsHome.getPath() + "/conf/${artifactId}.properties","after:${artifactId}-default");
	}
	
	public static void contributeComponentMessagesSource(
			@Value("/META-INF/lang/${artifactId}.properties")
			Resource ${artifactId}CatalogResource,
			OrderedConfiguration<Resource> configuration) {
			configuration.add("${artifactId}Catalog", ${artifactId}CatalogResource, "before:AppCatalog");
	}
	
	public void contributeApplicationInitializer(OrderedConfiguration<ApplicationInitializerFilter> configuration, @Inject WebApplicationContext webApplicationContext) {
        configuration.add("${artifactId}ModuleInitializer", new ExtensionModuleInitializer(), "after:coreModuleInitializer");
	}
	
		
}
