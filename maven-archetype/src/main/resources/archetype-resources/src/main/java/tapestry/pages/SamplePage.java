package ${package}.tapestry.pages;

import java.util.Date;

import org.libermundi.theorcs.core.tapestry.base.BasePage;

/**
 * Sample Page for TheORCS Extension
 */
public class SamplePage extends BasePage {
	public Date getCurrentTime() { 
		return new Date(); 
	}
}
