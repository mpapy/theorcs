package ${package}.tapestry.services;

import org.apache.tapestry5.services.ApplicationInitializer;
import org.apache.tapestry5.services.Context;
import org.libermundi.theorcs.core.tapestry.base.AbtractModuleInitializer;

/**
 * Allows module initialization when Tapestry starts. A this point we can use Hibernate
 * to check if default datas are here, and create them if not
 * @author Martin Papy
 *
 */

public class ExtensionModuleInitializer extends AbtractModuleInitializer {
	public void initializeApplication(Context context, ApplicationInitializer initializer) {
		//TODO : Initialize stuffs
		// Example 
		/*
			ChronicleManager chronicleManager = AppContext.getBean("chronicleManager");
			chronicleManager.initialize();
		 */
		
		//Pass on to the Next Initializer
		initializer.initializeApplication(context);
	}
}
