package ${package}.tapestry.components;

import java.util.Date;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;

public class SampleComponent {
	@Inject
	private Logger _logger;
	
	@Property(write=false)
	private String _dummyMessage;
	
	@SetupRender
	void setupRender() {
		_dummyMessage = "Hi ! It is : " + new Date();
	}
	
}