// [LICENCE-HEADER]
package org.libermundi.theorcs.layout.tapestry.helper;

import java.net.URL;
import java.util.Locale;

import org.apache.tapestry5.ioc.Resource;


/**
 * <Briefly describing the purpose of the class/interface...>
 *
 */
public abstract class DynamicResource implements Resource {
    public final Resource forClassName(String className) {
        if (allows(className)) {
            return newResource(className);
        }
        
        return null;
    }
    
    /*
     * @see org.apache.tapestry5.ioc.Resource#forFile(java.lang.String)
     */

    @Override
    public Resource forFile(String relativePath) {
        throw new UnsupportedOperationException("Method not implemented.");
    }
    
    /*
     * @see org.apache.tapestry5.ioc.Resource#forLocale(java.util.Locale)
     */

    @Override
    public Resource forLocale(Locale locale) {
        return this;
    }
    
    /*
     * @see org.apache.tapestry5.ioc.Resource#getFile()
     */

    @Override
    public String getFile() {
        throw new UnsupportedOperationException("Method not implemented.");
    }
    
    /*
     * @see org.apache.tapestry5.ioc.Resource#getFolder()
     */

    @Override
	public String getFolder() {
        throw new UnsupportedOperationException("Method not implemented.");
    }
    
    /*
     * @see org.apache.tapestry5.ioc.Resource#getPath()
     */

    @Override
	public String getPath() {
        throw new UnsupportedOperationException("Method not implemented.");
    }
    
    /*
     * @see org.apache.tapestry5.ioc.Resource#toURL()
     */

    @Override
	public URL toURL() {
        return null;
    }
    
    /*
     * @see org.apache.tapestry5.ioc.Resource#withExtension(java.lang.String)
     */

    @Override
	public Resource withExtension(String extension) {
        throw new UnsupportedOperationException("Method not implemented.");
    }
       
    protected abstract boolean allows(String className);
    
    protected abstract Resource newResource(String className);
}
