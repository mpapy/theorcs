package org.libermundi.theorcs.layout.tapestry.components;

import java.util.ArrayList;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.libermundi.theorcs.layout.tapestry.model.PagedSource;

/**
 * Blatant copy and paste of {@link org.apache.tapestry5.corelib.components.GridPager} with modifications.
 *
 * @version $Id$
 */
public class Pager {
	/**
	 * The source of the data displayed by the Containing Component
	 */
	@Parameter
	private java.util.List<Object> _source;

	/**
	 * The number of rows displayed per page.
	 */
	@Parameter
	private int _rowsPerPage;
	
	@Parameter(defaultPrefix=BindingConstants.LITERAL)
	private String _zone;
	
	@Parameter(required=true)
	private int _currentPageIndex;

	@Property
	private PagedSource<Object> _currentPagedSource;
	
	private int _currentPagedSourceIndex;

	/**
	 * Number of pages before and after the current page in the range. The pager
	 * always displays links for 2 * range + 1 pages, unless that's more than
	 * the total number of available pages.
	 */
	@Parameter("5")
	private int _range;

	@Property
	private int _loopIndex;
	
	private int _minPageIndexInRange;
	
	private int _maxPageIndexInRange;
	
	private int _maxPageIndex;
	
	private int _sourceSize;
	
	@SetupRender
	public void setupRender() {
		// Calculate the Min/Max Index of Pages ( based on Range and currentPage )
		_minPageIndexInRange = (_currentPagedSourceIndex - _range) > 0 ? (_currentPagedSourceIndex - _range) : 0;
		_maxPageIndexInRange = _minPageIndexInRange + 2 * _range + 1;
		_sourceSize = _source.size();
		_maxPageIndex = (int)Math.floor(_sourceSize / _rowsPerPage)-1;
	}
	
	public Integer getPageLabel() {
		return (_currentPagedSource.getPageIndex() + 1);
	}

	public Integer getPageContext() {
		return _currentPagedSource.getPageIndex();
	}
	
	public boolean isFirstPage() {
		return (_currentPageIndex == 0);
	}
	
	public boolean isLastPage() {
		return (_currentPageIndex == _maxPageIndex);
	}	
	
	public String getPageStyle() {
		String style="";
		if(_currentPageIndex == _currentPagedSource.getPageIndex()) {
			style += "active";
		}
		return style;
	}
	
	public Iterable<PagedSource<Object>> getPartialSource() {
		int pageIndex = 0;

		java.util.List<PagedSource<Object>> partialSource = new ArrayList<>();
		PagedSource<Object> pagedSource = null;
		int start = 0;
		int stop = 0;

		for(int i = 0; i < _sourceSize; i++) {
			if(pagedSource == null) {
				pagedSource = new PagedSource<>(_source);
			}
			stop++;
			if((stop - start) == _rowsPerPage) {
				pagedSource.prepare(start, stop);
				
				
				if(_minPageIndexInRange <= pageIndex && pageIndex <= _maxPageIndexInRange) {
					pagedSource.setPageIndex(pageIndex);
					partialSource.add(pagedSource);
				}
				
				pageIndex++;
				
				start = stop ;

				pagedSource = null;
				
			}
		}
		
		return partialSource;
		
	}

}