/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.components.chronicles;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.libermundi.theorcs.core.tapestry.services.PageRenderSupport;
import org.libermundi.theorcs.layout.dto.TemplateSettings;
import org.libermundi.theorcs.layout.tapestry.base.AbstractLayout;

@Import(library={
		"${layout.scripts}/html5shiv-printshiv.js"
},
stylesheet={
		"${layout.styles}/chronicles/bootstrap.css"
})
public final class Layout extends AbstractLayout {
	@Environmental
	private PageRenderSupport _pageRenderSupport;	
	
	@Property
    @Parameter
    private Block _sidebar;
	
	@Property(write=false)
	@Parameter(defaultPrefix = BindingConstants.PROP, value="false", required=false)
	private boolean _sideBarHidden;

	public String getChronicleName() {
		return _pageRenderSupport.getProperty(TemplateSettings.KEY_CHRONICLE_NAME);
	}
}
