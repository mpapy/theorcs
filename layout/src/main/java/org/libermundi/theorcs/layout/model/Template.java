package org.libermundi.theorcs.layout.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.libermundi.theorcs.layout.LayoutConstants;
import org.libermundi.theorcs.security.model.UserStatefulEntity;

@Entity(name="Template")
@Table(name=LayoutConstants.TBL_TEMPLATE)
public class Template extends UserStatefulEntity {
	public final static String PROP_CODE = "code";
	public static final String PROP_TML = "tml";	
	
	private static final long serialVersionUID = 1131061204450222323L;
	
	private String _code;
	
	private String _tml;

	@Basic
	@Column(name=Template.PROP_CODE,length=25,unique=true,nullable=false)
	public String getCode() {
		return _code;
	}

	@Basic
	@Lob
	@Column(name=Template.PROP_TML,nullable=false)
	public String getTml() {
		return _tml;
	}

	public String setCode(String code) {
		return _code = code;
	}

	public String setTml(String tml) {
		return _tml = tml;
	}
}
