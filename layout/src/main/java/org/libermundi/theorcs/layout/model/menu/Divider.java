/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.model.menu;

import java.util.List;

import org.libermundi.theorcs.core.services.StringSource;

import com.google.common.collect.Lists;


/**
 * @author Martin Papy
 *
 */
public class Divider implements NavMenuItem {
	private static final long serialVersionUID = -3829148751871015077L;

	private String _id;
	private String _css;
	@SuppressWarnings("unused")
	private List<String> _sec = Lists.newArrayListWithCapacity(0);

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getId()
	 */
	@Override
	public String getId() {
		return _id;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getLabel()
	 */
	@Override
	public String getLabel() {
		return "";
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getDescription()
	 */
	@Override
	public String getDescription() {
		return "";
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getType()
	 */
	@Override
	public NavMenuItemType getType() {
		return NavMenuItemType.DIVIDER;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getURI()
	 */
	@Override
	public String getURI() {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.StaticNavMenuItem#setId(java.lang.String)
	 */
	@Override
	public void setMainPageId(String id) {
		_id = id;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.StaticNavMenuItem#setLabel(java.lang.String)
	 */
	@Override
	public void setLabel(String label) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.StaticNavMenuItem#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String description) {
		throw new UnsupportedOperationException();
	}


	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#setURI(java.lang.String)
	 */
	@Override
	public void setURI(String uri) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getExtraCss()
	 */
	@Override
	public String getCss() {
		return _css;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#setExtraCss(java.lang.String)
	 */
	@Override
	public void setCss(String css) {
		_css = css;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getMainPageId()
	 */
	@Override
	public String getMainPageId() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.NodeData#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return Boolean.FALSE;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#setDynamicLabel(org.libermundi.theorcs.layout.model.menu.DynamicLabel)
	 */
	@Override
	public void setLabel(StringSource label) {
		throw new UnsupportedOperationException();		
	}

}
