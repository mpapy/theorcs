/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.services.impl;

import java.util.List;

import org.libermundi.theorcs.core.services.impl.AbstractManagerImpl;
import org.libermundi.theorcs.core.util.IOHelper;
import org.libermundi.theorcs.layout.LayoutConstants;
import org.libermundi.theorcs.layout.dao.TemplateDao;
import org.libermundi.theorcs.layout.model.Template;
import org.libermundi.theorcs.layout.services.TemplateManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("templateManager")
@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)
public class TemplateManagerImpl extends AbstractManagerImpl<Template,Long>  implements TemplateManager {
	private static final Logger logger = LoggerFactory.getLogger(TemplateManagerImpl.class);
	
	@Autowired
	public TemplateManagerImpl(TemplateDao templateDao) {
		super();
		setDao(templateDao);
		logger.debug("Instantiate the TemplateManager");
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.services.TemplateManager#getTemplates()
	 */
	@Override
	public List<Template> getTemplates() {
		return getDao().getAll();
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.services.TemplateManager#getByCode(java.lang.String)
	 */
	@Override
	public Template getByCode(String code) {
		logger.debug("getByCode({})",code);
		return ((TemplateDao)getDao()).getByCode(code);
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.core.services.impl.AbstractManagerImpl#initialize()
	 */
	@Override
	public void initialize() {
		Template defaultTemplate = getByCode(LayoutConstants.DEFAULT_TEMPLATE_CODE);
		if(defaultTemplate == null) {
			defaultTemplate = new Template();
			defaultTemplate.setCode(LayoutConstants.DEFAULT_TEMPLATE_CODE);
			defaultTemplate.setTml(loadTemplateFile());
			save(defaultTemplate);
		}
	}

	private static String loadTemplateFile() {
		return IOHelper.readFileFromClasspath("/org/libermundi/theorcs/layout/setup/Default.tml");
	}

	@Override
	public Template createNew() {
		return new Template();
	}
}
