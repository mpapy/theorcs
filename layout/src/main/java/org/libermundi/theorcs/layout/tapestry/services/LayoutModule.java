package org.libermundi.theorcs.layout.tapestry.services;

import java.util.List;

import org.apache.tapestry5.ioc.Configuration;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.MethodAdviceReceiver;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.Resource;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.ioc.annotations.Advise;
import org.apache.tapestry5.ioc.annotations.Contribute;
import org.apache.tapestry5.ioc.annotations.Decorate;
import org.apache.tapestry5.ioc.annotations.Local;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.plastic.MethodAdvice;
import org.apache.tapestry5.services.ApplicationInitializerFilter;
import org.apache.tapestry5.services.ApplicationStateManager;
import org.apache.tapestry5.services.AssetFactory;
import org.apache.tapestry5.services.ComponentClassResolver;
import org.apache.tapestry5.services.ContextProvider;
import org.apache.tapestry5.services.Core;
import org.apache.tapestry5.services.LibraryMapping;
import org.apache.tapestry5.services.javascript.JavaScriptStack;
import org.apache.tapestry5.services.javascript.StackExtension;
import org.apache.tapestry5.services.pageload.ComponentResourceLocator;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.core.tapestry.services.AnnotatedMethodAdvisor;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;
import org.libermundi.theorcs.core.tapestry.services.impl.AnnotatedMethodAdvisorImpl;
import org.libermundi.theorcs.layout.LayoutConstants;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.services.TemplateManager;
import org.libermundi.theorcs.layout.tapestry.helper.DynamicResource;
import org.libermundi.theorcs.layout.tapestry.helper.PageTemplateResource;
import org.libermundi.theorcs.layout.tapestry.services.impl.LayoutAppHelperImpl;
import org.libermundi.theorcs.layout.tapestry.services.impl.MenuServiceImpl;
import org.libermundi.theorcs.layout.tapestry.services.impl.RoleContrainMethodAdvice;
import org.libermundi.theorcs.layout.tapestry.services.internal.DynamicComponentResourceLocator;
import org.libermundi.theorcs.layout.tapestry.services.internal.DynamicPageTemplateSourceImpl;
import org.libermundi.theorcs.security.services.SecurityManager;
import org.slf4j.Logger;


/**
 * This module is automatically included as part of the Tapestry IoC Registry, it's a good place to
 * configure and extend Tapestry, or to place your own service definitions.
 */
public class LayoutModule {
	public static void contributeComponentClassResolver(Configuration<LibraryMapping> configuration) {
		configuration.add(new LibraryMapping(LayoutConstants.TAPESTRY_MAPPING, "org.libermundi.theorcs.layout.tapestry"));
	}
	
	
	@Decorate(serviceInterface = ComponentResourceLocator.class)
	public static ComponentResourceLocator decorateComponentResourceLocator(
           ComponentResourceLocator delegate,
           @ContextProvider AssetFactory assetFactory,
           ComponentClassResolver componentClassResolver,
           @Local DynamicPageTemplateSource source  ) {

		return new DynamicComponentResourceLocator(
                      delegate,
                      assetFactory.getRootResource(),
                      componentClassResolver,
                      source);
	}
	
	public static void bind(ServiceBinder binder) {
        binder.bind(LayoutAppHelper.class, LayoutAppHelperImpl.class);
        binder.bind(AnnotatedMethodAdvisor.class, AnnotatedMethodAdvisorImpl.class);
	}
	
	public static void contributeComponentMessagesSource(
			@Value("META-INF/lang/layout.properties")
			Resource layoutCatalogResource,
			OrderedConfiguration<Resource> configuration) {
			configuration.add("LayoutCatalog", layoutCatalogResource, "before:AppCatalog");
	}	

	public static DynamicPageTemplateSource buildDynamicPageTemplateSource(
			List<DynamicResource> dynResources) {
		return new DynamicPageTemplateSourceImpl(dynResources);
	}
	
	public static void contributeDynamicPageTemplateSource(
			OrderedConfiguration<DynamicResource> configuration,
			TemplateManager templateManager){
		configuration.add("DBPageTemplate", new PageTemplateResource(templateManager), "after:*");
	}
	
	public void contributeApplicationInitializer(OrderedConfiguration<ApplicationInitializerFilter> configuration) {
        configuration.add("layoutModuleInitializer", new LayoutModuleInitializer(), "after:securityModuleInitializer");
	}
	
	public static void contributeApplicationDefaults(MappedConfiguration<String, String> configuration) {
        configuration.add("layout.assets", "META-INF/assets");
        configuration.add("layout.scripts", "META-INF/assets/js");
        configuration.add("layout.styles", "META-INF/assets/css");
        configuration.add("layout.images", "META-INF/assets/images");
        configuration.add("layout.spacer-image", "META-INF/assets/images/spacer.gif");
    }
	
	// fixing default bootstrap include so that our custom bootstap.css can work properly
	@Contribute(JavaScriptStack.class)
	@Core
	public static void setupCoreJavaScriptStack(OrderedConfiguration<StackExtension> configuration) {
	 configuration.override("bootstrap.css", null);
	}

	/*
	 * Build the 4 different Menus + MenuServices
	 */
	@SuppressWarnings("rawtypes")
	public static MenuService buildMenuService(final List<Node> menuItems, final AppHelper appHelper, final Logger logger) {
		return new MenuServiceImpl(menuItems, appHelper, logger);
	}
	
	@SuppressWarnings("rawtypes")
	public static void contributeMenuService(OrderedConfiguration<Node> configuration, LayoutAppHelper layoutHelper) {
		
		// Portal
		Node<NavMenuItem> portalTopLeftMenu = layoutHelper.buildStaticMenuNode(null, LayoutConstants.PORTAL_TOP_LEFT_MENU_ID, "/");
		portalTopLeftMenu.setId(LayoutConstants.PORTAL_TOP_LEFT_MENU_ID);
		
		Node<NavMenuItem> portalSecMenu = layoutHelper.buildStaticMenuNode(null, LayoutConstants.PORTAL_TOP_RIGHT_MENU_ID, "/");
		portalSecMenu.setId(LayoutConstants.PORTAL_TOP_RIGHT_MENU_ID);
		
		// Chronique
		Node<NavMenuItem> chronicleMainMenu = layoutHelper.buildStaticMenuNode(null, LayoutConstants.CHRO_TOP_LEFT_MENU_ID, "/");
		chronicleMainMenu.setId(LayoutConstants.CHRO_TOP_LEFT_MENU_ID);

		Node<NavMenuItem> chronicleSecMenu = layoutHelper.buildStaticMenuNode(null, LayoutConstants.CHRO_TOP_RIGHT_MENU_ID, "/");
		chronicleSecMenu.setId(LayoutConstants.CHRO_TOP_RIGHT_MENU_ID);

		configuration.add(LayoutConstants.PORTAL_TOP_LEFT_MENU_ID, portalTopLeftMenu,"before:*");
		configuration.add(LayoutConstants.PORTAL_TOP_RIGHT_MENU_ID, portalSecMenu,"after:" + LayoutConstants.PORTAL_TOP_LEFT_MENU_ID);
		
		configuration.add(LayoutConstants.CHRO_TOP_LEFT_MENU_ID, chronicleMainMenu,"after:" + LayoutConstants.PORTAL_TOP_RIGHT_MENU_ID);
		configuration.add(LayoutConstants.CHRO_TOP_RIGHT_MENU_ID, chronicleSecMenu,"after:" + LayoutConstants.CHRO_TOP_LEFT_MENU_ID);
	}
	
	@Advise(id="RoleContrainMethodAdvice",serviceInterface=MenuService.class)
	public static void filterMenuServiceResults(MethodAdviceReceiver receiver,
				AnnotatedMethodAdvisor annotatedMethodAdvisor, final SecurityManager securityManager, final ApplicationStateManager applicationStateManager) {
		// RoleConstrain Advisor
		MethodAdvice rcma = new RoleContrainMethodAdvice(securityManager);
		annotatedMethodAdvisor.addAdvice(rcma, receiver);
	}
	
}