// [LICENCE-HEADER]
//
package org.libermundi.theorcs.layout.tapestry.helper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import org.apache.tapestry5.ioc.Resource;
import org.libermundi.theorcs.layout.LayoutConstants;
import org.libermundi.theorcs.layout.model.Template;
import org.libermundi.theorcs.layout.services.TemplateManager;
import org.libermundi.theorcs.layout.tapestry.pages.inner.LayoutTemplates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <Briefly describing the purpose of the class/interface...>
 *
 */
public class PageTemplateResource extends DynamicResource {
	private static final Logger logger = LoggerFactory.getLogger(PageTemplateResource.class);
	
	private final TemplateManager _templateManager;
	
	private final String _className;
	
	public PageTemplateResource(TemplateManager templateManager) {
		this(templateManager, null);
	}
	
    protected PageTemplateResource(TemplateManager templateManager, String className) {
    	_templateManager = templateManager;
        _className = className;
        if(logger.isDebugEnabled()) {
        	logger.debug("Intantiate PageTemplateResource for {}", className);
        }
    }
	
    @Override
	protected boolean allows(String className) {
		boolean result = className.equals(LayoutTemplates.class.getName());
    	logger.debug("allows({}) : {}", className, result);
    	return result;
    }
    
    @Override
    protected Resource newResource(String className) {
    	if(logger.isDebugEnabled()){
    		logger.debug("newResource({})", className);
    	}
        return new PageTemplateResource(_templateManager, className);
    }
    
    @Override
    public boolean exists() {
        return true;
    }
    
    @Override
    public InputStream openStream() throws IOException {
        String template = getLayoutTemplate();
        if(logger.isDebugEnabled()){
        	logger.debug("openStream() for {}", _className);
        	logger.debug("\n" + template);
        }
        return new ByteArrayInputStream(template.getBytes("utf-8"));
    }
    
    private String getLayoutTemplate() {
        StringBuilder builder = new StringBuilder();
        builder.append("<!DOCTYPE html PUBLIC  \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n");
        builder.append("<t:container xmlns=\"http://www.w3.org/1999/xhtml\"\n");
        builder.append("    xmlns:t=\"" + LayoutConstants.TAPESTRY_SCHEMA + "\"\n");
        builder.append("    xmlns:p=\"tapestry:parameter\">\n");
        List<Template> lTemplate = _templateManager.getAll();
        if(lTemplate.isEmpty()) {
            builder.append("    <t:block id=\"" + LayoutConstants.DEFAULT_TEMPLATE_CODE + "\">\n");
            builder.append(			getDbTemplate(LayoutConstants.DEFAULT_TEMPLATE_CODE));       
            builder.append("    </t:block>\n");
        } else {
        	for (Iterator<Template> iterator = lTemplate.iterator(); iterator.hasNext();) {
        		Template template = iterator.next();
        		builder.append("    <t:block id=\""+ template.getCode() + "\">\n");
        		builder.append(			getDbTemplate(template.getCode()));       
        		builder.append("    </t:block>\n");
        	}
        }
        
        builder.append("</t:container>\n");
        
        return builder.toString();
    }
    
    private String getDbTemplate(String code) {
	    Template template = null;
		if (code !=null && code.length() > 0) {
			template = _templateManager.getByCode(code);
		}
	  
		if (template == null) {
			logger.warn("Return DefaultTemplate");
			StringBuilder builder = new StringBuilder();
		    builder.append("<html>\n");
			builder.append("    <body>\n");
			
			builder.append("		<h1>No Template Found in Database for code : " + code + "</h1>\n");
			
			builder.append("    </body>\n");
			builder.append("</html>\n");	      
			return builder.toString();
		}
		logger.debug("Return Template from Database");
		return template.getTml(); 
	}

	@Override
	public boolean isVirtual() {
		return false;
	}

}
