package org.libermundi.theorcs.layout.tapestry.services.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.tapestry5.ComponentResources;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.core.model.Tree;
import org.libermundi.theorcs.core.model.impl.TreeImpl;
import org.libermundi.theorcs.core.tapestry.BasePage;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.tapestry.exception.MenuItemNotFoundException;
import org.libermundi.theorcs.layout.tapestry.services.MenuService;
import org.slf4j.Logger;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

public class MenuServiceImpl implements MenuService {
	private Map<String, Tree<NavMenuItem>> _treeMenu = new HashMap<>();
	
	private Map<String, Node<NavMenuItem>> _flatIdNodes = new HashMap<>();
	
	private Map<String, Node<NavMenuItem>> _flatMainPageIdNodes = new HashMap<>();
	
	private final Logger logger;

	private final AppHelper appHelper;
	
	@SuppressWarnings("rawtypes")
	public <T extends NavMenuItem> MenuServiceImpl(List<Node> NavMenuItems, AppHelper appHelper, Logger logger) throws MenuItemNotFoundException {
		this.logger = logger;
		this.appHelper = appHelper;
		buildMenus(NavMenuItems);
	}

	@Override
	public Node<NavMenuItem> getNodeById(String rootId) {
		try {
			return findNode(rootId).clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public boolean isItemActive(ComponentResources resources, Node<NavMenuItem> currentNode) {
		BasePage currentPage = (BasePage)resources.getPage();
		//Check if the Node is associated with the Main Page
		List<Node<NavMenuItem>> breadcrum = getBreadcrum(currentPage);
		for (Node<NavMenuItem> item : breadcrum) {
			if(currentNode.getId().equals(item.getId())) {
				return Boolean.TRUE;
			}			
		}
		return Boolean.FALSE;
	}

	@Override
	public List<Node<NavMenuItem>> getBreadcrum(BasePage basePage) {
		String pageId = appHelper.getPageId(basePage);
		Node<NavMenuItem> currentItem = duplicate(_flatMainPageIdNodes.get(pageId));
		
		List<Node<NavMenuItem>> breadcrum = Lists.newArrayList();
		if(currentItem != null) {
			if(logger.isDebugEnabled()) {
				logger.debug("Getting breadcrum. Current Page ID is : {}", pageId);
				logger.debug("Item matching URL is : {}",currentItem);
			}
			breadcrum.add(currentItem);
			while(currentItem.getParentId()!=null) {
				currentItem=duplicate(_flatIdNodes.get(currentItem.getParentId()));
				breadcrum.add(0,currentItem); //  We must add at the beginning of the list
			}
		}
		if(logger.isDebugEnabled()) {
			logger.debug("Processed Breadcrum : {}", breadcrum);
		}
		return breadcrum;
	}

	// ~ -------------------- Private Methods ---------------------------
	
	@SuppressWarnings({"unchecked","rawtypes"})
	private void buildMenus(List<Node> navMenuItems) {
		int previousSize = 0;
		while(true) {
			for (int j = 0; j < navMenuItems.size(); j++) {
				boolean found = false;
				Node<NavMenuItem> node = navMenuItems.get(j);

				if(logger.isDebugEnabled()){
					logger.debug("Loading Node ID : {}",node.getId());
				}

				_flatIdNodes.put(node.getId(), node);

				if(!Strings.isNullOrEmpty(node.getData().getMainPageId())) {
					_flatMainPageIdNodes.put(node.getData().getMainPageId(), node);
				}
				
				if(node.getParentId() == null) {
					//This is a new Menu we insert it
					_treeMenu.put(node.getId(), new TreeImpl<>(node));
					if(logger.isDebugEnabled()){
						logger.debug("Parent ID is NULL, creating a new TreeMenu with ID {}",node.getId());
					}
					found = true;
				} else {
					if(logger.isDebugEnabled()){
						logger.debug("Looking for a Parent Menu with ID : {}",node.getParentId());
					}

					Node<NavMenuItem> parent = findNode(node.getParentId());
					
					if(parent != null) {
						if(logger.isDebugEnabled()){
							logger.debug("Adding Node {} to the Node found with Id {}",node.getId(),node.getParentId());
						}
						parent.addChild(node);
						found = true;
					}
				}					
				if(found) {
					navMenuItems.remove(j);
					j--; // need to process the "former" next child
				}
			}
			
			if(navMenuItems.isEmpty()){
				return;
			}
			
			if(navMenuItems.size() == previousSize){
				break;
			}
			
			previousSize = navMenuItems.size();
		}
		StringBuffer errorMsg = new StringBuffer();

		errorMsg.append(String.format("\nThere are '%s' Item(s) that could not be associated to any menu : \n",navMenuItems.size()));
		
		for (Iterator iterator = navMenuItems.iterator(); iterator.hasNext();) {
			Node node = (Node) iterator.next();
			errorMsg.append(String.format("\t\tItem '%s' declares '%s' as Parent ID // %s \n",node.getId(),node.getParentId(),node.toString()));
		}
		
		throw new MenuItemNotFoundException(errorMsg.toString());
	}

	private Node<NavMenuItem> findNode(String nodeId) {
		Object[] menuSet = _treeMenu.keySet().toArray();
		Node<NavMenuItem> node = null;
		for (int i = 0; i < menuSet.length; i++) {
			String key = (String)menuSet[i];
			Tree<NavMenuItem> tree = _treeMenu.get(key);
			if(logger.isDebugEnabled()){
				logger.debug("Loading TreeMenu : {}",key);
			}
			Node<NavMenuItem> rootNode = tree.getRootElement();
			
			if(rootNode.getId().equals(nodeId)) {
				if(logger.isDebugEnabled()){
					logger.debug("Item is a direct Child of the TreeMenu : {}",key);
				}
				return rootNode;
			}
			
			if(logger.isDebugEnabled()){
				logger.debug("Trying to check if Item belongs to TreeMenu : {}",key);
			}
			node = rootNode.getChild(nodeId);
			if(node != null) {
				if(logger.isDebugEnabled()){
					logger.debug("Node {} Found in the TreeMenu {} !",nodeId,key);
				}
				return node;
			}
		}
		
		return null;
	}
	
	private static Node<NavMenuItem> duplicate(Node<NavMenuItem> original) {
		if(original == null) {
			return null;
		}
		
		try {
			return original.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}
}
