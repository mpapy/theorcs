/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.model.menu;

import java.util.UUID;

import org.libermundi.theorcs.core.services.StringSource;

import com.google.common.base.Strings;


/**
 * @author Martin Papy
 *
 */
public abstract class AbstractNavMenuItem implements NavMenuItem {
	private static final long serialVersionUID = 2848298034422862144L;

	private NavMenuItemType _type;
	private String _id;
	private String _description;
	private String _mainPageId;
	private String _uri;
	private String _css;
	private StringSource _label;
	
	public AbstractNavMenuItem(NavMenuItemType type) {
		_id = UUID.randomUUID().toString();
		_type = type;
		_css = "";
		_mainPageId="";
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getId()
	 */
	@Override
	public String getId() {
		return _id;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getLabel()
	 */
	@Override
	public String getLabel() {
		return _label.renderString();
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getDescription()
	 */
	@Override
	public String getDescription() {
		return _description;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getType()
	 */
	@Override
	public NavMenuItemType getType() {
		return _type;
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getURI()
	 */
	@Override
	public String getURI() {
		return _uri;
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#setMainPageId(java.lang.String)
	 */
	@Override
	public void setMainPageId(String pageId) {
		_mainPageId = pageId;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#setLabel(java.lang.String)
	 */
	@Override
	public void setLabel(final String label) {
		setLabel(new StringSource() {
			@Override
			public String renderString() {
				return label;
			}
		});
	}
	
	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#setDynamicLabel(org.libermundi.theorcs.layout.model.menu.DynamicLabel)
	 */
	@Override
	public void setLabel(StringSource label) {
		_label = label;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String description) {
		_description = description;
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#setURI(java.lang.String)
	 */
	@Override
	public void setURI(String uri) {
		_uri = uri;
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getCss()
	 */
	@Override
	public String getCss() {
		return _css;
	}

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#setCss(java.lang.String)
	 */
	@Override
	public void setCss(String css) {
		_css = css;
	}
	
		
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.NavMenuItem#getMainPageId()
	 */
	@Override
	public String getMainPageId() {
		return _mainPageId;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.NodeData#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return (Strings.isNullOrEmpty(_label+_description));
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NavMenuItem [Type=" + _type + ", Id=" + _id + ", URI=" + _uri + "]";
	}
}
