package org.libermundi.theorcs.layout.dao.hibernate;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.libermundi.theorcs.core.dao.hibernate.HibernateGenericDao;
import org.libermundi.theorcs.layout.dao.TemplateDao;
import org.libermundi.theorcs.layout.model.Template;
import org.springframework.stereotype.Repository;

@Repository
public class TemplateDaoImpl extends HibernateGenericDao<Template, Long> implements TemplateDao {
	public TemplateDaoImpl(){
		super();
		_mappedClazz = Template.class;
	}	

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.dao.TemplateDao#getByCode(java.lang.String)
	 */
	@Override
	public Template getByCode(String code) {
		Criteria criteria = createSafeCriteria().
			add(Restrictions.eq(Template.PROP_CODE, code));
		return findUniqueByCriteria(criteria);
	}
}
