package org.libermundi.theorcs.layout.tapestry.services;

import java.util.List;

import org.libermundi.theorcs.layout.tapestry.helper.DynamicResource;

public interface DynamicPageTemplateSource {
	List<DynamicResource> getSources();
}
