/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.components.menu;

import java.util.List;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SupportsInformalParameters;
import org.apache.tapestry5.corelib.components.Any;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.core.tapestry.services.configuration.ApplicationConfig;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.tapestry.services.MenuService;

/**
 * @author Martin Papy
 *
 */
@SupportsInformalParameters
@Import(module={
		"bootstrap/dropdown",
		"bootstrap/transition"
})
public class InnerNav {
	
	@Property(write=false)
	private String _registerPage;
	
    @Inject
    private ComponentResources _resources;
    
	@Inject
	private JavaScriptSupport _jsSupport;
	
	@Component(id="LoginBox")
	private Any _loginBox;
	
	@Inject
	private Block _defaultMenuItem;

	@Inject
	private Block _dropdownMenuItem;
	
	@Inject
	private Block _dynamicDropdownMenuItem;
	
	@Inject
	private Block _searchFormMenuItem;
	
	@Inject
	private Block _loginMenuItem;
	
	@Inject
	private MenuService _menuService;
	
	@Inject
	private ApplicationConfig _appConfig;	
	
	@Property
	private Node<NavMenuItem> _currentMenuItem;
	
	@Parameter(defaultPrefix=BindingConstants.LITERAL)
	private String _menuId;
	
	public Block getRenderMenuItem() {
		switch (_currentMenuItem.getData().getType()) {
			case STATIC_LIST:
				return _dropdownMenuItem;
				
			case LOGINBOX:
				return _loginMenuItem;
				
			case SEARCHFORM:
				return _searchFormMenuItem;
				
			case DYNAMIC_LIST:
				return _dynamicDropdownMenuItem;
				
			default:
				return _defaultMenuItem;
		}
	}
	
	public List<Node<NavMenuItem>> getMenuItems(){
		return _menuService.getNodeById(_menuId).getChildren();
	}
	
	public String getLoginBoxId() {
		return _loginBox.getClientId();
	}
    
    @OnEvent("renderInformationalParameters")
    public void renderInformationalParameters(MarkupWriter writer){
    	_resources.renderInformalParameters(writer);
    }
    
    @OnEvent("renderLoginJs")
    public void renderLoginJs(){
		_jsSupport.require("zone.modal").invoke("init").with(getLoginBoxId());
    }
}
