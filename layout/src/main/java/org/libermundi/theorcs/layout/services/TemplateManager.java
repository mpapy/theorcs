package org.libermundi.theorcs.layout.services;

import java.util.List;

import org.libermundi.theorcs.core.services.Manager;
import org.libermundi.theorcs.layout.model.Template;

public interface TemplateManager extends Manager<Template,Long> {

	List<Template> getTemplates();

	Template getByCode(String code);

}
