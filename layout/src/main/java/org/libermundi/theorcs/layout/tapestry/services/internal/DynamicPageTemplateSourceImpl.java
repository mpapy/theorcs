package org.libermundi.theorcs.layout.tapestry.services.internal;

import java.util.List;

import org.libermundi.theorcs.layout.tapestry.helper.DynamicResource;
import org.libermundi.theorcs.layout.tapestry.services.DynamicPageTemplateSource;

public final class DynamicPageTemplateSourceImpl implements DynamicPageTemplateSource {
	private List<DynamicResource> _sources;
	
	public DynamicPageTemplateSourceImpl(List<DynamicResource> sources){
		_sources = sources;
	}

	@Override
	public List<DynamicResource> getSources() {
		return _sources;
	}

}
