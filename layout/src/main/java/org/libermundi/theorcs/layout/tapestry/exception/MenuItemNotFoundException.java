package org.libermundi.theorcs.layout.tapestry.exception;

public class MenuItemNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -7178100843024592572L;

	public MenuItemNotFoundException(String message) {
		super(message);
	}

}
