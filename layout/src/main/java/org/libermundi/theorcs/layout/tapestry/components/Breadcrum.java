/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.components;

import java.util.List;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.core.tapestry.base.BasePageImpl;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.tapestry.services.MenuService;

public class Breadcrum {
	@Inject
	private MenuService _menuService;
	
	@Inject
	private ComponentResources _resources;
	
	@Parameter(required=true,defaultPrefix=BindingConstants.LITERAL)
	private String _pageId;
	
	@Property
	private List<Node<NavMenuItem>> _breadcrum;
	
	@Property
	private Node<NavMenuItem> _item;
	
	private Node<NavMenuItem> _lastItem;
	
	@SetupRender
	public void setupRender() {
		BasePageImpl currentPage =  (BasePageImpl)_resources.getPage();
		_breadcrum = _menuService.getBreadcrum(currentPage);
		if(_breadcrum.size()!=0) {
			_lastItem = _breadcrum.get(_breadcrum.size()-1);
		}
	}
	
	public boolean isLast() {
		return (_item.equals(_lastItem));
	}

}
