/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.components;

import java.util.Iterator;
import java.util.List;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.SupportsInformalParameters;
import org.libermundi.theorcs.core.model.LinkItem;
import org.libermundi.theorcs.core.model.ListItem;
import org.libermundi.theorcs.core.model.Node;
import org.springframework.security.core.userdetails.UserDetails;

import com.google.common.base.Strings;


@SupportsInformalParameters
public class ListRenderer<T extends ListItem> {

	@Parameter(name="maxDepth", value="1", defaultPrefix=BindingConstants.LITERAL, required=true)
	private int _maxDepth;
	
	@Parameter(name="listMarkup",required=false, value="ul",defaultPrefix=BindingConstants.LITERAL)
	private String _listMarkup;
	
	@Parameter(name="listItemMarkup",required=false, value="li",defaultPrefix=BindingConstants.LITERAL)
	private String _listItemMarkup;

	@Parameter(name="listClass", defaultPrefix=BindingConstants.LITERAL, allowNull=true)
	private String _listClass;
	
	@Parameter(name="listStyle", defaultPrefix=BindingConstants.LITERAL, allowNull=true)
	private String _listStyle;
	
	@Parameter(name="itemClass", defaultPrefix=BindingConstants.LITERAL, allowNull=true)
	private String _itemClass;
	
	@Parameter(name="emptyItemClass", defaultPrefix=BindingConstants.LITERAL, allowNull=true)
	private String _emptyItemClass;
	
	@Parameter(name="rootNode", required=true)
	private Node<T> _rootNode;

	@Parameter(name="active", required=false, value="false")
	private boolean _active;
	
	@Parameter(name="userDetails", required=false, allowNull=true)
	private UserDetails _userDetails;
	
	@Parameter(name="currentItemClass", required=false,defaultPrefix=BindingConstants.LITERAL,value="current")
	private String _currentItemClass;
	
	@Parameter(name="currentNode", required=false)
	private Node<T> _currentNode; 
	
	@BeginRender
	void beginRender(MarkupWriter writer) {
		renderList(writer,_rootNode,0);
	}
	
	private void renderList(MarkupWriter writer, Node<T> rootItem, int currentDepth) {
		writer.element(_listMarkup, "class", _listClass, "style", _listStyle);
		
		renderItems(writer, rootItem, currentDepth);
		
		// End "listMarkup"
		writer.end();		
	}
	
	private void renderItems(MarkupWriter writer, Node<T> rootItem, int currentDepth) {
		int depth = currentDepth;
		List<Node<T>> listItem = getChildren(rootItem, _userDetails);
		for (Iterator<Node<T>> iterator = listItem.iterator(); iterator.hasNext();) {
			_currentNode = iterator.next();
			T currentData = _currentNode.getData();
			if(!(Strings.isNullOrEmpty(currentData.getLabel()))){
				//start "itemMarkup"
				if(_active) {
					writer.element(_listItemMarkup, "class", _currentItemClass);
				} else {
					writer.element(_listItemMarkup, "class", _itemClass);
				}

				if(currentData instanceof LinkItem) {
					LinkItem linkItem =(LinkItem)currentData;
					//start a
					if(linkItem.getURL().trim().length()>0) {
						writer.element("a", "href", linkItem.getURL(), "title", linkItem.getDescription());
					} else {
						writer.element("a", "name", _currentNode.getId(), "title", linkItem.getDescription());
					}
					
				}
				
				writer.write(currentData.getLabel());
				
				if(currentData instanceof LinkItem) {
					//End "a"
					writer.end();
				}
			} else {
				//start "itemMarkup"
				writer.element(_listItemMarkup, "class", _emptyItemClass);
			}
			
			//End "itemMarkup"
			writer.end();
			
			if(_currentNode.hasChildren() && depth < _maxDepth)  {
				renderList(writer, _currentNode, depth++);
			}			
		}
	}	
	
	public List<Node<T>> getChildren(Node<T> node, UserDetails userDetails) {
//		if(node instanceof SecuredNode) {
//			return ((SecuredNode<T>)node).getGrantedChildren(userDetails);
//		}
		return node.getChildren();
	}	
}
