/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.components.menu;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.ClientElement;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;

import com.google.common.base.Strings;

/**
 * @author Martin Papy
 *
 */
@Import(module="bootstrap/tab")
public class TabbedMenu implements ClientElement {
	@Inject
	private JavaScriptSupport _javaScriptSupport;
	
	@Inject
	private ComponentResources _resources;
	
	private String _clientId;
	
	@Parameter(required=true,defaultPrefix=BindingConstants.LITERAL,allowNull=false)
	@Property(write=false)
	private String _tab1Title;
	
	@Parameter(defaultPrefix=BindingConstants.LITERAL,value="Tab 2",allowNull=false)
	@Property(write=false)
	private String _tab2Title;
	
	@Parameter(defaultPrefix=BindingConstants.LITERAL,value="Tab 3",allowNull=false)
	@Property(write=false)
	private String _tab3Title;
	
	@Parameter(defaultPrefix=BindingConstants.LITERAL,value="Tab 4",allowNull=false)
	@Property(write=false)
	private String _tab4Title;
	
	@Parameter(defaultPrefix=BindingConstants.LITERAL,value="Tab 5",allowNull=false)
	@Property(write=false)
	private String _tab5Title;
	
	@Parameter(defaultPrefix=BindingConstants.LITERAL,value="Tab 6",allowNull=false)
	@Property(write=false)
	private String _tab6Title;
	
	@Parameter(defaultPrefix=BindingConstants.LITERAL,value="Tab 7",allowNull=false)
	@Property(write=false)
	private String _tab7Title;
	
	@Parameter(defaultPrefix=BindingConstants.LITERAL,value="Tab 8",allowNull=false)
	@Property(write=false)
	private String _tab8Title;
	
	@Parameter(allowNull=true)
	@Property(write=false)
	private Block _tab1;

	@Parameter(allowNull=true)
	@Property(write=false)
	private Block _tab2;
	
	@Parameter(allowNull=true)
	@Property(write=false)
	private Block _tab3;
	
	@Parameter(allowNull=true)
	@Property(write=false)
	private Block _tab4;
	
	@Parameter(allowNull=true)
	@Property(write=false)
	private Block _tab5;
	
	@Parameter(allowNull=true)
	@Property(write=false)
	private Block _tab6;
	
	@Parameter(allowNull=true)
	@Property(write=false)
	private Block _tab7;

	@Parameter(allowNull=true)
	@Property(write=false)
	private Block _tab8;
	
	/* (non-Javadoc)
	 * @see org.apache.tapestry5.ClientElement#getClientId()
	 */
	@Override
	public String getClientId() {
		if(Strings.isNullOrEmpty(_clientId)) {
			_clientId = _javaScriptSupport.allocateClientId(_resources);
		}
		return _clientId;
	}

}
