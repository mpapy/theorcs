/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.components.ui;

import java.util.List;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;

import com.google.common.base.Strings;

/**
 * @author Martin Papy
 *
 */
public class AdminMenu {
	@Parameter(defaultPrefix=BindingConstants.PROP)
	@Property(write=false)
	private List<Node<NavMenuItem>> _source;
	
	@Property
	private Node<NavMenuItem> _currentMenuChild;
	
	@Property
	private Node<NavMenuItem> _adminMenuItem;	
	
	@Property
	private int _menuIndex;
	
	@Inject
	private Messages _messages;
	
	private int _menuLength;

	public boolean isLastItem(){
		return _menuIndex == _menuLength-1 ;
	}	
	
	public List<Node<NavMenuItem>> getCurrentMenuChildren(){
		List<Node<NavMenuItem>> menuChildren;

		menuChildren = _adminMenuItem.getChildren();
		
		_menuLength = menuChildren.size();
		
		return menuChildren;
	}
	
	public String getItemDescription(){
		return getLocalizedMessage(_currentMenuChild.getData().getDescription());
	}
	
	public String getItemLabel(){
		return getLocalizedMessage(_currentMenuChild.getData().getLabel());
	}
	
	public String getMenuDescription(){
		return getLocalizedMessage(_adminMenuItem.getData().getDescription());
	}
	
	public String getMenuLabel(){
		return getLocalizedMessage(_adminMenuItem.getData().getLabel());
	}
		
	private String getLocalizedMessage(String message){
		if(Strings.isNullOrEmpty(message)){
			return "";
		}
		
		if(message.startsWith(BindingConstants.MESSAGE + ":")) {
			int offset = BindingConstants.MESSAGE.length() + 1;
			return _messages.get(message.substring(offset));
		}
		return message;
	}
}
