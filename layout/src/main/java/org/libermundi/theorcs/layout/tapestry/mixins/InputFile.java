package org.libermundi.theorcs.layout.tapestry.mixins;

import org.apache.tapestry5.Field;
import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.InjectContainer;
import org.apache.tapestry5.annotations.MixinAfter;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;

@MixinAfter
public class InputFile {
	@Environmental
	private JavaScriptSupport _jsSupport;

	@InjectContainer
	private Field _field;

	@AfterRender
	public void afterRender() {
		_jsSupport.require("file-input").invoke("initField").with(_field.getClientId());
	}

}
