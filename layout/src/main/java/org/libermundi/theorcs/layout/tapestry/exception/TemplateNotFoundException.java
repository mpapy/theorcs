package org.libermundi.theorcs.layout.tapestry.exception;

public class TemplateNotFoundException extends Exception {
	private static final long serialVersionUID = -8807908291612501509L;

	public TemplateNotFoundException(String message) {
		super(message);
	}


}
