/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.components;

import org.apache.tapestry5.ClientElement;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.apache.tapestry5.util.TextStreamResponse;
import org.libermundi.theorcs.security.tapestry.services.SecurityHelper;

/**
 * @author Martin Papy
 *
 */
public class SessionDialogTimeOut implements ClientElement {
	private static final String KEEPALIVE_EVENT="keepalive";
	
	@Inject
	private Request _request;
	
	@Inject
	private JavaScriptSupport _javaScriptSupport;
	
	@Inject
	private SecurityHelper _securityHelper;
	
    @Inject
    private ComponentResources _resources;
    
    @Inject
    private Messages _messages;
	
	private String _clientId;
	
	@BeginRender
	public void beginRender() {
		_clientId = _javaScriptSupport.allocateClientId(_resources);
	}
	
	@AfterRender
	public void afterRender(){
		JSONObject params = new JSONObject();
			params.put("clientId",_clientId);
			params.put("keepAliveUrl",_resources.createEventLink(KEEPALIVE_EVENT).toAbsoluteURI());
			params.put("timeOutUrl", _securityHelper.getSpringSecurityLogoutUrl());
			params.put("idleAfter",_request.getSession(false).getMaxInactiveInterval());
		_javaScriptSupport.require("idletimeout").with(params);
	}
	
	public String getWarningMessage() {
		return _messages.format("layout.timeout.dialog.message", getClientId());
	}
	
	@OnEvent(value=KEEPALIVE_EVENT)
	public StreamResponse keppAlive() {
		return new TextStreamResponse("text/plain","OK");
	}

	/* (non-Javadoc)
	 * @see org.apache.tapestry5.ClientElement#getClientId()
	 */
	@Override
	public String getClientId() {
		return _clientId;
	}
}
