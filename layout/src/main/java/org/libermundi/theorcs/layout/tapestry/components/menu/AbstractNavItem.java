/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.components.menu;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.tapestry.model.menu.ImageNavMenuItem;
import org.libermundi.theorcs.layout.tapestry.services.MenuService;

import com.google.common.base.Strings;

/**
 * @author Martin Papy
 *
 */
public abstract class AbstractNavItem {
	@Inject
	private Messages _messages;
	
	@Inject
	private MenuService _menuService;
	
	@Inject
	private ComponentResources _resources;

	protected String getLocalizedMessage(String message){
		if(Strings.isNullOrEmpty(message)){
			return "";
		}
		
		if(message.startsWith(BindingConstants.MESSAGE + ":")) {
			int offset = BindingConstants.MESSAGE.length() + 1;
			return _messages.get(message.substring(offset));
		}
		return message;
	}
	
	protected void defaultItemRendering(MarkupWriter writer, Node<NavMenuItem> item){
		NavMenuItem navItem = item.getData();
		switch (navItem.getType()) {
		
		case DIVIDER:
			writer.element("li", "class","divider-vertical");
			writer.end();
			break;
			
		case ICON:
			writer.element("li");
			if(isItemActive(item)) {
				writer.attributes("class","active");
			}
			writer.element("a",
					"href",navItem.getURI(),
					"title",getLocalizedMessage(navItem.getDescription()));

			writer.element("span","class", navItem.getCss());
			writer.end(); //i
			
			writer.end(); //a
			
			writer.end(); //li	
			break;
			
		case IMAGE:
			assert (navItem instanceof ImageNavMenuItem);
			writer.element("li");
			if(isItemActive(item)) {
				writer.attributes("class","active");
			}
			if(!Strings.isNullOrEmpty(navItem.getURI())) {
				writer.element("a",
						"href",navItem.getURI(),
						"title",getLocalizedMessage(navItem.getDescription()));
			}
			
			writer.element("img",
					"src",((ImageNavMenuItem)navItem).getImage().toClientURL(),
					"alt",getLocalizedMessage(navItem.getDescription()));
			writer.end(); // img
			
			if(!Strings.isNullOrEmpty(navItem.getURI())) {
				writer.end(); // a
			}
			
			writer.end(); // li
			break;

		default:
			writer.element("li");
			if(isItemActive(item)) {
				writer.attributes("class","active");
			}
			
			writer.element("a",
					"href",navItem.getURI(),
					"title",getLocalizedMessage(navItem.getDescription()));
			
			if(navItem.getCss().length() > 0){
				writer.element("span","class", navItem.getCss());
				writer.end();
			}
			writer.write(" "); // Add a Space between the Span and the Label
			writer.write(getLocalizedMessage(navItem.getLabel()));
			
			writer.end(); //a
			
			writer.end(); //li			
			break;
		}
	}
	
	protected boolean isItemActive(Node<NavMenuItem> item) {
		return _menuService.isItemActive(_resources, item);
	}
	
}
