package org.libermundi.theorcs.layout.tapestry.services.internal;

import java.util.List;

import org.apache.tapestry5.ioc.Resource;
import org.apache.tapestry5.model.ComponentModel;
import org.apache.tapestry5.services.ComponentClassResolver;
import org.apache.tapestry5.services.pageload.ComponentResourceLocator;
import org.apache.tapestry5.services.pageload.ComponentResourceSelector;
import org.libermundi.theorcs.layout.tapestry.helper.DynamicResource;
import org.libermundi.theorcs.layout.tapestry.services.DynamicPageTemplateSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DynamicComponentResourceLocator implements ComponentResourceLocator {
	private static final Logger logger = LoggerFactory.getLogger(DynamicComponentResourceLocator.class);	

	private final DynamicPageTemplateSource _dynTemplateResources;
    
    private final ComponentResourceLocator _delegate;

    public DynamicComponentResourceLocator(
          ComponentResourceLocator delegate,
          Resource contextRoot,
          ComponentClassResolver resolver,
          DynamicPageTemplateSource dynTemplateResources) {

        this._delegate = delegate;
        this._dynTemplateResources = dynTemplateResources;
    }

    @Override
    public Resource locateTemplate(ComponentModel model, ComponentResourceSelector selector) {

        if(!model.isPage()) {
            return null;
        }

        String className = model.getComponentClassName();

        
        Resource resource = findDynamicResource(className);
        if(resource != null) {
        	return resource;
        }
        
        return _delegate.locateTemplate(model, selector);
    }

    @Override
    public List<Resource> locateMessageCatalog(
              Resource baseResource,
              ComponentResourceSelector selector) {

        return _delegate.locateMessageCatalog(baseResource, selector);
    }
    
    private Resource findDynamicResource(String className) {
        Resource resource = null;
        for (DynamicResource dynTemplateResource : _dynTemplateResources.getSources()) {
            resource = dynTemplateResource.forClassName(className);
            if (resource != null) {
                break;
            }
        }
        
        if(logger.isDebugEnabled()) {
        	logger.debug("Trying to locate a Template for : " + className + " : " +(resource!=null?resource:"NotFound"));
        }
        
        return resource;
    }    
}