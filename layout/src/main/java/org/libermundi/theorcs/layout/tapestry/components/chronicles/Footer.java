package org.libermundi.theorcs.layout.tapestry.components.chronicles;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ClientElement;
import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.libermundi.theorcs.security.services.SecurityManager;

public class Footer implements ClientElement {
	@Inject
	private JavaScriptSupport _jsSupport;
	
	private String _assignedClientId;
	
	@Parameter(required=true,defaultPrefix=BindingConstants.LITERAL)
	private String _id;

	@Property
    @Inject
    @Symbol(SymbolConstants.APPLICATION_VERSION)
    private String _appVersion;
	
	@Inject
	private SecurityManager _securityManager;
	
	@SetupRender
	public void setupRender(){
		_assignedClientId = _jsSupport.allocateClientId(_id);
	}

	/* (non-Javadoc)
	 * @see org.apache.tapestry5.ClientElement#getClientId()
	 */
	@Override
	public String getClientId() {
		return _assignedClientId;
	}
	
	public String getCurrentUsername() {
		return _securityManager.getCurrentUsername();
	}
}
