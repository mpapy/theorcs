/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.model.menu;

import org.libermundi.theorcs.core.model.NodeData;
import org.libermundi.theorcs.core.services.StringSource;
import org.libermundi.theorcs.core.tapestry.base.BasePageImpl;
import org.libermundi.theorcs.layout.tapestry.components.Breadcrum;
import org.libermundi.theorcs.layout.tapestry.components.menu.AbstractNavItem;
import org.libermundi.theorcs.layout.tapestry.services.MenuService;


/**
 * @author Martin Papy
 *
 */
public interface NavMenuItem extends NodeData {
	String getId();
	String getMainPageId();
	String getLabel();
	String getDescription();
	NavMenuItemType getType();
	String getURI();
	String getCss();
	
	/**
	 * Defines the ID of the {@link NavMenuItem} it is internally used by the {@link MenuService}
	 * to Build the {@link Breadcrum}
	 * 
	 * In order to build the Breadcrum the pageId of the node must be set to the {@link BasePageImpl#getPageId()}
	 * @param pageId
	 */
	void setMainPageId(String pageId);
	
	void setLabel(String label);
	void setLabel(StringSource label);
	void setDescription(String description);
	void setURI(String uri);
	
	/**
	 * User to set any extra CSS that would be added during the rendering phase
	 * Typically the {@link AbstractNavItem} will add a <i class="extraCss"></i>
	 * @param css
	 */
	void setCss(String css);
}
