/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.mixins;

import org.apache.tapestry5.ClientElement;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.InjectContainer;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.EventLink;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;

import com.google.common.base.Strings;

/**
 * @author Martin Papy
 *
 */
public class Confirm implements ClientElement {
	@Parameter
	@Property(write=false)
	private String _title;
	
	@Parameter
	@Property(write=false)
	private String _message;
	
	@Inject
	private Messages _messages;
	
	@Inject
	private JavaScriptSupport _javaScriptSupport;
	
	@Inject
	private ComponentResources _resources;
	
	@InjectContainer
	private EventLink _link;
	
	private String _clientId;
	
	@AfterRender
	public void afterRender(MarkupWriter writer){
		
		renderConfirmDialog(writer);
		
		JSONObject params = new JSONObject();
			params.append("confirmId", getClientId())
				.append("buttonId", _link.getClientId());
			
		_javaScriptSupport.require("confirm").with(params);
	}

	/* (non-Javadoc)
	 * @see org.apache.tapestry5.ClientElement#getClientId()
	 */
	@Override
	public String getClientId() {
		if(Strings.isNullOrEmpty(_clientId)){
			_clientId = _javaScriptSupport.allocateClientId(_resources);
		}
		return _clientId;
	}
	
	private void renderConfirmDialog(MarkupWriter writer) {
		writer.element("div", "id", getClientId(),
				"class", "modal fade",
				"role","dialog",
				"aria-hidden","true");
			writer.element("div", "class","modal-dialog");
				writer.element("div", "class","modal-content");
					writer.element("div", "class","modal-header");
					
						writer.element("button", "class","close",
										"data-dismiss","modal");
						writer.write("x");
						
						writer.end();//button close
						
						writer.element("h4");
						
						writer.write(_title);
						
						writer.end();//h3
					
					writer.end();//div modal header
					
					writer.element("div", "class","modal-body");
				
						writer.element("p");
						
						writer.writeRaw(_message);
						
						writer.end(); //p
					
					writer.end();//div modal body
					
					writer.element("div", "class","modal-footer");
						writer.element("button", "class","btn btn-danger");
						
						writer.write(_messages.get("layout.buttons.confirm"));
						
						writer.end();//button confirm
						
						writer.element("button", "class","btn",
								"data-dismiss","modal");
						writer.write(_messages.get("layout.buttons.cancel"));
						
						writer.end();//button cancel
					
					writer.end();//div modal footer
				writer.end();//div modal content
			writer.end();//div modal dialog
		writer.end();//div modal window
	}

}
