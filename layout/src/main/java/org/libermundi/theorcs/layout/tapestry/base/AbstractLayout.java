/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.base;

import org.apache.commons.io.FilenameUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;
import org.libermundi.theorcs.core.tapestry.services.PageRenderSupport;
import org.libermundi.theorcs.layout.tapestry.services.MenuService;
import org.libermundi.theorcs.security.services.SecurityManager;
import org.springframework.security.core.userdetails.UserDetails;
@Import(module="btstrp/fix-grid")
public abstract class AbstractLayout {
	@Inject
	@Property(write=false)
	private Request _request;
	
	@Environmental
	private PageRenderSupport _pageRenderSupport;
	
	@Inject
	private MenuService _menuService;

	@Inject
	private ComponentResources _resources;	
	
	@Inject
	private SecurityManager _securityManager;
	
	@Inject
	@Path("${layout.images}/TheORCS-favicon.png")
	private Asset _defaultFavicon;
	
	@SetupRender
	private void setDefaultFavicon() {
		Asset fav = _pageRenderSupport.getFavicon();
		if(fav == null || !fav.getResource().exists()){
			_pageRenderSupport.setFavicon(_defaultFavicon);
		}
	}
	
	public Asset getFavicon() {
		return _pageRenderSupport.getFavicon();
	}
	
	public String getFaviconType(){
		Asset favicon = getFavicon();
		String ext = FilenameUtils.getExtension(favicon.getResource().getFile()).toLowerCase();
		if( ext.equals("gif") ) {
			return "image/gif";
		} else if ( ext.equals("png") ) {
			return "image/png";
		}
		return "image/x-icon";
	}
	
	public String getPageDescription(){
		return _pageRenderSupport.getPageDescription();
	}
	
	public String getPageKeywords(){
		return _pageRenderSupport.getPageKeywords();
	}
	
	public String getPageTitle(){
		return _pageRenderSupport.getPageTitle();
	}
	
	public MenuService getMenuService() {
		return _menuService;
	}

	public SecurityManager getSecurityManager() {
		return _securityManager;
	}
	
	public UserDetails getUserDetails() {
		return _securityManager.getCurrentUserDetails();
	}

}
