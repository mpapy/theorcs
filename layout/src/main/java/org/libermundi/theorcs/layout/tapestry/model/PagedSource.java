/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PagedSource<T> implements Iterable<T> {
	
	private List<T> _source = new ArrayList<>();
	private List<T> _pageSource = new ArrayList<>();
	private Integer _iterableSize;
	private Integer _pageIndex;
	
	public PagedSource(Iterable<T> source) {
		for(T aSource : source) {
			_source.add(aSource);
		}
	}

	@Override
	public Iterator<T> iterator() {
		return _pageSource.iterator();
	}

	public int getTotalRowCount() {
		if (_iterableSize != null) {
			return _iterableSize;
		}
		
		_iterableSize = _source.size();

		 return _iterableSize;
	}
	
	 public void prepare(int startIndex, int endIndex) {
		 for (int i = startIndex; i < endIndex; i++) {
			 _pageSource.add(_source.get(i));
		 }
	 }

	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		return _pageIndex;
	}

	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		_pageIndex = pageIndex;
	}
}
