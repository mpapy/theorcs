/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.services;

import org.apache.tapestry5.Block;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.core.model.NodeConstraint;
import org.libermundi.theorcs.core.tapestry.BasePage;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.services.NavItemSource;

public interface LayoutAppHelper {
	/**
	 * 
	 * @param templatePath
	 * @return
	 */
	Block findTemplate(String templatePath);	

	Node<NavMenuItem> buildStaticMenuNode(String parentId, String label, String uri, NodeConstraint...constrains);
	Node<NavMenuItem> buildDynamicMenuNode(String parentId, NavItemSource source, String label, String uri, NodeConstraint...constrains);

	<T extends BasePage> Node<NavMenuItem>  buildStaticMenuNode(String parentId, String label, T page, Object[] context, NodeConstraint...constrains);
	<T extends BasePage> Node<NavMenuItem>  buildDynamicMenuNode(String parentId, NavItemSource source, String label, T page, Object[] context, NodeConstraint...constrains);
	
	
}
