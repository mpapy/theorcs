/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.model.menu;

import org.apache.tapestry5.Asset2;
import org.libermundi.theorcs.layout.model.menu.AbstractNavMenuItem;
import org.libermundi.theorcs.layout.model.menu.NavMenuItemType;

/**
 * @author Martin Papy
 *
 */
public class ImageNavMenuItem extends AbstractNavMenuItem {
	private static final long serialVersionUID = -261860178095330780L;

	private final Asset2 _image;

	public ImageNavMenuItem(Asset2 image) {
		super(NavMenuItemType.IMAGE);
		_image = image;
	}

	/**
	 * @return the image
	 */
	public Asset2 getImage() {
		return _image;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.core.model.NodeData#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return (_image != null && _image.getResource().exists());
	}

}
