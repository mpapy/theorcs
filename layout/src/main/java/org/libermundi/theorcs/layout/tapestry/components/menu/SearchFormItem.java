/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.components.menu;

import java.util.List;

import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.core.services.SearchServices;

/**
 * @author Martin Papy
 *
 */
public class SearchFormItem {
	@Inject
	private Messages _messages;
	
	@Inject
	private SearchServices _searchServices;
	
	@Property
	private String _keyword;
	
	public String getPlaceHolderMessage(){
		return _messages.get("layout.portal.navbar.search.placeholder");
	}

	@OnEvent(value = "provideCompletions")
	public List<String> provideCompletions(String start) {
		return _searchServices.getSuggestions(start);
	}
	
	
}
