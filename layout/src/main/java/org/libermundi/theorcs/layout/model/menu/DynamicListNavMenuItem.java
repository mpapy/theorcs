/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.model.menu;

import java.util.List;

import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.layout.services.NavItemSource;

/**
 * @author Martin Papy
 *
 */
public class DynamicListNavMenuItem extends AbstractNavMenuItem implements ListNavMenuItem {
	private static final long serialVersionUID = 3508428947707816868L;

	private final NavItemSource _source;
	
	@SuppressWarnings("unused")
	private final boolean _displayIfEmpty;

	/**
	 * @param type
	 */
	public DynamicListNavMenuItem(NavItemSource source) {
		this(source,true);
	}
	
	public DynamicListNavMenuItem(NavItemSource source, boolean displayIfEmpty) {
		super(NavMenuItemType.DYNAMIC_LIST);
		_displayIfEmpty = displayIfEmpty;
		_source = source;
	}
	

	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.ListNavMenuItem#getElements()
	 */
	@Override
	public List<Node<NavMenuItem>> getElements() {
		return _source.getElements();
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.model.menu.AbstractNavMenuItem#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return _source.getElements().isEmpty();
	}
	
}
