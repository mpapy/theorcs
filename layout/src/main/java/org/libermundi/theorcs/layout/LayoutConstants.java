/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout;


public final class LayoutConstants {
	private LayoutConstants() {}
	
	public static final String TAPESTRY_MAPPING = "layout";
	
	public static final String TAPESTRY_SCHEMA = "http://tapestry.apache.org/schema/tapestry_5_3_0.xsd";
    public static final String FO_LAYOUT_TEMPLATES_PATH = "layout/inner/LayoutTemplates";
    public static final String FO_LAYOUT_DEFAULT_TEMPLATE = "Portal";
	public static final String TBL_TEMPLATE = "tbl_template";
	public static final String DEFAULT_TEMPLATE_CODE = "Portal";
	public static final String DEFAULT_CHRONICLE_TEMPLATE_CODE = "Chronicle";
	
	public static final String PORTAL_TOP_LEFT_MENU_ID = "portalTopLeftMenu";
	public static final String PORTAL_TOP_RIGHT_MENU_ID = "portalTopRightMenu";

	public static final String CHRO_TOP_LEFT_MENU_ID = "chroTopLeftMenu";
	public static final String CHRO_TOP_RIGHT_MENU_ID = "chroTopRightMenu";
	public static final String CHRO_SIDEBAR_ID = "chroTopRightMenu";
	
	public static final String INDEX_PAGENAME = "layout.navigation.pagename";
	public static final String INDEX_PAGELABEL = "layout.navigation.pagelabel";
	public static final String INDEX_PAGETITLE = "layout.navigation.pagetitle";
	
}
