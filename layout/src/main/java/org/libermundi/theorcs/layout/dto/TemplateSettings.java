/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.dto;

import org.libermundi.theorcs.layout.LayoutConstants;
import org.libermundi.theorcs.layout.model.TemplateType;

/**
 * Template Manager of Tapestry to change the template.
 * It is an ASO
 *
 */
public class TemplateSettings {
	public final static String KEY_CHRONICLE_NAME="chronicleName";
	
	private String _currentTemplateCode;
	private TemplateType _templateType;
//	private Map<String, String> _properties=new HashMap<>(0);
	
	public TemplateSettings() {
		_templateType = TemplateType.STATIC;
		_currentTemplateCode = LayoutConstants.DEFAULT_TEMPLATE_CODE;
	}
	
	/**
	 * Getter 
	 * @return currentTemplate
	 */
	public String getCurrentTemplateCode() {
		return _currentTemplateCode;
	}

	public TemplateType getTemplateType() {
		return _templateType;
	}

	/**
	 * Setter
	 * @param templateCode
	 */
	public void setCurrentTemplate(String templateCode) {
		this._currentTemplateCode = templateCode;
	}

	public void setTemplateType(TemplateType templateType) {
		_templateType = templateType;
	}
	
//	public void setProperty(String key, String value) {
//		_properties.put(key, value);
//	}
//
//	public String getProperty(String key) {
//		if(_properties.containsKey(key)){
//			return _properties.get(key);
//		}
//		return "";
//	}
}
