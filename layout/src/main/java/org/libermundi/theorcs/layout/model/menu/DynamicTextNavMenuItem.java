/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.model.menu;

import org.libermundi.theorcs.layout.services.NavItemTextSource;

/**
 * @author Martin Papy
 *
 */
public class DynamicTextNavMenuItem extends AbstractNavMenuItem {
	private static final long serialVersionUID = -5736426708039493125L;

	private final NavItemTextSource _source;
	
	public DynamicTextNavMenuItem(NavItemTextSource source) {
		super(NavMenuItemType.DEFAULT);
		_source = source;
	}
	
	public DynamicTextNavMenuItem(NavMenuItemType type, NavItemTextSource source) {
		super(type);
		_source = source;
	}

	@Override
	public String getDescription() {
		return _source.getDescription();
	}

	@Override
	public String getLabel() {
		return _source.getLabel();
	}

	@Override
	public void setLabel(String label) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setDescription(String description) {
		throw new UnsupportedOperationException();
	}

}
