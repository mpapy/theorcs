package org.libermundi.theorcs.layout.tapestry.services;

import java.util.List;

import org.apache.tapestry5.ComponentResources;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.core.tapestry.BasePage;
import org.libermundi.theorcs.core.tapestry.annotation.AdviseMethod;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;

public interface MenuService {
	@AdviseMethod
	Node<NavMenuItem> getNodeById(String rootId);

	boolean isItemActive(ComponentResources resources, Node<NavMenuItem> currentNode);
	
	@AdviseMethod
	List<Node<NavMenuItem>> getBreadcrum(BasePage nodeId);
}
