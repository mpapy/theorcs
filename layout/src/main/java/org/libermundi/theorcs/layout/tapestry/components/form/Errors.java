/*
 * Copyright (c) 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.libermundi.theorcs.layout.tapestry.components.form;

import java.util.List;

import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.ValidationTracker;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.Parameter;

@Import(module="bootstrap/alert")
public class Errors {
	
	/**
     * The banner message displayed above the errors. The default value is "You must correct the
     * following errors before
     * you may continue.".
     */
    @Parameter("message:layout.error.banner")
    private String banner;

    @Parameter("message:layout.error.title")
    private String title;
    
    // Allow null so we can generate a better error message if missing
    @Environmental(false)
    private ValidationTracker tracker;

    public boolean beginRender(MarkupWriter writer) {
        if (tracker == null){
        	throw new RuntimeException("The Errors component must be enclosed by a Form component.");
        }

        if (!tracker.getHasErrors()) {
        	return Boolean.FALSE;
        }

        renderMarkup(writer, tracker.getErrors());
        return Boolean.TRUE;
    }

    private void renderMarkup(MarkupWriter writer, List<String> errors){
        writer.element("div", "class", "alert alert-error");
        /*
         * Write : <a class="close" data-dismiss="alert" href="#">×</a>
         * ( cf. Bootstrap )
         */
        writer.element("a", 
        		"class", "close",
        		"data-dismiss", "alert",
        		"href","#");
        writer.write("x");
        writer.end();
        
        //Strong title
    	writer.element("strong");
    	writer.write(title);
    	writer.end();
        
        // Banner text
        writer.write(" " + banner);

        if (!errors.isEmpty())
        {
            // Only write out the <UL> if it will contain <LI> elements. An empty <UL> is not
            // valid XHTML.

            writer.element("ul");

            for (String message : errors)
            {
                writer.element("li");
                writer.write(message);
                writer.end();
            }

            writer.end(); // ul
        }

        writer.end(); // div    	
    }
}
