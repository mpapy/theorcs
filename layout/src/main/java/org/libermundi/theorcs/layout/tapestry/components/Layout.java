/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.components;

import org.apache.tapestry5.Asset2;
import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.layout.LayoutConstants;
import org.libermundi.theorcs.layout.dto.TemplateSettings;
import org.libermundi.theorcs.layout.model.TemplateType;
import org.libermundi.theorcs.layout.tapestry.exception.TemplateNotFoundException;
import org.libermundi.theorcs.layout.tapestry.services.LayoutAppHelper;
import org.slf4j.Logger;

import com.google.common.base.Strings;

public class Layout {
	@Inject
	private Logger _logger;
	
	@SessionState
	private TemplateSettings _templateSettings; 
	
	@Inject
	private LayoutAppHelper _layoutHelper;
	
	@Inject
	private Block _portalLayout;
	
	@Inject
	private Block _chronicleLayout;
	
	@Inject
	@Path("${layout.images}/TheORCS-favicon.png")
	private Asset2 _defaultFavicon;
	
	@Property
    @Parameter(defaultPrefix = BindingConstants.LITERAL)
    private Block _sidebar;		
	
	@Property
	@Parameter(defaultPrefix = BindingConstants.PROP, value="false", required=false)
	private boolean _sideBarHidden;
	
	/**
	 * Get the LayoutTemplate from the ASO
	 * @return Object Layout
	 * @throws TemplateNotFoundException 
	 */
	public Block getLayoutTemplate() throws TemplateNotFoundException {
		String templateCode = _templateSettings.getCurrentTemplateCode();
		Block template = null;
		
		assert(!Strings.isNullOrEmpty(templateCode));
		
		if(_templateSettings.getTemplateType().equals(TemplateType.DYNAMIC)) {
			if(_logger.isDebugEnabled()) {
				_logger.debug("Loading Dynamic Template from Database");
			}
			template =_layoutHelper.findTemplate(LayoutConstants.FO_LAYOUT_TEMPLATES_PATH + '/' + templateCode);
		} else {
			if(_logger.isDebugEnabled()) {
				_logger.debug("Loading Static Template from Classpath, trying to return Template with code : " + templateCode);
			}
			if (templateCode.equals(LayoutConstants.DEFAULT_TEMPLATE_CODE)) {
				template = _portalLayout;
			} else if (templateCode.equals(LayoutConstants.DEFAULT_CHRONICLE_TEMPLATE_CODE)) {
				template = _chronicleLayout;
			}
		}
		
		if(template != null) {
			return template;
		}
		_logger.error("Cannot Find any Template with code [{}] in Database or in Static files",templateCode);
		throw new TemplateNotFoundException("Cannot Find Dynamic Template in Database or in Static files");
		

	}

	
}
