/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.components.portal;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ClientElement;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.libermundi.theorcs.core.tapestry.services.PageRenderSupport;
import org.libermundi.theorcs.layout.LayoutConstants;

import com.google.common.base.Strings;

@Import(module="bootstrap/collapse")
public class NavBar implements ClientElement {
	@Inject
	private JavaScriptSupport _jsSupport;

	@Environmental
	private PageRenderSupport _pageRenderSupport;
	
	@Property(write=false)
	private String _indexPageLabel;
	
	@Property(write=false)
	private String _indexPageName;	
	
	@Property(write=false)
	private String _indexPageTitle;

	@Parameter(required=true,defaultPrefix=BindingConstants.LITERAL)
	private String _id;
	
	private String _assignedClientId;

	@SetupRender
	public void setupRender(){
		_assignedClientId = _jsSupport.allocateClientId(_id);
		_indexPageLabel = _pageRenderSupport.getProperty(LayoutConstants.INDEX_PAGELABEL);
		_indexPageName = _pageRenderSupport.getProperty(LayoutConstants.INDEX_PAGENAME);
		_indexPageTitle = _pageRenderSupport.getProperty(LayoutConstants.INDEX_PAGETITLE);
	}

	/* (non-Javadoc)
	 * @see org.apache.tapestry5.ClientElement#getClientId()
	 */
	@Override
	public String getClientId() {
		return _assignedClientId;
	}
	
	public boolean isDisplayBrand(){
		if(!Strings.isNullOrEmpty(_indexPageLabel)) {
			return Boolean.TRUE;					
		}
		return Boolean.FALSE;
	}	

}