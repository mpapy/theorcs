/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.services.impl;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.internal.services.RequestPageCache;
import org.apache.tapestry5.internal.structure.Page;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.ApplicationStateManager;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.core.model.NodeConstraint;
import org.libermundi.theorcs.core.model.NodeFactory;
import org.libermundi.theorcs.core.tapestry.BasePage;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;
import org.libermundi.theorcs.layout.model.menu.Divider;
import org.libermundi.theorcs.layout.model.menu.DynamicListNavMenuItem;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.model.menu.NavMenuItemType;
import org.libermundi.theorcs.layout.model.menu.StaticNavMenuItem;
import org.libermundi.theorcs.layout.services.NavItemSource;
import org.libermundi.theorcs.layout.tapestry.services.LayoutAppHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LayoutAppHelperImpl implements LayoutAppHelper {
	private static final Logger logger = LoggerFactory.getLogger(LayoutAppHelperImpl.class);
	
    /** The request page cache. */
    @Inject
    private RequestPageCache _requestPageCache; 
	
	@Inject
	private AppHelper _appHelper;
	
	@Inject
	private Messages _messages;

	@Inject
	private ApplicationStateManager _asm;
	
	/*
	 * (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.tapestry.services.LayoutHelper#findTemplate(java.lang.String)
	 */
	@Override
	public Block findTemplate(String templatePath) {
        int index = templatePath.lastIndexOf('/');
        String pageName = templatePath.substring(0, index);
        String blockId = templatePath.substring(index + 1);
        Page page = _requestPageCache.get(pageName);     
        
        if(logger.isDebugEnabled()) {
	        logger.debug("TemplatePath : {}",templatePath);
	        logger.debug("PageName : {}",pageName);
	        logger.debug("BlockId : {}",blockId);
	        logger.debug("Page : {}",page);
        }
        
        Block block = page.getRootElement().getBlock(blockId);
        
        return block;
    }
	
/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.tapestry.services.LayoutAppHelper#buildStaticMenuNode(java.lang.String, java.util.List, java.lang.String, java.lang.String)
	 */
	@Override
	public Node<NavMenuItem> buildStaticMenuNode(String parentId, String label, String uri, NodeConstraint...constrains) {
		NavMenuItem item = createNavMenuItem(NavMenuItemType.DEFAULT);
		
		item.setLabel(label);
		item.setURI(uri);

		Node<NavMenuItem> node = buildNode(parentId, item, constrains);

		return node;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.tapestry.services.LayoutAppHelper#buildDynamicMenuNode(java.lang.String, java.util.List, org.libermundi.theorcs.layout.tapestry.services.NavItemSource, java.lang.String, java.lang.String)
	 */
	@Override
	public Node<NavMenuItem> buildDynamicMenuNode(String parentId,
			NavItemSource source, String label, String uri, NodeConstraint...constrains) {
		NavMenuItem item = new DynamicListNavMenuItem(source,Boolean.FALSE);
		
		
		item.setLabel(label);
		item.setURI(uri);

		Node<NavMenuItem> node = buildNode(parentId, item, constrains);
		
		return node;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.tapestry.services.LayoutAppHelper#buildStaticMenuNode(java.lang.String, java.util.List, java.lang.String, java.lang.Class, java.lang.Object[])
	 */
	@Override
	public <T extends BasePage> Node<NavMenuItem> buildStaticMenuNode(
			String parentId, String label, T pageClass,
			Object[] context, NodeConstraint...constrains) {
		Link link = _appHelper.getPageLink(pageClass.getClass(), context);
		
		NavMenuItem item = createNavMenuItem(NavMenuItemType.DEFAULT);
		item.setMainPageId(_appHelper.getPageId(pageClass));
		item.setLabel(label);
		item.setURI(link.toURI());
		
		Node<NavMenuItem> node = buildNode(parentId, item, constrains);
		
		return node;
	}

	/* (non-Javadoc)
	 * @see org.libermundi.theorcs.layout.tapestry.services.LayoutAppHelper#buildDynamicMenuNode(java.lang.String, java.util.List, org.libermundi.theorcs.layout.tapestry.services.NavItemSource, java.lang.String, java.lang.Class, java.lang.Object[])
	 */
	@Override
	public <T extends BasePage> Node<NavMenuItem> buildDynamicMenuNode(
			String parentId, NavItemSource source, String label,
			T page, Object[] context, NodeConstraint...constrains) {
		Link link = _appHelper.getPageLink(page.getClass(), context);
		
		NavMenuItem item = new DynamicListNavMenuItem(source,Boolean.FALSE);
		item.setMainPageId(_appHelper.getPageId(page));
		item.setLabel(label);
		item.setURI(link.toURI());
		
		Node<NavMenuItem> node = buildNode(parentId, item, constrains);
		
		return node;
	}

	private static NavMenuItem createNavMenuItem(NavMenuItemType type) {
		switch (type) {
			case DEFAULT: case STATIC_LIST: case SEARCHFORM: case LOGINBOX: 
				return new StaticNavMenuItem(type);
			case DIVIDER:
				return new Divider();
			default:
				break;
		}
		
		throw new IllegalArgumentException("This NavMenuItemType is not supported by LayoutAppHelper : " + type);
	}
	
	
	private static Node<NavMenuItem> buildNode(String parentId, NavMenuItem menu, NodeConstraint...constrains) {
		Node<NavMenuItem> node = NodeFactory.getNode(menu);
		if(constrains != null){
			for (int i = 0; i < constrains.length; i++) {
				node.addConstraint(constrains[i]);
			}
		}
		node.setParentId(parentId);
		return node;
	}
}
