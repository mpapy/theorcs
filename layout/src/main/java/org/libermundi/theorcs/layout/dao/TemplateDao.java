package org.libermundi.theorcs.layout.dao;

import org.libermundi.theorcs.core.dao.GenericDao;
import org.libermundi.theorcs.layout.model.Template;

public interface TemplateDao extends GenericDao<Template,Long> {
	Template getByCode(String code);
}
