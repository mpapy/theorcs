/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.layout.tapestry.components.menu;

import java.util.List;

import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Parameter;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.layout.model.menu.ListNavMenuItem;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.model.menu.NavMenuItemType;

/**
 * @author Martin Papy
 *
 */
public class DynamicDropdownItem extends AbstractNavItem {
	@Parameter
	private Node<NavMenuItem> _menuItem;
	
	@BeginRender
	public boolean render(MarkupWriter writer) {
		if(!_menuItem.mustDisplay()) {
			return Boolean.FALSE;
		}
		
		NavMenuItem navItem = _menuItem.getData();
		assert (navItem.getType() == NavMenuItemType.DYNAMIC_LIST && navItem instanceof ListNavMenuItem);
		
		if(isItemActive(_menuItem)) {
			writer.element("li","class","dropdown active");
		} else {
			writer.element("li","class","dropdown");
		}
		
		writer.element("a",
				"href","#",
				"class","dropdown-toggle",
				"data-toggle","dropdown"
				);
		if(navItem.getCss().length() > 0){
			writer.element("span","class", navItem.getCss());
			writer.end(); //span
		}
		writer.write(" ");
		writer.write(getLocalizedMessage(navItem.getLabel()));
		writer.element("b", "class", "caret");
		writer.end(); //b
		writer.end(); //a
		
		// Render the children elements
		List<Node<NavMenuItem>> children = ((ListNavMenuItem)navItem).getElements();
		if(!children.isEmpty()) {
			writer.element("ul", "class","dropdown-menu");
			for (Node<NavMenuItem> item : children) {
				defaultItemRendering(writer, item);
			}
			writer.end();
		}
		
		writer.end(); //li			

		return Boolean.FALSE;
	}	

}
