package org.libermundi.theorcs.config.spring;

import java.util.ArrayList;

import org.libermundi.theorcs.core.dao.hibernate.EntityFactoryPostProcessor;
import org.libermundi.theorcs.layout.model.Template;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:META-INF/spring/layout-context.xml")
public class LayoutConfiguration {
	@Bean
	public static EntityFactoryPostProcessor layoutEntityFactoryPostProcessor() {
		EntityFactoryPostProcessor efpp = new EntityFactoryPostProcessor();
		ArrayList<Class<?>> managedClasses = new ArrayList<>();
		
		managedClasses.add(Template.class);
		
		efpp.setManagedClasses(managedClasses);
		
		return efpp;
	}
}
