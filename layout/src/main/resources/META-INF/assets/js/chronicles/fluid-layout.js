!function($){
	"use strict"; // jshint ;_;
	$(window).resize(function(){
		resizeContent();
	});
	
	$(function() {
		resizeContent();
	});
	
	$('#globalwrapper').resize(function(){
		resizeContent();
	});
	
	function resizeContent() {
		var contentWidth = $(window).width() - 40;
		if($('#sidebar').is(":visible")) {
			contentWidth = contentWidth - $('#sidebar').outerWidth(false) - 20;
		}
		$('#content').width(contentWidth);
	};
	
}(window.jQuery);