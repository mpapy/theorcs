define(["jquery","bootstrap/modal","bootstrap/transition"], function($) {
	var init;
	
	init = function(params) {
	    var confirmId = '#' + params.confirmId;
	    var buttonId = '#' + params.buttonId;
	    var callback = params.callback;
	    
	    $(confirmId).appendTo('body');
	    $(confirmId).modal({show:false});
	    
	    $(confirmId + ' .btn-danger').bind('click', function(event){
		    if(callback == null) {
		    	window.location = $(buttonId).attr('href');
		    } else {
		    	callback.call(event);
		    }
	    });
	    
	    $(buttonId).bind('click', function(){
	    	$(confirmId).modal('show'); //Show the Modal window
	    	return false; // Block the action of the click
	    });
	};
	
	return init;
});