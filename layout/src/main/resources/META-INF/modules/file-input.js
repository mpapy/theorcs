requirejs.config({
	shim : {
		"./btstrp/file-input": ["jquery"] 
	}
});
define(["jquery","./btstrp/file-input"], function($) {
	var initField, exports;
	
	initField = function(fieldId) {
		$('#' + fieldId).bootstrapFileInput();
	};
	
	return exports = {
		initField:initField	
	};
});