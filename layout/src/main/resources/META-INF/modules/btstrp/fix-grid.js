define(["jquery"], function($) {
	var init, addSpan;
	
	addSpan = function() {
		$('th[data-grid-column-sort] a').each(function(i){
			var elem = $(this); 
			if(!elem.children(":first").is('span')){
				elem.prepend('<span />');
			}
		});

	};
	
	init = function() {
		addSpan();
		$('div[data-container-type="zone"]').bind('t5:zone:did-update',function(){
			addSpan();
		});
	};

	return init;
});

