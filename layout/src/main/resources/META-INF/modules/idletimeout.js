requirejs.config({
	shim : {
		"./idle/idletimer": ["jquery"], 
		"./idle/idletimeout": ["jquery"]
	}
});
define(["jquery","./idle/idletimer","./idle/idletimeout","bootstrap/modal","bootstrap/transition"], function($) {

	var init;
	
	init = function(params) {
		var dialogId = params.clientId;
		var keepAliveUrl = params.keepAliveUrl;
		var timeOutUrl = params.timeOutUrl;
		var idleAfter = params.idleAfter;
		
		$('#' + dialogId).modal({
			show: false,
			keyboard: false
		});
		
		$('#' + dialogId +'-close').click(function() {
			$.idleTimeout.options.onResume.call(this);
		});
		
		$('#' + dialogId +'-no').click(function() {
			$.idleTimeout.options.onTimeout.call(this);
		});
	
		// cache a reference to the countdown element so we don't have to query the DOM for it on each ping.
		var $countdown = $('#' + dialogId + '-countdown');
	
		// start the idle timer plugin
		$.idleTimeout('#' + dialogId,'#' + dialogId + '-yes', {
			idleAfter: idleAfter,
			pollingInterval: 60,
			keepAliveURL: keepAliveUrl,
			serverResponseEquals: 'OK',
			onTimeout: function(){
				window.location = timeOutUrl;
			},
			onIdle: function(){
				$('#' + dialogId).modal('show');
			},
			onCountdown: function(counter){
				$countdown.html(counter); // update the counter
			},
			onResume: function(){
				$('#' + dialogId).modal('hide');
			}
		});
	};

	return init;
	
});