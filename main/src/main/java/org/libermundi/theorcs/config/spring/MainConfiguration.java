package org.libermundi.theorcs.config.spring;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:META-INF/spring/main-context.xml")
public class MainConfiguration {
//	private static final Logger logger = LoggerFactory.getLogger(MainConfiguration.class);
	
//	private @Value("${jdbc.classname}") String _driverClass;
//	private @Value("${jdbc.url}") String _jdbcUrl;
//	private @Value("${jdbc.username}") String _jdbcUsername;
//	private @Value("${jdbc.password}") String _jdbcPassword;
//	
//	private @Value("${hibernate.dialect}") String _hibernateDialect;
//	private @Value("${hibernate.hbm2ddl-auto}") String _hibernateHbm2ddl;
//	private @Value("${hibernate.show-sql}") String _hibernateShowSql;
//	private @Value("${hibernate.format-sql}") String _HibernateFormatSql;
	
//	@Bean
//	public JndiObjectFactoryBean configurationPath() {
//		JndiObjectFactoryBean configurationPath = new JndiObjectFactoryBean();
//		configurationPath.setJndiName("java:comp/env/config/configurationPath");
//		configurationPath.setDefaultObject("classpath:/META-INF/theorcs-default.properties");
//		return configurationPath;
//	}
//	
//	@Bean
//	@Autowired
//	public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer(JndiObjectFactoryBean jndiConfigurationPath) throws IOException {
//		PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
//		
//		propertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(Boolean.TRUE);
//		logger.info("Loading Configuration properties from : " + jndiConfigurationPath.getObject());
//		String configPath = (String)jndiConfigurationPath.getObject();
//		Resource confResource = null;
//		if(configPath.startsWith("classpath:")) {
//			confResource = new ClassPathResource(ListUtil.listLast(configPath,":"));
//		} else if(configPath.startsWith("file:")) {
//			confResource = new FileSystemResource(configPath);
//		}
//		propertyPlaceholderConfigurer.setLocation(confResource);
//		return propertyPlaceholderConfigurer;
//	}
	

}
