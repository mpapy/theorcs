/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.main.tapestry.pages.chronicles;

import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.alerts.AlertManager;
import org.apache.tapestry5.alerts.Duration;
import org.apache.tapestry5.alerts.Severity;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.model.ChronicleType;
import org.libermundi.theorcs.chronicles.model.Faction;
import org.libermundi.theorcs.chronicles.model.Persona;
import org.libermundi.theorcs.chronicles.services.ChronicleManager;
import org.libermundi.theorcs.chronicles.services.FactionManager;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.chronicles.tapestry.pages.Index;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;
import org.libermundi.theorcs.main.tapestry.base.MainPrivatePage;
import org.libermundi.theorcs.security.model.User;
import org.libermundi.theorcs.security.services.SecurityManager;
import org.libermundi.theorcs.security.services.UserManager;

/**
 * @author Martin Papy
 *
 */
public class Create extends MainPrivatePage {
	
	@Inject
	private ChronicleManager _chronicleManager;
	
	@Inject
	private SecurityManager _securityManager;
	
	@Inject
	private UserManager _userManager;
	
	@Inject
	private PersonaManager _personaManager;
	
	@Inject
	private FactionManager _factionManager;
	
	@Inject
	private AlertManager _alertManager;
	
	@Inject
	private Messages _messages;
	
	@Inject
	private AppHelper _appHelper;
	
	@Property
	@Persist(PersistenceConstants.FLASH)
	private Chronicle _newChronicle;
	
	@SetupRender
	public void setup() {
		_newChronicle = _chronicleManager.createNew();
	}
	
	//
	// Form Management
	//
	@OnEvent(value="success",component="chronicleForm")
	public Class<?> success() {
		User admin = _securityManager.getCurrentUser();
		
		if(_newChronicle.getType() == ChronicleType.PRIVATE) {
			_newChronicle.setActive(Boolean.TRUE);
		}
		
		_newChronicle.setAdmin(admin);
		_chronicleManager.saveAndGrantAdminRights(_newChronicle, admin);
		
		Faction faction = _factionManager.createNew();
			faction.setName("Faction A");
			faction.setChronicle(_newChronicle);

		_factionManager.save(faction);
		
		Persona perso = _personaManager.createBasicPersona(admin, _newChronicle, "*Admin");
			perso.getFactions().add(faction);
			perso.setDefaultFaction(faction);
			perso.setDefaultPerso(Boolean.TRUE);
			
		_personaManager.save(perso);
		
		if(_newChronicle.getType() == ChronicleType.STANDARD)
			_alertManager.success(_messages.get("main.chronicle.form.success.standard"));
		else
			_alertManager.alert(Duration.SINGLE, Severity.SUCCESS, _messages.format("main.chronicle.form.success.private",_appHelper.getPageLink(Index.class, _newChronicle.getId())),Boolean.TRUE);
		
		return org.libermundi.theorcs.main.tapestry.pages.Index.class;
	}

}
