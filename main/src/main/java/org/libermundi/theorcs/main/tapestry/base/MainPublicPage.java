/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.main.tapestry.base;

import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;
import org.libermundi.theorcs.core.tapestry.services.PageRenderSupport;
import org.libermundi.theorcs.layout.LayoutConstants;
import org.libermundi.theorcs.layout.dto.TemplateSettings;
import org.libermundi.theorcs.layout.model.TemplateType;
import org.libermundi.theorcs.main.tapestry.pages.Index;
import org.libermundi.theorcs.security.services.SecurityManager;
import org.libermundi.theorcs.security.tapestry.base.PublicPage;

/**
 * @author Martin Papy
 *
 */
public abstract class MainPublicPage extends PublicPage {
	
	@SessionState
	private TemplateSettings _templateSettings;
	
	@Environmental
	private PageRenderSupport _pageRenderSupport;
	
	@Inject
	private AppHelper _appHelper;
	
	@Inject
	private Messages _messages;
	
	@Inject
	private SecurityManager _securityManager;
	
	@SetupRender
	public void initLabelsAndSettings(){
		
		// Make sure if you come back from the Chronicle logged in using
		// ADMIN or ROOT rights, you get your identity back.
		_securityManager.restoreUser();
		
		_pageRenderSupport.setProperty(LayoutConstants.INDEX_PAGELABEL, "");
		_pageRenderSupport.setProperty(LayoutConstants.INDEX_PAGENAME, _appHelper.getPageNameFromClass(Index.class));
		
		//TODO : Improve that in the future as there is no need of setting that in each request 
		_templateSettings.setTemplateType(TemplateType.STATIC);
		_templateSettings.setCurrentTemplate(LayoutConstants.DEFAULT_TEMPLATE_CODE);
		
	}

}
