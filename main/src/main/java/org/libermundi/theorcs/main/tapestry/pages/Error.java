/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.main.tapestry.pages;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.services.ExceptionReporter;
import org.libermundi.theorcs.main.tapestry.base.MainPublicPage;

/**
 * @author Martin Papy
 *
 */
public class Error extends MainPublicPage implements ExceptionReporter {
	@Property(write=false)
	private String _errorMessage;

	/* (non-Javadoc)
	 * @see org.apache.tapestry5.services.ExceptionReporter#reportException(java.lang.Throwable)
	 */
	@Override
	public void reportException(Throwable exception) {
		_errorMessage = exception.getMessage();
	}
}
