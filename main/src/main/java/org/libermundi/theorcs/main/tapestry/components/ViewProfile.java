/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.main.tapestry.components;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.AssetSource;
import org.libermundi.theorcs.security.services.SecurityManager;

import com.google.common.base.Strings;

/**
 * @author Martin Papy
 *
 */
public class ViewProfile {
	@Inject
	private SecurityManager _securityManager;

	@Inject
	@Path("${layout.images}/avatar/avatar250.png")
	@Property(write=false)
	private Asset _defaultAvatar;
	
	@Inject
	private Messages _messages;
	
	@Inject
	private AssetSource _assetSource;
	
	public Asset getAvatar(){
		String avatar = _securityManager.getCurrentUser().getAvatar();
		if(!Strings.isNullOrEmpty(avatar)){
			try {
				return _assetSource.getClasspathAsset(avatar);
			} catch (RuntimeException e) {
				return _defaultAvatar;
			}
		}
		return _defaultAvatar;
	}
	
	public String getFirstName(){
		return notEmptyString(_securityManager.getCurrentUser().getFirstName());
	}
	
	public String getLastName(){
		return notEmptyString(_securityManager.getCurrentUser().getLastName());
	}
	
	public String getNickName(){
		return notEmptyString(_securityManager.getCurrentUser().getNickName());
	}
	
	public String getUsername(){
		return notEmptyString(_securityManager.getCurrentUser().getUsername());
	}

	public String getEmail(){
		return notEmptyString(_securityManager.getCurrentUser().getEmail());
	}
	
	public String getUserDescription(){
		return notEmptyString(_securityManager.getCurrentUser().getDescription());
	}
	
	private String notEmptyString(String input){
		if(Strings.isNullOrEmpty(input)){
			return "<em>" + _messages.get("main.undefined") + "</em>";
		}
		return input;
	}
}
