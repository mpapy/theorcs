package org.libermundi.theorcs.main.tapestry.pages.admin;

import java.util.List;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.MainConstants;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.tapestry.services.MenuService;
import org.libermundi.theorcs.main.tapestry.base.MainPrivatePage;
import org.libermundi.theorcs.security.SecurityConstants;
import org.springframework.security.access.annotation.Secured;

@Secured({SecurityConstants.ROLE_ROOT,SecurityConstants.ROLE_ADMIN})
public class Index extends MainPrivatePage {

	@Inject
	private MenuService _menuService;

	
	public List<Node<NavMenuItem>> getAdminMenuItems(){
		Node<NavMenuItem> adminMenu = _menuService.getNodeById(MainConstants.PORTAL_ADMIN_MENU_ID);
		
		return adminMenu.getChildren();
		
	}

}
