package org.libermundi.theorcs.main.tapestry.pages.admin.chronicles;

import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.services.ChronicleManager;
import org.libermundi.theorcs.main.tapestry.base.MainPrivatePage;

public class Edit extends MainPrivatePage {
	@Inject
	private ChronicleManager _chronicleManager;
	
	@Property
	@Persist(PersistenceConstants.FLASH)
	private Chronicle _chronicle;
	
	public void onActivate(Chronicle chronicle) {
		_chronicle = chronicle;
	}
	
	public Long onPassivate() {
		return _chronicle.getId();
	}

	@OnEvent(value="success",component="chronicleForm")
	public Class<?> onSuccess(){
		_chronicleManager.save(_chronicle);
		return Manage.class;
	}
}
