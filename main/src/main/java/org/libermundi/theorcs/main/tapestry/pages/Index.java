/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.main.tapestry.pages;

import java.util.List;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.libermundi.theorcs.chronicles.services.ChronicleManager;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.services.NavItemSource;
import org.libermundi.theorcs.layout.tapestry.services.LayoutAppHelper;
import org.libermundi.theorcs.main.tapestry.base.MainPublicPage;
import org.libermundi.theorcs.security.services.SecurityManager;

/**
 * Start page of application TheORCS.
 */
public class Index extends MainPublicPage {
	@Inject
	private SecurityManager _securityManager;
	
	@Inject
	private ChronicleManager _chronicleManager;
	
	@Inject
	private LayoutAppHelper _layoutAppHelper;
	
	@InjectService("ChronicleNavItemSource")
	private NavItemSource _chronicleNavItemSource;
	
	@Property
	private Node<NavMenuItem> _chronicle;
	
	public List<Node<NavMenuItem>> getChronicleSource(){
		List<Node<NavMenuItem>> list = _chronicleNavItemSource.getElements();
		return list;
	}

}
