package org.libermundi.theorcs.main.tapestry.pages.admin.chronicles;

import java.util.List;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Any;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.PropertyConduitSource;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;
import org.apache.tapestry5.services.ajax.JavaScriptCallback;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.libermundi.theorcs.chronicles.model.Chronicle;
import org.libermundi.theorcs.chronicles.services.ChronicleManager;
import org.libermundi.theorcs.chronicles.services.PersonaManager;
import org.libermundi.theorcs.main.tapestry.base.MainPrivatePage;

public class Manage extends MainPrivatePage {
	@Inject
	private ChronicleManager _chronicleManager;
	
	@Inject
	private PersonaManager _personaManager;
	
	@Inject
	private Messages _messages;

	@Inject
	private PropertyConduitSource _pcSource;
	
	@Inject
	private AjaxResponseRenderer _ajaxResponseRenderer;
	
	@Property
	@Persist
	private BeanModel<Chronicle> _chronicleModel;

	@Inject
	private BeanModelSource _beanModelSource;

	@InjectComponent
	private Zone _chroGrid;
	
	@Property
	@Persist
	private List<Chronicle> _chroItems;

	@Property
	private Chronicle _chroItem;
	
	@Property
	private Chronicle _chroPreview;
	
	@InjectComponent
	private Zone _chroniclePreviewZone;
	
	@Inject
	private Block _chroniclePreview;
	
	@Inject
	private JavaScriptSupport _jsSupport;
	
	@Component(id="previewChronicleModal")
	private Any _previewChronicleModal;
	
	@Persist(PersistenceConstants.SESSION)
	private String _previewChronicleModalId;
	
	@Persist(PersistenceConstants.SESSION)
	private String _chroniclePreviewZoneId;
	
	@Persist(PersistenceConstants.SESSION)
	private String _filterOn;
	
	@SetupRender
	public void setupRender() {
		if(_chronicleModel == null) {
			// have to add the property sender to display it as it is a complex class
			_chronicleModel = _beanModelSource.createDisplayModel(Chronicle.class, _messages);
			_chronicleModel.exclude("id", "modifiedDate", "createdBy", "modifiedBy", "description",
					"background","genre","style","password","date","subtitle","pace","openForInscription");
			_chronicleModel.add("game", _pcSource.create(Chronicle.class, "game.name")).sortable(true);
			_chronicleModel.addEmpty("action");
			_chronicleModel.addEmpty("perso");
			_chronicleModel.reorder("title","type","active","game","perso","createddate","deleted","action");
		}

		if( _filterOn == null ) {
			_filterOn = "inactive";
		}
		
		loadChroItems();
	}
	
	@AfterRender
	public void afterRender() {
		_previewChronicleModalId =  _previewChronicleModal.getClientId();
		_chroniclePreviewZoneId = _chroniclePreviewZone.getClientId();
	}
	
	@OnEvent("filter")
	public Block onFilter(String filterOn) {
		_filterOn = filterOn;
		loadChroItems();
		return _chroGrid.getBody();
	}
	
	@OnEvent("previewChro")
	public void onPreviewChro(final Chronicle chronicle){
		_chroPreview = chronicle;
		_ajaxResponseRenderer.addRender(_chroniclePreviewZoneId, _chroniclePreview);
		_ajaxResponseRenderer.addCallback(new JavaScriptCallback() {
			@Override
			public void run(JavaScriptSupport javascriptSupport) {
				_jsSupport.require("zone.modal").invoke("title").with(getPreviewModalId(),chronicle.getTitle());
				_jsSupport.require("zone.modal").invoke("show").with(getPreviewModalId());
			}
		});
	}
	
    @OnEvent("renderPreviewJs")
    public void renderPreviewJs(){
		_jsSupport.require("zone.modal").invoke("init").with(_previewChronicleModal.getClientId());
    }

    // Various Properties
	public String getPreviewModalId() {
		return _previewChronicleModalId;
	}

	public int getPersoNbre() {
		return _personaManager.getChroniclePersonas(_chroItem).size();
	}

	public double getRandNum() {
		return Math.random();
	}
	
	private void loadChroItems() {
		switch (_filterOn) {
			case "inactive":
				_chroItems = _chronicleManager.getInactive();
				break;
			case "active":
				_chroItems = _chronicleManager.getActive();
				break;
			case "deleted":
				_chroItems = _chronicleManager.getDeleted();
				break;
			case "all": 
				_chroItems = _chronicleManager.getAll();
				break; 
			default:
				_chroItems = _chronicleManager.getInactive();
				break;
		}
	}
}
