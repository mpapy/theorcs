package org.libermundi.theorcs.main.tapestry.pages.admin.sysadmin;

import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.libermundi.theorcs.core.services.SearchServices;
import org.libermundi.theorcs.main.tapestry.base.MainPrivatePage;

public class Search extends MainPrivatePage {
	
	@Inject
	private SearchServices _searchServices;
	
	@OnEvent(value="fullIndex")
	public void doFullindex() {
		_searchServices.doFullindex();
	}

}
