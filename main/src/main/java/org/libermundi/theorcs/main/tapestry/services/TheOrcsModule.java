/*
 * Copyright (c) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.libermundi.theorcs.main.tapestry.services;

import java.io.IOException;

import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.Resource;
import org.apache.tapestry5.ioc.annotations.Contribute;
import org.apache.tapestry5.ioc.annotations.Decorate;
import org.apache.tapestry5.ioc.annotations.ImportModule;
import org.apache.tapestry5.ioc.annotations.Local;
import org.apache.tapestry5.ioc.annotations.Order;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.ioc.services.ApplicationDefaults;
import org.apache.tapestry5.ioc.services.SymbolProvider;
import org.apache.tapestry5.services.ComponentClassResolver;
import org.apache.tapestry5.services.ComponentSource;
import org.apache.tapestry5.services.ExceptionReporter;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.RequestExceptionHandler;
import org.apache.tapestry5.services.RequestFilter;
import org.apache.tapestry5.services.RequestHandler;
import org.apache.tapestry5.services.Response;
import org.apache.tapestry5.services.ResponseRenderer;
import org.libermundi.tapestry.ckeditor.services.CKEditorModule;
import org.libermundi.tapestry.elfinder.services.ElFinderModule;
import org.libermundi.tapestry.xssfilter.services.XSSFilterModule;
import org.libermundi.theorcs.chronicles.tapestry.services.ChronicleModule;
import org.libermundi.theorcs.comet.tapestry.services.CometModule;
import org.libermundi.theorcs.core.tapestry.services.CoreModule;
import org.libermundi.theorcs.layout.tapestry.services.LayoutModule;
import org.libermundi.theorcs.security.tapestry.services.SecurityModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This module is automatically included as part of the Tapestry IoC Registry, it's a good place to
 * configure and extend Tapestry, or to place your own service definitions.
 */
@ImportModule({TheOrcsMenuModule.class, SecurityModule.class,CoreModule.class,
	LayoutModule.class,ChronicleModule.class,CometModule.class,CKEditorModule.class,
	XSSFilterModule.class,ElFinderModule.class})
public class TheOrcsModule {

	/**
	 * Use annotation or method naming convention: <code>contributeApplicationDefaults</code>
	 */
	@Contribute(SymbolProvider.class)
	@ApplicationDefaults
	public static void setupEnvironment(MappedConfiguration<String, Object> configuration)
	{
		configuration.add(SymbolConstants.JAVASCRIPT_INFRASTRUCTURE_PROVIDER, "jquery");
		configuration.add(SymbolConstants.MINIFICATION_ENABLED, true);
		configuration.add(SymbolConstants.SUPPORTED_LOCALES, "fr,en");
		configuration.add(SymbolConstants.ENABLE_HTML5_SUPPORT, true);
    }	
	
	
    /**
     * This is a service definition, the service will be named "TimingFilter". The interface,
     * RequestFilter, is used within the RequestHandler service pipeline, which is built from the
     * RequestHandler service configuration. Tapestry IoC is responsible for passing in an
     * appropriate Logger instance. Requests for static resources are handled at a higher level, so
     * this filter will only be invoked for Tapestry related requests.
     * 
     * <p>
     * Service builder methods are useful when the implementation is inline as an inner class
     * (as here) or require some other kind of special initialization. In most cases,
     * use the static bind() method instead. 
     * 
     * <p>
     * If this method was named "build", then the service id would be taken from the 
     * service interface and would be "RequestFilter".  Since Tapestry already defines
     * a service named "RequestFilter" we use an explicit service id that we can reference
     * inside the contribution method.
     */    
    public RequestFilter buildTimingFilter(final Logger log) {
        return new RequestFilter()
        {
            @Override
			public boolean service(Request request, Response response, RequestHandler handler)
                    throws IOException
            {
                long startTime = System.currentTimeMillis();

                try
                {
                    // The responsibility of a filter is to invoke the corresponding method
                    // in the handler. When you chain multiple filters together, each filter
                    // received a handler that is a bridge to the next filter.
                    
                    return handler.service(request, response);
                }
                finally
                {
                    long elapsed = System.currentTimeMillis() - startTime;
                    log.info(String.format("Request time: %d ms for %s", elapsed, request.getPath()));
                }
            }
        };
    }

    /**
     * This is a contribution to the RequestHandler service configuration. This is how we extend
     * Tapestry using the timing filter. A common use for this kind of filter is transaction
     * management or security. The @Local annotation selects the desired service by type, but only
     * from the same module.  Without @Local, there would be an error due to the other service(s)
     * that implement RequestFilter (defined in other modules).
     */
    public void contributeRequestHandler(OrderedConfiguration<RequestFilter> configuration,
            @Local
            RequestFilter filter) {
        // Each contribution to an ordered configuration has a name, When necessary, you may
        // set constraints to precisely control the invocation order of the contributed filter
        // within the pipeline.
        
        configuration.add("Timing", filter);
    }
    
 
    
	public static void contributeComponentMessagesSource(
			@Value("META-INF/lang/main.properties")
			Resource mainCatalogResource,
			OrderedConfiguration<Resource> configuration) {
			configuration.add("mainCatalog", mainCatalogResource, "before:AppCatalog");
	}
	
	// handle Unexpected RuntimeExceptions
	@Decorate(serviceInterface=RequestExceptionHandler.class,id="finalExceptionHandler")
	@Order("after:*")
    public static RequestExceptionHandler decorateRequestExceptionHandler(
    				final RequestExceptionHandler delegate,
    				final ResponseRenderer renderer,
    				@Symbol(SymbolConstants.PRODUCTION_MODE)
    	            boolean productionMode,
    	            final ComponentSource componentSource,
    				final ComponentClassResolver resolver) {
    	
    	if (!productionMode){
    		return null; // Let us work ^^
    	}
    	
    	// Here we redirect to the ErrorPage of the Application
        return new RequestExceptionHandler() {
        	private final Logger logger = LoggerFactory.getLogger(RequestExceptionHandler.class);
            @Override
			public void handleRequestException(Throwable exception) throws IOException {
            	if(logger.isDebugEnabled()) {
            		logger.debug("---------- RequestException RequestExceptionHandler Final Decorator ----------");
            	}
            	
            	logger.error("Unexpected runtime exception: " + exception.getMessage(), exception);
                ExceptionReporter index = (ExceptionReporter) componentSource.getPage("Error");

                index.reportException(exception);

                renderer.renderPageMarkupResponse("Error");
            }
       };
    }
}
