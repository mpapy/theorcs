package org.libermundi.theorcs.main.tapestry.services;

import org.apache.tapestry5.Asset2;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.libermundi.theorcs.MainConstants;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.core.model.NodeConstraint;
import org.libermundi.theorcs.core.model.NodeFactory;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;
import org.libermundi.theorcs.layout.LayoutConstants;
import org.libermundi.theorcs.layout.model.menu.NavMenuItem;
import org.libermundi.theorcs.layout.model.menu.NavMenuItemType;
import org.libermundi.theorcs.layout.model.menu.StaticNavMenuItem;
import org.libermundi.theorcs.layout.services.NavItemSource;
import org.libermundi.theorcs.layout.tapestry.model.menu.ImageNavMenuItem;
import org.libermundi.theorcs.layout.tapestry.services.LayoutAppHelper;
import org.libermundi.theorcs.main.tapestry.pages.Index;
import org.libermundi.theorcs.main.tapestry.pages.Profile;
import org.libermundi.theorcs.main.tapestry.pages.admin.chronicles.Manage;
import org.libermundi.theorcs.main.tapestry.pages.admin.sysadmin.Search;
import org.libermundi.theorcs.security.SecurityConstants;
import org.libermundi.theorcs.security.model.impl.RoleNodeConstraint;

public class TheOrcsMenuModule {
		@SuppressWarnings("rawtypes")
		public static void contributeMenuService(OrderedConfiguration<Node> configuration,
				Messages messages, LayoutAppHelper layoutHelper,
				@Inject @Path("${layout.images}/TheORCS-logo.png") Asset2 orcsLogo,
				AppHelper appHelper,
				@InjectService("ChronicleNavItemSource") NavItemSource chronicleNavItemSource) {		
			//Top Left Menu in Chronicle

	    	// Add TheORCS Logo (Default) //TODO => Add the possibility to Override from Platform configuration
	    	Node<NavMenuItem> logo = NodeFactory.getNode("LOGO_HOME_ORCS");
		    	logo.setData(new ImageNavMenuItem(orcsLogo));
		    	logo.getData().setMainPageId(appHelper.getPageId(Index.class));
		    	logo.getData().setDescription("message:main.mainmenu.homepage.hint"); // will be used by the title of the "a" element
		    	logo.getData().setLabel("message:main.mainmenu.homepage"); // will be used by the alt of the "img" element
		    	logo.getData().setURI(appHelper.getPageURI(Index.class));
		    	logo.setParentId(LayoutConstants.PORTAL_TOP_LEFT_MENU_ID); // Link to the Main Portal Menu (Left)
	    	
	    	configuration.add("logo", logo);
	    	
	    	// Add Chronicle List in the Menu
	    	Node<NavMenuItem> chronicles = layoutHelper.buildDynamicMenuNode(LayoutConstants.PORTAL_TOP_LEFT_MENU_ID,
	    			chronicleNavItemSource, "message:main.mainmenu.chronicles", "/",
	    			new RoleNodeConstraint(SecurityConstants.ROLE_USER));
		    	chronicles.getData().setDescription("message:main.mainmenu.chronicles.hint");
		    	chronicles.getData().setCss("glyphicon glyphicon-film white");
	    	configuration.add("chronicles", chronicles, "after:logo");
	    	
	    	//Top Right Menu in Portal
	    	
	    	// Add the Search Form
	    	Node<NavMenuItem> search = NodeFactory.getNode("SEARCH_FIELD");
		    	search.setData(new StaticNavMenuItem(NavMenuItemType.SEARCHFORM));
		    	search.getData().setMainPageId("SEARCH_FIELD");
		    	search.setParentId(LayoutConstants.PORTAL_TOP_RIGHT_MENU_ID); // Link to the Main Portal Menu (Right)
	    	configuration.add("search",search,"after:chronicles");

	    	// Add Profile Link
			Node<NavMenuItem> profile = NodeFactory.getNode();
				profile.setData(new StaticNavMenuItem(NavMenuItemType.ICON));
				profile.getData().setMainPageId(appHelper.getPageId(Profile.class));
				profile.getData().setDescription("message:main.mainmenu.profile.hint");
				profile.getData().setCss("glyphicon glyphicon-user white");
				profile.getData().setURI(appHelper.getPageURI(Profile.class));
				profile.addConstraint(new RoleNodeConstraint(SecurityConstants.ROLE_USER));
				profile.setParentId(LayoutConstants.PORTAL_TOP_RIGHT_MENU_ID); // Link to the Main Portal Menu (Right)

			configuration.add("profile", profile,"after:search");
			
			// Add Admin Link
			Node<NavMenuItem> admin = NodeFactory.getNode();
				admin.setData(new StaticNavMenuItem(NavMenuItemType.ICON));
				admin.getData().setMainPageId(appHelper.getPageId(org.libermundi.theorcs.main.tapestry.pages.admin.Index.class));
				admin.getData().setDescription("message:main.mainmenu.admin.hint");
				admin.getData().setCss("glyphicon glyphicon-lock white");
				admin.getData().setURI(appHelper.getPageURI(org.libermundi.theorcs.main.tapestry.pages.admin.Index.class));
				admin.setConstraintMode(NodeConstraint.Mode.ANY);
				admin.addConstraint(new RoleNodeConstraint(SecurityConstants.ROLE_ADMIN));
				admin.addConstraint(new RoleNodeConstraint(SecurityConstants.ROLE_ROOT));
				admin.setParentId(LayoutConstants.PORTAL_TOP_RIGHT_MENU_ID); // Link to the Main Portal Menu (Right)

			configuration.add("admin", admin, "after:profile");    	
	    	
	    	// Add the Login Box
	    	Node<NavMenuItem> login = NodeFactory.getNode("LOGIN_BOX");
		    	login.setData(new StaticNavMenuItem(NavMenuItemType.LOGINBOX));
		    	login.getData().setMainPageId("LOGIN_BOX");
		    	login.setParentId(LayoutConstants.PORTAL_TOP_RIGHT_MENU_ID); // Link to the Main Portal Menu (Right)
	    	configuration.add("login", login, "after:admin");
	    	
	    	// Administration Menu on the Admin Page
	    	// Building the Root Menu
	    	Node<NavMenuItem> portailAdminMenu = layoutHelper.buildStaticMenuNode(null, MainConstants.PORTAL_ADMIN_MENU_ID, "/");
	    		portailAdminMenu.setId(MainConstants.PORTAL_ADMIN_MENU_ID);

	    	configuration.add(MainConstants.PORTAL_ADMIN_MENU_ID, portailAdminMenu, "after:" + LayoutConstants.CHRO_TOP_RIGHT_MENU_ID);  	

			// Adding the General Admin Section
			Node<NavMenuItem> genAdmin= NodeFactory.getNode();
				genAdmin.setId("mainAdminGenAdmin");
				genAdmin.setData(new StaticNavMenuItem(NavMenuItemType.DEFAULT));
				genAdmin.getData().setLabel("message:main.adminpage.menu.general.label");
				genAdmin.getData().setDescription("message:main.adminpage.menu.general.description");
				genAdmin.addConstraint(new RoleNodeConstraint(SecurityConstants.ROLE_ADMIN));
				genAdmin.addConstraint(new RoleNodeConstraint(SecurityConstants.ROLE_ROOT));
				genAdmin.setConstraintMode(NodeConstraint.Mode.ANY);
				genAdmin.setParentId(MainConstants.PORTAL_ADMIN_MENU_ID); // Link to the Main Admin Menu
			
			configuration.add("mainAdminGenAdmin", genAdmin, "after:"+MainConstants.PORTAL_ADMIN_MENU_ID);

			// Adding the Chronicle Item to General Section
			Node<NavMenuItem> mchros = NodeFactory.getNode();
				mchros.setData(new StaticNavMenuItem(NavMenuItemType.DEFAULT));
				mchros.getData().setMainPageId(appHelper.getPageId(Manage.class));
				mchros.getData().setLabel("message:main.adminpage.menu.general.chronicles.label");
				mchros.getData().setDescription("message:main.adminpage.menu.general.chronicles.description");
				mchros.getData().setURI(appHelper.getPageURI(Manage.class));
				mchros.addConstraint(new RoleNodeConstraint(SecurityConstants.ROLE_ADMIN));
				mchros.addConstraint(new RoleNodeConstraint(SecurityConstants.ROLE_ROOT));
				mchros.setConstraintMode(NodeConstraint.Mode.ANY);
				mchros.setParentId("mainAdminGenAdmin");	    
				
			configuration.add(mchros.getId(), mchros, "after:mainAdminGenAdmin");

			// Adding the SysAdmin Section
			Node<NavMenuItem> sysAdmin= NodeFactory.getNode();
				sysAdmin.setId("mainAdminSysAdmin");
				sysAdmin.setData(new StaticNavMenuItem(NavMenuItemType.DEFAULT));
				sysAdmin.getData().setLabel("message:main.adminpage.menu.system.label");
				sysAdmin.getData().setDescription("message:main.adminpage.menu.system.description");
				sysAdmin.addConstraint(new RoleNodeConstraint(SecurityConstants.ROLE_ADMIN));
				sysAdmin.addConstraint(new RoleNodeConstraint(SecurityConstants.ROLE_ROOT));
				sysAdmin.setConstraintMode(NodeConstraint.Mode.ANY);
				sysAdmin.setParentId(MainConstants.PORTAL_ADMIN_MENU_ID); // Link to the Main Admin Menu
			
			configuration.add("mainAdminSysAdmin", sysAdmin, "after:"+MainConstants.PORTAL_ADMIN_MENU_ID);

			// Adding the Reindex Item to General Section
			Node<NavMenuItem> reindex = NodeFactory.getNode();
				reindex.setData(new StaticNavMenuItem(NavMenuItemType.DEFAULT));
				reindex.getData().setMainPageId(appHelper.getPageId(org.libermundi.theorcs.chronicles.tapestry.pages.admin.perso.Main.class));
				reindex.getData().setLabel("message:main.adminpage.menu.system.search.label");
				reindex.getData().setDescription("message:main.adminpage.menu.system.search.description");
				reindex.getData().setURI(appHelper.getPageURI(Search.class));
				reindex.addConstraint(new RoleNodeConstraint(SecurityConstants.ROLE_ADMIN));
				reindex.addConstraint(new RoleNodeConstraint(SecurityConstants.ROLE_ROOT));
				reindex.setConstraintMode(NodeConstraint.Mode.ANY);
				reindex.setParentId("mainAdminSysAdmin");	    
				
			configuration.add(reindex.getId(), reindex, "after:mainAdminSysAdmin");

	    }
}
