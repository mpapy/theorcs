package org.libermundi.theorcs.main.tapestry.pages;

import javax.inject.Inject;

import org.apache.tapestry5.alerts.AlertManager;
import org.apache.tapestry5.alerts.Duration;
import org.apache.tapestry5.alerts.Severity;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.services.Request;
import org.libermundi.theorcs.core.tapestry.services.AppHelper;
import org.libermundi.theorcs.main.tapestry.base.MainPublicPage;

import com.google.common.base.Strings;

/**
 * Login page of application TheORCS.
 */
public class Login extends MainPublicPage {
	@Inject
	private Request _request;
	
	@Inject
	private AlertManager _alertManager;
	
	@Inject
	private Messages _messages;
	
	@Inject
	private AppHelper _appHelper;
	
	@Property(write=false)
	private String _originalUrl;
	
	public void onActivate(String originalUrl){
		_originalUrl = originalUrl;
	}
	
	@SetupRender
	public void setupRender(){
		if(!Strings.isNullOrEmpty(_request.getParameter("failed"))){
			String reason = _request.getParameter("reason");
			if(!Strings.isNullOrEmpty(reason)){
				switch (reason) {
					case "BadCredential":
						_alertManager.error(_messages.get("main.login.failure.badcredentials"));
						break;
					case "LockedAccount":
						_alertManager.error(_messages.get("main.login.failure.accountlocked"));
						break;
					case "AccountExpired":
						_alertManager.error(_messages.get("main.login.failure.accountexpired"));
						break;
					case "CredentialsExpired":
						_alertManager.alert(Duration.SINGLE,Severity.ERROR, 
								_messages.format("main.login.failure.credentialsexpired",_appHelper.getPageLink(ForgotPassword.class)),
								Boolean.TRUE);
						break;
					default:
						_alertManager.error(_messages.get("main.login.failure"));
				}
			} else {
				_alertManager.error(_messages.get("main.login.failure"));
			}
		}
	}
	
}
