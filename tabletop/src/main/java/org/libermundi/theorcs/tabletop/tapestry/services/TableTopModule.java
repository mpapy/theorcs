package org.libermundi.theorcs.tabletop.tapestry.services;

import org.apache.tapestry5.ioc.Configuration;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.Resource;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.services.ApplicationInitializerFilter;
import org.apache.tapestry5.services.LibraryMapping;
import org.libermundi.theorcs.core.model.Node;
import org.libermundi.theorcs.core.util.OrcsHome;
import org.libermundi.theorcs.layout.tapestry.services.LayoutAppHelper;
import org.libermundi.theorcs.tabletop.TableTopConstants;
import org.springframework.web.context.WebApplicationContext;


/**
 * This module is automatically included as part of the Tapestry IoC Registry, it's a good place to
 * configure and extend Tapestry, or to place your own service definitions.
 */
public class TableTopModule {
	public static void contributeComponentClassResolver(final Configuration< LibraryMapping > configuration) {
        configuration.add(new LibraryMapping(TableTopConstants.TAPESTRY_MAPPING, "org.libermundi.theorcs.tabletop.tapestry"));
	}
    
	public static void contributeFactoryDefaults(MappedConfiguration<String, String> configuration) {
        configuration.add("tabletop.assets", "org/libermundi/theorcs/tabletop/tapestry/assets");

        configuration.add("tabletop.scripts", "classpath:${tabletop.assets}/js");
        configuration.add("tabletop.styles", "classpath:${tabletop.assets}/css");
        configuration.add("tabletop.images", "classpath:${tabletop.assets}/images");
    }	
	
	public static void contributeApplicationConfiguration(
			OrderedConfiguration<String> configuration,
			OrcsHome theOrcsHome) {
		configuration.add("tabletop-default", "classpath://META-INF/tabletop-default.properties","after:core-default");
		configuration.add("tabletop-local", theOrcsHome.getPath() + "/conf/tabletop.properties","after:tabletop-default");
	}
	
	public static void contributeComponentMessagesSource(
			@Value("/META-INF/lang/tabletop.properties")
			Resource tabletopCatalogResource,
			OrderedConfiguration<Resource> configuration) {
			configuration.add("tabletopCatalog", tabletopCatalogResource, "before:AppCatalog");
	}
	
	public void contributeApplicationInitializer(OrderedConfiguration<ApplicationInitializerFilter> configuration, @Inject WebApplicationContext webApplicationContext) {
        configuration.add("tabletopModuleInitializer", new TableTopModuleInitializer(), "after:chroniclesModuleInitializer");
	}
	
	@SuppressWarnings("rawtypes")
	public static void contributeMenuService(OrderedConfiguration<Node> configuration,
				Messages messages, LayoutAppHelper layoutHelper) {		
		
	}
}
