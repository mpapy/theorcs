package org.libermundi.theorcs.config.spring;

import java.util.ArrayList;

import org.libermundi.theorcs.core.dao.hibernate.EntityFactoryPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:/org/libermundi/theorcs/config/spring/tabletop-context.xml")
public class TableTopConfiguration {
	@Bean
	public EntityFactoryPostProcessor tabletopEntityFactoryPostProcessor() {
		EntityFactoryPostProcessor efpp = new EntityFactoryPostProcessor();
		ArrayList<Class<?>> managedClasses = new ArrayList<>();

		//TODO : Add Managed Classes
		//managedClasses.add(NameOfClassImpl.class);
		
		efpp.setManagedClasses(managedClasses);
		
		return efpp;
	}
}
